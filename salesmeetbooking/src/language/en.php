<?php

$operatore_prenota = "Book an appointment with an expert.";
$operatore_tipo_appuntamento = "Select the type of appointment.";
$operatore_disponibile_realtime = "No advisor is available right now";
$operatore_richiedi_location = "Select your preferred location to set an appointment with one of our experts.";
$operatore_richiedi_location_timezone = "Select your preferred time zone to set an appointment with one of our experts.";
$operatore_richiedi_appuntamento = "Select your preferred date to set an appointment with one of our experts.";
$operatore_richiedi_appuntamento_proposta = "If you can't find a date that suits you, propose one even if we are busy. If the slot becomes free we will contact you directly.";
$operatore_richiedi_appuntamento_proposta_click = "Click here.";
$operatore_richiedi_appuntamento_proposta_free = "Proceed with the request for a free date proposal.";
$operatore_seleziona_prodotti = "Select any other product of which you would like more information. So we will choose the best expert for us";
$operatore_procedi = "Proceed with the operation \"contact an expert\"";
$operatore_favourite_language = "Select the language you'd like to be contacted.";
$operatore_inserisci_email = "Enter your email: ";
$operatore_dati_opzionali = "Optional data";
$operatore_inserisci_nome = "Name";
$operatore_inserisci_cognome = "Surname";
$operatore_inserisci_telefono = "Phone";
$operatore_inserisci_domanda = "Reason of the request";
$operatore_specialista_assegnato = "We have assigned you the best expert for your needs.";
$operatore_favourite_language = "Favourite language";
$operatore_termina_prenotazione = "Terminate booking";
$operatore_step_successivo = "NEXT STEP";
$operatore_step_back = "Back";
$operatore_step_close = "Close";
$operatore_step_annulla = "Cancel";
$operatore_data_appuntamento = "Appointment date";
$operatore_scelta_data_appuntamento = "Choice of appointment date";
$operatore_altro_prodotto = "Choice of other products";
$operatore_esperto = "Personal data entry and expert assignment";
$operatore_appuntamento_ok = "Your appointment with our expert is registered.<br><br>You will receive by email all the information to proceed with the meeting you requested.<br><br>Thank you.";
$operatore_appuntamento_chiudi = "Close the window";
$operatore_email_non_valida = "You have entered an invalid email address!";

// uiex *****
$etichetta_uiex_image = "Image";
$etichetta_uiex_title = "Title";
$etichetta_uiex_price = "Price";
$etichetta_uiex_currency = "Currency";
$etichetta_uiex_vote = "Vote";
$etichetta_uiex_select = "Select";

$etichetta_location_country = "Country";
$etichetta_location_timezone = "Timezone expert";
$etichetta_location_country_esperto = "Expert location";
$etichetta_time_intro = "The timezone of the expert is different from yours. This might lead to an appointment outside your ordinary time slots.";
$etichetta_time_utente_locale = "If you are the "; // "Indication of your actual hour:";
$etichetta_time_utente_locale_indicazioni = "Your hour is: ";
$etichetta_time_esperto_locale = "from the expert will be "; // "Indication of the expert's hour: ";
$etichetta_time_utente_timezone_differenza = "Please note that the expert may answer at a different time slot <br>than yours: ";
$etichetta_location_time = "hour";
$etichetta_location_region = "Region";
$etichetta_location_province = "Province";
$etichetta_location_city = "City";
$etichetta_location_street = "Street";
$etichetta_email = "Email: ";

$etichetta_location_acasa_country = "Country";
$etichetta_location_acasa_province = "Province";
$etichetta_location_acasa_city = "City";
$etichetta_location_acasa_street = "Street";
$etichetta_location_acasa_titolo = "Fill in the following fields with the indications of your meeting place";

$etichetta_tipo_instore = "In the agency";
$etichetta_tipo_acasa = "At your place";
$etichetta_tipo_online = "Online";
$etichetta_tipo_appuntamento = "Appointment type";

$etichetta_esperto_nome = "Name";
$etichetta_esperto_specializzazione = "Specialization";
$etichetta_esperto_caratteristiche = "Features";
$etichetta_esperto_abbinamento = "Matching keywords";

$etichetta_chiudi = "Close";
$operatore_data_selezionata = "Selected date";

?>
