<?php

$operatore_prenota = "Prenez rendez-vous avec un expert.";
$operatore_tipo_appuntamento = "Sélectionnez le type de rendez-vous.";
$operatore_disponibile_realtime = "Aucun conseiller n'est disponible pour le moment.";
$operatore_richiedi_location = "Sélectionnez votre préférence de lieu pour un rendez-vous avec l'un de nos experts.";
$operatore_richiedi_location_timezone = "Sélectionnez votre fuseau horaire pour prendre rendez-vous avec un de nos experts.";
$operatore_richiedi_appuntamento = "Sélectionnez la date qui vous convient pour prendre rendez-vous avec un de nos experts.";
$operatore_richiedi_appuntamento_proposta = "Si vous ne trouvez pas de date qui vous convienne, proposez-en une y compris si elle est déjà occupée. Si la place se libère, nous vous contacterons directement.";
$operatore_richiedi_appuntamento_proposta_click = "Cliquez ici.";
$operatore_richiedi_appuntamento_proposta_free = "Continuer avec votre proposition de date personnalisée de rendez-vous.";
$operatore_seleziona_prodotti = "Sélectionnez n'importe quel autre produit sur lequel vous souhaitez plus d'informations.Cela nous permettra de vous choisir le meilleur expert selon vos besoins.";
$operatore_procedi = "Continuer la réservation \"contacter un expert\"";
$operatore_favourite_language = "Sélectionnez la langue dans laquelle vous voulez être contacté :";
$operatore_inserisci_email = "Entrez votre e-mail :";
$operatore_dati_opzionali = "Données optionnelles";
$operatore_inserisci_nome = "Nom";
$operatore_inserisci_cognome = "Prénom";
$operatore_inserisci_telefono = "Téléphone";
$operatore_inserisci_domanda = "Raison de votre demande";
$operatore_specialista_assegnato = "Nous avons sélectionné le meilleur expert selon vos besoins.";
$operatore_favourite_language = "Langue préférée";
$operatore_termina_prenotazione = "Terminer la réservation.";
$operatore_step_successivo = "ETAPE SUIVANTE";
$operatore_step_back = "Retour";
$operatore_step_close = "Fermer";
$operatore_step_annulla = "Annuler";
$operatore_data_appuntamento = "Date du rendez-vous";
$operatore_scelta_data_appuntamento = "Choix de la date de rendez-vous";
$operatore_altro_prodotto = "Choix d'autres produits";
$operatore_esperto = "Enregistrement des données personnelles et assignation d'un expert";
$operatore_appuntamento_ok = "Votre rendez-vous avec notre expert est enregistré.<br><br>Vous recevrez un e-mail avec toutes les informations pour démarrer le rendez-vous que vous avez demandé.<br><br>Merci.";
$operatore_appuntamento_chiudi = "Fermer la fenêtre";
$operatore_email_non_valida = "Vous avez entré une adresse e-mail invalide !";

// uiex *****
$etichetta_uiex_image = "Image";
$etichetta_uiex_title = "Titre";
$etichetta_uiex_price = "Prix";
$etichetta_uiex_currency = "Devise";
$etichetta_uiex_vote = "Vote";
$etichetta_uiex_select = "Sélectionner";

$etichetta_location_country = "Pays";
$etichetta_location_timezone = "Fuseau horaire de l'expert";
$etichetta_location_country_esperto = "Emplacement de l'expert";
$etichetta_time_intro = "Le fuseau horaire de l'expert est différent du votre. Cela peut entraîner un rendez-vous en dehors de vos créneaux horaires ordinaires.";
$etichetta_time_utente_locale = "Si vous êtes le "; // "Indication de votre heure actuelle : ";
$etichetta_time_utente_locale_indicazioni = "Votre heure est : ";
$etichetta_time_esperto_locale = "Pour l'expert il sera "; // "Indication de l'heure de l'expert : ";
$etichetta_time_utente_timezone_differenza = "Notez que l'expert peut répondre à une heure différente de la vôtre :";
$etichetta_location_time = "Heure";
$etichetta_location_region = "Région";
$etichetta_location_province = "Province";
$etichetta_location_city = "Localité";
$etichetta_location_street = "Rue";
$etichetta_email = "E-mail : ";

$etichetta_location_acasa_country = "Pays";
$etichetta_location_acasa_province = "Province";
$etichetta_location_acasa_city = "Localité";
$etichetta_location_acasa_street = "Rue";
$etichetta_location_acasa_titolo = "Remplissez les champs suivants avec les informations de votre lieu de rendez-vous";

$etichetta_tipo_instore = "A l'agence";
$etichetta_tipo_acasa = "Au lieu  de votre choix";
$etichetta_tipo_online = "En ligne";
$etichetta_tipo_appuntamento = "Type de rendez-vous";

$etichetta_esperto_nome = "Nom";
$etichetta_esperto_specializzazione = "Spécialité";
$etichetta_esperto_caratteristiche = "Caractéristiques";
$etichetta_esperto_abbinamento = "Mots-clés correspondants";

$etichetta_chiudi = "Fermer";
$operatore_data_selezionata = "Date sélectionnée";

?>