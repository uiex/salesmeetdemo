<?php

$operatore_prenota = "Prenota un appuntamento con un esperto.";
$operatore_tipo_appuntamento = "Seleziona il tipo di appuntamento.";
$operatore_disponibile_realtime = "Al momento nessun operatore risulta disponibile.";
$operatore_richiedi_location = "Seleziona la location a te preferita per fissare un appuntamento con un nostro esperto.";
$operatore_richiedi_location_timezone = "I nostri esperti risponderanno da queste località.<br>Seleziona la tua zona preferita.";
$operatore_richiedi_appuntamento = "Seleziona la data a te preferita per fissare un appuntamento con un nostro esperto.";
$operatore_richiedi_appuntamento_proposta = "Se non trovi una data a te congeniale proponine una anche se occupata. Se lo spazio si libererà ti contatteremo direttamente.";
$operatore_richiedi_appuntamento_proposta_click = "Clicca qui.";
$operatore_richiedi_appuntamento_proposta_free = "Procedi con la richiesta di porposta a data libera.";
$operatore_seleziona_prodotti = "Seleziona eventuali altri prodotti sulla quale vuoi avere maggiori informazioni. Questo ci permetterà di scegliere per te il migliore esperto.";
$operatore_procedi = "Procedi con l'operazione di \"contatta un esperto\"";
$operatore_favourite_language = "Seleziona la lingua per essere contattato.";
$operatore_inserisci_email = "Inserisci l'email: ";
$operatore_dati_opzionali = "Dati opzionali";
$operatore_inserisci_nome = "Nome";
$operatore_inserisci_cognome = "Cognome";
$operatore_inserisci_telefono = "Telefono";
$operatore_inserisci_domanda = "Motivo della richiesta";
$operatore_specialista_assegnato = "Ti abbiamo assegnato l'esperto migliore per le tue esigenza.";
$operatore_favourite_language = "Lingua preferita";
$operatore_termina_prenotazione = "Termina prenotazione";
$operatore_step_successivo = "Step successivo";
$operatore_step_back = "Back";
$operatore_step_close = "Chiudi";
$operatore_step_annulla = "Annulla";
$operatore_data_appuntamento = "Data appuntamento";
$operatore_scelta_data_appuntamento = "Scelta data appuntamento";
$operatore_altro_prodotto = "Scelta altri prodotti";
$operatore_esperto = "Inserimento dati personali ed assegnazione esperto";
$operatore_appuntamento_ok = "Appuntamento con l'esperto avenuto correttamente.<br><br>Riceverai per email con tutte le indicazioni per procedere con il meeting da te richiesto.<br><br>Grazie.";
$operatore_appuntamento_chiudi = "Chiudi la finestra";
$operatore_email_non_valida = "Indirizzo email non valido!";

// uiex *****
$etichetta_uiex_image = "Immagine";
$etichetta_uiex_title = "Titolo e link";
$etichetta_uiex_price = "Prezzo";
$etichetta_uiex_currency = "Valuta";
$etichetta_uiex_vote = "Voto";
$etichetta_uiex_select = "Seleziona";


$etichetta_location_country = "Nazione";
$etichetta_location_timezone = "Fuso orario esperto";
$etichetta_location_country_esperto = "Location esperto";
$etichetta_time_intro = "L'orario dell'esperto è differente dal tuo, potrebbe corrispondere così, un orario di disponibilità fuori dalla tua fascia oraria ordinaria.";
$etichetta_time_utente_locale = "Se da te sono le "; // "Indicazione del tuo orario attuale: ";
$etichetta_time_utente_locale_indicazioni = "Il tuo orario é: ";
$etichetta_time_esperto_locale = "dall'esperto saranno le "; // "Indicazione dell'orario dell'esperto: ";
$etichetta_time_utente_timezone_differenza = "N.B. l'esperto risponderà in un orario differente<br>dal tuo di ore: ";
$etichetta_location_time = "Orario";
$etichetta_location_region = "Regione";
$etichetta_location_province = "Provincia";
$etichetta_location_city = "Città";
$etichetta_location_street = "Via";
$etichetta_email = "Email: ";

$etichetta_location_acasa_country = "Nazione";
$etichetta_location_acasa_province = "Provincia";
$etichetta_location_acasa_city = "Città";
$etichetta_location_acasa_street = "Via";
$etichetta_location_acasa_titolo = "Compila i seguenti campi con le indicazioni del tuo luogo dell'appuntamento";

$etichetta_tipo_instore = "In agenzia";
$etichetta_tipo_acasa = "Da te";
$etichetta_tipo_online = "Online";
$etichetta_tipo_appuntamento = "Tipo appuntamento";

$etichetta_esperto_nome = "Nome";
$etichetta_esperto_specializzazione = "Specializzazione";
$etichetta_esperto_caratteristiche = "Caratteristiche";
$etichetta_esperto_abbinamento = "Parole chiave di abbinamento";

$etichetta_chiudi = "Chiudi";
$operatore_data_selezionata = "Data selezionata";


?>
<?php

$etichetta_location_acasa_postal_Code = "Postal code";
$etichetta_location_acasa_note = "Note";
$operatore_errore_email = "You have entered an invalid email address!";
$operatore_errore_phone = "Phone numeber empty!";

?>
