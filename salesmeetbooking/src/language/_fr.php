<?php

$operatore_prenota = "Prenez rendez-vous avec un expert.";
$operatore_tipo_appuntamento = "Sélectionnez le type de rendez-vous.";
$operatore_disponibile_realtime = "Al momento nessun operatore risulta disponibile.";
$operatore_richiedi_location = "Select your preferred location to make an appointment with one of our experts.";
$operatore_richiedi_location_timezone = "Select your preferred time zone to make an appointment with one of our experts.";
$operatore_richiedi_appuntamento = "Select your preferred date to make an appointment with one of our experts.";
$operatore_richiedi_appuntamento_proposta = "If you can't find a date that suits you, propose one even if it's busy. If the space becomes free we will contact you directly.";
$operatore_richiedi_appuntamento_proposta_click = "Click here.";
$operatore_richiedi_appuntamento_proposta_free = "Proceed with the request for a free date proposal.";
$operatore_seleziona_prodotti = "Select any other products on which you want to have more information. This will allow us to choose the best expert for you.";
$operatore_procedi = "Proceed with the operation \"contact an expert\"";
$operatore_favourite_language = "Select the language to be contacted.";
$operatore_inserisci_email = "Enter your email: ";
$operatore_dati_opzionali = "Optional data";
$operatore_inserisci_nome = "Name";
$operatore_inserisci_cognome = "Surname";
$operatore_inserisci_telefono = "Phone";
$operatore_inserisci_domanda = "Reason for the request";
$operatore_specialista_assegnato = "We have assigned you the best expert for your needs.";
$operatore_favourite_language = "Langue préférée";
$operatore_termina_prenotazione = "Terminate booking";
$operatore_step_successivo = "NEXT STEP";
$operatore_step_back = "Back";
$operatore_step_close = "Close";
$operatore_step_annulla = "Cancel";
$operatore_data_appuntamento = "Appointment date";
$operatore_scelta_data_appuntamento = "Choice of appointment date";
$operatore_altro_prodotto = "Choice of other products";
$operatore_esperto = "Personal data entry and expert assignment";
$operatore_appuntamento_ok = "Proper appointment with the expert.<br><br>You will receive by email all the information to proceed with the meeting you requested.<br><br>Thank you.";
$operatore_appuntamento_chiudi = "Close the window";
$operatore_email_non_valida = "You have entered an invalid email address!";

// uiex *****
$etichetta_uiex_image = "Image";
$etichetta_uiex_title = "Title";
$etichetta_uiex_price = "Price";
$etichetta_uiex_currency = "Currency";
$etichetta_uiex_vote = "Vote";
$etichetta_uiex_select = "Select";

$etichetta_location_country = "Country";
$etichetta_location_timezone = "Timezone expert";
$etichetta_location_country_esperto = "Expert location";
$etichetta_time_intro = "L'orario dell'esperto è differente dal tuo, potrebbe corrispondere così, un orario di disponibilità fuori dalla tua fascia oraria ordinaria.";
$etichetta_time_utente_locale = "Se da te sono le "; // "Indicazione del tuo orario attuale: ";
$etichetta_time_utente_locale_indicazioni = "Il tuo orario é: ";
$etichetta_time_esperto_locale = "dall'esperto saranno le "; // "Indicazione dell'orario dell'esperto: ";
$etichetta_time_utente_timezone_differenza = "N.B. l'esperto risponderà in un orario differente<br>dal tuo di ore: ";
$etichetta_location_time = "hour";
$etichetta_location_region = "Region";
$etichetta_location_province = "Province";
$etichetta_location_city = "City";
$etichetta_location_street = "Street";
$etichetta_email = "Email: ";

$etichetta_location_acasa_country = "Country";
$etichetta_location_acasa_province = "Province";
$etichetta_location_acasa_city = "City";
$etichetta_location_acasa_street = "Street";
$etichetta_location_acasa_titolo = "Fill in the following fields with the indications of your meeting place";

$etichetta_tipo_instore = "En agence";
$etichetta_tipo_acasa = "At your place";
$etichetta_tipo_online = "Online";
$etichetta_tipo_appuntamento = "Appointment type";

$etichetta_esperto_nome = "Name";
$etichetta_esperto_specializzazione = "Specialization";
$etichetta_esperto_caratteristiche = "Features";
$etichetta_esperto_abbinamento = "Matching keywords";

$etichetta_chiudi = "Close";
$operatore_data_selezionata = "Data selezionata";

?>
<?php

$etichetta_location_acasa_postal_Code = "Postal code";
$etichetta_location_acasa_note = "Note";
$operatore_errore_email = "You have entered an invalid email address!";
$operatore_errore_phone = "Phone numeber empty!";

?>
