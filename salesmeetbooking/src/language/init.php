<?php

$language_file = "en";
switch ($language) {
  case "it":
    // $language_file = "it";
    $language_file = "en";
    $language = "en";
    break;
  case "fr":
    $language_file = "fr";
    $language = "fr"; 
    break;
  case "ch":
    // $language_file = "ch";
    $language_file = "en";
    $language = "en";
    break;
  default:
    $language_file = "en";
    $language = "en";
}
include("language/" . $language_file . ".php");

?>
