        <div id="select_language">
          <?php include("common/logo_img.php"); ?><br><br>
          <?php include("common/titolo.php"); ?>
          <br><br>
          <form id="form_invio_language" method="post" action="">
              <?php echo $operatore_favourite_language; ?>:<br><br>
              <select class="input_text" id="favourite_language" name="favourite_language">
                  <?php if ( (strpos($companyLanguage,"en") !== false) || ($companyLanguage=="") ) { ?>
                    <option value="en" <?php if ($language=="en") { echo "selected"; }?>>English</option>
                  <?php } ?>
                  <!--
                  <?php if ( (strpos($companyLanguage,"it") !== false) || ($companyLanguage=="") ) { ?>
                    <option value="it" <?php if ($language=="it") { echo "selected"; }?>>Italiano</option>
                  <?php } ?>
                -->
                  <?php if ( (strpos($companyLanguage,"fr") !== false) || ($companyLanguage=="") ) { ?>
                    <option value="fr" <?php if ($language=="fr") { echo "selected"; }?>>Français</option>
                  <?php } ?>
                  <!--
                  <?php if ( (strpos($companyLanguage,"ch") !== false) || ($companyLanguage=="") ) { ?>
                    <option value="ch" <?php if ($language=="ch") { echo "selected"; }?>>中文</option>
                  <?php } ?>
                -->
              </select>
              <br><br>
              <input class="invia" type="button" onclick='document.getElementById("form_invio_language").submit();' value="<?php echo $operatore_step_successivo; ?>">
          </form>
        </div>
