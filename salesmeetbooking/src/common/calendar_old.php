<!--link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.css' rel='stylesheet' / -->
<!--script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.js'></script -->
<?php
  $time_slot_appointment = $db->getTimeSlotAppointment($id_company);
  $business_hours = $db->getBusinessHours(0,$id_company);
  // $locationObjJson[0]["timezone"];
  // $appointmentObjJson[0]["in_store_enable"]

  $date = date_create("now");
  // echo date_format($date, 'Y-m-d H:i:sP') . " - Nauru<br>";

  // date_timezone_set($date, timezone_open('Europe/Rome'));
  // echo date_format($date, 'Y-m-d H:i:sP') . " - Rome<br>";

  // Open the timezone of America/Chicago
  $timezone_azienda = timezone_open($locationObjJson[0]["timezone_name"]); // timezone_open("America/Chicago");
  $timezone_cliente = timezone_open(timezone_name_from_abbr("", 3600, 0)); //  timezone_open("Europe/Rome");

  // Displaying the offset of America/Chicago and Europe/Amsterdam
  /*
  $datetime_eur = date_create("now");
  echo date_format($datetime_eur, 'Y-m-d H:i:sP') . " - Default<br>";
  $datetime_eur = date_create("now", timezone_open("Europe/Moscow"));
  echo date_format($datetime_eur, 'Y-m-d H:i:sP') . " - Moscow<br>";
  */
  /*
  print_r($timezone_cliente);
  echo "<br>";
  echo "timezone azienda - America/Chicago<br>";
  echo timezone_offset_get($timezone_azienda, $date) / 3600;
  echo "<br>";
  echo "timezone cliente - Europe/Rome<br>";
  echo timezone_offset_get($timezone_cliente, $date) / 3600;
  echo "<br>";
  echo timezone_name_from_abbr("", 3600, 0) . "<br>";
  */


  $locate_tz = new DateTimeZone( $locationObjJson[0]["timezone_name"] );
  $locate = new DateTime('now', $locate_tz);

  //NY is 3 hours ahead, so it is 2am, 02:00
  $user_tz = new DateTimeZone( timezone_name_from_abbr("", 3600, 0) );
  $user = new DateTime('now', $user_tz);

  $usersTime = new DateTime($user->format('Y-m-d H:i:s'));
  $locatesTime = new DateTime($locate->format('Y-m-d H:i:s'));
  // print_r($usersTime) . "<br>";
  // print_r($locatesTime) . "<br>";

  $interval = $usersTime->diff($locatesTime);
  print_r($interval->h); //outputs 3


?>
<link href='fullcalendar/main.min.css' rel='stylesheet' />
<script src='fullcalendar/main.min.js'></script>
<script src='fullcalendar/locales/<?php echo $language_file; ?>.js'></script>
<script>
    console.log("<?php echo $language_file; ?>");
    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'timeGridWeek',
        locale: '<?php echo $language_file; ?>',
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'timeGridWeek,timeGridDay'
        },
        businessHours: [ // specify an array instead
          <?php
          foreach ($business_hours as &$value) {
             /*
             echo "{";
             echo "daysOfWeek:[" . $value["days_of_week"] . "],";
             echo "startTime: '" . $value["start_time"] . "',";
             echo "endTime: '" . $value["end_time"] . "'";
             echo "},";
             */
             $d=strtotime($value["start_time"] . " April 15 2014");
             $start_time = date('H:i',strtotime('+' . $locationObjJson[0]["timezone"] . ' hour',$d));
             $d=strtotime($value["end_time"] . " April 15 2014");
             $end_time = date('H:i',strtotime('+' . $locationObjJson[0]["timezone"] . ' hour',$d));
             echo "{";
             echo "daysOfWeek:[" . $value["days_of_week"] . "],";
             echo "startTime: '" . $start_time . "',";
             echo "endTime: '" . $end_time . "'";
             echo "},";
          }
          ?>
        ],
        selectable: true,
        dateClick: function(info) {
          inserimentoData(info.dateStr);
        },
        select: function(info) {
          // alert('selected ' + info.startStr + ' to ' + info.endStr);
        },
        <?php if ($freecalendar=="") { ?>
          <?php
          echo $db->getCalendarByCompany($id_company,$time_slot_appointment,$locationObjJson[0]["timezone"]); ?>
        <?php } ?>
      });
      calendar.render();
    });
    // 'dayGridMonth,timeGridWeek,timeGridDay'
    // initialDate: '2020-09-07',
    console.log("<?php echo $db->getCalendarByCompany($id_company,$time_slot_appointment,""); ?>");
</script>
