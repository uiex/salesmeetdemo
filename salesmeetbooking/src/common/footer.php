
    <?php include("common/close.php"); ?>
    <?php include("common/language_scelta.php"); ?>

    <div id="footer-mobile"></div>

    <script>
        $(".invia").click(function(){
          loadingButton();
        });
        function loadingButton() {
          $(".invia").css("background-color", "rgb(222 220 220)");
          $(".invia").css("color", "rgb(222 220 220)");
          $('.invia').prop('disabled', true);
          // $(".invia").css("background-size", "contain");
          $(".invia").css("background-size", "35px");
          $(".invia").css("background-repeat", "no-repeat");
          $(".invia").css("background-position", "center");
          $(".invia").css("background-image", "url(https://booking.salesmeet.it/asset/img/loading3.svg)");
        }
        function closeAlertPage() {
          $(".alert_all_page").hide();
        }
        function openLanguage() {
          $("#select_language").show();
        }
        function closeLanguage() {
          $("#select_language").hide();
        }

        function close() {
            window.parent.postMessage({
                'func': 'parentFuncName',
                'message': 'Message text from iframe.'
            }, "*");
        }
        function closeSalesmeet() {
            $(".alert_close_salesmeet").show();
        }
        function closeSalesmeetNo() {
            $(".alert_close_salesmeet").hide();
        }
        function closeSalesmeetSi() {
            close();
        }

        function openMenu() {
          $("#prodotto").show();
        }
        function closeMenu() {
          $("#prodotto").hide();
        }
    </script>

  </body>
</html>
