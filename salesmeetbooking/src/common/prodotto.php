<div id="prodotto">
  <!--div class="separatore"></div-->
  <div id="menu-close">
      <a href="javascript:closeMenu();"><?php echo $operatore_step_close; ?></a>
  </div>
  <div id="booking-dati">
      <?php include("common/dati.php"); ?>
  </div>
  <div id="prodotto-img">
    <?php
    $img_temp = $img;
    $img_arrray = explode("https://", $img);
    foreach ($img_arrray as &$value) {
        $img_temp = "https://" . $value;
    }
    ?>
    <img src="<?php echo $img_temp; ?>" style="max-width: 100px; max-height:100px;">
  </div>
  <div id="prodotto-titolo">
      <?php echo $title; ?>
  </div>
</div>
