<!DOCTYPE html>
<html lang="<?php echo $language_file; ?>">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title></title>
    <link rel="stylesheet" href="asset/css/main.css?<?php echo time();?>" media="all" type="text/css" >
    <link rel="stylesheet" href="https://use.typekit.net/coa3wtf.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
    #titolo, .operazione, #prodotto, #div-right-body, #div-left-body, input[type="button"]  {
        font-family: muli, sans-serif;
        font-weight: 400;
        font-style: normal;
    }
   .fc-toolbar.fc-header-toolbar, .fc-col-header-cell-cushion, .fc-timegrid-slot-label-cushion,.fc-timegrid-axis-cushion  {
        font-family: muli, sans-serif;
        font-weight: 400;
        font-style: normal;
    }
    </style>
    <?php
    if ($company_css[0]!="") {
          echo "<style>" . $company_css[0] . "</style>";
    }
    ?>
