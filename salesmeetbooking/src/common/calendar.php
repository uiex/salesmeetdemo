<!--link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.css' rel='stylesheet' / -->
<!--script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.js'></script -->
<?php
  $time_slot_appointment = $db->getTimeSlotAppointment($id_company);
  $business_hours = $db->getBusinessHours(0,$id_company);
  // $locationObjJson[0]["timezone"];
  // $appointmentObjJson[0]["in_store_enable"]
  $locate_tz = new DateTimeZone( $locationObjJson[0]["timezone_name"] );
  $locate = new DateTime('now', $locate_tz);

  //NY is 3 hours ahead, so it is 2am, 02:00
  $user_tz = new DateTimeZone( timezone_name_from_abbr("", 3600, 0) );
  $user = new DateTime('now', $user_tz);

  $usersTime = new DateTime($user->format('Y-m-d H:i:s'));
  $locatesTime = new DateTime($locate->format('Y-m-d H:i:s'));

  $interval = $usersTime->diff($locatesTime);
  $timezone_location_hours = $interval->h;
  if (date_format($usersTime, 'Y-m-d H:i:s') > date_format($locatesTime, 'Y-m-d H:i:s')) {
      // echo date_format($usersTime, 'Y-m-d H:i:s') . ' greater than ' . date_format($locatesTime, 'Y-m-d H:i:s') ;
      // $timezone_location_hours = $timezone_location_hours * -1;

      $timezone_location_hours = abs($timezone_location_hours);
  } else {
      // echo date_format($usersTime, 'Y-m-d H:i:s') . ' LESS than ' . date_format($locatesTime, 'Y-m-d H:i:s') ;
      // $timezone_location_hours = $timezone_location_hours * +1;
      $timezone_location_hours = abs($timezone_location_hours) * -1;
  }

  $timezone_location_hours_inverse = $timezone_location_hours;
  if ($timezone_location_hours > 0) {
      $timezone_location_hours_inverse = abs($timezone_location_hours)  * -1;
  } elseif ($timezone_location_hours < 0) {
      $timezone_location_hours_inverse = abs($timezone_location_hours);
  }

?>
<link href='fullcalendar/main.min.css' rel='stylesheet' />
<script src='fullcalendar/main.min.js'></script>
<script src='fullcalendar/locales/<?php echo $language_file; ?>.js'></script>
<style>
.fc .fc-timegrid-col-bg .fc-highlight, .fc .fc-timegrid-col-events {
    background: #4caf50;
}
</style>
<script>
    var dt = new Date();
    var getMonth = dt.getMonth() + 1; if (getMonth<10) { getMonth = "0" + getMonth; }
    // console.log(getMonth);

    var getDay = dt.getDate(); if (getDay<10) { getDay = "0" + getDay; }
    var datadioggi = dt.getFullYear() + "-" + getMonth + "-" + getDay;

    // datadioggi = datadioggi;  + "T12:00:00";
    // console.log(datadioggi);

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'timeGridWeek',
        validRange: {
          start: datadioggi
        },
        allDaySlot: false,
        locale: '<?php echo $language_file; ?>',
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'timeGridWeek,timeGridDay'
        },
        <?php if ($freecalendar == "") {
                  echo 'selectConstraint: "businessHours",';
             } ?>
        businessHours: [ // specify an array instead
          <?php
          foreach ($business_hours as &$value) {

             $d_start=strtotime($value["start_time"] . " April 15 2014");
             $start_time = date('H:i',strtotime('+' . $timezone_location_hours . ' hour',$d_start));
             $start_time_hours = date('H',strtotime('+' . $timezone_location_hours . ' hour',$d_start));

             $d_end=strtotime($value["end_time"] . " April 15 2014");
             $end_time = date('H:i',strtotime('+' . $timezone_location_hours . ' hour',$d_end));
             $end_time_hours = date('H',strtotime('+' . $timezone_location_hours . ' hour',$d_end));

             if (($start_time_hours * 1) > ($end_time_hours * 1)) {

                  $d_start = date('m/d/Y H:i:s',$d_start);
                  $d_end = date('m/d/Y H:i:s',$d_end);
                  $datetime1 = date_create($d_start);
                  $datetime2 = date_create($d_end);
                  /*
                  print_r($datetime1);
                  echo "<hr>";
                  print_r($datetime2);
                  echo "<hr>";
                  */
                  $interval = date_diff($datetime1, $datetime2);
                  // echo $interval->h;
                  // echo "<hr>";
                  $h =  $interval->h - (24 - $start_time_hours);
                  $h = sprintf("%02d", $h);
                  $m =  $interval->m;
                  $m = sprintf("%02d", $m);
                  //echo $h;
                  //echo "<hr>";
                  // $diff=date_diff($d_end,$d_start);
                  // $interval = date_diff($d_start, $d_end);
                  echo "{";
                  echo "daysOfWeek:[" . $value["days_of_week"] . "],";
                  echo "startTime: '" . $start_time . "',";
                  echo "className: 'fc-nonbusiness',";
                  echo "endTime: '24:00'";
                  echo "},";
                  echo "{";
                  echo "daysOfWeek:[" . $value["days_of_week"] . "],";
                  echo "startTime: '00:00',";
                  echo "className: 'fc-nonbusiness',";
                  echo "endTime: '" . $h . ":" . $m . "'";
                  echo "},";

             } else {
                  //echo $start_time_hours . "<br>";
                  // echo $end_time_hours . "<br>";
                  echo "{";
                  echo "daysOfWeek:[" . $value["days_of_week"] . "],";
                  echo "startTime: '" . $start_time . "',";
                  echo "endTime: '" . $end_time . "'";
                  echo "},";
             }
          }
          ?>
        ],
        selectable: true,
        dateClick: function(info) {
          if (flagok) {
            // console.log("dateClickdateClickdateClickdateClick");
            dataselezionata = info.dateStr;

            var today = new Date();
            var datacor = info.date;
            // console.log(today);
            // console.log(datacor);
            var diffDays = parseFloat((datacor - today ) / (1000 * 60 * 60 * 24), 10);
            // console.log( diffDays );
            if (diffDays == -0) {
                deleteData( info );
            } else if (diffDays >= -0)  {
                // console.log( "bellllaaaa" );
                inserimentoData(info.dateStr, <?php echo $timezone_location_hours_inverse; ?>);
            } else {
                deleteData( info );
            }
          }
          flagok = false;
        },
        unselect: function( info) {
          // console.log("unselect unselect unselect");
          deleteData(info);
        },
        select: function(info) {
          flagok = true;
          // console.log("selectselectselectselectselectselect");
        },
        <?php if ($freecalendar=="") { ?>
          <?php
          echo $db->getCalendarByCompany($id_company,$time_slot_appointment,$timezone_location_hours); ?>
        <?php } ?>
      });
      calendar.render();
    });

    var flagok = false;
    var dataselezionata = "";


    // 'dayGridMonth,timeGridWeek,timeGridDay'
    // initialDate: '2020-09-07',
    // console.log("<?php echo $db->getCalendarByCompany($id_company,$time_slot_appointment,""); ?>");

    $(document).ready(function(){
      var position = $( "#calendar" ).position();
      var altezza_pagina = $( window ).height();
      $('#calendar').css("height", + (altezza_pagina-position.top) + "px");
    });


</script>
