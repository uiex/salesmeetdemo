

<?php //************ Tipologia appuntamento ************ ?>
<?php
$flaglink = "";
if ($appointmentObjJson[0]["in_store_enable"]==1) {
    $flaglink = $etichetta_tipo_online;
}
if ($appointmentObjJson[0]["in_store_enable"]==2) {
    $flaglink = $etichetta_tipo_instore;
}
if ($appointmentObjJson[0]["in_store_enable"]==3) {
    $flaglink = $etichetta_tipo_acasa;
}
if ($flaglink != "") { ?>
  <?php if ($menu_scelta!="") { ?><a href="index.php"><?php } ?>
       <img class="img_link" src="asset/img/link.png">
      <?php echo $etichetta_tipo_appuntamento . ": " . $flaglink; ?>
  <?php if ($menu_scelta!="") { ?></a><?php } ?>
  <div class="separatore_dati"></div>
<?php } ?>


<?php //************ Location ************ ?>
<?php if ($locationObjJson[0]["id_location"]>0) { ?>
  <?php if ($menu_scelta!="") { ?><a href="location.php"><?php } ?>
    <img class="img_link" src="asset/img/link.png">
    <?php echo $etichetta_location_country_esperto . ": " . $locationObjJson[0]["country"]; ?>
    <!--<?php echo $etichetta_location_timezone . ": " . $locationObjJson[0]["timezone_name"]; ?>-->
  <?php if ($menu_scelta!="") { ?></a><?php } ?>
  <div class="separatore_dati"></div>
<?php } ?>


<?php //************ Data appuntamento ************ ?>
<?php if ($appointmentObjJson[0]["data_selezionata_timezone"]!="") { ?>
  <?php if ($menu_scelta!="") { ?><a href="calendar.php"><?php } ?>
      <img class="img_link" src="asset/img/link.png">
  <?php
      $data_selezionata_timezone = $appointmentObjJson[0]["data_selezionata_timezone"];
      $data_selezionata_timezone = new DateTime($data_selezionata_timezone);
      $data_selezionata_timezone = $data_selezionata_timezone->format('M j Y, G:i');
  ?>
      <?php echo $operatore_data_appuntamento; ?>: <?php echo $data_selezionata_timezone; ?>
  <?php if ($menu_scelta!="") { ?></a><?php } ?>
  <div class="separatore_dati"></div>
<?php } else {
  echo "No date ...";
} ?>


<?php //************ Lingua favorita ************ ?>
<?php if ($appointmentObjJson[0]["favourite_language"]!="") { ?>
  <?php if ($menu_scelta!="") { ?><a href="javascript:openLanguage();"><?php } ?>
    <img class="img_link" src="asset/img/link.png">
    <?php echo $operatore_favourite_language; ?>: <?php echo $favourite_language; ?>
  <?php if ($menu_scelta!="") { ?></a><?php } ?>
  <div class="separatore_dati"></div>
<?php } ?>


<?php //************ Dati cliente ************ ?>
<?php if ($appointmentObjJson[0]["email"]!="") { ?>
  <?php echo $etichetta_email = "Email: ";; ?>: <?php echo $email; ?><br><br>
<?php } ?>
<?php if ($appointmentObjJson[0]["name"]!="") { ?>
  <?php echo $operatore_inserisci_nome; ?>: <?php echo $nome; ?><br><br>
<?php } ?>
<?php if ($appointmentObjJson[0]["surname"]!="") { ?>
  <?php echo $operatore_inserisci_cognome; ?>: <?php echo $cognome; ?><br><br>
<?php } ?>
<?php if ($appointmentObjJson[0]["telephone"]!="") { ?>
  <?php echo $operatore_inserisci_telefono; ?>: <?php echo $telefono; ?><br><br>
<?php } ?>
