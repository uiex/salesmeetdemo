<?php include("setting.php"); ?>
<?php include("common/head.php"); ?>
<?php $expert = $db->getExpertByCompany($id_company);
      $expertObj = json_decode($expert, true);
?>

  <script>
    function submitStep() {
        var error = "";
        var inputText = document.getElementById("email").value;
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if(!inputText.match(mailformat)) {
            error = "<?php echo $operatore_errore_email; ?>\n";
        }
        if(document.getElementById("email").value=="") {
            error += "<?php echo $operatore_errore_phone; ?>\n";
        }

        if (error!="") {
          alert(error);
        } else {
          var element = document.getElementById("inviabutton");
          element.classList.add("invia");
          loadingButton();
          document.getElementById("form_invio").submit();
        }
    }
  </script>
  </head>
  <body style="">

    <?php include("common/language.php"); ?>

    <div id="div-left">
      <div id="div-left-body">
          <?php include("common/logo.php"); ?>
          <?php include("common/prodotto.php"); ?>
          <form id="form_invio" method="post" action="recap.php">

              <input placeholder="<?php echo $operatore_inserisci_email; ?>" class="input_text" type="email" id="email" name="email" value="<?php echo $email; ?>"><br><br>
              <input placeholder="<?php echo $operatore_inserisci_telefono; ?>" class="input_text" type="input" id="telefono" name="telefono"><br><br>

              <?php echo $operatore_dati_opzionali; ?>:<br><br>

              <input placeholder="<?php echo $operatore_inserisci_nome; ?>" class="input_text" type="input" id="nome" name="nome"><br><br>
              <input placeholder="<?php echo $operatore_inserisci_cognome; ?>" class="input_text" type="input" id="cognome" name="cognome"><br><br>
              <textarea placeholder="<?php echo $operatore_inserisci_domanda; ?>" class="input_textarea" id="description" name="description"></textarea><br><br>

              <input type="hidden" id="laststep" name="laststep" value="1">
              <input type="hidden" id="id_expert" name="id_expert" value="<?php echo $expertObj[0]['id_expert']; ?>">
              <input id="inviabutton" class="" type="button" onclick='javascript:submitStep();'  value="<?php echo $operatore_termina_prenotazione; ?>">
          </form>
      </div>
    </div>
    <div id="div-right">
        <div id="div-right-body">

          <div class="booking_title">
            <?php echo $operatore_esperto; ?>:
          </div>

          <div id="esperto">
            <?php echo $operatore_specialista_assegnato; ?><br><br>
            <div id="esperto-img">
                <?php if ($expertObj[0]['img']=="") { ?>
                    <img src="asset/images/esperto.png" style="height:200px" />
                <?php } else { ?>
                    <img src="<?php echo $expertObj[0]['img']; ?>" style="height:200px" />
                <?php }?>
            </div>
            <div id="esperto-titolo">
                <?php echo $etichetta_esperto_nome; ?>: <?php echo $expertObj[0]['name']; ?><br><br>
                <?php if ($expertObj[0]['specialization']!="") { ?>
                  <?php echo $etichetta_esperto_specializzazione; ?>: <?php echo $expertObj[0]['specialization']; ?><br><br>
                <?php } ?>
                <?php if ($expertObj[0]['features']!="") { ?>
                  <?php echo $etichetta_esperto_caratteristiche; ?>: <?php echo $expertObj[0]['features']; ?><br><br>
                <?php } ?>
                <?php if ($expertObj[0]['keyword_work']!="") { ?>
                  <?php echo $etichetta_esperto_abbinamento; ?>: <?php echo $expertObj[0]['keyword_work']; ?>,<?php echo $expertObj[0]['keywords_life']; ?><br><br>
                <?php } ?>
            </div>
          </div>
        </div>
    </div>

<?php include("common/footer.php"); ?>
