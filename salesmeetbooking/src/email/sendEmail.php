<?php

/*
sendgrid.com
user: facchini.corrado@gmail.com
password: AKIAVUGBITZEFWWLZTXO
*/

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailerUIEX/src/Exception.php';
require 'PHPMailerUIEX/src/PHPMailer.php';
require 'PHPMailerUIEX/src/SMTP.php';

class email {

		var $sender = 'noreply@salesmeet.co'; // 'facchini.corrado@gmail.com'; // 'noreply@salesmeet.it'; // !wB?MQnst7*?7u3
		var $senderName = 'Booking SalesMeet';

		function sendAppointment($id_appointment) {

				$dba = new db();

				$appointment = $dba->getAppointment($id_appointment);
				$appointmentobj = json_decode($appointment, true);
				$favourite_language = $appointmentobj[0]["favourite_language"];
				$id_company = $appointmentobj[0]["id_company"];

				$expert = $dba->getExpert($id_appointment);
				$expertobj = json_decode($expert, true);

				$link = $dba->getLinkAppointment($id_appointment);
				$linkobj = json_decode($link, true);


				/* 0 email di conferma, 1 appuntamento già confermato ... */
				$type_expert = "booking_expert";
				$type_customer = "booking_customer";
				$id_expert = $expertobj[0]["id_expert"];
        $emailSetting = $dba->getEmailSetting($id_expert,$id_company);
        if ($emailSetting["action_customer_booking"]==0) {
						$type_expert = "booking_expert_pre_confirmation";
						$type_customer = "booking_customer_pre_confirmation";
						$dba->updateAppointmentStepLast($id_appointment);
        } else {
					  $dba->updateAppointmentEmailConfirmed($id_appointment);
				}

				$email_template = $dba->getEmailTemplate($type_expert,$favourite_language,$id_company);
				$subject = $this->changeSubject($email_template["subject"],$appointmentobj,$expertobj,$linkobj);
				$email_body = $this->changeEmailBody($email_template["description"],$appointmentobj,$expertobj,$linkobj);

				$sender = $expertobj[0]["email"];
				$senderName = $expertobj[0]["email"];

				// Expert
				$obj = array(
						"sender" => $this->sender,
						"senderName" => $this->senderName,
					  "recipient" => $expertobj[0]["email"],
				    "subject" => $subject,
						"message_html" => $email_body,
						"message_txt" => $email_body,
				);
				$this->send($obj);

				// Customer
				$email_template = $dba->getEmailTemplate($type_customer,$favourite_language,$id_company);
				$subject = $this->changeSubject($email_template["subject"],$appointmentobj,$expertobj,$linkobj);
				$email_body = $this->changeEmailBody($email_template["description"],$appointmentobj,$expertobj,$linkobj);
				$obj = array(
						"sender" => $this->sender,
						"senderName" => $this->senderName,
					  "recipient" => $appointmentobj[0]["email"],
				    "subject" => $subject,
						"message_html" => $email_body,
						"message_txt" => $email_body,
				);
				$this->send($obj);

		}

		function changeSubject($subject,$appointmentobj,$expertobj,$linkobj) {

				$data_selezionata = $appointmentobj[0]["data_selezionata"];

				$subject = str_replace("{date_appointment}", $data_selezionata, $subject);
				return $subject;
		}

		function changeEmailBody($email_body,$appointmentobj,$expertobj,$linkobj) {

				$cliente_email = $appointmentobj[0]["email"];
				$cliente_name = $appointmentobj[0]["name"];
				$cliente_surname = $appointmentobj[0]["surname"];
				$data_selezionata = $appointmentobj[0]["data_selezionata"];

				$esperto_email = $expertobj[0]["email"];
				$esperto_name = $expertobj[0]["name"];

				$dashboard_expert = $linkobj[0]["dashboard_expert"];
				$link_salesmeet_expert = $linkobj[0]["link_salesmeet_expert"];
				$link_salesmeet_customer = $linkobj[0]["link_salesmeet_customer"];
				$external_expert = $linkobj[0]["external_expert"];
				$external_customer = $linkobj[0]["external_customer"];

				$email_body = str_replace("{expert_name}", $esperto_name, $email_body);
				if ($cliente_name!="") {
						$email_body = str_replace("{customer_name}", $cliente_name . " " . $cliente_surname, $email_body);
				} else {
						$email_body = str_replace("{customer_name}", $cliente_email, $email_body);
				}
				$email_body = str_replace("{date_appointment}", $data_selezionata, $email_body);
				$email_body = str_replace("{link_dashboard}", $dashboard_expert, $email_body);

				if ($external_customer=="") {
						$email_body = str_replace("{link_appointment_customer}", $link_salesmeet_customer, $email_body);
				} else {
						$email_body = str_replace("{link_appointment_customer}", $external_customer, $email_body);
				}

				$email_body = $email_body . $expertobj[0]["signature"];

				return $email_body;
		}

		function send($obj) {

				// Replace smtp_username with your Amazon SES SMTP user name.
		    $usernameSmtp = 'apikey'; // '0-t436Z8RvmSlsqFCdSw2g';
				// Replace smtp_password with your Amazon SES SMTP password.
		    $passwordSmtp = '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';
				// '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';
		    $host = 'smtp.sendgrid.net';
				$port = 587;
				$mail = new PHPMailer(true);

				try {
				    // Specify the SMTP settings.
				    $mail->isSMTP();
				    $mail->setFrom($obj["sender"], $obj["senderName"]);
				    $mail->Username   = $usernameSmtp;
				    $mail->Password   = $passwordSmtp;
				    $mail->Host       = $host;
				    $mail->Port       = $port;
				    $mail->SMTPAuth   = true;
				    $mail->SMTPSecure = 'tls';
				//    $mail->addCustomHeader('X-SES-CONFIGURATION-SET', $configurationSet);

				    // Specify the message recipients.
				    $mail->addAddress($obj["recipient"]);
				    // You can also add CC, BCC, and additional To recipients here.

						/*
						$attachment = 'ics/files/839.ics';
						$mail->AddAttachment($attachment , 'calendar.ics');
						*/


				    // Specify the content of the message.
				    $mail->isHTML(true);
				    $mail->Subject    = $obj["subject"];
				    $mail->Body       = $obj["message_html"];
				    $mail->AltBody    = $obj["message_txt"];
				    $mail->Send();

						//  echo '{ "": "", "message": "Messaggio inviato correttamente." }', PHP_EOL;
				} catch (phpmailerException $e) {
						//  echo '{ "alert": "error", "message": "' . $e->errorMessage() . '" }', PHP_EOL;
				} catch (Exception $e) {
						//  echo '{ "alert": "error", "message": "' . $mail->ErrorInfo .'" }', PHP_EOL;
				}

		}
}

/*
$email = new email();
$email->sendAppointment(91);
*/

?>
