    function getPreferitiDTable(token,user,site,url) {
        // getCronologia(token,user,site);
        // getPreferiti(token,user,site);
        $('#preferiti').DataTable( {
            "ajax": {
                "url": "https://uiextools.salesmeet.it/chronology/experience/limit/",
                "type": "POST",
                "dataSrc": "",
                "data" : {
                    "token" : token,
                    "user": user,
                    "site": site
                }
            },
            "columns": [
                { "data": "url",
                    "render": function (data, type, row) {
                        return this.createInput( data, url, row );
                    }
                },
                { "data": "img",
                    "render": function (data, type, row) {
                        if (data == '') {
                            return '<img src="asset/images/picture.png" style="height:25px" />';
                        }  else {
                            return '<img src="' + data + '" style="max-width: 100px;max-height:60px;" />';
                        }
                    }
                },
                { "data": "title"},
                { className: "mobile-none", "data": "voto_AI",
                    "render": function (data, type, row) {
                        return this.stelle( data );
                    }
                },

            ]
        } );
    }

    function getPreferitiDTableDARIPRISTINARE(token,user,site,url) {
        getCronologia(token,user,site);
        getPreferiti(token,user,site);
        $('#preferiti').DataTable( {
            "ajax": {
                "url": "https://uiextools.salesmeet.it/chronology/experience/limit/",
                "type": "POST",
                "dataSrc": "",
                "data" : {
                    "token" : token,
                    "user": user,
                    "site": site
                }
            },
            "columns": [
                { "data": "url",
                    "render": function (data, type, row) {
                        return this.createInput( data, url, row );
                    }
                },
                { "data": "img",
                    "render": function (data, type, row) {
                        if (data == '') {
                            return '<img src="asset/images/picture.png" style="height:25px" />';
                        }  else {
                            return '<img src="' + data + '" style="max-width: 100px;max-height:60px;" />';
                        }
                    }
                },
                { "data": "title"},
                { className: "mobile-none", "data": "price"},
                { className: "mobile-none", "data": "currency"},
                { className: "mobile-none", "data": "voto_AI",
                    "render": function (data, type, row) {
                        return this.stelle( data );
                    }
                },

            ]
        } );
    }

    function createInput( value, sito, row ) {
        var img = row.img;
        img = img.replace("'", "");
        var title = row.title;
        title = title.replace("'", "");
        var checked = "";
        if (sito==value) {
            checked = " checked";
        }
        var input = `<input onchange="insertProdotto('${value}','${img}','${title}');" type="checkbox" id="scales" name="scales" ${checked}>`;
        return input;
    }

    function stelle( value ) {
        var stelle = "";
        var voto_AItemp = parseInt( value ).toString().length / 2;
        for (var i = 0; i < Math.floor(voto_AItemp); i++) {
            stelle += `<img style="display: inline-block;" src="asset/images/star.png">`;
        }
        if (voto_AItemp.toString().indexOf(".") != -1) {
            stelle += `<img style="display: inline-block;" src="asset/images/star.png">`;
        }
        return stelle;
    }


    function getPreferiti(token,user,site) {

        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {
            var arraybuffer = oReq.response;
            // console.log(arraybuffer);
            var jsonData = JSON.parse(arraybuffer);
            console.log(jsonData);
            var element =  "";
            for (var i = 0; i < jsonData.length; i++) {

            }
        }
        console.log("token: " + token);
        oReq.open("POST", "https://uiextools.salesmeet.it/chronology/experience/limit/");
        var formData = new FormData();
        formData.append("token", token );
        formData.append("user", user );
        formData.append("site", site );
        oReq.send( formData );
    }

    function getCronologia(token,user,site) {

        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {
            var arraybuffer = oReq.response;
            // console.log(arraybuffer);
            var jsonData = JSON.parse(arraybuffer);
            console.log(jsonData);
            var element =  "";
            for (var i = 0; i < jsonData.length; i++) {

            }
        }
        console.log("token: " + token);
        oReq.open("POST", "https://uiextools.salesmeet.it/chronology/chronology/");
        var formData = new FormData();
        formData.append("token", token );
        formData.append("user", user );
        formData.append("site", site );
        oReq.send( formData );
    }
