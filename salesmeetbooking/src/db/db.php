<?php

class db {

    function getConnect() {
        $db   = 'salesmeet';
        /*
        $host = 'salesmeet-db';
        $user = 'root';
        $pass = 'your_mysql_root_password';
        $charset = 'utf8mb4';
        */
        // $host = 'salesmeet.caacentelger.eu-west-1.rds.amazonaws.com';
        $host = 'salesmeet.c9hljosmivib.eu-west-1.rds.amazonaws.com';
        $user = 'admin';
        $pass = '123#4567AszsaL3smeet';
        $charset = 'utf8';
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        try {
             // $pdo = new PDO($dsn, $user, $pass, $options);
             $pdo = new \PDO($dsn, $user, $pass);
             return $pdo;
        } catch (\PDOException $e) {
             throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    function createAppointment($site,$referer,$id_company,$ip,$os,$browser) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments (site, referer, id_company, id_token, ip, os, browser) VALUES(?,?,?,?,?,?,?)");
            try {
                $id_token = md5(uniqid(Rand(), true));
                $pdo->beginTransaction();
                $stmt->execute( array( $site, $referer, $id_company,$id_token,$ip,$os,$browser) );
                $id = $pdo->lastInsertId();
                $pdo->commit();
                $this->createAppointmentLink($id,$referer,$id_token);
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function createAppointmentLink($id_appointment,$referer,$id_token) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments_link (id_appointment, link_salesmeet_expert,link_salesmeet_customer,dashboard_expert) VALUES(?,?,?,?)");
            try {
                $pdo->beginTransaction();
                $pos = strpos($referer, "?");
                if ($pos === false) {
                    $referer = $referer . "?";
                } else {
                    $referer = $referer . "&";
                }
                $link_salesmeet_expert = $referer . "salesmeettoken=" . $id_token . "v";
                $link_salesmeet_customer = $referer . "salesmeettoken=" . $id_token . "c";
                $dashboard_expert = "https://app.salesmeet.it/dashboard/index.php?token=" . $id_token;

                $stmt->execute( array( $id_appointment, $link_salesmeet_expert, $link_salesmeet_customer,$dashboard_expert) );
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentTypeBooking($type_booking,$id_appointment) {
        if (!$this->is_number($type_booking)) { $type_booking = 0; }
        $pdo = $this->getConnect();
        try {
            // echo "<hr>" . $type_booking . " - " . $id_appointment . "<hr>" ;
            $stmt = $pdo->prepare("UPDATE appointments set in_store_enable = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($type_booking,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function updateAppointmentLocation($id_location,$id_appointment) {
        if (!$this->is_number($id_location)) { $id_location = 0; }
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set id_location = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_location,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function updateAppointmentUiex($token,$user,$language,$title,$img,$price,$id_appointment) {
        if (!$this->is_number($price)) { $price = 0; }
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set tokenuiex = ?, user = ?, language = ?, title = ?, img = ?, price = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($token,$user,$language,$title,$img,$price,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentStepDataSelezionata($data_selezionata,$data_selezionata_timezone,$user_timezone,$freecalendar,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set data_selezionata = ?, data_selezionata_timezone = ?, user_timezone = ?, freecalendar = ? WHERE id_appointment = ?");
            try {
                $date = new DateTime($data_selezionata);
                $data_selezionata = $date->format('Y-m-d H:i:s');
                $date_timezone = new DateTime($data_selezionata_timezone);
                $data_selezionata_timezone = $date_timezone->format('Y-m-d H:i:s');
                if ($freecalendar=="") {
                  $freecalendar = 0;
                }
                $pdo->beginTransaction();
                $stmt->execute( array($data_selezionata,$data_selezionata_timezone,$user_timezone,$freecalendar,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentLanguage($favourite_language,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set favourite_language = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($favourite_language,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentStepEmail($email,$description,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set email = ?, description = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($email,$description,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentStepOpzionali($nome,$cognome,$telefono,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set name = ?, surname = ?, telephone = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($nome,$cognome,$telefono,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function updateAppointmentStepLast($id_appointment) {
        $this->updateAppointmentStateCommon($id_appointment,1);
    }
    function updateAppointmentEmailConfirmed($id_appointment) {
        $this->updateAppointmentStateCommon($id_appointment,2);
    }
    function updateAppointmentStateCommon($id_appointment,$stato) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set state = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($stato,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function isProductPresent($url,$image,$titolo,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_products WHERE id_appointment = ? and url = ? and visible = 0");
            $stmt->execute([$id_appointment, $url]);
            $id_product = 0;
            while ($row = $stmt->fetch()) {
                $id_product = $row['id_product'];
            }
            if ($id_product==0) {
                $this->insertProduct($url,$image,$titolo,$id_appointment);
            } else {
                $this->deleteProduct($url,$id_appointment);
            }

        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function insertProduct($url,$image,$titolo,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments_products (url,img,title,id_appointment) VALUES(?,?,?,?)");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($url,$image,$titolo,$id_appointment));
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function deleteProduct($url,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments_products set visible = 1 WHERE id_appointment = ? and url = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_appointment,$url));
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getIdCompany($url) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM company_registry WHERE site = ? and active = 0");
            $stmt->execute([$url]);
            while ($row = $stmt->fetch()) {
                return $row['id_company'];
            }
            return 0;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getCompany($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM salesmeet.company_registry where id_company = ?");
            $stmt->execute([$id_company]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getTimeSlotAppointment($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM company_registry WHERE id_company = ? and active = 0");
            $stmt->execute([$id_company]);
            while ($row = $stmt->fetch()) {
                return $row['time_slot_appointment'];
            }
            return 30;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getCalendarByCompany($id_company,$time_slot_appointment,$timezone) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments a WHERE id_company = ? and state > 0 ");
            // and data_selezionata >= now();
            $stmt->execute([$id_company]);
            $str = "events: [";
            $flag = false;
            while ($row = $stmt->fetch()) {
                $flag = true;
                $data_selezionata= $row['data_selezionata'];
                $date = new DateTime($data_selezionata);

                if ($timezone!="") {
                  $date->modify('+' . $timezone . ' hours');
                }

                $datefine = new DateTime($date->format('d-m-Y H:i'));
                $datefine->modify('+' . $time_slot_appointment . ' minutes');
                $str .= "{";
                $str .= "title:'',";
                $str .= "start:'" . $date->format('Y-m-d') . "T". $date->format('H:i:') . "00',";
                $str .= "end:'" . $datefine->format('Y-m-d') . "T". $datefine->format('H:i:') . "00'";
                $str .= "},";
            }
            if ($flag) {
              return substr($str,0,-1) . "]";
            } else {
              return $str . "]";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getExpertByCompany($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT a.*, b.specialization, b.features, b.keyword_work, b.keywords_life, b.id_language FROM experts a, experts_details b WHERE a.id_company = ? and a.active = 0 and a.id_expert = b.id_expert ORDER BY RAND() LIMIT 1;");
            $stmt->execute([$id_company]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function insertCalendarExperts($id_company,$id_appointment,$id_expert) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO calendar_experts (id_company,id_appointment,id_expert) VALUES(?,?,?)");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_company,$id_appointment,$id_expert));
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getTokenAppointment($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT id_token FROM appointments WHERE id_appointment = ? ");
            $stmt->execute([$id_appointment]);
            while ($row = $stmt->fetch()) {
                return $row['id_token'];
            }
            return "";
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getLinkAppointment($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_link WHERE id_appointment = ?");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function is_number($var) {
        if ($var == (string) (float) $var) {
            return (bool) is_numeric($var);
        }
        if ($var >= 0 && is_string($var) && !is_float($var)) {
            return (bool) ctype_digit($var);
        }
        return (bool) is_numeric($var);
    }


    function getAppointment($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments WHERE id_appointment = ? ");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getExpert($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT b.* FROM salesmeet.calendar_experts a, salesmeet.experts b where a.id_appointment = ? and a.active = 0 and a.id_expert = b.id_expert");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getEmailTemplate($type,$language,$id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT description, subject FROM appointments_email_template WHERE type = ? and id_company = ? and language = ? ");
            $stmt->execute([$type,$id_company,$language]);
            while ($row = $stmt->fetch()) {
                return array("description" =>  $row['description'], "subject" =>  $row['subject']);
            }
            if ($id_company==0) {
                return "";
            } else {
                return $this->getEmailTemplate($type,$language,0);
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function createVideocall($id_expert,$id_company,$id_appointment,$data_selezionata_zoom) {

        // echo "createVideocall</br>";
        $urlVideoCall = "";
        if (!$this->isAppointmentVideocallSet($id_appointment)) {
            $video_call = $this->getTypeVideocall($id_expert,$id_company);
            // echo "type_video_call:" . $video_call["type_video_call"] . "</br>";
            if ($video_call["type_video_call"]=="zoom") {
                // echo "jwt_video_call:" . $video_call["jwt_video_call"] . "</br>";
                $zoom = new zoom();
                $iduser = $zoom->getIdUser($video_call["jwt_video_call"]);
                // echo "iduser:" . $iduser . "</br>";
                if ($iduser!="") {
                    // echo " - creo_videocall" . "</br>";
                    $urlVideoCall = $zoom->createMeeting($video_call["jwt_video_call"],$iduser,$data_selezionata_zoom);
                    // echo "urlVideoCall:" . $urlVideoCall . "</br>";
                    $this->updateAppointmentVideoCall($urlVideoCall,$urlVideoCall,$id_appointment);
                }
            }
        }
        return $urlVideoCall;
    }

    function updateAppointmentVideoCall($external_expert,$external_customer,$id_appointment) {
        if (!$this->is_number($price)) { $price = 0; }
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments_link set external_expert = ?, external_customer = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($external_expert,$external_customer,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function isAppointmentVideocallSet($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_link WHERE id_appointment = ? ");
            $stmt->execute([$id_appointment]);
            while ($row = $stmt->fetch()) {
                // echo "external_expert:" . $row['external_expert'] . "<br>";
                if ($row['external_expert']=="") {
                  return false;
                } else {
                  return true;
                }
            }
            return false;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function getTypeVideocall($id_expert,$id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM experts_videocall WHERE id_expert = ? and id_company =  ? ");
            $stmt->execute([$id_expert,$id_company]);
            while ($row = $stmt->fetch()) {
                return array("user_video_call" =>  $row['user_video_call'], "pass_video_call" =>  $row['pass_video_call'], "type_video_call" =>  $row['type_video_call'], "jwt_video_call" =>  $row['jwt_video_call']);
            }
            if ($id_company==0) {
                return array("user_video_call" => "");
            } else if ($id_expert==0) {
                return $this->getTypeVideocall(0,0);
            } else {
                return $this->getTypeVideocall(0,$id_company);
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function getEmailSetting($id_expert,$id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_email_setting WHERE id_expert = ? and id_company =  ? ");
            $stmt->execute([$id_expert,$id_company]);
            while ($row = $stmt->fetch()) {
                return array("action_customer_booking" =>  $row['action_customer_booking']);
            }
            if ($id_company==0) {
                return array("action_customer_booking" => "0");
            } else if ($id_expert==0) {
                return $this->getEmailSetting(0,0);
            } else {
                return $this->getEmailSetting(0,$id_company);
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function getLocation($id_company, $id_type) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM location where id_company = ? and (type = ? or type = 10) and active = 0 order by sequence, country, region, province,  city ");
            $stmt->execute([$id_company,$id_type]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getLocationById($id_location) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM location where id_location = ? and active = 0 ");
            $stmt->execute([$id_location]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function createAppointmentLocationCasa($id_appointment,$country,$province,$city,$street,$note,$postalcode) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments_location_acasa (id_appointment, country, province, city, street, note, postalcode) VALUES(?,?,?,?,?,?,?)");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array( $id_appointment,$country,$province,$city,$street,$note,$postalcode) );
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function getBusinessHours($id_expert,$id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM company_business_hours WHERE id_expert = ? and id_company =  ? and active = 0 ");
            $stmt->execute([$id_expert,$id_company]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            if (count($results)>0) {
                return $results;
            }
            /*
            while ($row = $stmt->fetch()) {
                return array("days_of_week" =>  $row['days_of_week'] ,"start_time" =>  $row['start_time'] ,"end_time" =>  $row['end_time']);
            }
            */
            if ($id_company==0) {
                return array();
            } else if ($id_expert==0) {
                return $this->getBusinessHours(0,0);
            } else {
                return $this->getBusinessHours(0,$id_company);
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function getCompanyCss($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM company_css WHERE id_company = ? and active = 0 ");
            $stmt->execute([$id_company]);
            while ($row = $stmt->fetch()) {
                return [$row['code'],$row['company_name_visible']];
            }
            return "";
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getCompanyLanguage($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM company_language WHERE id_company = ? and active = 0 ");
            $stmt->execute([$id_company]);
            while ($row = $stmt->fetch()) {
                return $row['language_list'];
            }
            return "";
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


}

?>
