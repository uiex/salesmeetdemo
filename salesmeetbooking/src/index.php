<?php include("setting.php"); ?>
<?php include("common/head.php"); ?>
<?php
/*
0 = Online
1 = in_store
2 = both
*/
if ( ($companyObj[0]["in_store_enable"]==1) || ($companyObj[0]["in_store_enable"]==2) || ($companyObj[0]["in_store_enable"]==3) ) {
    // solo online ?>

    <?php include("common/loading.php"); ?>
    <form id="form_invio" method="post" action="location.php">
        <input type="hidden" name="type_booking" id="type_booking" value="<?php echo $companyObj[0]["in_store_enable"]; ?>">
    </form>
    <script>
        document.getElementById("form_invio").submit();
    </script>
<?
    exit;
}
?>

  </head>
  <body style="">

    <?php include("common/language.php"); ?>

    <div id="div-left">
      <div id="div-left-body">
        <?php include("common/logo.php"); ?>
        <?php include("common/titolo.php"); ?>
        <?php include("common/prodotto.php"); ?>
        <div class="operazione"><?php echo $operatore_tipo_appuntamento; ?></div>

      </div>
    </div>
    <div id="div-right">
      <div id="div-right-body">

          <div class="booking_title">
            <?php echo $etichetta_tipo_appuntamento; ?>:
          </div>

          <?php $in_store_enable_val = strval( $companyObj[0]["in_store_enable"] ); ?>

          <?php if  ((strpos($in_store_enable_val, "1") !== false) || ($companyObj[0]["in_store_enable"]==0))  { ?>

              <form id="form_invio_0" method="post" action="location.php">
                  <input type="hidden" name="type_booking" id="type_booking" value="1">
              </form>
              <a class="booking_a" href="javascript:inviaIndex('form_invio_0','booking_online');">
                <div class="booking_online">
                  <div class="booking_online_img"></div>
                  <?php echo $etichetta_tipo_online; ?>
                </div>
              </a>

          <?php } ?>
          <?php if  ((strpos($in_store_enable_val, "2") !== false) || ($companyObj[0]["in_store_enable"]==0))  { ?>

              <form id="form_invio_1" method="post" action="location.php">
                  <input type="hidden" name="type_booking" id="type_booking" value="2">
              </form>
              <a class="booking_a" href="javascript:inviaIndex('form_invio_1','booking_in_store');">
                <div class="booking_in_store">
                  <div class="booking_in_store_img"></div>
                  <?php echo $etichetta_tipo_instore; ?>
                </div>
              </a>

          <?php } ?>
          <?php if  ((strpos($in_store_enable_val, "3") !== false) || ($companyObj[0]["in_store_enable"]==0))  { ?>

              <form id="form_invio_3" method="post" action="location.php">
                  <input type="hidden" name="type_booking" id="type_booking" value="3">
              </form>
              <a class="booking_a" href="javascript:inviaIndex('form_invio_3','booking_to');">
                <div class="booking_to">
                  <div class="booking_to_img"></div>
                  <?php echo $etichetta_tipo_acasa; ?>
                </div>
              </a>

          <?php } ?>

      </div>
    </div>


    <script>
        function inviaIndex(name_form,id_div) {
          $("." + id_div + " ." + id_div + "_img").css("background-size", "35px");
          $("." + id_div + " ." + id_div + "_img").css("background-repeat", "no-repeat");
          $("." + id_div + " ." + id_div + "_img").css("background-position", "center");
          $("." + id_div + " ." + id_div + "_img").css("background-image", "url(https://booking.salesmeet.it/asset/img/loading3.svg)");
          document.getElementById(name_form).submit();
        }
    </script>

<?php include("common/footer.php"); ?>
