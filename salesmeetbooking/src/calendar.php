<?php include("setting.php"); ?>
<?php include("common/head.php"); ?>

<?php
    $locate_tz = new DateTimeZone( $locationObjJson[0]["timezone_name"] );
    $locate = new DateTime('now', $locate_tz);
    $user_tz = new DateTimeZone( timezone_name_from_abbr("", 3600, 0) );
    $user = new DateTime('now', $user_tz);
    $usersTime = $user->format('H:i:s');
    $locatesTime = $locate->format('H:i:s');
?>
    <style>
      #nextstep {
          display: none;
      }
    </style>

    <script src="https://booking.salesmeet.it/asset/momentjs/moment.min.js"></script>
    <script>
        function inserimentoData(data, diff_hours) {
            var d = new Date(data);
            var data_timezone = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
            $('#data_selezionata_user_timezone').val(data_timezone);

            var newDate = moment(d);
            $('#data_selezionata_visual').html("<?php echo $operatore_data_selezionata . ': '; ?>" + newDate.format('lll'));

            d.setHours(d.getHours() + diff_hours);
            var data_original = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
            $('#data_selezionata').val(data_original);
            $('#nextstep').show();
            $('#orario').hide();
        }
        function deleteData( obj ) {
            // $('#data_selezionata_user_timezone').val("");
            $('#data_selezionata_visual').html("");
            // $('#data_selezionata').val("");
            $('#nextstep').hide();
            $('#orario').hide();
        }
    </script>
  </head>
  <body style="">

    <?php include("common/language.php"); ?>

    <div id="div-left">
      <div id="div-left-body">

          <?php include("common/logo.php"); ?>
          <?php include("common/prodotto.php"); ?>
          <!--<?php echo $operatore_disponibile_realtime; ?>-->

          <br>
          <form id="form_invio" method="post" action="prodotti.php">
              <input type="hidden" id="user_timezone" name="user_timezone" value="<?php echo timezone_name_from_abbr("", 3600, 0); ?>">
              <input type="hidden" id="data_selezionata_user_timezone" name="data_selezionata_user_timezone">
              <input type="hidden" id="data_selezionata" name="data_selezionata">
          </form>
          <b><?php echo $operatore_richiedi_appuntamento; ?></b><br><br>

          <div class="mobile-none">
            <?php if ($freecalendar=="") { ?>
              <?php echo $operatore_richiedi_appuntamento_proposta; ?>
              <a href="calendar.php?freecalendar=1"><?php echo $operatore_richiedi_appuntamento_proposta_click; ?></a>
              <br><br>
            <?php } else { ?>
              <?php echo $operatore_richiedi_appuntamento_proposta_free; ?>
              <br><br>
            <?php } ?>
          </div>

          <?php if ($usersTime != $locatesTime) { ?>

              <div id="orario" class="alert_all_page">

                  <?php
                          echo "<br><hr class='mobile-none'><br>";
                          echo "<div class='margine_div'>" . $etichetta_time_intro . "</div>";
                          echo "<br><br>";
                          echo $etichetta_time_utente_locale . " " . $usersTime;
                          echo "<br>";
                          echo $etichetta_time_esperto_locale . " " . $locatesTime;
                  ?>
                  <input class="close_all_page close_all_page_bottom" type="button" onclick='javascript:closeAlertPage();' value="<?php echo $etichetta_chiudi; ?>">
              </div>

          <?php } ?>

          <div id="data_selezionata_visual" name="data_selezionata_visual"><br></div>
          <div id="nextstep">
              <div class="operatore_procedi"><!--<?php echo $operatore_procedi; ?><br><br>--></div>
              <input class="invia" type="button" onclick='document.getElementById("form_invio").submit();' value="<?php echo $operatore_step_successivo; ?>">
          </div>
      </div>
    </div>
    <div id="div-right">
        <div id="div-right-body">

            <div class="booking_title">
              <?php echo $operatore_scelta_data_appuntamento; ?>:
            </div>

            <div id='calendar'></div>
            <?php include("common/calendar.php"); ?>
        </div>
    </div>

<?php include("common/footer.php"); ?>
