<?php include("setting.php"); ?>
<?php include("common/head.php"); ?>

    <script src="asset/js/uiex.js"></script>
    <link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
    <script src="asset/datatables/jquery.dataTables.min.js"></script>
  </head>
  <body style="">

    <?php include("common/language.php"); ?>

    <div id="div-left">
      <div id="div-left-body">
          <?php include("common/logo.php"); ?>
          <?php include("common/prodotto.php"); ?>
          <b><?php echo $operatore_seleziona_prodotti; ?></b>
          <div class="separatore"></div>
          <form id="form_invio" method="post" action="expert.php">
              <input class="invia" type="button" onclick='document.getElementById("form_invio").submit();' value="<?php echo $operatore_step_successivo; ?>">
          </form>
      </div>
    </div>
    <div id="div-right">
        <div id="div-right-body">

          <div class="booking_title">
            <?php echo $operatore_altro_prodotto; ?>:
          </div>
          <div class="contenuto">
              <table id="preferiti" class="display" style="width:100%">
                  <thead>
                      <tr>
                          <th><?php echo $etichetta_uiex_select; ?></th>
                          <th><?php echo $etichetta_uiex_image; ?></th>
                          <th><?php echo $etichetta_uiex_title; ?></th>
                          <!-- DA RIPRISTINARE -->
                          <!--th class="mobile-none" style="display: none;"><?php echo $etichetta_uiex_price; ?></th>
                          <th class="mobile-none" style="display: none;"><?php echo $etichetta_uiex_currency; ?></th-->
                          <th class="mobile-none"><?php echo $etichetta_uiex_vote; ?></th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th><?php echo $etichetta_uiex_select; ?></th>
                          <th><?php echo $etichetta_uiex_image; ?></th>
                          <th><?php echo $etichetta_uiex_title; ?></th>
                          <!-- DA RIPRISTINARE -->
                          <!--th class="mobile-none" style="display: none;"><?php echo $etichetta_uiex_price; ?></th>
                          <th class="mobile-none" style="display: none;"><?php echo $etichetta_uiex_currency; ?></th-->
                          <th class="mobile-none"><?php echo $etichetta_uiex_vote; ?></th>
                      </tr>
                  </tfoot>
              </table>
          </div>
          <script>
              function insertProdotto(url,img,title) {
                  var oReq = new XMLHttpRequest();
                  oReq.onload = function(e) {
                      var arraybuffer = oReq.response;
                  }
                  oReq.open("POST", "https://booking.salesmeet.it/common/prodottoop.php");
                  var formData = new FormData();
                  formData.append("url",url);
                  formData.append("img",img);
                  formData.append("title",title);
                  formData.append("id_appointment",<?php echo $id_appointment; ?>);
                  oReq.send( formData );
              }
              getPreferitiDTable("<?php echo $token; ?>","<?php echo $user; ?>","<?php echo $site; ?>","<?php echo $_SESSION["urlcaller"]; ?>");
              getPreferiti("<?php echo $token; ?>","<?php echo $user; ?>","<?php echo $site; ?>");
          </script>
        </div>
    </div>

<?php include("common/footer.php"); ?>
