<?php include("setting.php"); ?>
<?php include("common/head.php"); ?>
<?php
/*
0 = Online
1 = in_store
2 = both
*/

$location = $db->getLocation($id_company,$_SESSION["type_booking"]);
$location_array = json_decode($location, true);
if ($_SESSION["type_booking"]==1) {
    if (count($location_array)==1) { ?>
      <form id="form_invio_automatico" method="post" action="calendar.php">
          <input type="hidden" id="id_location" name="id_location" value="<?php echo $location_array[0]['id_location']; ?>">
      </form>
      <?php include("common/loading.php"); ?>
      <script>
        document.getElementById("form_invio_automatico").submit();
      </script>
      <?
      exit;
    }
}

$n_location = count($location_array);
$id_location = 0;
if ($n_location==1) {
   $id_location = $location_array[0]['id_location'];
}
?>

    <style>
      #nextstep, #nextstepdate, #timezone_utente_scelto_view {
          display: none;
      }
    </style>
    <link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
    <script src="asset/datatables/jquery.dataTables.min.js"></script>
  </head>
  <body style="">
    <div id="div-left">

      <?php include("common/language.php"); ?>

      <div id="div-left-body">

        <?php include("common/logo.php"); ?>
        <!-- ?php include("common/titolo.php"); ?-->
        <?php include("common/prodotto.php"); ?>

        <?php if ($_SESSION["type_booking"]==1) { $etichetta_temp = $operatore_richiedi_location_timezone; }
              elseif ($_SESSION["type_booking"]==2) { $etichetta_temp = $operatore_richiedi_location; }
              elseif ($_SESSION["type_booking"]==3) { $etichetta_temp = $operatore_richiedi_location_timezone; } ?>
        <b><?php echo $etichetta_temp; ?></b><br><br>

        <br>
        <div id="timezone_utente"></div>

        <script src="https://booking.salesmeet.it/asset/momentjs/moment.min.js"></script>
        <script src="https://booking.salesmeet.it/asset/momentjs/moment-timezone-with-data.min.js"></script>

        <script>
            var jun = moment();
            <?php if ($_SESSION["type_booking"]==1) { ?>
            document.getElementById("timezone_utente").innerHTML = "<?php echo $etichetta_time_utente_locale_indicazioni; ?> " + jun.format('h:mm:ss a');
            <?php } ?>
            /*
            console.log("----casa----");
            console.log(jun.format());
            console.log(jun.utcOffset() / 60);
            console.log("----LA----");
            console.log(jun.tz('America/Los_Angeles').format());
            console.log(jun.tz('America/Los_Angeles').utcOffset() / 60);
            console.log("----London----");
            console.log(jun.tz('Europe/London').format());
            console.log(jun.tz('Europe/London').utcOffset() / 60);
            console.log("----tokio----");
            console.log(jun.tz('Asia/Tokyo').format());
            console.log(jun.tz('Asia/Tokyo').format("ha - MMM Do YY"));
            console.log(jun.tz('Asia/Tokyo').utcOffset() / 60);
            */
        </script>
        <div id="nextstep">
            <div class="operatore_procedi"><!--<?php echo $operatore_procedi; ?><br><br>--></div>
            <div id="timezone_utente_scelto_view" class="alert_all_page">
              <div id="timezone_utente_scelto"></div>
              <input class="close_all_page" type="button" onclick='javascript:closeAlertPage();' value="<?php echo $etichetta_chiudi; ?>">
            </div>

            <input class="invia" type="button" onclick='document.getElementById("form_invio").submit();' value="<?php echo $operatore_step_successivo; ?>">
        </div>

      </div>
    </div>
    <div id="div-right">
      <div id="div-right-body">

        <form id="form_invio" method="post" action="calendar.php">
            <input type="hidden" id="id_location" name="id_location" value="<?php echo $id_location; ?>">
            <div id="nextstepdate">
                <div id="esperto">
                  <?php echo $etichetta_location_acasa_titolo; ?> <br><br>
                  <input placeholder="<?php echo $etichetta_location_acasa_country; ?>" class="input_text" type="input" id="country" name="country"><br><br>
                  <input placeholder="<?php echo $etichetta_location_acasa_province; ?>" class="input_text" type="input" id="province" name="province"><br><br>
                  <input placeholder="<?php echo $etichetta_location_acasa_city; ?>" class="input_text" type="input" id="city" name="city"><br><br>
                  <input placeholder="<?php echo $etichetta_location_acasa_street; ?>" class="input_text" type="input" id="street" name="street"><br><br>
                  <input placeholder="<?php echo $etichetta_location_acasa_postal_Code; ?>" class="input_text" type="input" id="postalcode" name="postalcode"><br><br>
                  <textarea style="width: 100% !important; height: 60px !important;" placeholder="<?php echo $etichetta_location_acasa_note; ?>" class="input_text" id="note" name="note"></textarea><br><br>
                  <div class="operatore_procedi"><!--<?php echo $operatore_procedi; ?><br><br>--></div>

                  <!--input type="button" onclick='hiddenAcasa();' value="<?php echo $operatore_step_back; ?>"-->
                  <input class="invia" type="button" onclick='insertLocationAcasa();'  value="<?php echo $operatore_step_successivo; ?>">
                </div>
            </div>
        </form>


        <?php  if (($n_location==1) && ($_SESSION["type_booking"]==3)) { ?>

          <script>
            $('#nextstepdate').show();
            $('#location_table').hide();
          </script>

        <?php
        } else {
        ?>

            <div class="contenuto" id="location_table">


                <div class="booking_title"> <!-- titolo_appuntamento -->
                  <?php $flaglink = false; ?>
                  <?php if ($appointmentObjJson[0]["in_store_enable"]==1) { ?>
                    <?php echo $etichetta_tipo_appuntamento; ?>: <b><?php echo $etichetta_tipo_online; ?></b>
                  <?php } elseif ($appointmentObjJson[0]["in_store_enable"]==2) { ?>
                    <?php echo $etichetta_tipo_appuntamento; ?>: <b><?php echo $etichetta_tipo_instore; ?></b>
                  <?php } elseif ($appointmentObjJson[0]["in_store_enable"]==3) { ?>
                    <?php echo $etichetta_tipo_appuntamento; ?>: <b><?php echo $etichetta_tipo_acasa; ?></b>
                  <?php } ?>
                  <hr>
                </div>

                <table id="preferiti" class="display" style="width:100%">
                    <thead>
                        <tr>
                          <th><?php echo $etichetta_uiex_select; ?></th>
                          <th><?php echo $etichetta_location_country; ?></th>
                          <?php if ($_SESSION["type_booking"]==1) { ?>
                              <th><?php echo $etichetta_location_time; ?></th>
                          <?php } ?>
                          <?php if ($_SESSION["type_booking"]>=2) { ?>
                              <th><?php echo $etichetta_location_city; ?></th>
                              <th><?php echo $etichetta_location_region; ?></th>
                              <th><?php echo $etichetta_location_province; ?></th>
                              <th><?php echo $etichetta_location_street; ?></th>
                          <?php } ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $etichetta_uiex_select; ?></th>
                            <th><?php echo $etichetta_location_country; ?></th>
                            <?php if ($_SESSION["type_booking"]==1) { ?>
                                <th><?php echo $etichetta_location_time; ?></th>
                            <?php } ?>
                            <?php if ($_SESSION["type_booking"]>=2) { ?>
                                <th><?php echo $etichetta_location_city; ?></th>
                                <th><?php echo $etichetta_location_region; ?></th>
                                <th><?php echo $etichetta_location_province; ?></th>
                                <th><?php echo $etichetta_location_street; ?></th>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>

        <?php  } ?>

        <script>
          function insertLocationAcasa() {
              document.getElementById("form_invio").submit();
          }
          function hiddenAcasa() {
              $('#location_table').show();
              $('#nextstepdate').hide();
          }

          function insertLocation( id,country, city, timezone_name ) {
              document.getElementById("id_location").value = id;

              var junnew = moment();
              var offset_default = junnew.utcOffset() / 60;
              var orario_default = document.getElementById("timezone_utente").innerHTML;
              var offset = junnew.tz(timezone_name).utcOffset() / 60;
              var orario_nuovo = junnew.tz(timezone_name).format('h:mm:ss a');
              if (offset_default != offset) {
                document.getElementById("timezone_utente_scelto").innerHTML = "<?php echo $etichetta_time_utente_timezone_differenza; ?>" + (offset_default - offset);
                // $('#timezone_utente_scelto_view').show();
              } else {
                document.getElementById("timezone_utente_scelto").innerHTML = "";
                // $('#timezone_utente_scelto_view').hide();
              }


              <?php if ($_SESSION["type_booking"]==3) {  ?>
                  $('#nextstepdate').show();
                  $('#location_table').hide();
              <?php } else { ?>
                  $('#nextstep').show();
              <?php } ?>

          }
          function createInput( value, type, row) {
              var input = `<input onchange="insertLocation('${value}','${row.country}','${row.city}','${row.timezone_name}');" type="radio" id="location" name="location">`;
              return input;
          }
          function fusoOrario( value, type, row) {
              var junnew = moment();
              return junnew.tz(value).format('h:mm:ss a');
          }
          function loadAppointments() {
              $('#preferiti').DataTable( {
                  "data" : <?php echo $location; ?>,
                  "columns": [
                      { "data": "id_location",
                          "render": function (data, type, row) {
                              return createInput( data, type, row);
                          }
                      },
                      { "data": "country"},
                      <?php if ($_SESSION["type_booking"]==1) { ?>
                        { "data": "timezone_name",
                            "render": function (data, type, row) {
                                return fusoOrario( data, type, row);
                            }
                        },
                      <?php } ?>
                      <?php if ($_SESSION["type_booking"]>=2) { ?>
                        { "data": "city"},
                        { "data": "region"},
                        { "data": "province"},
                        { "data": "street"}
                      <?php } ?>
                  ]
              } );
          }
          loadAppointments();
        </script>
      </div>
    </div>

<?php include("common/footer.php"); ?>
