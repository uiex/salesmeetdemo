<?php
error_reporting(E_ERROR);

$newid = "aaaaaa";
session_id($newid);
session_start();
// echo $session_id . "<br>";

// Class per gestione ed inserimento appuntamento nel DB
include("db/db.php");
$db = new db();

// Raccolta informazioni sul chiamante
$id_company = "";
$site = "";
$referer = "";
$new_appointment = false;

if ( $_SERVER['HTTP_REFERER'] != "" ) {

  if ( parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER['HTTP_HOST'] ) {

      //
      $referer = $_SERVER['HTTP_REFERER'];
      $_SESSION["referer"] = $referer;
      $site = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
      $_SESSION["site"] = $site;
      $new_appointment = true;
      // con l'iframe si è dovuti chiamare questa ...
      $referer = $_POST['urlcaller'];

      $_SESSION["urlcaller"] = $_POST['urlcaller'];

      $id_company = $db->getIdCompany($site);
      $_SESSION["id_company"] = $id_company;

      $_SESSION["insertCalendarExperts"] = "";
      $_SESSION["nome"] = "";
      $_SESSION["freecalendar"] = "";
      $_SESSION["data_selezionata"] = "";
      $_SESSION["cognome"] = "";
      $_SESSION["telefono"] = "";
      $_SESSION["email"] = "";
      $_SESSION["favourite_language"] = "";

      /*
      setcookie("salesmeet_site", $site);
      setcookie("salesmeet_referer", $referer);
      setcookie("salesmeet_id_company", $id_company);
      */

  } else {

      /*
      $site = $_COOKIE["salesmeet_site"];
      $referer = $_COOKIE["salesmeet_referer"];
      $id_company = $_COOKIE["salesmeet_id_company"];
      */

      $site = $_SESSION["site"];
      $referer = $_SESSION["referer"];
      $id_company = $_SESSION["id_company"];

  }
}

// echo "urlcaller: " . $_SESSION["urlcaller"]; 

// Chiamante non esistente
if ($id_company==0) {

  echo "Invalid appointment. Error 1001.";
  exit;

} else {

  $company = $db->getCompany($id_company);
  $companyObj = json_decode($company, true);
  $logo_azienda = $companyObj[0]["logo"];
  $nome_azienda = $companyObj[0]["name"];
  $abilitata = true;

}


//********************************************************

include("common/info.php");
$info = new info();

$title = $_POST['title'];
$img = $_POST['img'];

if ($new_appointment) {
  $id_appointment = $db->createAppointment($site,$referer,$id_company,$info->getIP(),$info->getSystemInfo()["os"],$info->getBrowser(),$price);
  $db->isProductPresent($referer,$img,$title,$id_appointment);
  $_SESSION["id_appointment"] = $id_appointment;
} else {
  $id_appointment = $_SESSION["id_appointment"];
}


// Raccolta dati form chiamato dalla pagina dove è installato il plugin ################################
$token = "";
$user = "";
$language = "";
$title = "";
$img = "";
$price = 0;
$new_token = false;
// collect value of input field
$token = $_POST['token'];
if (empty($token)) {
    $token = $_SESSION["token"];
} else {
    $_SESSION["token"] = $token;
    $new_token = true;
}
$user = $_POST['user'];
if (empty($user)) {
    $user = $_SESSION["user"];
} else {
    $_SESSION["user"] = $user;
}
$language = $_POST['language'];
if (empty($language)) {
    $language = $_SESSION["language"];
} else {
    $_SESSION["language"] = $language;
}


$favourite_language = $_POST['favourite_language'];
if (empty($favourite_language)) {
    $favourite_language = $_SESSION["favourite_language"];
    if ($favourite_language!="") {
      if ($favourite_language=="it") { $favourite_language = "en"; }
      $language = $favourite_language;
    } else {
      $favourite_language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
      if ($favourite_language=="it") { $favourite_language = "en"; }
      $_SESSION["favourite_language"] = $favourite_language;
      $db->updateAppointmentLanguage($favourite_language,$id_appointment);
      $language = $favourite_language;
    }
} else {
    if ($favourite_language=="it") { $favourite_language = "en"; }
    $_SESSION["favourite_language"] = $favourite_language;
    $db->updateAppointmentLanguage($favourite_language,$id_appointment);
    $language = $favourite_language;
}

include("language/init.php");



$title = $_POST['title'];
if (empty($title)) {
    $title = $_SESSION["title"];
} else {
    $_SESSION["title"] = $title;
}
$img = $_POST['img'];
if (empty($img)) {
    $img = $_SESSION["img"];
} else {
    $_SESSION["img"] = $img;
}
$price = $_POST['price'];
if (empty($price)) {
    $price = $_SESSION["price"];
} else {
    $_SESSION["price"] = $price;
}

// aggiorno i dati dell'appuntamento con quelli mandati da Uiex
if ($new_token) {
  $db->updateAppointmentUiex($token,$user,$language,$title,$img,$price,$id_appointment);
}


$type_booking = $_POST['type_booking'];
if (empty($type_booking)) {
    /*if ($type_booking=="0") {
      $_SESSION["type_booking"] = $type_booking;
      $db->updateAppointmentTypeBooking($type_booking,$id_appointment);
    } else {
    */
      $type_booking = $_SESSION["type_booking"];
    // }
} else {
    $_SESSION["type_booking"] = $type_booking;
    $db->updateAppointmentTypeBooking($type_booking,$id_appointment);
}

$id_location= $_POST['id_location'];
if (empty($id_location)) {
    /*if ($id_location==0) {
      echo "Update location: " . $id_location;
      $_SESSION["id_location"] = $id_location;
      $db->updateAppointmentLocation($_SESSION["id_location"],$id_appointment);
    } else { */
      $id_location = $_SESSION["id_location"];
    // }
} else {
    $_SESSION["id_location"] = $id_location;
    $db->updateAppointmentLocation($_SESSION["id_location"],$id_appointment);
}


// Scelta della data
$data_selezionata = "";
$new_data_selezionata = false;
$data_selezionata = $_POST['data_selezionata'];
if (empty($data_selezionata)) {
    $data_selezionata = $_SESSION["data_selezionata"];
} else {
    $_SESSION["data_selezionata"] = $data_selezionata;
    $new_data_selezionata = true;
}

$date = new DateTime($data_selezionata);
$data_selezionata = $date->format('d-m-Y H:i');
$data_selezionata_zoom = $date->format('Y-m-d H:i:00');


$freecalendar = 0;
if (isset($_GET['freecalendar'])) {
    $freecalendar = $_GET['freecalendar'];
    $_SESSION["freecalendar"] = $freecalendar;
} else {
    $freecalendar = $_SESSION["freecalendar"];
}

if ($new_data_selezionata) {

  $data_selezionata_user_timezone = $_POST['data_selezionata_user_timezone'];
  $date_timezone = new DateTime($data_selezionata_user_timezone);
  $data_selezionata_timezone = $date_timezone->format('d-m-Y H:i');

  $user_timezone = $_POST['user_timezone'];

  $db->updateAppointmentStepDataSelezionata($data_selezionata,$data_selezionata_timezone,$user_timezone,$freecalendar,$id_appointment);
}

// Inserimento email
$email = "";
$new_email = false;
$email = $_POST['email'];
if (empty($email)) {
    $email = $_SESSION["email"];
} else {
    $_SESSION["email"] = $email;
    $new_email = true;
}
$description = $_POST['description'];
if (empty($description)) {
    $description = $_SESSION["description"];
} else {
    $_SESSION["description"] = $description;
}

if ($new_email) {
  $db->updateAppointmentStepEmail($email,$description,$id_appointment);
}


// Inserimento dati opzionali
$new_nome = false;
$nome = $_POST['nome'];
if (empty($nome)) {
    $nome = $_SESSION["nome"];
} else {
    $_SESSION["nome"] = $nome;
    $new_nome = true;
}
$cognome = $_POST['cognome'];
if (empty($cognome)) {
    $cognome = $_SESSION["cognome"];
} else {
    $_SESSION["cognome"] = $cognome;
}
$telefono = $_POST['telefono'];
if (empty($telefono)) {
    $telefono = $_SESSION["telefono"];
} else {
    $_SESSION["telefono"] = $telefono;
}

if ($new_nome) {
  $db->updateAppointmentStepOpzionali($nome,$cognome,$telefono,$id_appointment);
}


$laststep = $_POST['laststep'];
if (!empty($laststep)) {
  $db->updateAppointmentStepLast($id_appointment);
  $id_expert= $_POST['id_expert'];
  if ($_SESSION["insertCalendarExperts"]=="") {
      $db->insertCalendarExperts($id_company,$id_appointment,$id_expert);
      $_SESSION["insertCalendarExperts"] = "si";
  }
}



// Inserimento dati nelle selezione di appuntamento a casa
$new_acasa = false;
$country = $_POST['country'];
if (empty($country)) {
    $country = $_SESSION["country"];
} else {
    $_SESSION["country"] = $country;
    $new_acasa = true;
}
$province = $_POST['province'];
if (empty($province)) {
    $province = $_SESSION["province"];
} else {
    $_SESSION["province"] = $province;
}
$city = $_POST['city'];
if (empty($city)) {
    $city = $_SESSION["city"];
} else {
    $_SESSION["city"] = $city;
}
$street = $_POST['street'];
if (empty($street)) {
    $street = $_SESSION["street"];
} else {
    $_SESSION["street"] = $street;
}

$postalcode = $_POST['postalcode'];
if (empty($postalcode)) {
    $postalcode = $_SESSION["postalcode"];
} else {
    $_SESSION["postalcode"] = $postalcode;
}
$note = $_POST['note'];
if (empty($note)) {
    $note = $_SESSION["note"];
} else {
    $_SESSION["note"] = $note;
}

if ($new_acasa) {
  $db->createAppointmentLocationCasa($id_appointment,$country,$province,$city,$street,$note,$postalcode);
}

// Location
$appointmentObj = $db->getAppointment($id_appointment);
$appointmentObjJson = json_decode($appointmentObj, true);
$locationObj = $db->getLocationById($appointmentObjJson[0]["id_location"]);
$locationObjJson = json_decode($locationObj, true);

// CSS
$company_css = $db->getCompanyCss($id_company);

// Utilizzato in logo.php per fare apparire il menu mobile di navigazione.
// utilizzato in dati.php per abilitare se il riepilogo navigazione è cliccabile
$pagina_back = "";
$menu_scelta = "si";
if ($_SERVER["SCRIPT_NAME"]=="/location.php") {
    $pagina_back = "index.php";
} elseif ($_SERVER["SCRIPT_NAME"]=="/calendar.php") {
    $pagina_back = "location.php";
} elseif ($_SERVER["SCRIPT_NAME"]=="/prodotti.php") {
    $pagina_back = "calendar.php";
} elseif ($_SERVER["SCRIPT_NAME"]=="/expert.php") {
    $pagina_back = "prodotti.php";
} elseif ($_SERVER["SCRIPT_NAME"]=="/recap.php") {
      $pagina_back = "";
      $menu_scelta = "";
}

?>
