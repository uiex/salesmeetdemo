<?
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');

$site = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$url = $_SERVER['HTTP_REFERER'];

if ($url=="") {
    echo "Non autorizzato - 1";
    exit;
}

include("db.php");
$db = new db();
$companyInfo = $db->getCompanyInfo($site);
if (count($companyInfo)>0) {
    //print_r($companyInfo);
    echo(json_encode($companyInfo));
}

?>
