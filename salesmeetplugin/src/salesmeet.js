class SalesMeetApp {

    constructor() {

        this.domain = "salesmeet.it";
        this.radice = "SalesMeetApp";
        this.hostname = window.location.hostname;
        this.href = window.location.href;
        this.token = "";
        this.bottonSetting;

        this.w = window.innerWidth;
        this.h = window.innerHeight;
        this.mobile = false;
        if ( (this.w < 600 ) || (this.h < 600 ) ) {
            this.mobile = true;
        }
        this.settingContext();

    }
    init() {
        if (this.isSiteNotIndex(this.hostname)) {
            return;
        }
        this.getInfoCompany();
    }
    isSiteRegistered() {
        console.log("isSiteRegistered");
        // importo il tutto se sono in video call
        this.token = this.getParameterByName("salesmeettoken",window.location.href);
        // attivo UIEX sempre
        this.addScript("https://uiexfrontend.salesmeet.it/service/default.js","uiexfrontend");
        // AGGIUNGE Bottone al sito
        if (SalesMeetAppInstance.button_automatic == "0") {
            console.log("button_automatic");
            SalesMeetAppInstance.addCTA();
        }
    }

    // ######################################################################################################
    // ################################## COMMON #############################################
    // ######################################################################################################
    isEmpty(value){
      return (value == null || value.length === 0);
    }
    isSiteNotIndex(url) {
        if (url.indexOf(this.domain)>0) {
            return true;
        }
        return false;
    }
    addScript(url) {
        var imported = document.createElement('script', name);
        imported.src = url;
        imported.rel = 'import';
        imported.id = name;
        document.head.appendChild(imported);
    }
    addCookie(cname, cvalue) {
        localStorage.setItem(cname, cvalue);
    }
    getCookie(cname) {
        var temp = localStorage.getItem(cname);
        if (temp==null) {
            return "";
        } else {
            return temp;
        }
    }
    getParameterByName(name,url) {
        // var url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }


    // ######################################################################################################
    // ################################## INJECTION PAGE #############################################
    // ######################################################################################################

    // getInfoCompany(infouser, step) {
    getInfoCompany() {
          console.log("_____getInfoCompany_______");
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              console.log("2 arraybuffer: " + arraybuffer);
              if (arraybuffer!="") {
                  var jsonData = JSON.parse(arraybuffer);
                  SalesMeetAppInstance.bottonSetting = jsonData;
                  SalesMeetAppInstance.isSiteRegistered();
              } else {
                  console.log("sito non censito.");
              }
          }
          oReq.open("POST", "https://plugin.salesmeet.it/api/info.php");
          var formData = new FormData();
          formData.append("referer", this.href );
          oReq.send( formData );

    }

    addCTA() {

          console.log("addCTA");
          var first = document.createElement("a");
          first.href = "javascript:SalesMeetAppInstance.startSalesmeet();"
          //first.href = "javascript:SalesMeetAppInstance.getInfoCompany();"
          first.style = "margin-top: 10px; background-color: #33a531; padding: 20px; width: 100%; margin-bottom: 10px;color: #fff; font-size: 20px;text-align: center;"
          // var text = document.createTextNode(this.button_text);
          first.innerHTML = this.button_text;
          // first.appendChild(text);
          var temp = 0;
          /* DA TOGLIERE */
          SalesMeetAppInstance.addCTADefault();
          /* DA RIPRISTINARE
          Object.entries(SalesMeetAppInstance.bottonSetting).forEach(([key, value]) => {
              if ( (value["all_site"]==0) && (temp==0) ) {
                    var css = document.getElementsByClassName( value["CSS_TAG"] );
                    if (css.length > 0) {
                          console.log("*******Custom-ok");
                          temp = 1;
                          if (value["CSS"]!="") {
                              first.style = value["CSS"];
                          }
                          document.getElementsByClassName(value["CSS_TAG"])[0].appendChild(first);
                          return;
                    }
              } else if ((value["all_site"]==1 ) && (temp==0) )   {
                    console.log("*******All");
                    SalesMeetAppInstance.addCTADefault();
              }
          });
          */


    }
    addCTADefault() {

          console.log("addCTADefault");
          var elemDiv = document.createElement('div');
          elemDiv.setAttribute("id", this.radice + "Button");
          elemDiv.style.cssText = 'position:fixed;bottom:30px;left:0px;width:auto;height:auto;\
                                   padding:10px;z-index:99999;background:' + this.button_color + ';\
                                   border:1px solid ' + this.button_color_border + ';border-left:0px solid;';
          elemDiv.innerHTML = "<a style='color:" + this.button_label_color + ";' id='" + this.radice + "seopen' href='javascript:void(0);'>" + this.button_text + "</a>";

          document.body.appendChild(elemDiv);
          var el = document.getElementById( this.radice + 'Button');
          el.onclick = function (e) {
              SalesMeetAppInstance.startSalesmeet();
          }

    }

    // ######################################################################################################
    // ################################## AL CLICK DI SALESMEET #############################################
    // ######################################################################################################

    getExpert() {
          try {
              var formData = new FormData();
              formData.append("token", userIntelligentExperienceAppInstance.token );
              var oReq = new XMLHttpRequest();
              oReq.onload = function(e) {
                  var arraybuffer = oReq.response;
                  console.log("3 arraybuffer: " + arraybuffer);
              }
              oReq.open("POST", "https://expert.salesmeet.it/api/sync.php");
              formData.append("referer", this.href );
              oReq.send( formData );
          } catch(err) {
              console.log("erroooooorreee");
          }

    }

    // Funzione apertura processo di booking o chatbot o ...
    startSalesmeet() {
        // this.getExpert();
        this.getInfoUser("start");
    }
    // GET DATI UTENTE
    getInfoUser(step) {
          try {
              var formData = new FormData();
              formData.append("token", userIntelligentExperienceAppInstance.token );
              var oReq = new XMLHttpRequest();
              oReq.onload = function(e) {
                  var arraybuffer = oReq.response;
                  var jsonData = JSON.parse(arraybuffer);
                  SalesMeetAppInstance.getInfoProfile( jsonData);
              }
              oReq.open("POST", "https://uiextools.salesmeet.it/useranalytics/");
              oReq.send( formData );
          } catch(err) {
              setTimeout(function(){ SalesMeetAppInstance.getInfoUser(step); }, 100);
          }

    }
    getInfoProfile(infouser) {
          // console.log("_____getInfoProfile_______");
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              console.log("1 arraybuffer: " + arraybuffer);
              var n = arraybuffer.indexOf("booking");
              /* DA TOGLIERE */
              SalesMeetAppInstance.addForm();
              /* DA RIPRISTINARE
              if (n==0) {
                  // apre il processo di booking
                  SalesMeetAppInstance.addForm();
              } else {
                  // apre altro proprietario del sito processo del sito (chatbot, contact form ...)
                  // console.log("JS: " + arraybuffer);
                  eval(arraybuffer);
              }
              */
          }
          // console.log("price: " + userIntelligentExperienceAppInstance.price);
          oReq.open("POST", "https://profile.salesmeet.it/api/info.php");
          var formData = new FormData();
          // formData.append("token", userIntelligentExperienceAppInstance.token );
          // formData.append("infouser", infouser );
          formData.append("count", infouser[0]["Count"] );
          formData.append("voto", infouser[0]["Voto"] );
          formData.append("votopagina", infouser[0]["Votopagina"] );
          formData.append("prezzopagina", userIntelligentExperienceAppInstance.price );
          formData.append("price", infouser[0]["Price"] );
          // formData.append("pricemax", infouser[0]["Pricemax"] );
          // formData.append("pricemin", infouser[0]["Pricemin"] );
          // formData.append("step", step );
          formData.append("referer", this.href );
          oReq.send( formData );
    }
    // apre il processo di booking
    addForm() {

          // Update dei dati della pagina per UIEx
          userIntelligentExperienceAppInstance.update();

          // Creazione form e submit dello stesso con apertura di una nuvoa scheda
          // Si vanno a recuperare i dati significativi da UIEx per darli in pasto
          var mapForm = document.createElement("form");
          mapForm.method = "POST"; // or "post" if appropriate
          mapForm.action = "https://booking.salesmeet.it";
          // mapForm.target = "_blank";
          mapForm.target = "salesmeet_iframe";
          mapForm.id = "salesmeetform";
          mapForm.name = "salesmeetform";

          var mapInputlanguage = document.createElement("input");
          mapInputlanguage.type = "text";
          mapInputlanguage.name = "language";
          mapInputlanguage.id = "language";
          mapInputlanguage.value = userIntelligentExperienceAppInstance.language;

          var mapInputtitle = document.createElement("input");
          mapInputtitle.type = "text";
          mapInputtitle.name = "title";
          mapInputtitle.id = "title";
          mapInputtitle.value = userIntelligentExperienceAppInstance.title;

          var mapInputPrice = document.createElement("input");
          mapInputPrice.type = "text";
          mapInputPrice.name = "price";
          mapInputPrice.id = "price";
          mapInputPrice.value = userIntelligentExperienceAppInstance.price;

          var mapInputimg = document.createElement("input");
          mapInputimg.type = "text";
          mapInputimg.name = "img";
          mapInputimg.id = "img";
          mapInputimg.value = userIntelligentExperienceAppInstance.img;

          var mapInputtoken = document.createElement("input");
          mapInputtoken.type = "text";
          mapInputtoken.name = "token";
          mapInputtoken.id = "token";
          mapInputtoken.value = userIntelligentExperienceAppInstance.token;

          var mapInputuser = document.createElement("input");
          mapInputuser.type = "text";
          mapInputuser.name = "user";
          mapInputuser.id = "user";
          mapInputuser.value = userIntelligentExperienceAppInstance.user;

          var mapInputUrl = document.createElement("input");
          mapInputUrl.type = "text";
          mapInputUrl.name = "urlcaller";
          mapInputUrl.id = "urlcaller";
          mapInputUrl.value = window.location.href;

          mapForm.appendChild(mapInputlanguage);
          mapForm.appendChild(mapInputtitle);
          mapForm.appendChild(mapInputPrice);
          mapForm.appendChild(mapInputimg);
          mapForm.appendChild(mapInputtoken);
          mapForm.appendChild(mapInputuser);
          mapForm.appendChild(mapInputUrl);

          document.body.appendChild(mapForm);

          // DIV MENU
          var mapDiv = document.createElement('div');
          mapDiv.style.cssText = 'position:fixed;top:0px;width:100%;height:100%;z-index:9999999999;background:rgb(0 0 0 / 80%);';
          mapDiv.id = "salesmeetdiv";

          var mapIframe = document.createElement("iframe");
          mapIframe.name = "salesmeet_iframe";
          mapIframe.id = "salesmeet_iframe";

          var dimensione = "width:95%;height:92%;top: 4%; ";
          if (this.mobile) {
               dimensione = "width:100%;height:100%;top: 0%; ";
          }
          mapIframe.style.cssText = dimensione + 'background:rgb(255 255 255 / 100%);background-repeat:no-repeat;background-position:center;background-image:url(https://booking.salesmeet.it/asset/img/loading3.svg); margin: 0 auto; display: flex;position: relative;';
          // mapIframe.style.cssText = 'width:100%;height:100%;';

          mapDiv.appendChild(mapIframe);
          document.body.appendChild(mapDiv);
          mapForm.submit();
          var el = document.getElementById('salesmeetform');
          el.remove();

      }

      closeSalesmeet(){
        var el = document.getElementById('salesmeetdiv');
        el.remove();
      }

      // ######################################################################################################
      // ################################## setting context #############################################
      // ######################################################################################################
      /*
      salesmeet_parameters={"button_automatic":"0","button_text":"bella li"}
      */
      settingContext() {
          this.button_automatic = "0";
          this.button_color = "#33a531";
          this.button_color_border = "#fff";
          this.button_label_color = "#fff";
          // this.button_text = "Scopri il valore del prodotto,<br>consulta un nostro esperto.";
          this.button_text = "Bisogno di maggiori informazioni?<br>Consulta un nostro esperto.";
          this.button_text = "Avez-vous besoin de plus d'informations?<br>Consultez un de nos experts.";
          // Assegno variabili esterne
          // console.log("window.salesmeet_parameters: " + window.salesmeet_parameters);
          if (window.salesmeet_parameters!=null) {
              for (let [key, value] of Object.entries(window.salesmeet_parameters)) {
                  eval( "this." + key + " = '" + value + "';" );
              }
          }
      }

}

let SalesMeetAppInstance = new SalesMeetApp();
window.onload = function() {
    // SalesMeetAppInstance.init();
}
SalesMeetAppInstance.init();


// ######################################################################################################
// ################################## CLOSE BOOKING #############################################
// ######################################################################################################

if (window.addEventListener) {
    window.addEventListener("message", salesmeetOnMessage, false);
} else if (window.attachEvent) {
    window.attachEvent("onmessage", salesmeetOnMessage, false);
}

function salesmeetOnMessage(event) {
    // Check sender origin to be trusted
    if (event.origin !== "https://booking.salesmeet.it") return;
    SalesMeetAppInstance.closeSalesmeet();
    /*
    var data = event.data;
    if (typeof(window[data.func]) == "function") {
        window[data.func].call(null, data.message);
    }
    */
}

// Function to be called from iframe
/*
function parentFuncName(message) {
    alert(message);
}
*/
