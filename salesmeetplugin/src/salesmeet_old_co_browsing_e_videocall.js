class SalesMeetApp {

    constructor() {

        this.domain = "salesmeet.it";
        this.radice = "SalesMeetApp";
        this.hostname = window.location.hostname;
        this.href = window.location.href;
        this.token = "";
        this.tokenparent = "";

        this.heightMenu = 34;
        this.widthVideo = 200;
        this.heightVideo = 230;
        this.w = window.innerWidth;
        this.h = window.innerHeight;
        this.heightWithoutMenu = (this.h - this.heightMenu) - 1; // - 1 sono i margini
        // this.widthWithoutVideo = (this.w - this.widthVideo) - 1; // - 1 sono i margini

        this.mobile = false;
        if ( (this.w < 600 ) || (this.h < 600 ) ) {
            this.mobile = true;
        }
        this.settingContext();

    }
    init() {

        if (this.isSiteNotIndex(this.hostname)) {
            return;
        }
        // importo il tutto se sono in video call
        this.token = this.getParameterByName("salesmeettoken",window.location.href);
        this.tokenparent = this.getParameterByName("salesmeettoken",window.parent.location.href);
        // console.log("token: " + this.token);
        if ( !this.isEmpty(this.token) ) {
            // Loading VIDEOCALL salesmeet
            this.addLoading();
            // attivo GUI
            setTimeout(function(){ SalesMeetAppInstance.addSalesmeet(); }, 5000);
        } else {
            // attivo UIEX sempre
            this.addScript("https://uiexfrontend.salesmeet.it/service/default.js","uiexfrontend");
            // console.log("tokenparent: " + this.tokenparent);
            if (this.tokenparent==null) {
                // attivo bottone esperto
                console.log("attivo bottone esperto");
                this.addCTA();
            } else {
                // attivo co-browsing
                var togetherjsquery = "?#&togetherjs=" + this.tokenparent.substring(0, this.tokenparent.length - 1);
                var href = window.location.href;
                href = href.replace("?" + togetherjsquery, "");
                href = href.replace(togetherjsquery, "");
                this.addCookie("UserIntelligentExperienceAppCurrentUrl", href);
                this.addCookie("UserIntelligentExperienceAppUrl", this.hostname);
            }
        }
        return this.tokenparent;

    }

    // ######################################################################################################
    // ################################## COMMON #############################################
    // ######################################################################################################


    isEmpty(value){
      return (value == null || value.length === 0);
    }
    isSiteNotIndex(url) {
        if (url.indexOf(this.domain)>0) {
            return true;
        }
        return false;
    }
    addScript(url) {
        var imported = document.createElement('script', name);
        imported.src = url;
        imported.rel = 'import';
        imported.id = name;
        document.head.appendChild(imported);
    }
    addCookie(cname, cvalue) {
        localStorage.setItem(cname, cvalue);
    }
    getCookie(cname) {
        var temp = localStorage.getItem(cname);
        if (temp==null) {
            return "";
        } else {
            return temp;
        }
    }
    getParameterByName(name,url) {
        // var url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    // ######################################################################################################
    // ################################## VIDEO CALL #############################################
    // ######################################################################################################


    // CALL ATTIVA
    addSalesmeet() {
        // AZZERO PAGINA
        const patt = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script\s*>/gi
        const html = document.getElementsByTagName('html')[0]
        html.innerHTML =  html.innerHTML.replace(patt, '');
        const patt2 = /<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style\s*>/gi
        html.innerHTML =  html.innerHTML.replace(patt2, '');
        document.body.innerHTML = "";
        setTimeout(function(){ SalesMeetAppInstance.addSalesmeetCreate(); }, 2000);
    }
    addSalesmeetCreate() {

        // prendo il link corretto della pagina
        var href = this.href.replace("salesmeettoken=" + this.token, "");

        // DIV MENU
        var elemMenu = document.createElement('div');
        elemMenu.style.cssText = 'position:relative;width:100%;height:' + SalesMeetAppInstance.heightMenu + 'px;z-index:1;background:#0095dd;border-bottom:1px solid #000;color:#000000;';
        elemMenu.id = "salesmeetmenu";
        elemMenu.innerHTML = "Loading menu ...";

        // DIV VIDEO
        var elemVideo = document.createElement('div');
        elemVideo.style.cssText = 'overflow: auto; bottom:20px; left:20px; position:fixed;width:' + SalesMeetAppInstance.widthVideo + 'px;height:' + SalesMeetAppInstance.heightVideo + 'px;z-index:999;background:#fff;color:#000000;';
        elemVideo.id = "salesmeetvideo";
        elemVideo.classList.add("ui-widget-content");
        elemVideo.innerHTML = "Loading video ...";

        // DIV MODALE
        /*
        var elemModale = document.createElement('div');
        elemModale.style.cssText = 'top:' + SalesMeetAppInstance.heightMenu + 'px;height:' + SalesMeetAppInstance.heightWithoutMenu + 'px';
        elemModale.id = "salesmeet-div-modale";
        */

        document.body.innerHTML = "";
        // AGGIUNGO DIV MENU
        document.body.appendChild(elemMenu);
        // this.addMenu();
        this.addMenuStart();
        // AGGIUNGO DIV Video
        document.body.appendChild(elemVideo);
        salesMeetAddVideoScript();
        // AGGIUNGO DIV modale
        /*
        document.body.appendChild(elemModale);
        */

        // AGGIUNGO IFRAME CON IL SITO
        href = href + "?#&togetherjs=" + this.token.substring(0, this.token.length - 1); // aggiungo ID stanza co-browsing
        var elemIframe = document.createElement('div');
        elemIframe.id = "salesmeetsite";
        elemIframe.style.cssText = 'position:relative;width:100%;height:' + SalesMeetAppInstance.heightMenu + 'px;z-index:1;background:#fff;';
        elemIframe.innerHTML = '<iframe id="salesmeetcobrowsing" frameBorder="0" src="' + href + '" style="width:100%;height:' + SalesMeetAppInstance.heightWithoutMenu + 'px;">';
        document.body.appendChild(elemIframe);

        // document.body.insertBefore(elemDiv, document.body.firstChild);
    }
    addLoading() {
      document.body.innerHTML = "";
      var elemLoading = document.createElement('div');
      elemLoading.style.cssText = 'position:fixed;width:100%;height:100%;z-index:9999999;background:#fff;color:#000;text-align:center;padding-top:100px;';
      elemLoading.id = "salesmeetLoading";
      elemLoading.innerHTML = 'Loading Video Calling App<br><br><img style="height: 70px;" src="https://app.salesmeet.it/asset/img/loading.svg">';
      document.body.appendChild(elemLoading);
    }
    // GET MENU
    /*
    addMenu() {
          // SalesMeetAppInstance.addScript("https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js","sm_jquery");
          // SalesMeetAppInstance.addScript("https://app.salesmeet.it/asset/datatables/jquery.dataTables.min.js","sm_dataTables");
          // SalesMeetAppInstance.addScript("https://app.salesmeet.it/asset/js/salesmeet.js","sm_common_app");
          // SalesMeetAppInstance.addScript("https://app.salesmeet.it/uiex/uiex.js","sm_uiex");
          setTimeout(function(){ SalesMeetAppInstance.addMenuStart(); }, 2000);
    }
    */
    // GET MENU
    addMenuStart() {
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              document.getElementById('salesmeetmenu').innerHTML = arraybuffer;
          }
          oReq.open("GET", "https://app.salesmeet.it/menu.php?token=" + this.token);
          var formData = new FormData();
          formData.append("token", this.token );
          oReq.send( formData );
    }

    // ######################################################################################################
    // ################################## INJECTION PAGE #############################################
    // ######################################################################################################

    // TOKEN NON PRESENTE
    addCTAlink() {

          var elemDiv = document.createElement('div');
          elemDiv.setAttribute("id", this.radice + "Button");
          elemDiv.style.cssText = 'position:fixed;bottom:30px;left:0px;width:auto;height:auto;\
                                   padding:10px;z-index:99999;background:' + this.button_color + ';\
                                   border:1px solid ' + this.button_color_border + ';border-left:0px solid;';
          elemDiv.innerHTML = "<a style='color:" + this.button_label_color + ";' id='" + this.radice + "seopen' href='javascript:void(0);'>" + this.button_text + "</a>";

          var first = document.createElement("a");
          first.href = "javascript:SalesMeetAppInstance.startBooking();"
          first.style = "margin-top: 10px; background-color: #33a531; padding: 20px; width: 100%; margin-bottom: 10px;color: #fff; font-size: 20px;text-align: center;"
          // var text = document.createTextNode(this.button_text);
          first.innerHTML = this.button_text;
          // first.appendChild(text);

          var ferrarisboutique = document.getElementsByClassName('steasy_element_block');
          // The double F
          var thedoublef = document.getElementsByClassName('price-box');
          // Apple
          var apple = document.getElementsByClassName('ribbon');
          // valentino
          var valentino = document.getElementsByClassName('item-ctaInteraction__cta');
          // autohero
          var autohero = document.getElementsByClassName('vehiclePriceContainer___1pKN-');
          // scottsports
          var scottsports = document.getElementsByClassName('key-features');


          if (autohero.length > 0) {
                console.log("*******autohero");
                first.style = "position:relative;float: left;position:absolute;background-color: #33a531; padding: 10px; width: 300px; right: 100%;text-decoration: none;color: #fff; font-size: 14px;text-align: center;";
                document.getElementsByClassName("vehiclePriceContainer___1pKN-")[0].appendChild(first);
          } else if (thedoublef.length > 0) {
                console.log("*******The double F");
                document.getElementsByClassName("price-box")[0].appendChild(first);
          } else if (apple.length > 0) {
                console.log("*******Apple");
                document.getElementsByClassName("ribbon")[0].appendChild(first);
          } else if (ferrarisboutique.length > 0) {
                console.log("*******ferrarisboutique");
                first.style = "top: 30px;position: relative;background-color: #33a531; padding: 20px; width: 92%; margin-bottom: 10px;color: #fff; font-size: 20px;text-align: center;";
                document.getElementsByClassName("steasy_element_block")[0].appendChild(first);
          } else if (scottsports.length > 0) {
                console.log("*******scott-sports");
                first.style = "position:relative; background-color: rgb(0 154 255); padding: 2px; text-decoration: none; color: #fff; font-size: 14px;text-align: center;";
                document.getElementsByClassName("key-features")[0].appendChild(first);
          } else if (valentino.length > 0) {
                console.log("*******valentino");
                first.style = "position:relative;margin-top: 10px; background-color: #33a531; padding: 10px; width: 100%; margin-bottom: 10px;color: #fff; font-size: 14px;text-align: center;";
                document.getElementsByClassName("item-ctaInteraction__cta")[0].appendChild(elemDiv);
                document.getElementsByClassName("item-ctaInteraction__cta")[0].appendChild(first);
          } else {
              // ALL
              /* TEMP
              console.log("*******All");
              document.body.appendChild(elemDiv);
              var el = document.getElementById( this.radice + 'Button');
              el.onclick = function (e) {
                  SalesMeetAppInstance.startBooking();
              }
              */
          }
    }

    // AGGIUNGE CTA al sito
    addCTA() {
        if (SalesMeetAppInstance.button_automatic == "0") {
            SalesMeetAppInstance.addCTAlink();
        }
        //TEMP this.getInfoUser("addcta");
        // this.getInfoCompany();
    }

    // ######################################################################################################
    // ################################## AL CLICK DI SALESMEET #############################################
    // ######################################################################################################

    // Funzione apertura processo di booking o chatbot o ...
    startBooking() {
        this.getInfoUser("start");
        // this.getInfoCompany();
    }
    // GET DATI UTENTE
    /* da importare in https://profile.salesmeet.it/api/info.php e fare la chiamata
       da addFormInit() a getInfoCompany(infouser)
    */
    getInfoUser(step) {
          try {
              var formData = new FormData();
              formData.append("token", userIntelligentExperienceAppInstance.token );
              var oReq = new XMLHttpRequest();
              oReq.onload = function(e) {
                  var arraybuffer = oReq.response;
                  var jsonData = JSON.parse(arraybuffer);
                  // SalesMeetAppInstance.getInfoCompany( jsonData, step );
                  SalesMeetAppInstance.getInfoCompany( jsonData);
              }
              oReq.open("POST", "https://uiextools.salesmeet.it/useranalytics/");
              oReq.send( formData );
          } catch(err) {
              setTimeout(function(){ SalesMeetAppInstance.getInfoUser(step); }, 100);
          }

    }
    // getInfoCompany(infouser, step) {
    getInfoCompany(infouser) {
          // console.log("_____getInfoCompany_______");
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              console.log("arraybuffer: " + arraybuffer);
              // console.log("step: " + step);
              // step click CTA per l'apertura del booking o altra azione proprietaria del sito
              //TEMP if (step=="start") {
                  var n = arraybuffer.indexOf("booking");
                  if (n==0) {
                      // apre il processo di booking
                      SalesMeetAppInstance.addForm();
                  } else {
                      // apre altro proprietario del sito processo del sito (chatbot, contact form ...)
                      // console.log("JS: " + arraybuffer);
                      eval(arraybuffer);
                  }
              // Aggiunta o no della CTA all'interno del sito ...
              //TEMP } else {
                  // se rispetta le regole accetta la visibilità ...
                  //TEMP var n = arraybuffer.indexOf("addcta");
                  // console.log("bottone pre visibile...: " + n);
                  //TEMP if (n==0) {
                      // console.log("button_automatic: " + SalesMeetAppInstance.button_automatic);
                  //TEMP     if (SalesMeetAppInstance.button_automatic == "0") {
                  //TEMP         SalesMeetAppInstance.addCTAlink();
                  //TEMP     }
                  //TEMP }
              //TEMP }
          }
          // console.log("price: " + userIntelligentExperienceAppInstance.price);
          oReq.open("POST", "https://profile.salesmeet.it/api/info.php");
          var formData = new FormData();
          // formData.append("token", userIntelligentExperienceAppInstance.token );
          // formData.append("infouser", infouser );
          formData.append("count", infouser[0]["Count"] );
          formData.append("voto", infouser[0]["Voto"] );
          formData.append("votopagina", infouser[0]["Votopagina"] );
          formData.append("prezzopagina", userIntelligentExperienceAppInstance.price );
          formData.append("price", infouser[0]["Price"] );
          // formData.append("pricemax", infouser[0]["Pricemax"] );
          // formData.append("pricemin", infouser[0]["Pricemin"] );
          // formData.append("step", step );
          formData.append("referer", this.href );
          oReq.send( formData );
    }
    // apre il processo di booking
    addForm() {

          // Update dei dati della pagina per UIEx
          userIntelligentExperienceAppInstance.update();

          // Creazione form e submit dello stesso con apertura di una nuvoa scheda
          // Si vanno a recuperare i dati significativi da UIEx per darli in pasto
          var mapForm = document.createElement("form");
          mapForm.method = "POST"; // or "post" if appropriate
          mapForm.action = "https://booking.salesmeet.it";
          // mapForm.target = "_blank";
          mapForm.target = "salesmeet_iframe";
          mapForm.id = "salesmeetform";
          mapForm.name = "salesmeetform";

          var mapInputlanguage = document.createElement("input");
          mapInputlanguage.type = "text";
          mapInputlanguage.name = "language";
          mapInputlanguage.id = "language";
          mapInputlanguage.value = userIntelligentExperienceAppInstance.language;

          var mapInputtitle = document.createElement("input");
          mapInputtitle.type = "text";
          mapInputtitle.name = "title";
          mapInputtitle.id = "title";
          mapInputtitle.value = userIntelligentExperienceAppInstance.title;

          var mapInputPrice = document.createElement("input");
          mapInputPrice.type = "text";
          mapInputPrice.name = "price";
          mapInputPrice.id = "price";
          mapInputPrice.value = userIntelligentExperienceAppInstance.price;

          var mapInputimg = document.createElement("input");
          mapInputimg.type = "text";
          mapInputimg.name = "img";
          mapInputimg.id = "img";
          mapInputimg.value = userIntelligentExperienceAppInstance.img;

          var mapInputtoken = document.createElement("input");
          mapInputtoken.type = "text";
          mapInputtoken.name = "token";
          mapInputtoken.id = "token";
          mapInputtoken.value = userIntelligentExperienceAppInstance.token;

          var mapInputuser = document.createElement("input");
          mapInputuser.type = "text";
          mapInputuser.name = "user";
          mapInputuser.id = "user";
          mapInputuser.value = userIntelligentExperienceAppInstance.user;

          var mapInputUrl = document.createElement("input");
          mapInputUrl.type = "text";
          mapInputUrl.name = "urlcaller";
          mapInputUrl.id = "urlcaller";
          mapInputUrl.value = window.location.href;

          mapForm.appendChild(mapInputlanguage);
          mapForm.appendChild(mapInputtitle);
          mapForm.appendChild(mapInputPrice);
          mapForm.appendChild(mapInputimg);
          mapForm.appendChild(mapInputtoken);
          mapForm.appendChild(mapInputuser);
          mapForm.appendChild(mapInputUrl);

          document.body.appendChild(mapForm);

          // ##############

          // DIV MENU
          var mapDiv = document.createElement('div');
          mapDiv.style.cssText = 'position:fixed;top:0px;width:100%;height:100%;z-index:9999999999;background:rgb(0 0 0 / 80%);';
          mapDiv.id = "salesmeetdiv";

          var mapIframe = document.createElement("iframe");
          mapIframe.name = "salesmeet_iframe";
          mapIframe.id = "salesmeet_iframe";

          var dimensione = "width:95%;height:92%;top: 4%; ";
          if (this.mobile) {
               dimensione = "width:100%;height:100%;top: 0%; ";
          }
          mapIframe.style.cssText = dimensione + 'background:rgb(255 255 255 / 100%);background-repeat:no-repeat;background-position:center;background-image:url(https://booking.salesmeet.it/asset/img/loading3.svg); margin: 0 auto; display: flex;position: relative;';
          // mapIframe.style.cssText = 'width:100%;height:100%;';


          mapDiv.appendChild(mapIframe);

          document.body.appendChild(mapDiv);

          // ##############

          mapForm.submit();

          // ##############
          var el = document.getElementById('salesmeetform');
          el.remove();

      }

      closeSalesmeet(){
        var el = document.getElementById('salesmeetdiv');
        el.remove();
      }

      // ######################################################################################################
      // ################################## setting context #############################################
      // ######################################################################################################
      /*
      salesmeet_parameters={"button_automatic":"0","button_text":"bella li"}
      */
      settingContext() {
          this.button_automatic = "1";
          this.button_color = "#33a531";
          this.button_color_border = "#fff";
          this.button_label_color = "#fff";
          // this.button_text = "Scopri il valore del prodotto,<br>consulta un nostro esperto.";
          this.button_text = "Bisogno di maggiori informazioni?<br>Consulta un nostro esperto.";
          this.button_text = "Avez-vous besoin de plus d'informations?<br>Consultez un de nos experts.";
          // Assegno variabili esterne
          // console.log("window.salesmeet_parameters: " + window.salesmeet_parameters);
          if (window.salesmeet_parameters!=null) {
              for (let [key, value] of Object.entries(window.salesmeet_parameters)) {
                  eval( "this." + key + " = '" + value + "';" );
              }
          }
      }

}

let SalesMeetAppInstance = new SalesMeetApp();
window.onload = function() {
    // SalesMeetAppInstance.init();
}
var salesMeettoken = SalesMeetAppInstance.init();
// se il token è nullo vuol dire che sono nell'iframe con il sito di riferimento ...


// ######################################################################################################
// ################################## VIDEO CALL #############################################
// ######################################################################################################

/*
if (salesMeettoken!=""){
    // console.log("TogetherJSConfig");
    TogetherJSConfig_disableWebRTC = true;
    TogetherJSConfig_suppressJoinConfirmatione = true;
    TogetherJSConfig_hubBase = "https://cobrowsinghub.salesmeet.it";
    TogetherJSConfig_suppressInvite = true;
    SalesMeetAppInstance.addScript("https://togetherjs.com/togetherjs-min.js","sm_togetherjs");
}

// VIDEO
function  salesMeetAddVideoScript(){
        SalesMeetAppInstance.addScript("https://code.jquery.com/ui/1.12.1/jquery-ui.js","sm_twilio");
        SalesMeetAppInstance.addScript("//media.twiliocdn.com/sdk/js/video/releases/2.7.2/twilio-video.min.js","sm_twilio");
        SalesMeetAppInstance.addScript("https://app.salesmeet.it/video/common.js","sm_video");
        setTimeout(function(){ this.salesMeetAddVideo(); }, 2000);
}
function salesMeetAddVideo() {
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              document.getElementById('salesmeetvideo').innerHTML = arraybuffer;
              setTimeout(function(){ salesMeetAddStartVideo(); }, 5000);
          }
          oReq.open("GET", "https://app.salesmeet.it/video/index.php?token=" + this.salesMeettoken);
          var formData = new FormData();
          formData.append("token", this.salesMeettoken );
          oReq.send( formData );
}
function  salesMeetAddStartVideo(){
        // SalesMeetAppInstance.addScript("https://app.salesmeet.it/video/VIDEOsample_old.js","sm_video");
}
*/



// ######################################################################################################
// ################################## CLOSE BOOKING #############################################
// ######################################################################################################

if (window.addEventListener) {
    window.addEventListener("message", salesmeetOnMessage, false);
} else if (window.attachEvent) {
    window.attachEvent("onmessage", salesmeetOnMessage, false);
}

function salesmeetOnMessage(event) {
    // Check sender origin to be trusted
    if (event.origin !== "https://booking.salesmeet.it") return;
    SalesMeetAppInstance.closeSalesmeet();
    /*
    var data = event.data;
    if (typeof(window[data.func]) == "function") {
        window[data.func].call(null, data.message);
    }
    */
}

// Function to be called from iframe
/*
function parentFuncName(message) {
    alert(message);
}
*/
