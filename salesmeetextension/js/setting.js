/* UIEX - Apre direttamente la pagina esterna al click sull'icona nella barra di Chrome. */
//

window.onload = function() {

    // disattiva estenzione
    document.getElementById("disattiva").onclick = function() {
            chrome.storage.local.set({'uiex_active':'no'}, function() {});
            actionButton ("disattiva","attiva");
    }
    // attiva estenzione
    document.getElementById("attiva").onclick = function() {
            chrome.storage.local.set({'uiex_active':'yes'}, function() {});
            actionButton ("attiva","disattiva");
    }
    // controlla status dell'estenzione
    chrome.storage.local.get(['uiex_active'], function(result) {
        if (result.uiex_active=="yes") {
            actionButton ("attiva","disattiva");
        } else {
            actionButton ("disattiva","attiva");
        }
    });

    // Gestione uiex_referrer_costum
    document.getElementById("uiex_referrer_costum").onclick = function() {
        chrome.storage.local.set({'uiex_referrer_costum_body':'no'}, function() {});
        actionButton ("uiex_referrer_costum","uiex_referrer_costum_body");
        document.getElementById("uiex_referrer_costum_position").value = "";
        document.getElementById("uiex_referrer_costum_position").classList.remove('selezionato');
    }
    //
    document.getElementById("uiex_referrer_costum_body").onclick = function() {
        chrome.storage.local.set({'uiex_referrer_costum_body':'yes'}, function() {});
        actionButton ("uiex_referrer_costum_body","uiex_referrer_costum");
        document.getElementById("uiex_referrer_costum_position").value = "";
        document.getElementById("uiex_referrer_costum_position").classList.remove('selezionato');
    }

    document.getElementById("uiex_referrer_costum_position").onkeyup = function() {
        console.log(document.getElementById("uiex_referrer_costum_position").value);
        chrome.storage.local.set({'uiex_referrer_costum_body': document.getElementById("uiex_referrer_costum_position").value }, function() {});
        document.getElementById("uiex_referrer_costum_position").classList.add('selezionato');
        document.getElementById("uiex_referrer_costum_body").classList.remove('selezionato');
        document.getElementById("uiex_referrer_costum").classList.remove('selezionato');
    }

    chrome.storage.local.get(['uiex_referrer_costum_body'], function(result) {
        if (result.uiex_referrer_costum_body=="yes") {
            actionButton ("uiex_referrer_costum_body","uiex_referrer_costum");
        } else {
            if (result.uiex_referrer_costum_body=="no") {
                actionButton ("uiex_referrer_costum","uiex_referrer_costum_body");
            }  else {
                document.getElementById("uiex_referrer_costum_position").value = result.uiex_referrer_costum_body;
                document.getElementById("uiex_referrer_costum_position").classList.add('selezionato');
            }
            // actionButton ("uiex_referrer_costum","uiex_referrer_costum_body");
        }
    });

    // uiex_script_to_launch
    /*
    document.getElementById("uiex_script_to_launch").onchange = function() {
        var sel = document.getElementById("uiex_script_to_launch");
        var opt = sel.options[sel.selectedIndex];
        chrome.storage.local.set({'uiex_script_to_launch':opt.value}, function() {});
    }
    */
    chrome.storage.local.get(['uiex_script_to_launch'], function(result) {
        // document.getElementById("uiex_script_to_launch").value = result.uiex_script_to_launch;
        document.getElementById("ustl_" + result.uiex_script_to_launch).checked = true;
    });

    var names = document.getElementsByName("uiex_script_to_launch");
    for (var i = 0; i < names.length; i++) {
        names[i].onchange = function() {
            for (var ie = 0, length = names.length; ie < length; ie++) {
              if (names[ie].checked) {
                // do whatever you want with the checked radio
                // alert(names[ie].value);
                chrome.storage.local.set({'uiex_script_to_launch': names[ie].value }, function() {});
                // only one radio can be logically checked, don't check the rest
                break;
              }
            }
        }
    }

    // common
    function actionButton (add,remove,remove2) {
        if (add=="attiva") {
            chrome.browserAction.setIcon({path:"../img/uiex_logo.png"});
        }
        if (add=="disattiva") {
            chrome.browserAction.setIcon({path:"../img/uiex_logo_spento.png"});
        }
        document.getElementById(add).classList.add('selezionato');
        document.getElementById(remove).classList.remove('selezionato');
    }

}
