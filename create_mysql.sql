CREATE DATABASE `salesmeet`;

/* elenco e censimento aziende */
CREATE TABLE `salesmeet`.`company_registry` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `logo` varchar(256) DEFAULT NULL,
  `site` varchar(256) DEFAULT NULL, /* solo www.prova.it senza protocollo */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `time_slot_appointment` int(11) DEFAULT '30',  /*  */

  `in_store_enable` int(11) DEFAULT '0', /* 1 online, 2 in store, 3 to home, e poi abbiamo le combinazioni 12 23 123 13  */

  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company`),
  UNIQUE KEY `id_company_UNIQUE` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* paramentrizzazione aziende */
CREATE TABLE `salesmeet`.`company_action` (
  `id_company_action` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `pre_visibility_button` int(11) DEFAULT '0',  /* 0 rende visibile il tasto lato plugin sulla pagina  ... */
  `url_page` text DEFAULT NULL,  /* se valorizzata l'url della pagina la regola vale solo per quella */
  `action_type` int(11) DEFAULT '0', /* 0 = esterno, 1 = chatbot, 2 = booking, 3 instant messenger */
  `action_order` int(11) DEFAULT '0', /* ordine di importanza. Il numero più alto è il più importante */
  `action_js` text DEFAULT NULL,  /* js alternativo al booking, lanciato se le clausole non permetto di aprire il booking */
  `minimum_price` int(11) DEFAULT '0',  /* prezzo medio minimo per la visualizzazione */
  `minimum_number_of_pages` int(11) DEFAULT '0',  /* numero minino di pagine */
  `minimum_vote_site` int(11) DEFAULT '0', /* voto AI minimo */
  `minimum_vote_page` int(11) DEFAULT '0', /* voto AI minimo per la singola pagina */
  `minimum_price_page` int(11) DEFAULT '0', /* prezzo minimo per la singola pagina */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company_action`),
  UNIQUE KEY `id_company_action_UNIQUE` (`id_company_action`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* CTA aziende */
CREATE TABLE `salesmeet`.`company_action_cta` (
  `id_company_action_cta` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `all_site` int(11) DEFAULT '0', /* 0 = nessuna pagina - 1 = tutte le pagine */
  `url_page` text DEFAULT NULL,  /* se valorizzata l'url della pagina la regola vale solo per quella */
  `CSS_TAG` varchar(256) DEFAULT '',
  `CSS` text DEFAULT NULL,  /* js alternativo al booking, lanciato se le clausole non permetto di aprire il booking */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company_action_cta`),
  UNIQUE KEY `id_company_action_cta_UNIQUE` (`id_company_action_cta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* elenco esperti */
CREATE TABLE `salesmeet`.`experts` (
  `id_expert` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(256) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `id_company` int(11) DEFAULT '0',
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_expert`),
  UNIQUE KEY `id_expert_UNIQUE` (`id_expert`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* tipologia di video call */
CREATE TABLE `salesmeet`.`experts_videocall` (
  `id_expert_videocall` int(11) NOT NULL AUTO_INCREMENT,
  `id_expert` int(11) DEFAULT '0', /* se zero prende quella di default ... */
  `id_company` int(11) DEFAULT '0', /* se zero prende quella di default ... */
  `type_video_call` varchar(45) DEFAULT NULL, /* Meet, Zoom ... */
  `user_video_call` varchar(45) DEFAULT NULL,
  `pass_video_call` varchar(45) DEFAULT NULL,
  `jwt_video_call` tinytext DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_expert_videocall`),
  UNIQUE KEY `id_expert_videocall_UNIQUE` (`id_expert_videocall`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* tipologia di calendario per esperto */
CREATE TABLE `salesmeet`.`experts_calendar` (
  `id_expert_calendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_expert` int(11) DEFAULT '0', /* se zero prende quella di default ... */
  `id_company` int(11) DEFAULT '0', /* se zero prende quella di default ... */
  `type_calendar` varchar(45) DEFAULT NULL, /* Meet, Zoom ... */
  `user_calendar` varchar(45) DEFAULT NULL,
  `pass_calendar` varchar(45) DEFAULT NULL,
  `jwt_calendar` text DEFAULT NULL,
  `id_calendar_remote` text DEFAULT NULL,
  `list_calendars` tinytext DEFAULT NULL, /* se vuoto prende quello di default se saparato da ; leggi tutti (il primo nella lista sarà quello principale) */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_expert_calendar`),
  UNIQUE KEY `id_expert_calendar_UNIQUE` (`id_expert_calendar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* dettaglio degli esperti, da dare in pasto alla machine learning per trovare l'esperto migliore */
CREATE TABLE `salesmeet`.`experts_details` (
  `id_expert_details` int(11) NOT NULL AUTO_INCREMENT,
  `specialization` tinytext,
  `features` tinytext,
  `keyword_work` tinytext,
  `keywords_life` tinytext,
  `id_language` int(11) DEFAULT '0',
  `id_expert` int(11) DEFAULT '0',
  `id_company` int(11) DEFAULT '0',
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_expert_details`),
  UNIQUE KEY `id_expert_details_UNIQUE` (`id_expert_details`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* abbinamento appountamento con esperto */
CREATE TABLE `salesmeet`.`calendar_experts` (
  `id_calendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `id_appointment` int(11) DEFAULT '0',
  `id_expert` int(11) DEFAULT '0',
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_calendar`),
  UNIQUE KEY `id_calendar_UNIQUE` (`id_calendar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* tabella base appuntamento */
CREATE TABLE `salesmeet`.`appointments` (
  `id_appointment` int(11) NOT NULL AUTO_INCREMENT,
  `id_token` varchar(45) DEFAULT NULL,
  `id_company` int(11) DEFAULT '0',
  `site` varchar(256) DEFAULT NULL,
  `referer` tinytext,
  `tokenuiex` varchar(100) DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `favourite_language` varchar(3) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `title` tinytext,
  `img` tinytext,
  `price` int(11) DEFAULT '0',
  `description` text,
  `data_selezionata` datetime DEFAULT NULL,
  `data_selezionata_fine` datetime DEFAULT NULL,
  `data_selezionata_timezone` datetime DEFAULT NULL,
  `user_timezone` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `freecalendar` int(11) DEFAULT '0',
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,

  `in_store_enable` int(11) DEFAULT '0',
  `id_location` int(11) DEFAULT '0',

  `id_cobrowsing` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `os` varchar(45) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointment`),
  UNIQUE KEY `id_appointment_UNIQUE` (`id_appointment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* prodotti legati all'appuntamento, per singolo appuntamento possiamo avere più prodotti */
CREATE TABLE `salesmeet`.`appointments_products` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL,
  `img` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `id_appointment` int(11) DEFAULT '0',
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `id_product_UNIQUE` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* link creati per la video call, la dashboard ... per il singolo appuntamento */
CREATE TABLE `salesmeet`.`appointments_link` (
  `id_appointment_link` int(11) NOT NULL AUTO_INCREMENT,
  `id_appointment` int(11) DEFAULT '0',
  `dashboard_expert` varchar(256) DEFAULT NULL, /* Dashboard di salesmeet */
  `link_salesmeet_expert` varchar(256) DEFAULT NULL,
  `link_salesmeet_customer` varchar(256) DEFAULT NULL,
  `external_expert` varchar(256) DEFAULT NULL,
  `external_customer` varchar(256) DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointment_link`),
  UNIQUE KEY `id_appointment_link_UNIQUE` (`id_appointment_link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `salesmeet`.`appointments_note` (
  `id_appointment_note` int(11) NOT NULL AUTO_INCREMENT,
  `id_appointment` int(11) DEFAULT '0',
  `description` text DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointment_note`),
  UNIQUE KEY `id_appointment_note_UNIQUE` (`id_appointment_note`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* templetes delle email da mandare ai clietni   */
CREATE TABLE `salesmeet`.`appointments_email_template` (
  `id_appointment_email_text` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_company` int(11) DEFAULT '0',
  `language` varchar(10) DEFAULT NULL,
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointment_email_text`),
  UNIQUE KEY `id_appointment_email_text_UNIQUE` (`id_appointment_email_text`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/*  */
CREATE TABLE `salesmeet`.`appointments_email_setting` (
  `id_appointment_email_setting` int(11) NOT NULL AUTO_INCREMENT,
  `action_customer_booking` varchar(45) DEFAULT NULL, /* 0 email di conferma, 1 appuntamento già confermato ... */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_expert` int(11) DEFAULT '0',  /* se zero prende quella di default ... */
  `id_company` int(11) DEFAULT '0',  /* se zero prende quella di default ... */
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointment_email_setting`),
  UNIQUE KEY `id_appointment_email_setting_UNIQUE` (`id_appointment_email_setting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `salesmeet`.`location` (
  `id_location` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '0', /* 1 = ufficio, 2 = store, 3 a casa e 10 all */
  `country` varchar(256) DEFAULT NULL,
  `timezone` int(11) DEFAULT '0',
  `timezone_name` varchar(45) DEFAULT '', /* Europe/Rome ...  https://www.php.net/manual/en/timezones.php ... https://momentjs.com/timezone/ */
  `region` varchar(256) DEFAULT NULL,
  `province` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `street` varchar(256) DEFAULT NULL,
  `id_company` int(11) DEFAULT '0',
  `sequence` int(11) DEFAULT '0',
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_location`),
  UNIQUE KEY `id_location_UNIQUE` (`id_location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `salesmeet`.`appointments_location_acasa` (
  `id_appointments_lacation_acasa` int(11) NOT NULL AUTO_INCREMENT,
  `id_appointment` int(11) DEFAULT '0',
  `country` varchar(256) DEFAULT NULL,
  `province` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `street` varchar(256) DEFAULT NULL,
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `visible` int(11) DEFAULT '0',
  PRIMARY KEY (`id_appointments_lacation_acasa`),
  UNIQUE KEY `id_appointments_lacation_acasa_UNIQUE` (`id_appointments_lacation_acasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* paramentrizzazione aziende */
CREATE TABLE `salesmeet`.`company_business_hours` (
  `id_company_business_hours` int(11) NOT NULL AUTO_INCREMENT,
  `id_expert` int(11) DEFAULT '0',  /* se zero prende quella di default ... */
  `id_company` int(11) DEFAULT '0',  /* se zero prende quella di default ... */
  `days_of_week` varchar(256) DEFAULT NULL, /* [ 1, 2, 3, 4 , 5 ], // Monday, Tuesday, Wednesday, Thursday, Friday */
  `start_time` varchar(256) DEFAULT NULL, /* 08:00 */
  `end_time` varchar(256) DEFAULT NULL, /* 13:00 */
  `data_init` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company_business_hours`),
  UNIQUE KEY `id_company_business_hours_UNIQUE` (`id_company_business_hours`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* Personalizzazioni per azienda */
CREATE TABLE `salesmeet`.`company_css` (
  `id_company_css` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `code` text DEFAULT NULL,
  `company_name_visible` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company_css`),
  UNIQUE KEY `id_company_css_UNIQUE` (`id_company_css`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/* Personalizzazioni per azienda */
CREATE TABLE `salesmeet`.`company_language` (
  `id_company_language` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT '0',
  `language_list` text DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id_company_language`),
  UNIQUE KEY `id_company_language_UNIQUE` (`id_company_language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
