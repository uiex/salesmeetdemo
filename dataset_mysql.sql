
insert into salesmeet.company_registry (name,logo,site) values ("Ferraris Boutique","https://www.ferrarisboutique.com/img/ferraris-boutique-logo-1552901183.jpg","www.ferrarisboutique.com");
insert into salesmeet.company_registry (name,logo,site) values ("TheDoubleF","https://www.thedoublef.com/skin/frontend/thedoublef/default/images/logo.svg","www.thedoublef.com");
insert into salesmeet.company_registry (name,logo,site) values ("Valentino","https://upload.wikimedia.org/wikipedia/commons/b/b9/Logo_Valentino.gif","www.valentino.com");
insert into salesmeet.company_registry (name,logo,site) values ("Tods","https://upload.wikimedia.org/wikipedia/it/d/dc/Tod%27s.png","www.tods.com");
insert into salesmeet.company_registry (name,logo,site) values ("Ecco","https://cdn2.bigcommerce.com/server1000/4d17b/product_images/uploaded_images/ecco-bp.png?t=1439324992","us.ecco.com");


insert into salesmeet.itmpany_action (id_company,action_js) values (1,"$crisp.push(['do', 'chat:open']);");
insert into salesmeet.itmpany_action (id_company,action_js) values (2,"Tawk_API.toggle();");
insert into salesmeet.itmpany_action (id_company,action_js) values (3,'window.location.href = "https://www.valentino.com/it-it/help/customercare/boutique-services/boutique-appointment";');
insert into salesmeet.itmpany_action (id_company,action_js) values (4,"vivocha.getWidget('5d9dda2dae952700060b9d6a-152-1570626041381').engage('weblead');");

insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado@gmail.com",1,"https://booking.salesmeet.it/asset/img_esperti/uomo2.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("John","Cerutti","3498603096","facchini.corrado2@gmail.com",1,"https://booking.salesmeet.it/asset/img_esperti/uomo3.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Laura","Rossi","3498603096","facchini.corrado3@gmail.com",1,"https://booking.salesmeet.it/asset/img_esperti/donna1.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Alisha","Bianchi","3498603096","facchini.corrado4@gmail.com",1,"https://booking.salesmeet.it/asset/img_esperti/donna2.jpg");

insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado5@gmail.com",2,"https://booking.salesmeet.it/asset/img_esperti/uomo2.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("John","Cerutti","3498603096","facchini.corrado6@gmail.com",2,"https://booking.salesmeet.it/asset/img_esperti/uomo3.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Laura","Rossi","3498603096","facchini.corrado7@gmail.com",2,"https://booking.salesmeet.it/asset/img_esperti/donna1.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Alisha","Bianchi","3498603096","facchini.corrado8@gmail.com",2,"https://booking.salesmeet.it/asset/img_esperti/donna2.jpg");

insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado9@gmail.com",3,"https://booking.salesmeet.it/asset/img_esperti/uomo2.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("John","Cerutti","3498603096","facchini.corrado10@gmail.com",3,"https://booking.salesmeet.it/asset/img_esperti/uomo3.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Laura","Rossi","3498603096","facchini.corrado11@gmail.com",3,"https://booking.salesmeet.it/asset/img_esperti/donna1.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Alisha","Bianchi","3498603096","facchini.corrado12@gmail.com",3,"https://booking.salesmeet.it/asset/img_esperti/donna2.jpg");

insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado13@gmail.com",4,"https://booking.salesmeet.it/asset/img_esperti/uomo2.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("John","Cerutti","3498603096","facchini.corrado14@gmail.com",4,"https://booking.salesmeet.it/asset/img_esperti/uomo3.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Laura","Rossi","3498603096","facchini.corrado15@gmail.com",4,"https://booking.salesmeet.it/asset/img_esperti/donna1.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Alisha","Bianchi","3498603096","facchini.corrado16@gmail.com",4,"https://booking.salesmeet.it/asset/img_esperti/donna2.jpg");


insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("10 anni di esperienza in selezione e vendita nel settore uomo.","Preciso e ricercato.","scarpe,uomo,sneakers,premiata","",1,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("15 working on the front line.","In step with fashion.","scarpe,uomo,polacchetti","",2,2);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("Co-founder ed influencer di moda.","La moda non è mai abbastanza","scarpe,donna,sneakers","",3,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("8 years of experience and research of the right match.","The watchword is 'fashion'.","scarpe,uomo,polacchetti","",4,2);

insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("10 anni di esperienza in selezione e vendita nel settore uomo.","Preciso e ricercato.","scarpe,uomo,sneakers,premiata","",5,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("15 working on the front line.","In step with fashion.","scarpe,uomo,polacchetti","",6,2);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("Co-founder ed influencer di moda.","La moda non è mai abbastanza","scarpe,donna,sneakers","",7,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("8 years of experience and research of the right match.","The watchword is 'fashion'.","scarpe,uomo,polacchetti","",8,2);

insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("10 anni di esperienza in selezione e vendita nel settore uomo.","Preciso e ricercato.","scarpe,uomo,sneakers,premiata","",9,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("15 working on the front line.","In step with fashion.","scarpe,uomo,polacchetti","",10,2);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("Co-founder ed influencer di moda.","La moda non è mai abbastanza","scarpe,donna,sneakers","",11,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("8 years of experience and research of the right match.","The watchword is 'fashion'.","scarpe,uomo,polacchetti","",12,2);

insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("10 anni di esperienza in selezione e vendita nel settore uomo.","Preciso e ricercato.","scarpe,uomo,sneakers,premiata","",13,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("15 working on the front line.","In step with fashion.","scarpe,uomo,polacchetti","",14,2);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("Co-founder ed influencer di moda.","La moda non è mai abbastanza","scarpe,donna,sneakers","",15,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("8 years of experience and research of the right match.","The watchword is 'fashion'.","scarpe,uomo,polacchetti","",16,2);

insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Ciao {expert_name},<br><br>{customer_name} ha richiesto un appuntamento in data {date_appointment}.<br><br>Qui il link della dashboard per l\'appuntamento <a href=\"{link_dashboard}\">{link_dashboard}</a>\n',0,'it',0,'booking_expert','Nuovo appuntamento - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Ciao {customer_name},<br><br>hai richiesto un appuntamento in data {date_appointment}.<br><br>Qui il link della dashboard per l\'appuntemento <a href=\"{link_appointment_customer}\">{link_appointment_customer}</a>\n',0,'it',0,'booking_customer','Nuovo appuntamento - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Hi {expert_name},<br><br>{customer_name} requested an appointment on {date_appointment}.<br><br>Here the link of the dashboard for the appointment <a href=\"{link_dashboard}\">{link_dashboard}</a>\n',0,'en',0,'booking_expert','New appointment - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Hi {customer_name},<br><br>you have requested an appointment on {date_appointment}.<br><br>Here is the dashboard link for the appointment <a href=\"{link_appointment_customer}\">{link_appointment_customer}</a>\n',0,'en',0,'booking_customer','New appointment - {date_appointment}');

insert into salesmeet.experts_videocall (id_expert,id_company,type_video_call,jwt_video_call) values (0,0,"zoom","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IndqOUpzRFExUTFLVm1NWHJQbzliRGciLCJleHAiOjE2MzYyMzQwMjAsImlhdCI6MTYwNDA5MzI2MX0.t4VZMNhfeQnkCyEQ41uC6FWFx1KbXvO_xdZKPXNNw8M");


insert into salesmeet.company_registry (name,logo,site) values ("Apoldist","https://apoldist.com/img/cms/logo%20apoldist.png","apoldist.com");
insert into salesmeet.itmpany_action (id_company,action_js) values (5,"");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado@gmail.com",5,"https://booking.salesmeet.it/asset/img_esperti/uomo2.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("John","Cerutti","3498603096","facchini.corrado@gmail.com",5,"https://booking.salesmeet.it/asset/img_esperti/uomo3.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Laura","Rossi","3498603096","facchini.corrado@gmail.com",5,"https://booking.salesmeet.it/asset/img_esperti/donna1.jpg");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Alisha","Bianchi","3498603096","facchini.corrado@gmail.com",5,"https://booking.salesmeet.it/asset/img_esperti/donna2.jpg");
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("10 anni di esperienza in selezione e vendita nel settore uomo.","Preciso e ricercato.","scarpe,uomo,sneakers,premiata","",17,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("15 working on the front line.","In step with fashion.","scarpe,uomo,polacchetti","",18,2);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("Co-founder ed influencer di moda.","La moda non è mai abbastanza","scarpe,donna,sneakers","",19,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("8 years of experience and research of the right match.","The watchword is 'fashion'.","scarpe,uomo,polacchetti","",20,2);


/* NEW */
insert into salesmeet.appointments_email_setting (action_customer_booking) values (0);
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Ciao {customer_name},<br><br>hai richiesto un appuntamento in data {date_appointment}.<br><br>L\'appuntemento dovra\' essere confermato. Ricevera\' una email di conferma con i dati in dettaglio.',0,'it',0,'booking_customer_pre_confirmation','Nuovo appuntamento - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Hi {customer_name},<br><br>you have requested an appointment on {date_appointment}.<br><br>The appointment must be confirmed. You will receive a confirmation email with the details in detail.',0,'en',0,'booking_customer_pre_confirmation','New appointment - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Ciao {expert_name},<br><br>{customer_name} ha richiesto un appuntamento in data {date_appointment}.<br><br>Qui il link della dashboard per l\'appuntemento <a href=\"{link_dashboard}\">{link_dashboard}</a><br><br>L\'APPUNTAMENTO PER ESSERE CONFERMATO RICHIEDERA\' LA TUA CONFERMA.',0,'it',0,'booking_expert_pre_confirmation','Nuovo appuntamento - {date_appointment}');
insert into salesmeet.appointments_email_template (`description`,`id_company`,`language`,`visible`,`type`,`subject`) VALUES ('Hi {expert_name},<br><br>{customer_name} requested an appointment on {date_appointment}.<br><br>Here the link of the dashboard for the appointment <a href=\"{link_dashboard}\">{link_dashboard}</a><br><br>THE APPOINTMENT TO BE CONFIRMED WILL BE YOUR CONFIRMATION.',0,'en',0,'booking_expert_pre_confirmation','New appointment - {date_appointment}');

insert into salesmeet.itmpany_business_hours (days_of_week,start_time,end_time) values ("1,2,3,4,5","08:00","13:00");
insert into salesmeet.itmpany_business_hours (days_of_week,start_time,end_time) values ("1,2,3,4,5","14:00","19:00");

insert into salesmeet.itmpany_css (id_company,code) values (2,"#div-left-body { background: #03A9F4; color: #fff;}");



insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (10,'Italy',2,'Piemonte','TO','Torino',NULL,'Via da qui',2,'2020-11-04 10:16:04',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (2,'France',1,NULL,NULL,'Parigi',NULL,'Viaooo',2,'2020-11-04 10:16:50',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (1,'USA',-5,NULL,NULL,'New York',NULL,NULL,2,'2020-11-04 10:21:14',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (3,'Italy',2,'Piemonte','MI','Milano',NULL,'Via da qui',2,'2020-11-05 21:53:38',0,0);

insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (10,'Italy',2,'Piemonte','TO','Torino',NULL,'Via da qui',4,'2020-11-04 10:16:04',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (2,'France',1,NULL,NULL,'Parigi',NULL,'Viaooo',4,'2020-11-04 10:16:50',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (1,'USA',-5,NULL,NULL,'New York',NULL,NULL,4,'2020-11-04 10:21:14',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (3,'Italy',2,'Piemonte','MI','Milano',NULL,'Via da qui',4,'2020-11-05 21:53:38',0,0);

insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (10,'Italy',2,'Piemonte','TO','Torino',NULL,'Via da qui',3,'2020-11-04 10:16:04',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (2,'France',1,NULL,NULL,'Parigi',NULL,'Viaooo',3,'2020-11-04 10:16:50',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (1,'USA',-5,NULL,NULL,'New York',NULL,NULL,3,'2020-11-04 10:21:14',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (3,'Italy',2,'Piemonte','MI','Milano',NULL,'Via da qui',3,'2020-11-05 21:53:38',0,0);

insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (10,'Italy',2,'Piemonte','TO','Torino',NULL,'Via da qui',1,'2020-11-04 10:16:04',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (2,'France',1,NULL,NULL,'Parigi',NULL,'Viaooo',1,'2020-11-04 10:16:50',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (1,'USA',-5,NULL,NULL,'New York',NULL,NULL,1,'2020-11-04 10:21:14',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (3,'Italy',2,'Piemonte','MI','Milano',NULL,'Via da qui',1,'2020-11-05 21:53:38',0,0);



/* salesmeet */

insert into salesmeet.company_registry (name,logo,site) values ("SalesMeet","","salesmeet.it");
insert into salesmeet.itmpany_action (id_company,action_js) values (6,"");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Corrado","Facchini","3498603096","facchini.corrado@gmail.com",6,"https://salesmeet.it/wp-content/uploads/2020/11/corrado.png");
insert into salesmeet.experts (name,surname,telephone,email,id_company,img) values ("Federico","Remiti","3498603096","info@salesmeet.it",6,"https://salesmeet.it/wp-content/uploads/2020/11/federico.png");

insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("20 anni nel settore tech.","","","",21,1);
insert into salesmeet.experts_details (specialization,features,keyword_work,keywords_life,id_expert,id_language) values ("20 anni nel settore tech a livello internazionale.","","","",22,1);

insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (1,'Italy',2,'','','',NULL,'',6,'2020-11-04 10:16:04',0,NULL);
insert into salesmeet.location (`type`,`country`,`timezone`,`region`,`province`,`city`,`email`,`street`,`id_company`,`data_init`,`active`,`sequence`) VALUES (3,'Italy',2,'Piemonte','','',NULL,'',6,'2020-11-05 21:53:38',0,0);
