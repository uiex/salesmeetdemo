<?php
error_reporting(E_ERROR);
session_start();

// Class per gestione ed inserimento appuntamento nel DB
include("db/db.php");
$db = new db();

$id_venditore = "v";
$id_cliente = "c";
$id_token = "";
$id_favourite_language = "en";

// echo $_SERVER['HTTP_USER_AGENT'] . "<br>";

if (isset($_GET['token'])) {

    $id_token = $_GET['token'];
    // Controllo se la tipologia di utente è valida
    $id_token_venditore_o_cliente = substr($id_token, -1);
    if (!in_array($id_token_venditore_o_cliente, array($id_venditore, $id_cliente))) {
        echo "Invalid appointment. Error 2001.";
        exit;
    }
    // Prendo il token vero
    $id_token = substr($id_token, 0, -1);

    $appointment = $db->getAppointment($id_token);
    $appointmentObj = json_decode($appointment, true);
    if (count($appointmentObj)==0) {
        echo "Invalid appointment. Error 2002.";
        exit;
    }
    $id_appointment = $appointmentObj[0]["id_appointment"];
    $_SESSION["id_token_uiex"] = $appointmentObj[0]["tokenuiex"];
    $_SESSION["id_appointment"] = $id_appointment;

    $referer = $appointmentObj[0]["referer"];
    $data_init = $appointmentObj[0]["data_init"];
    $site = $appointmentObj[0]["site"];
    if ( ($appointmentObj[0]["favourite_language"]!="") || (!is_null($appointmentObj[0]["favourite_language"] ) ) ) {
        $id_favourite_language = $appointmentObj[0]["favourite_language"];
    }

    $data_init = strtotime($data_init);
    // Video - Cliente
    if ($appointmentObj[0]["name"]!="") {
      $salesMeetClienteUserNameDefault = $appointmentObj[0]["name"];
      $salesMeetClienteUserNickDefault = $appointmentObj[0]["name"];
    } else {
      $salesMeetClienteUserNameDefault = $appointmentObj[0]["email"];
      $salesMeetClienteUserNickDefault = $appointmentObj[0]["email"];
    }


    $expert = $db->getExpert($id_appointment);
    $expertObj = json_decode($expert, true);
    // echo $expert;

    // Video - Venditore
    $salesMeetVenditoreUsernameDefault = $expertObj[0]["name"];
    $salesMeetVenditoreNickDefault = $expertObj[0]["name"];

    $id_company = $appointmentObj[0]["id_company"];
    $company = $db->getCompany($id_company);
    $companyObj = json_decode($company, true);

    $companyName = $companyObj[0]["name"];
    $companyLogo = $companyObj[0]["logo"];

} else {

    echo "Invalid appointment. Error 2003.";
    exit;

}

include("language/" . $id_favourite_language . ".php");

// ********* ID call salesmeet
$salesmeetidLabel = "salesmeetid";
$salesMeetId = $data_init;

// Domain ************************************************************************************************************
// ******** Domain
$salesMeetDomain = "https://app.salesmeet.it";

// Video ************************************************************************************************************
// Video - Chiave software
$salesMeetApplicationKey = "689276a8-8ff9-4d2a-92ec-99dbe934d029";

// Video - Venditore
$salesMeetVenditoreUsername = "venditore" . $salesMeetId;
$salesMeetVenditorePassword = "bella10";
$salesMeetVenditoreNick= $salesMeetVenditoreNickDefault;

// Video - Cliente
$salesMeetClienteUserName = "cliente" . $salesMeetId;
$salesMeetClienteUserPassword = "prova10";
$salesMeetClienteUserNick = $salesMeetClienteUserNickDefault;

// Co-browsing *********
/*
$salesMeetPathCobrowsingHub = "https://cobrowsinghub.salesmeet.it"; //"https://www.togetherjssalesmeet.it";
$salesMeetPathCobrowsing = "https://app.salesmeet.it/co-browsing/venditore.php?#&togetherjs=";
*/

?>
