<?php

//require_once('vendor/autoload.php'); // Loads the library
require_once('vendor/autoload.php'); // corrado
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class Twilio_Auth
{
    protected $accountSid = 'AC68dbb3d8d44dabf5551254a0cafe180b';
    protected $apiKeySid = 'SKdc42e92c7ae4062e61337f1aa561c75d';
    protected $apiKeySecret = 'PEpvsKY7AM0EeEu376QfNA0fV7PivDma';

    public function get_twilio_token($identity, $roomName)
    {
        // Create an Access Token
        $token = new AccessToken(
            $this->accountSid,
            $this->apiKeySid,
            $this->apiKeySecret,
            3600,
            $identity
        );
        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom($roomName);
        $token->addGrant($grant);

        // Serialize the token as a JWT
        return $token->toJWT();
    }
}
