<?php header("Access-Control-Allow-Origin: *");

include("../setting.php");

$token = (!empty($_GET['token'])) ? $_GET['token'] : '';
$video = (!empty($_GET['video'])) ? $_GET['video'] : '';
$salesMeetroomName = $id_token;

if ($id_token_venditore_o_cliente==$id_venditore ) {
    $salesMeetUsername = $salesMeetVenditoreUsername;
    $salesMeetPassword = $salesMeetVenditorePassword;
    $salesMeetNick = $salesMeetVenditoreNick;
} elseif ($id_token_venditore_o_cliente==$id_cliente ) {
    $salesMeetUsername = $salesMeetClienteUserName;
    $salesMeetPassword = $salesMeetClienteUserPassword;
    $salesMeetNick = $salesMeetClienteUserNick;
}
?>
<?php if ($video!="") { ?>
    <!doctype html>
    <html lang="en">
       <head>
          <meta charset="utf-8"/>
          <meta name="viewport" content="width=device-width,initial-scale=1"/>
              <script src="//media.twiliocdn.com/sdk/js/video/releases/2.7.2/twilio-video.min.js"></script>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
              <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
              <script src="common.js"></script>
       </head>
       <body>
         <input name="identity" id="identity" value="<?php echo $salesMeetNick; ?>">
         <input name="roomName" id="roomName" value="<?php echo $id_token; ?>">
         <button onclick="salesmeetInitVideo();" value="Connect"><?php echo $etichetta_video_connect; ?></button>
         <button onclick="salesmeetDisconnectVideo();" value="Disconnect"><?php echo $etichetta_video_disconnect; ?></button>
         <button onclick="salesmeetMuteVideo();" value="Disconnect"><?php echo $etichetta_video_sound_on; ?></button>
         <button onclick="salesmeetDeMuteVideo();" value="Disconnect"><?php echo $etichetta_video_sound_off; ?></button>
         <div id="sezionevideo"></div>
         <hr>
       </body>
    </html>

<?php } else { ?>

      <input name="identity" id="identity" type="hidden" value="<?php echo $salesMeetNick; ?>">
      <input name="roomName" id="roomName" type="hidden" value="<?php echo $id_token; ?>">
      <div style="background:#0095dd;color:#fff;width:100%;border-bottom: 1px solid #000;clear:both;padding-left: 2px;">
          <?php echo $etichetta_video; ?>
          &nbsp;<a href="javascript:salesmeetVideoSmall();"><img style="height:9px;" src="https://app.salesmeet.it/asset/img/rectangle.png"></a>
          &nbsp;<a href="javascript:salesmeetVideoNormal();"><img style="height:11px;" src="https://app.salesmeet.it/asset/img/rectangle.png"></a>
          &nbsp;<a href="javascript:salesmeetVideoBig();"><img style="height:13px;" src="https://app.salesmeet.it/asset/img/rectangle.png"></a>
          &nbsp;<img src="https://app.salesmeet.it/asset/img/dragndrop.png" style="cursor: move;max-height:15px;top: 2px;position: absolute;">
      </div>
      <div style="width:100%;clear:both;">
        <button id="salesmeetConnect" class="" onclick="salesmeetInitVideo();" value=""><?php echo $etichetta_video_connect; ?></button>
        <button id="salesmeetDisconnect" class="button_non_visibile" onclick="salesmeetDisconnectVideo();" value=""><?php echo $etichetta_video_disconnect; ?></button>
        <button id="salesmeetSoundOff" class="button_non_visibile" onclick="salesmeetSoundOff();" value=""><?php echo $etichetta_video_sound_on; ?></button>
        <button id="salesmeetSoundOn" class="button_non_visibile" onclick="salesmeetSoundOn();" value=""><?php echo $etichetta_video_sound_off; ?></button>
        <button id="salesmeetVideoOff" class="button_non_visibile" onclick="salesmeetVideoOff();" value="">Video off</button>
        <button id="salesmeetVideoOn" class="button_non_visibile" onclick="salesmeetVideoOn();" value="">Video on</button>
      </div>
      <div id="sezionevideo" class="salesmeettesto" style="clear:both;"><div><br><?php echo $etichetta_video_click_to_connect; ?></div></div>

<?php } ?>
