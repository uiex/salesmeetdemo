<?php header("Access-Control-Allow-Origin: *"); ?>
<?php

require_once('vendor/autoload.php'); // corrado
use Twilio\Rest\Client;


$roomName = (!empty($_GET['roomName'])) ? $_GET['roomName'] : '';
$token = (!empty($_GET['token'])) ? $_GET['token'] : '';

$sid    = "AC68dbb3d8d44dabf5551254a0cafe180b";
$twilio = new Client($sid, $token);

$room = $twilio->video->v1->rooms($roomName)
                          ->update("completed");

print($room->uniqueName);
