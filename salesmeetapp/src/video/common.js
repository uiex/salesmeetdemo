
let gloablToken = "";
let gloablRoomName = "";
let gloablVideo;
let gloablRoom;


$( function() {
  $( "#salesmeetvideo" ).draggable();
} );

function salesmeetInitVideo() {
          var identity = document.getElementById('identity').value;
          var roomName = document.getElementById('roomName').value;
          document.getElementById("sezionevideo").innerHTML = "<div>" + document.getElementById('salesmeetEtichettaVideoWaiting').value + "</div>";
          salesmeetGetToken(identity,roomName)
}
function salesmeetGetToken(identity,roomName) {
          console.log("salesmeetGetToken");
          var oReq = new XMLHttpRequest();
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;
              salesmeetStartVideo(arraybuffer,roomName);
          }
          oReq.open("GET", "https://app.salesmeet.it/video/get_token.php?identity=" + identity + "&roomName=" + roomName);
          var formData = new FormData();
          formData.append("token", "" );
          oReq.send( formData );
}

function salesmeetStartVideo(token,roomName) {

          console.log("salesmeetStartVideo");
          const Video = Twilio.Video;
          gloablVideo = Video
          gloablToken = token;
          gloablRoomName = token;

           Video.connect(token, { name: roomName }).then(room => {
                 console.log('Connected to Room "%s"', room.name);

                 gloablRoom = room;


                 // Log your Client's LocalParticipant in the Room
                 /*
                 const localParticipant = room.localParticipant;
                 console.log(`Connected to the Room as LocalParticipant "${localParticipant.identity}"`);
                 participantConnected(localParticipant);
                 */

                 /*
                 // Log any Participants already connected to the Room
                 room.participants.forEach(participant => {
                   console.log(`Participant "${participant.identity}" is connected to the Room`);
                 });

                 // Log new Participants as they connect to the Room
                 room.once('participantConnected', participant => {
                   console.log(`Participant "${participant.identity}" has connected to the Room`);
                 });

                 // Log Participants as they disconnect from the Room
                 room.once('participantDisconnected', participant => {
                   console.log(`Participant "${participant.identity}" has disconnected from the Room`);
                 });

                 room.on('participantConnected', participant => {
                   console.log(`A remote Participant connected: ${participant}`);
                 });
                 */


                 room.participants.forEach(participantConnected);
                 room.on('participantConnected', participantConnected);

                 room.on('participantDisconnected', participantDisconnected);
                 room.once('disconnected', error => room.participants.forEach(participantDisconnected));
                 /**/

                 salesmeetConnectCss();

           }, error => {
             console.error(`Unable to connect to Room: ${error.message}`);
           });
 }


 function participantConnected(participant) {
   console.log('Participant "%s" connected', participant.identity);

   const div = document.createElement('div');
   div.id = participant.sid;
   div.innerText = participant.identity;

   participant.on('trackSubscribed', track => trackSubscribed(div, track));
   participant.on('trackUnsubscribed', trackUnsubscribed);

   participant.tracks.forEach(publication => {
     if (publication.isSubscribed) {
       console.log("isSubscribed");
       trackSubscribed(div, publication.track);
     }
   });
   /*
   if (gloablTipologia=="") {
     document.getElementById("videoPartecipante").appendChild(div);
   } else {
     document.getElementById("videoVenditore").appendChild(div);
   }
   */
   document.getElementById("sezionevideo").innerHTML = "";
   document.getElementById("sezionevideo").appendChild(div);
   // document.body.appendChild(div);
 }

 function participantDisconnected(participant) {
   console.log('Participant "%s" disconnected', participant.identity);
   document.getElementById(participant.sid).remove();
 }


 function trackSubscribed(div, track) {
   console.log("trackSubscribed");
   div.appendChild(track.attach());
 }
 function trackUnsubscribed(track) {
   console.log("trackUnsubscribed");
   track.detach().forEach(element => element.remove());
 }

function salesmeetSoundOff(){
   console.log("salesmeetSoundOff");
   gloablRoom.localParticipant.audioTracks.forEach(publication => {
     publication.track.disable();
     console.log(publication);
   });
   salesmeetVideoOff();
   salesmeetSoundOffCss();
}
function salesmeetSoundOn(){
   console.log("salesmeetSoundOn");
   gloablRoom.localParticipant.audioTracks.forEach(publication => {
     publication.track.enable();
   });
   salesmeetVideoOn();
   salesmeetSoundOnCss();
}
function salesmeetVideoOff(){
  console.log("salesmeetVideoOff");
   gloablRoom.localParticipant.videoTracks.forEach(publication => {
     publication.track.disable();
     console.log(publication);
   });
}
function salesmeetVideoOn(){
   console.log("salesmeetVideoOn");
   gloablRoom.localParticipant.videoTracks.forEach(publication => {
     publication.track.enable();
   });
}

function salesmeetDisconnectVideo(){
    console.log("salesmeetDisconnectVideo");
    gloablRoom.disconnect();
    salesmeetDisconnectCss();
    document.getElementById("sezionevideo").innerHTML = "<div>" + document.getElementById('salesmeetEtichettaVideoClickToConnect').value + "</div>";

}

function salesmeetDisconnectCss(){
  // document.getElementById("myDIV").style.display = "none";
  document.getElementById("salesmeetDisconnect").classList.add("button_non_visibile");
  document.getElementById("salesmeetSoundOff").classList.add("button_non_visibile");
  document.getElementById("salesmeetSoundOn").classList.add("button_non_visibile");
  document.getElementById("salesmeetVideoOff").classList.add("button_non_visibile");
  document.getElementById("salesmeetVideoOn").classList.add("button_non_visibile");
  document.getElementById("salesmeetConnect").classList.remove("button_non_visibile");
}
function salesmeetConnectCss(){
  document.getElementById("salesmeetDisconnect").classList.remove("button_non_visibile");
  document.getElementById("salesmeetSoundOff").classList.remove("button_non_visibile");
  document.getElementById("salesmeetConnect").classList.add("button_non_visibile");
}
function salesmeetSoundOffCss(){
  document.getElementById("salesmeetSoundOn").classList.remove("button_non_visibile");
  document.getElementById("salesmeetSoundOff").classList.add("button_non_visibile");
}
function salesmeetSoundOnCss(){
  document.getElementById("salesmeetSoundOn").classList.add("button_non_visibile");
  document.getElementById("salesmeetSoundOff").classList.remove("button_non_visibile");
}

function salesmeetVideoBig() {
    console.log("salesmeetVideoBig");
    document.getElementById("salesmeetvideo").classList.add("video_big");
    document.getElementById("salesmeetvideo").classList.remove("video_small");
}
function salesmeetVideoSmall() {
    console.log("salesmeetVideoSmall");
    document.getElementById("salesmeetvideo").classList.remove("video_big");
    document.getElementById("salesmeetvideo").classList.add("video_small");
}
function salesmeetVideoNormal() {
    console.log("salesmeetVideoNormal");
    document.getElementById("salesmeetvideo").classList.remove("video_big");
    document.getElementById("salesmeetvideo").classList.remove("video_small");
}
