var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

function salesmeetDisplayShareScreen() {
    var temp = `
        Desktop:<br>
        - Possibilità di condividere lo schermo per dare la possibilità di fare vedere materiale aggiuntivo o migliorativo per la vendita ...<br>
        Mobile:<br>
        - Modalità sempre attiva. Il co-browsing non risulta così efficace come lo share screen.<br>
        <br><br><br>
        Il serizio si prende direttamente da Twilio
    `;
    salesmeetDisplayModale(temp);
}
function salesmeetDisplayNote() {
    var temp = `
        Possibilità di prendere nota durante la video call...
    `;
    salesmeetDisplayModale(temp);
}
function salesmeetDisplayMenuMobile() {
    var els = document.getElementsByClassName("salesmeet-menu-venditore");
    var menu = "";
    for(var i=0; i<els.length; i++)
    {
        menu = menu + '<div class="salesmeet-menu-venditore-mobile">' + els[i].innerHTML + "</div>";
    }
    salesmeetDisplayModale(menu);
}

function salesmeetDisplayModale(value) {
    $( "#salesmeet-div-modale" ).show();
    var temp =  salesmeetGetAddCloseModale() + "<div>" + value + "</div>";
    $( "#salesmeet-div-modale" ).html( temp );
}
function salesmeetGetAddCloseModale() {
    return `<div class="salesmeet-close">
              <button class="salesmeetbutton" onclick="javascript:salesmeetCloseModale();">Close</button>
            </div>`;
}
function salesmeetCloseModale() {
    $( "#salesmeet-div-modale" ).hide();
}
