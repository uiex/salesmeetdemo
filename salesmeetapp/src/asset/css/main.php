<style>
html,body {
    padding:0;
    margin:0;
    height:100%;
}

#salesmeet-div-analytics {
    height: 97%;
    width: 100%;
    border-top: 1px solid #000;
    display: none;
    position: fixed;
    z-index: 9999;
    right: 0px;
    top: 3%;
    background-color: #fff;
    overflow: scroll;
}
.salesmeet-menu-voci {
    padding-left: 5px;
    padding-right: 5px;
    float: left;
    padding-top: 3px;
    color:#fff;
}
.salesmeet-contenuto {
    padding: 10px;
}
.salesmeettesto {
    color: #000000;
    /* font-size: 15px; */
    font-family: arial;
    padding-top: 6px;
}

/*
input[type="submit"], button, .button, .link-as-button {
*/
.salesmeet-button {
    font-family: arial !important;
    font-size: 12px !important;
    line-height: 4px !important;
    letter-spacing: 1px !important;
    cursor: pointer !important;
    user-select: none !important;
    width: 100px !important;
    padding: 12px !important;
    border-width: 1px !important;
    border-style: solid !important;
    border-color: transparent !important;
    border-image: initial !important;
    border-radius: 2px !important;
    outline: none !important;
}
button {
	color: #fff;
	background-color: #03A9F4;
	font-size: 14px;
	font-family: 'Roboto', sans-serif;
	border-radius: 0;
	vertical-align: bottom;
	border: none;
	outline: 0 none;
	padding: 0px 6px 0px 6px;
	float: left;
	margin: 2px;
  text-transform: lowercase;
  min-width: 10px;
  border-radius: 5px;
  height: 24px !important;
  min-height: auto;
}
button:hover {
  background-color: #008fcf;
}
.button_deactive {
	background-color: #7cd6ff;
}
.button_non_visibile {
	display: none;
}
#salesmeetvideo {
  -webkit-box-shadow: 0px 2px 5px 0px rgba(153,153,153,1);
  -moz-box-shadow: 0px 2px 5px 0px rgba(153,153,153,1);
  box-shadow: 0px 2px 5px 0px rgba(153,153,153,1);
  border-radius: 6px;
  border:1px solid #000;
}

#pageRank {
	padding: 14px;
}
#pageRankTitolo {
    padding-top: 40px;
    width: 100%;
}
#pageRankUtente {
    float: left;
    width: 50%;
}
#pageRankVisitatori {
    float: right;
    width: 50%;
}


video {
    max-width: 100%;
}
.video_big{
    width: 95% !important;
    height: 90% !important;
}
.video_small{
    width: 138px !important;
    height: 52px !important;
}

#salesmeet-div-modale {
    height: 97%;
    width: 100%;
    border-top: 1px solid #000;
    display: none;
    position: fixed;
    z-index: 9999;
    right: 0px;
    top: 3%;
    background-color: #ffffff;
    overflow: scroll;
}

.salesmeet-close{
    position:relative;
    width: 100%;
    height: 29px;
    display: flex;
    justify-content: center;
}

.salesmeet-menu-mobile{
    display:none;
}

@media screen and (max-width: 940px) {
    .salesmeet-menu{
        display:none;
    }
    .salesmeet-menu-mobile{
        display:block;
    }
    .salesmeet-menu-venditore-mobile {
        display:block;
        width: 100%;
        clear: both;
        padding-top: 20px;
        padding-left: 10px;
    }
}

</style>
