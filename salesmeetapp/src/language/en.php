<?php

// menu *****
$etichetta_menu_analytics = "Analytics";
$etichetta_menu_favorites = "Favorites";
$etichetta_menu_recommended_by_tag = "Recommended by tag";
$etichetta_menu_recommended_for_flow = "Recommended for flow";
$etichetta_menu_page_rank = "Page rank";
$etichetta_menu_exit = "Exit";
$etichetta_menu_dashboard = "Dashboard";
$etichetta_menu_share_screen = "Share screen";
$etichetta_menu_note = "Note";

// video *****
$etichetta_video = "Video";
$etichetta_video_connect = "Connect";
$etichetta_video_disconnect = "Disconnect";
$etichetta_video_sound_on = "Sound on";
$etichetta_video_sound_off = "Sound off";
$etichetta_video_click_to_connect = 'Click CONNECT to start the video';
$etichetta_video_waiting = "... Waiting for the other user";

// uiex *****
$etichetta_uiex_image = "Image";
$etichetta_uiex_title = "Title & link";
$etichetta_uiex_price = "Price";
$etichetta_uiex_currency = "Currency";
$etichetta_uiex_vote = "Vote";
$etichetta_uiex_close = "Close";

$etichetta_uiex_page_utente = "User";
$etichetta_uiex_page_visitatori = "All visitors";
$etichetta_uiex_page_voto_max = "Maximum rating";
$etichetta_uiex_page_voto_medio = "Average rating";
$etichetta_uiex_page_voto_min = "Minimum grade";
$etichetta_uiex_page_numero_visualizzazioni = "Number of views";
$etichetta_uiex_page_ultima_visita = "Last visit";

// co-browsing *****

?>
