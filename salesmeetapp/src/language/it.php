<?php

// menu *****
$etichetta_menu_analytics = "Analytics";
$etichetta_menu_favorites = "I preferiti";
$etichetta_menu_recommended_by_tag = "I consigliati per tag";
$etichetta_menu_recommended_for_flow = "I consigliati per flusso";
$etichetta_menu_page_rank = "Page rank";
$etichetta_menu_exit = "Exit";
$etichetta_menu_dashboard = "Dashboard";
$etichetta_menu_share_screen = "Share screen";
$etichetta_menu_note = "Note";

// video *****
$etichetta_video = "Video";
$etichetta_video_connect = "Connetti";
$etichetta_video_disconnect = "Disconnetti";
$etichetta_video_sound_on = "Volume on";
$etichetta_video_sound_off = "Volume off";
$etichetta_video_click_to_connect = 'Clicca su CONNETTI per avviare il video';
$etichetta_video_waiting = "... In attesa dell'altro utente";

// uiex *****
$etichetta_uiex_image = "Immagine";
$etichetta_uiex_title = "Titolo e link";
$etichetta_uiex_price = "Prezzo";
$etichetta_uiex_currency = "Valuta";
$etichetta_uiex_vote = "Voto";
$etichetta_uiex_close = "Chiudi";

$etichetta_uiex_page_utente = "Utente";
$etichetta_uiex_page_visitatori = "Tutti i visitatori";
$etichetta_uiex_page_voto_max = "Voto massimo";
$etichetta_uiex_page_voto_medio = "Voto medio";
$etichetta_uiex_page_voto_min = "Voto minimo";
$etichetta_uiex_page_numero_visualizzazioni = "Numero visualizzazioni";
$etichetta_uiex_page_ultima_visita = "Ultima visita";

// co-browsing *****

?>
