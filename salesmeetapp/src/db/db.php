<?php

class db {

    function getConnect() {
        $db   = 'salesmeet';
        /*
        $host = 'salesmeet-db';
        $user = 'root';
        $pass = 'your_mysql_root_password';
        $charset = 'utf8mb4';
        */
        // $host = 'salesmeet.caacentelger.eu-west-1.rds.amazonaws.com';
        $host = 'salesmeet.c9hljosmivib.eu-west-1.rds.amazonaws.com';
        $user = 'admin';
        $pass = '123#4567AszsaL3smeet';
        $charset = 'utf8';
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        try {
             // $pdo = new PDO($dsn, $user, $pass, $options);
             $pdo = new \PDO($dsn, $user, $pass);
             return $pdo;
        } catch (\PDOException $e) {
             throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    function getAppointment($id_token) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments WHERE id_token = ? ");
            $stmt->execute([$id_token]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getAppointmentById($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments WHERE id_appointment = ? ");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getExpert($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT b.* FROM salesmeet.calendar_experts a, salesmeet.experts b where a.id_appointment = ? and a.active = 0 and a.id_expert = b.id_expert");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getCompany($id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM salesmeet.company_registry where id_company = ?");
            $stmt->execute([$id_company]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getAppuntamentiCliente($email,$id_company) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT a.*, c.name nameexpert, c.surname surnameexpert, c.telephone telephoneexpert, c.email emailexpert FROM appointments a, calendar_experts b, experts c where b.id_expert = c.id_expert and a.id_appointment = b.id_appointment and a.email = ? and a.id_company = ?");
            $stmt->execute([$email,$id_company]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getAppointmentProdotti($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_products where id_appointment = ?");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getLinkAppointment($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_link WHERE id_appointment = ?");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    /*
    function updateAppointmentCobrowsing($id_cobrowsing,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set id_cobrowsing = ? WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_cobrowsing,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }
    */

    function getAppointmentCobrowsing($id_appointment) {
          $pdo = $this->getConnect();
          try {
              $stmt = $pdo->prepare("SELECT id_cobrowsing FROM appointments WHERE id_appointment = ? ");
              $stmt->execute([$id_appointment]);
              $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $json = json_encode($results);
              return $json;
          } catch( PDOExecption $e ) {
              echo "Error!: " . $e->getMessage() . "</br>";
      }
    }

    function createAppointmentCustomerDetail($width_screen,$heigth_screen,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments_details_customer (width_screen, heigth_screen, id_appointment) VALUES(?,?,?)");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array( $width_screen, $heigth_screen, $id_appointment ) );
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getExpertAppointment($id_expert) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT b.* FROM salesmeet.calendar_experts a, salesmeet.appointments b where a.id_expert = ? and a.active = 0 and a.id_appointment = b.id_appointment ");
            $stmt->execute([$id_expert]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getExpertAppointmentCalendar($id_expert,$time_slot_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT b.* FROM salesmeet.calendar_experts a, salesmeet.appointments b where a.id_expert = ? and a.active = 0 and a.id_appointment = b.id_appointment and b.state > 0 ");
            $stmt->execute([$id_expert]);
            $str = "events: [";
            $flag = false;
            while ($row = $stmt->fetch()) {
                $flag = true;
                $data_selezionata= $row['data_selezionata'];
                $date = new DateTime($data_selezionata);
                $datefine = new DateTime($date->format('d-m-Y H:i'));
                $datefine->modify('+' . $time_slot_appointment . ' minutes');
                $str .= "{";
                $str .= "title:'" . $row['name'] . " " . $row['surname'] . "',";
                $str .= "id:'" . $row['id_token'] . "',";
                if ($row['state']==1) {
                  $str .= "color: 'red',"; // override!
                }
                // $str .= "url:'" . $row['title'] . "',";
                $str .= "start:'" . $date->format('Y-m-d') . "T". $date->format('H:i:') . "00',";
                $str .= "end:'" . $datefine->format('Y-m-d') . "T". $datefine->format('H:i:') . "00'";
                $str .= "},";
            }
            if ($flag) {
              return substr($str,0,-1) . "]";
            } else {
              return $str . "]";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function createAppointmentNote($description,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("INSERT INTO appointments_note (description, id_appointment) VALUES(?,?)");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array( $description, $id_appointment ) );
                $id = $pdo->lastInsertId();
                $pdo->commit();
                return $id;
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function getAppointmentNotes($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("SELECT * FROM appointments_note where id_appointment = ? and visible = 0");
            $stmt->execute([$id_appointment]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $json = json_encode($results);
            return $json;
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

    function deleteAppointmentNote($id_appointment_note,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments_note set visible = 1 WHERE id_appointment_note = ? and id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_appointment_note,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                print "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            print "Error!: " . $e->getMessage() . "</br>";
        }
    }


    function updateAppointmentDataSelezionataConferma($id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set state = 2 WHERE id_appointment = ?");
            try {
                $pdo->beginTransaction();
                $stmt->execute( array($id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }
    function updateAppointmentDataSelezionata($data_selezionata,$id_appointment) {
        $pdo = $this->getConnect();
        try {
            $stmt = $pdo->prepare("UPDATE appointments set data_selezionata = ?, state = 2 WHERE id_appointment = ?");
            try {
                $date = new DateTime($data_selezionata);
                $data_selezionata = $date->format('Y-m-d H:i:s');
                $pdo->beginTransaction();
                $stmt->execute( array($data_selezionata,$id_appointment));
                $pdo->commit();
            } catch(PDOExecption $e) {
                $pdo->rollback();
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        } catch( PDOExecption $e ) {
            echo "Error!: " . $e->getMessage() . "</br>";
        }
    }

        function getEmailTemplate($type,$language,$id_company) {
            $pdo = $this->getConnect();
            try {
                $stmt = $pdo->prepare("SELECT description, subject FROM appointments_email_template WHERE type = ? and id_company = ? and language = ? ");
                $stmt->execute([$type,$id_company,$language]);
                while ($row = $stmt->fetch()) {
                    return array("description" =>  $row['description'], "subject" =>  $row['subject']);
                }
                if ($id_company==0) {
                    return "";
                } else {
                    return $this->getEmailTemplate($type,$language,0);
                }
            } catch( PDOExecption $e ) {
                echo "Error!: " . $e->getMessage() . "</br>";
            }
        }

}

?>
