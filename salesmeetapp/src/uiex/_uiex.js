function getPreferitiDTable(token,user,site,url) {

    // init(token,site);
    salesmeetAddDataTableContenuto();
    salesmeetDisplayUiex();
    salesmeeDisplayDataTable();
    var table = $('#preferiti').DataTable( {
        "ajax": {
            "url": "https://uiextools.salesmeet.it/chronology/experience/",
            "type": "POST",
            "dataSrc": "",
            "data" : {
                "token" : token,
                "user": user,
                "site": site
            }
        },
        "columns": [
            { "data": "img",
                "render": function (data, type, row) {
                    if (data == '') {
                        return '<img src="https://app.salesmeet.it/asset/images/picture.png" style="height:25px" />';
                    }  else {
                        return '<img src="' + data + '" style="height:45px" />';
                    }
                }
            },
            { "data": "title",
                "render": function (data, type, row) {
                    return salesmeetCreateUrl(row.url,data);
                }
            },
            { "data": "price"},
            { "data": "currency"},
            { "data": "voto_AI",
                "render": function (data, type, row) {
                    return this.stelle( data );
                }
            },
        ]
    } );
}

function getConsigliatiDTable(token,user,site,url) {

    salesmeetAddDataTableContenuto();
    salesmeetDisplayUiex();
    salesmeeDisplayDataTable();
    $('#preferiti').dataTable().fnClearTable();
    $('#preferiti').dataTable().fnDestroy();
    var words = salesmeetGetCookie("UserIntelligentExperienceAppwords");
    var currentUrl = salesmeetGetCookie("UserIntelligentExperienceAppCurrentUrl");
    var sito = salesmeetGetCookie("UserIntelligentExperienceAppUrl");
    var idBefore = "";
    var titleConsigliato = "";
    var schemaType = "";
    var ageGender = "";
    var table = $('#preferiti').DataTable( {
        "ajax": {
            "url": "https://uiextools.salesmeet.it/consigliati/pages/words/",
            "type": "POST",
            "dataSrc": "",
            "data" : {
                "token" : token,
                "user": user,
                "site": sito,
                "sito": sito,
                "idBefore" : idBefore,
                "titleConsigliato": titleConsigliato,
                "words" : words,
                "schemaType": schemaType,
                "ageGender" : ageGender
            }
        },
        "columns": [
            { "data": "Meta.ImgUrl",
                "render": function (data, type, row) {
                    if (data == '') {
                        return '<img src="https://app.salesmeet.it/asset/images/picture.png" style="height:25px" />';
                    }  else {
                        return '<img src="' + data + '" style="height:45px" />';
                    }
                }
            },
            { "data": "Meta.Title",
                "render": function (data, type, row) {
                    return salesmeetCreateUrl(row.Meta.Url,data);
                }
            },
            { "data": "Price.Price"},
            { "data": "Price.Currency"},
            { "data": "NFind",
                "render": function (data, type, row) {
                    return this.dart( data );
                }
            }
        ]
    } );
}

function getConsigliatiFlussoDTable(token,user,site,url) {

    // init(token,site);
    salesmeetAddDataTableContenuto();
    salesmeetDisplayUiex();
    salesmeeDisplayDataTable();
    $('#preferiti').dataTable().fnClearTable();
    $('#preferiti').dataTable().fnDestroy();
    var words = salesmeetGetCookie("UserIntelligentExperienceAppwords");
    var currentUrl = salesmeetGetCookie("UserIntelligentExperienceAppCurrentUrl");
    var sito = salesmeetGetCookie("UserIntelligentExperienceAppUrl");

    var idBefore = "";
    var titleConsigliato = "";
    var schemaType = "";
    var ageGender = "";
    var table = $('#preferiti').DataTable( {
        "ajax": {
            "url": "https://uiextools.salesmeet.it/consigliati/",
            "type": "POST",
            "dataSrc": "",
            "data" : {
                "token" : token,
                "user": user,
                "site": sito,
                "sito": sito,
                "idBefore" : idBefore,
                "titleConsigliato": titleConsigliato,
                "words" : words,
                "schemaType": schemaType,
                "ageGender" : ageGender
            }
        },
        "columns": [
            { "data": "Imgurl",
                "render": function (data, type, row) {
                    if (data == '') {
                        return '<img src="https://app.salesmeet.it/asset/images/picture.png" style="height:25px" />';
                    }  else {
                        return '<img src="' + data + '" style="height:45px" />';
                    }
                }
            },
            { "data": "Pagina.0.Meta.Title",
                "render": function (data, type, row) {
                    return salesmeetCreateUrl(row.Url,data);
                }
            },
            { "data": "Pagina.0.Price.Price"},
            { "data": "Pagina.0.Price.Currency"},
            { "data": "Pagina.0.Somma",
                "render": function (data, type, row) {
                    return this.dart( row.Somma );
                }
            }
        ]
    } );
}

function pageRank(token,site) {
    pageRankCommon(token,site,"https://uiextools.salesmeet.it/chronology/page/single/",0);
    pageRankCommon(token,site,"https://uiextools.salesmeet.it/chronology/page/all/",1);
    salesmeetDisplayUiex();
    salesmeetHiddenDataTable();
}
function pageRankCommon(token,site,urlUiex,flag) {
    var words = salesmeetGetCookie("UserIntelligentExperienceAppwords");
    var sito = salesmeetGetCookie("UserIntelligentExperienceAppUrl");
    var currentUrl = salesmeetGetCookie("UserIntelligentExperienceAppCurrentUrl");
    var oReq = new XMLHttpRequest();
    oReq.onload = function(e) {
        var arraybuffer = oReq.response;
        var jsonData = JSON.parse(arraybuffer);
        console.log(jsonData);
        for (var i = 0; i < jsonData.length; i++) {
            if (flag==0) {
              $( "#pageRankTitolo" ).html(jsonData[i]["Title"]);
              $( "#pageRankImg" ).attr("src",jsonData[i]["Imgurl"]);
              $( "#pageRankSingleVotomax" ).html( stelle( jsonData[i]["Votomax"]) );
              $( "#pageRankSingleVotomedio" ).html( stelle( jsonData[i]["Votomedio"]) );
              $( "#pageRankSingleVotomin" ).html( stelle( jsonData[i]["Votomin"]) );
              $( "#pageRankSingleCount" ).html(jsonData[i]["Count"]);
            } else {
              $( "#pageRankAllMaxVoto" ).html( stelle( jsonData[i]["Statistic"]["MaxVoto"]) );
              $( "#pageRankAllMediaVoto" ).html( stelle( jsonData[i]["Statistic"]["MediaVoto"]) );
              $( "#pageRankAllMinVoto" ).html( stelle( jsonData[i]["Statistic"]["MinVoto"]) );
              $( "#pageRankAllCount" ).html(jsonData[i]["Statistic"]["Count"]);
              $( "#pageRankAllLastVisit" ).html(jsonData[i]["Statistic"]["LastVisit"]);
            }
        }
    }
    oReq.open("POST", urlUiex);
    var formData = new FormData();
    formData.append("token", token );
    formData.append("user", "" );
    formData.append("site", sito);
    formData.append("sito", sito);
    formData.append("referer", currentUrl);
    formData.append("idBefore", "" );
    formData.append("titleConsigliato", "");
    formData.append("words", words);
    formData.append("schemaType", "");
    formData.append("ageGender", "");
    oReq.send( formData );
}

function init(token,site) {
    var words = salesmeetGetCookie("UserIntelligentExperienceAppwords");
    var sito = salesmeetGetCookie("UserIntelligentExperienceAppUrl");
    var currentUrl = salesmeetGetCookie("UserIntelligentExperienceAppCurrentUrl");
    var oReq = new XMLHttpRequest();
    oReq.onload = function(e) {
        var arraybuffer = oReq.response;
        var jsonData = JSON.parse(arraybuffer);
        console.log(jsonData);
    }
    oReq.open("POST", "https://uiextools.salesmeet.it/consigliati/");
    // oReq.open("POST", "https://uiextools.salesmeet.it/consigliati/");
    // oReq.open("POST", "https://uiextools.salesmeet.it/chronology/experience/");
    // oReq.open("POST", "https://uiextools.salesmeet.it/consigliati/pages/words/");
    var formData = new FormData();
    formData.append("token", token );
    formData.append("user", "" );
    formData.append("site", sito);
    formData.append("sito", sito);
    formData.append("referer", currentUrl);
    formData.append("idBefore", "" );
    formData.append("titleConsigliato", "");
    formData.append("words", words);
    formData.append("schemaType", "");
    formData.append("ageGender", "");
    oReq.send( formData );
}


function salesmeetAddDataTableContenuto() {

    var salesmeetEtichettaUiexImage = document.getElementById('salesmeetEtichettaUiexImage').value;
    var salesmeetEtichettaUiexTitle = document.getElementById('salesmeetEtichettaUiexTitle').value;
    var salesmeetEtichettaUiexPrice = document.getElementById('salesmeetEtichettaUiexPrice').value;
    var salesmeetEtichettaUiexCurrency = document.getElementById('salesmeetEtichettaUiexCurrency').value;
    var salesmeetEtichettaUiexVote = document.getElementById('salesmeetEtichettaUiexVote').value;
    $( "#dataTableContenuto" ).html(`
        <table id="preferiti" class="display" style="width:100%">
            <thead>
                <tr>
                  <th>${salesmeetEtichettaUiexImage}</th>
                  <th>${salesmeetEtichettaUiexTitle}</th>
                  <th>${salesmeetEtichettaUiexPrice}</th>
                  <th>${salesmeetEtichettaUiexCurrency}</th>
                  <th>${salesmeetEtichettaUiexVote}</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                  <th>${salesmeetEtichettaUiexImage}</th>
                  <th>${salesmeetEtichettaUiexTitle}</th>
                  <th>${salesmeetEtichettaUiexPrice}</th>
                  <th>${salesmeetEtichettaUiexCurrency}</th>
                  <th>${salesmeetEtichettaUiexVote}</th>
                </tr>
            </tfoot>
        </table>
    `);
}


function stelle( value ) {
    var stelle = "";
    var voto_AItemp = parseInt( value ).toString().length / 2;
    for (var i = 0; i < Math.floor(voto_AItemp); i++) {
        stelle += `<img style="display: inline-block;" src="https://app.salesmeet.it/asset/images/star.png">`;
    }
    if (voto_AItemp.toString().indexOf(".") != -1) {
        stelle += `<img style="display: inline-block;" src="https://app.salesmeet.it/asset/images/star.png">`;
    }
    return stelle;
}
function dart( value ) {
    var dart = "";
    var value = value * 1;
    for (var i = 0; i < Math.floor(value); i++) {
        dart += `<img style="display: inline-block;" src="https://app.salesmeet.it/asset/images/dart.png">`;
    }
    if (value.toString().indexOf(".") != -1) {
        dart += `<img style="display: inline-block;" src="https://app.salesmeet.it/asset/images/dart.png">`;
    }
    return dart;
}

function salesmeetDisplayUiex() {
    $( "#salesmeet-div-analytics" ).css({ height: SalesMeetAppInstance.heightWithoutMenu + 'px', top: SalesMeetAppInstance.heightMenu + 'px', });
    $( "#salesmeet-div-analytics" ).show();
    $( "#salesmeetvideo" ).css({ position: "initial" });
    salesmeetHiddenSite();
    salesmeetCloseModale();
}
function salesmeeDisplayDataTable() {
    $( "#dataTableContenuto" ).show();
    $( "#pageRank" ).hide();
}
function salesmeetHiddenUiex() {
    $( "#salesmeet-div-analytics" ).hide();
    $( "#salesmeetvideo" ).css({ position: "fixed" });
    salesmeetDisplaySite();
}
function salesmeetHiddenDataTable() {
    $( "#dataTableContenuto" ).hide();
    $( "#pageRank" ).show();
}
function salesmeetDisplaySite() {
    $( "#salesmeetsite" ).show();
}
function salesmeetHiddenSite() {
    $( "#salesmeetsite" ).hide();
}

function salesmeetGoToUrl(url) {
    document.getElementById("salesmeetcobrowsing").src = url;
    salesmeetHiddenUiex();
}
function salesmeetCreateUrl(url,title) {
    // var urlTrack = userIntelligentExperienceInternalAppInstance.addTrack( row.Url, 3 );
    return'<a href="javascript:salesmeetGoToUrl(\'' + url + '\')"><img src="https://app.salesmeet.it/asset/img/link.png" style="padding-right: 9px;width: 12px;">' + title + '</a>'
}




function salesmeetGetCookie(cname) {
    var temp = localStorage.getItem(cname);
    if (temp==null) {
        return "";
    } else {
        return temp;
    }
}
