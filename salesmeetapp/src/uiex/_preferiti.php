<div id="salesmeet-div-analytics" style="display:none;">
  <div class="salesmeet-close">
    <button class="salesmeetbutton" onclick="javascript:salesmeetHiddenUiex();"><?php echo $etichetta_uiex_close; ?></button>
  </div>
  <div class="salesmeet-contenuto" id="dataTableContenuto">
      <table id="preferiti" class="display" style="width:100%">
          <thead>
              <tr>
                <th>Image</th>
                <th>Title & link</th>
                <th>Price</th>
                <th>Currency</th>
                <th>Vote</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                <th>Image</th>
                <th>Title & link</th>
                <th>Price</th>
                <th>Currency</th>
                <th>Vote</th>
              </tr>
          </tfoot>
      </table>
  </div>

  <div class="contenuto" id="pageRank">
      <div id="pageRankTitolo"></div>
      <div><img id="pageRankImg" src="" style="max-height: 100px;"></div>
      <br><br>
      <div id="pageRankUtente">
          <hr>
          <?php echo $etichetta_uiex_page_utente; ?>:
          <hr>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_max; ?>:</b> <div id="pageRankSingleVotomax"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_medio; ?>:</b> <div id="pageRankSingleVotomedio"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_min; ?>:</b> <div id="pageRankSingleVotomin"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_numero_visualizzazioni; ?>:</b> <div id="pageRankSingleCount"></div></div>
      </div>
      <div id="pageRankVisitatori">
          <hr>
          <?php echo $etichetta_uiex_page_visitatori; ?>:
          <hr>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_max; ?>:</b> <div id="pageRankAllMaxVoto"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_medio; ?>:</b> <div id="pageRankAllMediaVoto"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_voto_min; ?>:</b> <div id="pageRankAllMinVoto"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_numero_visualizzazioni; ?>:</b> <div id="pageRankAllCount"></div></div>
          <div id=""><b><?php echo $etichetta_uiex_page_ultima_visita; ?>:</b> <div id="pageRankAllLastVisit"></div></div>
      </div>
  </div>

</div>
