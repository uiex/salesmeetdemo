<?php
header("Access-Control-Allow-Origin: *");
include("setting.php");
?>

<?php include("asset/css/main.php"); ?>
<?php include("asset/datatables/jquery.dataTables.min.php"); ?>

<?php /* include("uiex/preferiti.php"); */ ?>

<script>
function addCookie(cname, cvalue) {
    localStorage.setItem(cname, cvalue);
}
addCookie( "UserIntelligentExperienceAppUrl" ,this.currentDomain);
addCookie( "UserIntelligentExperienceAppCurrentUrl" ,this.currentUrl);
</script>

<!-- etichette per il javascript -->
<input type="hidden" id="salesmeetEtichettaVideoWaiting" name="salesmeetEtichettaVideoWaiting" value="<?php echo $etichetta_video_waiting; ?>">
<input type="hidden" id="salesmeetEtichettaVideoClickToConnect" name="salesmeetEtichettaVideoClickToConnect" value="<?php echo $etichetta_video_click_to_connect; ?>">
<input type="hidden" id="salesmeetEtichettaUiexImage" name="salesmeetEtichettaUiexImage" value="<?php echo $etichetta_uiex_image; ?>">
<input type="hidden" id="salesmeetEtichettaUiexTitle" name="salesmeetEtichettaUiexTitle" value="<?php echo $etichetta_uiex_title; ?>">
<input type="hidden" id="salesmeetEtichettaUiexPrice" name="salesmeetEtichettaUiexPrice" value="<?php echo $etichetta_uiex_price; ?>">
<input type="hidden" id="salesmeetEtichettaUiexCurrency" name="salesmeetEtichettaUiexCurrency" value="<?php echo $etichetta_uiex_currency; ?>">
<input type="hidden" id="salesmeetEtichettaUiexVote" name="salesmeetEtichettaUiexVote" value="<?php echo $etichetta_uiex_vote; ?>">
<input type="hidden" id="salesmeetEtichettaUiexClose" name="salesmeetEtichettaUiexClose" value="<?php echo $etichetta_uiex_close; ?>">

<?php
$link_dashboard = "javascript:window.open('https://app.salesmeet.it/dashboard/index.php?idtokencallsalesmeet=" . $id_token . "&emailcallsalesmeet=" . $expertObj[0]["email"] . "','belllllaa');";
$link_preferiti = "javascript:window.open('https://app.salesmeet.it/dashboard/preferiti.php?idtokencallsalesmeet=" . $id_token . "&emailcallsalesmeet=" . $expertObj[0]["email"] . "','belllllaa');";
$link_note = "javascript:window.open('https://app.salesmeet.it/dashboard/note.php?idtokencallsalesmeet=" . $id_token . "&emailcallsalesmeet=" . $expertObj[0]["email"] . "','belllllaa');";
?>
<div class="salesmeet-menu">
    <?php if ($id_token_venditore_o_cliente==$id_venditore ) { ?>
        <div class="salesmeet-menu-voci">
            <button onclick="<?php echo $link_dashboard; ?>" id=""><?php echo $etichetta_menu_dashboard;?></button>
        </div>
        <div class="salesmeet-menu-voci">
            <button class="salesmeetbutton" onclick="<?php echo $link_preferiti; ?>"><?php echo $etichetta_menu_analytics; ?></button>
        </div>
        <div class="salesmeet-menu-voci">
            <button onclick="<?php echo $link_note; ?>" id=""><?php echo $etichetta_menu_note;?></button>
        </div>
    <?php } ?>

    <div class="salesmeet-menu-voci" style="float: right;">
        <button onclick='window.location.href="https://<?php echo $site; ?>"' id=""><?php echo $etichetta_menu_exit;?></button>
    </div>
    <div class="salesmeet-menu-voci" style="float: right;">
        <button onclick="javascript:salesmeetDisplayShareScreen();" id=""><?php echo $etichetta_menu_share_screen;?></button>
    </div>
</div>

<!-- mobile -->
<div class="salesmeet-menu-mobile">

  <?php if ($id_token_venditore_o_cliente==$id_venditore ) { ?>

      <div class="salesmeet-menu-voci">
          <button onclick="<?php echo $link_dashboard; ?>" id=""><?php echo $etichetta_menu_dashboard;?></button>
      </div>
      <div class="salesmeet-menu-voci">
          <button class="salesmeetbutton" onclick="<?php echo $link_preferiti; ?>"><?php echo $etichetta_menu_analytics; ?></button>
      </div>
      <div class="salesmeet-menu-voci">
          <button onclick="<?php echo $link_note; ?>" id=""><?php echo $etichetta_menu_note;?></button>
      </div>

  <?php } ?>

    <div class="salesmeet-menu-voci" style="float: right;">
        <button onclick='window.location.href="https://<?php echo $site; ?>"' id=""><?php echo $etichetta_menu_exit;?></button>
    </div>
    <div class="salesmeet-menu-voci" style="float: right;">
        <button onclick="javascript:salesmeetDisplayShareScreen();" id=""><?php echo $etichetta_menu_share_screen;?></button>
    </div>

</div>
