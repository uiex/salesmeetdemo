<?php

$etichetta_menu_home = "Dashboard";
$etichetta_menu_preferiti = "Preferiti";
$etichetta_menu_per_tag = "Consigliati per parole";
$etichetta_menu_per_flusso = "Consigliati per flusso";
$etichetta_menu_page_rank = "Page rank";
$etichetta_menu_cronologia = "Cronologia";
$etichetta_menu_note = "Note";
$etichetta_menu_logout = "Logout";
$etichetta_menu_profilo = "Mio profilo";

$etichetta_dati_appuntamento = "Dati appuntamento";
$etichetta_dati_cliente = "Dati cliente";
$etichetta_dati_pagina = "Dati pagina";

$icona_open = "asset/img/link.png";

$etichetta_id_appuntamento = "Id appuntamento";
$etichetta_id_token = "Id token";
$etichetta_id_company = "Id company";
$etichetta_nome_company = "Nome azienda";
$etichetta_site = "Site";
$etichetta_referer = "Referer";
$etichetta_tokenuiex = "Token uiex";
$etichetta_user = "User";
$etichetta_favourite_language = "Lingua favorita";
$etichetta_title = "Titolo";
$etichetta_img = "Immagine";
$etichetta_prezzo = "Prezzo";
$etichetta_currency = "Currency";
$etichetta_date = "Data";
$etichetta_vote = "Voto";
$etichetta_note = "Note";
$etichetta_data_selezionata = "Data selezionata";
$etichetta_cliente = "cliente";
$etichetta_email = "Email";
$etichetta_freecalendar = "Tipologia appuntamento";
$etichetta_freecalendar_0 = "Standard";
$etichetta_freecalendar_1 = "Fuori calendario";
$etichetta_name = "Nome";
$etichetta_surname = "Cognome";
$etichetta_telephone = "Telefono";
$etichetta_language_site = "Lingua sito";
$etichetta_numero_appuntamenti = "Numero appuntamenti richiesti";
$etichetta_numero_prodotti = "Numero prodotti richiesti";
$etichetta_elenco_appuntamenti = "Elenco appuntamenti richiesti";
$etichetta_elenco_prodotti = "Elenco prodotti richiesti";
$etichetta_visibile_si = "Si";
$etichetta_visibile_no = "No";

$etichetta_apri_appuntamento_videocall_salesmeet = "Apri videocall";
$etichetta_apri_appuntamento_default = "Apri appuntamento di default";
$etichetta_or = "o";
$etichetta_apri_appuntamento_venditore = "Apri appuntamento esperto";
$etichetta_apri_appuntamento_cliente = "Apri appuntamento cliente";
$etichetta_appuntamento_system = "Appuntamento dati di sistema";
$etichetta_cliente_altri_appuntamenti = "Altri appuntamenti cliente";

$etichetta_ultimo_prodotto = "Ultimo prodotto visitato";

$etichetta_uiex_page_utente = "Utente";
$etichetta_uiex_page_visitatori = "Tutti i visitatori";
$etichetta_uiex_page_voto_max = "Voto massimo";
$etichetta_uiex_page_voto_medio = "Voto medio";
$etichetta_uiex_page_voto_min = "Voto minimo";
$etichetta_uiex_page_numero_visualizzazioni = "Numero visualizzazioni";
$etichetta_uiex_page_ultima_visita = "Ultima visita";

$etichetta_elenco_altri_appuntamenti = "Elenco altri appuntamenti";
$etichetta_elenco_dati_anagrafici = "Dati anagrafici";
$etichetta_calendario = "Calendario";
$etichetta_elenco = "Elenco";
$etichetta_elenco_modifica_data_calendario = "Modifica data";
$etichetta_elenco_modifica = "Modifica";


$etichetta_nota_inserisci = "Aggiungi nota";
$etichetta_esperto = "Esperto";
$etichetta_resend_email_ok = "Email inviate correttamente.";
$etichetta_resend_email = "Reinviare email";
$etichetta_appuntamento_non_confermato = "Appuntemento non confermato.";

$etichetta_email_di_verifica = "Email di verifica";
$etichetta_pagine_visitate = "Pagine visitate";
$etichetta_voto_medio = "Voto medio";
$etichetta_prezzo_medio = "Prezzo medio";
$etichetta_prezzo_max_visto = "Prezzo max visto";
$etichetta_tempo_medio_sulle_pagine = "Tempo medio sulle pagine";
$etichetta_numero_di_operazioni_medie_sulle_pagine = "Numero di operazioni medie sulle pagine";
$etichetta_tipologia_cliente = "Tipologia cliente";

$etichetta_ = "";

?>
