<?php

$etichetta_menu_home = "Dashboard";
$etichetta_menu_preferiti = "Favorites";
$etichetta_menu_per_tag = "Recommended for words";
$etichetta_menu_per_flusso = "Recommended for flow";
$etichetta_menu_page_rank = "Page rank";
$etichetta_menu_cronologia = "Chronology";
$etichetta_menu_note = "Note";
$etichetta_menu_logout = "Logout";
$etichetta_menu_profilo = "My profile";

$etichetta_dati_appuntamento = "Appointment data";
$etichetta_dati_cliente = "Customer data";
$etichetta_dati_pagina = "Page data";

$icona_open = "asset/img/link.png";

$etichetta_id_appuntamento = "Id appointment";
$etichetta_id_token = "Id token";
$etichetta_id_company = "Id company";
$etichetta_nome_company = "Company name";
$etichetta_site = "Site";
$etichetta_referer = "Referer";
$etichetta_tokenuiex = "Token uiex";
$etichetta_user = "User";
$etichetta_favourite_language = "Favorite language";
$etichetta_title = "Title";
$etichetta_img = "Image";
$etichetta_prezzo = "Price";
$etichetta_currency = "Currency";
$etichetta_date = "Data";
$etichetta_vote = "Vote";
$etichetta_note = "Note";
$etichetta_data_selezionata = "Selected date";
$etichetta_cliente = "customer";
$etichetta_email = "Email";
$etichetta_freecalendar = "Type of appointment";
$etichetta_freecalendar_0 = "Standard";
$etichetta_freecalendar_1 = "Out of calendar";
$etichetta_name = "Name";
$etichetta_surname = "Surname";
$etichetta_telephone = "Telephone";
$etichetta_language_site = "Site language";
$etichetta_numero_appuntamenti = "Number of appointments required";
$etichetta_numero_prodotti = "Number of products requested";
$etichetta_elenco_appuntamenti = "Required appointment list";
$etichetta_elenco_prodotti = "List of required products";
$etichetta_visibile_si = "Yes";
$etichetta_visibile_no = "No";

$etichetta_apri_appuntamento_videocall_salesmeet = "Open videocall";
$etichetta_apri_appuntamento_default = "Open default appointment";
$etichetta_or = "or";
$etichetta_apri_appuntamento_venditore = "Open expert appointment";
$etichetta_apri_appuntamento_cliente = "Open customer appointment";
$etichetta_appuntamento_system = "System data appointment";
$etichetta_cliente_altri_appuntamenti = "Other customer appointments";

$etichetta_ultimo_prodotto = "Last product visited";

$etichetta_uiex_page_utente = "User";
$etichetta_uiex_page_visitatori = "All visitors";
$etichetta_uiex_page_voto_max = "Max vote";
$etichetta_uiex_page_voto_medio = "Middle vote";
$etichetta_uiex_page_voto_min = "Max vote";
$etichetta_uiex_page_numero_visualizzazioni = "Number of views";
$etichetta_uiex_page_ultima_visita = "Last visit";

$etichetta_elenco_altri_appuntamenti = "List of other appointments";
$etichetta_elenco_dati_anagrafici = "Personal data";
$etichetta_calendario = "Calendar";
$etichetta_elenco = "List";
$etichetta_elenco_modifica_data_calendario = "Update date";
$etichetta_elenco_modifica = "Update";


$etichetta_nota_inserisci = "Add note";
$etichetta_esperto = "Expert";
$etichetta_resend_email_ok = "Emails sent successfully.";
$etichetta_resend_email = "Resend email";
$etichetta_appuntamento_non_confermato = "Unconfirmed appointment.";

$etichetta_email_di_verifica = "Verification email";
$etichetta_pagine_visitate = "Pages visited";
$etichetta_voto_medio = "Average rating";
$etichetta_prezzo_medio = "Average price";
$etichetta_prezzo_max_visto = "Max price consulted";
$etichetta_tempo_medio_sulle_pagine = "Average time on pages";
$etichetta_numero_di_operazioni_medie_sulle_pagine = "Number of average page operations";
$etichetta_tipologia_cliente = "Customer type";

$etichetta_riflessivo = "riflessivo";
$etichetta_impulsivo = "impulsivo";
$etichetta_tanto_interessato = "tanto interessato";
$etichetta_interessato = "interessato";
$etichetta_sta_cercando = "sta cercando il prodotto a minor prezzo";
$etichetta_ricco = "ricco";
$etichetta_molto_ricco = "Molto ricco";
$etichetta_ = "";
$etichetta_ = "";
$etichetta_ = "";
$etichetta_ = "";

$etichetta_ = "";

?>
