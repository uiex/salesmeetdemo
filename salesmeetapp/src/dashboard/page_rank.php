<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

  <div class="contenuto">

      <center>
      <div id="pageRankTitolo"></div>
      <div><img id="pageRankImg" src="" style="max-height: 100px;"></div>
    </center>
      <hr>
      <div class="box_page_rank">
        <div id="pageRankUtente">
            <?php echo $etichetta_uiex_page_utente; ?>:
            <hr>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_max; ?>:</b> <div id="pageRankSingleVotomax"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_medio; ?>:</b> <div id="pageRankSingleVotomedio"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_min; ?>:</b> <div id="pageRankSingleVotomin"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_numero_visualizzazioni; ?>:</b> <div id="pageRankSingleCount"></div></div>
        </div>
      </div>

      <div class="box_separatore"></div>

      <div class="box_page_rank">
        <div id="pageRankVisitatori">
            <?php echo $etichetta_uiex_page_visitatori; ?>:
            <hr>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_max; ?>:</b> <div id="pageRankAllMaxVoto"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_medio; ?>:</b> <div id="pageRankAllMediaVoto"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_voto_min; ?>:</b> <div id="pageRankAllMinVoto"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_numero_visualizzazioni; ?>:</b> <div id="pageRankAllCount"></div></div>
            <div id=""><b><?php echo $etichetta_uiex_page_ultima_visita; ?>:</b> <div id="pageRankAllLastVisit"></div></div>
        </div>
      </div>
</div>

  <script>
  function pageRankCommon(token,site,urlUiex,flag) {
      var oReq = new XMLHttpRequest();
      oReq.onload = function(e) {
          var arraybuffer = oReq.response;
          var jsonData = JSON.parse(arraybuffer);
          console.log(jsonData);
          for (var i = 0; i < jsonData.length; i++) {
              if (flag==0) {
                $( "#pageRankTitolo" ).html(jsonData[i]["Title"]);
                $( "#pageRankImg" ).attr("src",jsonData[i]["Imgurl"]);
                $( "#pageRankSingleVotomax" ).html( stelle( jsonData[i]["Votomax"]) );
                $( "#pageRankSingleVotomedio" ).html( stelle( jsonData[i]["Votomedio"]) );
                $( "#pageRankSingleVotomin" ).html( stelle( jsonData[i]["Votomin"]) );
                $( "#pageRankSingleCount" ).html(jsonData[i]["Count"]);
              } else {
                $( "#pageRankAllMaxVoto" ).html( stelle( jsonData[i]["Statistic"]["MaxVoto"]) );
                $( "#pageRankAllMediaVoto" ).html( stelle( jsonData[i]["Statistic"]["MediaVoto"]) );
                $( "#pageRankAllMinVoto" ).html( stelle( jsonData[i]["Statistic"]["MinVoto"]) );
                $( "#pageRankAllCount" ).html(jsonData[i]["Statistic"]["Count"]);
                $( "#pageRankAllLastVisit" ).html(jsonData[i]["Statistic"]["LastVisit"]);
              }
          }
      }
      if (flag==0) {
          oReq.open("POST", "https://uiextools.salesmeet.it/chronology/page/single/");
      } else {
          oReq.open("POST", "https://uiextools.salesmeet.it/chronology/page/all/");
      }
      var formData = new FormData();
      formData.append("token", token );
      formData.append("user", "" );
      formData.append("site", site);
      formData.append("sito", site);
      formData.append("referer", urlUiex);
      oReq.send( formData );
  }
  pageRankCommon("<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>",0);
  pageRankCommon("<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>",1);
</script>


<?php include("common/footer.php"); ?>
