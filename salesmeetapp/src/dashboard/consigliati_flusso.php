<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

<link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
<script src="asset/datatables/jquery.dataTables.min.js"></script>

<div class="contenitore">
  <table id="preferiti" class="display" style="width:100%">
      <thead>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
          </tr>
      </thead>
      <tfoot>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
          </tr>
      </tfoot>
  </table>
</div>

<script>
  function getConsigliatiFlusso(token,user,site,url,words) {
      var table = $('#preferiti').DataTable( {
          "ajax": {
              "url": "https://uiextools.salesmeet.it/consigliati/",
              "type": "POST",
              "dataSrc": "",
              "data" : {
                  "token" : token,
                  "user": "",
                  "site": site,
                  "sito": site,
                  "words" : words,
              }
          },
          "columns": [
              { "data": "Imgurl",
                  "render": function (data, type, row) {
                      if (data == '') {
                          return '<img src="https://app.salesmeet.it/dashboard/asset/images/picture.png" style="height:25px" />';
                      }  else {
                          return '<img src="' + data + '" style="height:45px" />';
                      }
                  }
              },
              { "data": "Pagina.0.Meta.Title",
                  "render": function (data, type, row) {
                      return createUrl(row.Url,data);
                  }
              },
              { "data": "Pagina.0.Price.Price"},
              { "data": "Pagina.0.Price.Currency"},
              { "data": "Pagina.0.Somma",
                  "render": function (data, type, row) {
                      return this.dart( row.Somma );
                  }
              }
          ]
      } );
  }
  getPageInfo("consigliati_flusso","<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>");
</script>


<?php include("common/footer.php"); ?>
