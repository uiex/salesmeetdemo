<?php include("setting.php"); ?>

<?php include("common/header.php"); ?>

<?php
if (isset($_POST['description_note'])) {
    $db->createAppointmentNote($_POST['description_note'],$id_appointment);
}
if (isset($_GET['id_note'])) {
    $db->deleteAppointmentNote($_GET['id_note'],$id_appointment);
}
?>

<div class="contenitore">
  <form action="note.php" method="post">
      <textarea name="description_note" id="description_note" style="width: 99%;height: 250px;"></textarea>
      <center>
      <input type="submit" value="<?php echo $etichetta_nota_inserisci; ?>">
      <center>
  </form>
</div>

<?php
$notes = $db->getAppointmentNotes($id_appointment);
// echo $notes;
$notesObj = json_decode($notes, true);
foreach ($notesObj as &$value) {
    echo '<div class="contenitore">';
    echo "<a href='note.php?id_note=" . $value["id_appointment_note"] . "'><img src='asset/img/delete.png'></a>" . $value["data_init"] . " - " . $value["description"];
    echo '</div>';
}
?>

<?php include("common/footer.php"); ?>
