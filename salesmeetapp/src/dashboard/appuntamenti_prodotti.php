<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

  <link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
  <script src="asset/datatables/jquery.dataTables.min.js"></script>

  <div class="contenitore">
    <div class="titolo_pagina"><?php echo $etichetta_elenco_prodotti; ?></div>
    <table id="preferiti" style="width:100%">
        <thead>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th>visible</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th>visible</th>
            </tr>
        </tfoot>
    </table>
  </div>

  <script>
    function loadAppointments() {
        $('#preferiti').DataTable( {
            "data" : <?php echo $appuntamentiProdotti; ?>,
            "columns": [
                { "data": "id_product"},
                { "data": "title",
                    "render": function (data, type, row) {
                        var titolo = "";
                        if ((data == '')|| (data == null)) {
                            titolo = row.url;
                        }  else {
                            titolo = data;
                        }
                        return "<a href='" + row.url + "' target='_blank'>" + titolo + "<img class='contenitore_icone' src='<?php echo $icona_open; ?>'></a>";
                    }
                },
                { "data": "img",
                    "render": function (data, type, row) {
                        if ((data == '')|| (data == null)) {
                            return '<img src="asset/img/salesmeet_icona.png" style="height:25px" />';
                        }  else {
                            return '<img src="' + data + '" style="height:45px" />';
                        }
                    }
                },
                { "data": "visible",
                    "render": function (data, type, row) {
                        if (data == 0) {
                            return '<?php echo $etichetta_visibile_si; ?>';
                        }  else {
                            return '<?php echo $etichetta_visibile_no; ?>';
                        }
                    }
                },
            ]
        } );
    }
    loadAppointments();
  </script>

<?php include("common/footer.php"); ?>
