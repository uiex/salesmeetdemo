<div class="contenitore">

  <div class="titolo"><?php echo $etichetta_pagine_visitate; ?>:</div>
  <div class="valore"><div id="pagine_visitate"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_voto_medio; ?>:</div>
  <div class="valore"><div id="voto_medio"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_prezzo_medio; ?>:</div>
  <div class="valore"><div id="prezzo_medio"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_prezzo_max_visto; ?>:</div>
  <div class="valore"><div id="prezzo_max"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_tempo_medio_sulle_pagine; ?>:</div>
  <div class="valore"><div id="tempo_medio"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_numero_di_operazioni_medie_sulle_pagine; ?>:</div>
  <div class="valore"><div id="operazioni_medio"></div></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_tipologia_cliente; ?>:</div>
  <div class="valore"><div id="tipo_cliente"></div></div>
  <div class="separatore"></div>

</div>

<script>
  function getInfoUser() {
      var formData = new FormData();
      formData.append("token", "<?php echo $appointmentObj[0]["tokenuiex"]; ?>");
      formData.append("site", "<?php echo $appointmentObj[0]["site"]; ?>");
      var oReq = new XMLHttpRequest();
      oReq.onload = function(e) {
          var arraybuffer = oReq.response;
          var jsonData = JSON.parse(arraybuffer);
          // console.log(jsonData);
          document.getElementById('tipo_cliente').innerHTML = tipo_cliente( jsonData );
          document.getElementById('pagine_visitate').innerHTML = jsonData[0].Count;
          document.getElementById('voto_medio').innerHTML = stelle(jsonData[0].Voto / jsonData[0].Count);
          document.getElementById('prezzo_medio').innerHTML = Math.round(jsonData[0].Price / jsonData[0].Count);
          document.getElementById('prezzo_max').innerHTML = jsonData[0].Pricemax;
          document.getElementById('tempo_medio').innerHTML = millisToMinutesAndSeconds(jsonData[0].Tempo / jsonData[0].Count) + "m" ;
          document.getElementById('operazioni_medio').innerHTML = Math.round(jsonData[0].Operazioni / jsonData[0].Count) ;
      }
      oReq.open("POST", "https://uiextools.salesmeet.it/useranalytics/");
      oReq.send( formData );
  }
  getInfoUser();
  function tipo_cliente( jsonData ) {
      var minuti_sulla_pagina = millisToMinutesAndSeconds(jsonData[0].Tempo / jsonData[0].Count);
      var pagine_visitate  =jsonData[0].Count;
      var numero_operazioni = Math.round(jsonData[0].Operazioni / jsonData[0].Count);
      var prezzo_medio =  Math.round(jsonData[0].Price / jsonData[0].Count);
      var voto_medio =  Math.round(jsonData[0].Voto / jsonData[0].Count);
      var tipo = "";

      if (minuti_sulla_pagina > 60) {
        tipo = tipo + " <?php echo $etichetta_riflessivo; ?>,";
      } else if (minuti_sulla_pagina < 20) {
        tipo = tipo + " <?php echo $etichetta_impulsivo; ?>,";
      } else  {
        tipo = tipo + "";
      }

      if (pagine_visitate > 60) {
        tipo = tipo + " <?php echo $etichetta_tanto_interessato; ?>,";
      } else if (pagine_visitate > 20) {
        tipo = tipo + " <?php echo $etichetta_interessato; ?>,";
      } else if (pagine_visitate < 10) {
        tipo = tipo + " <?php echo  $etichetta_sta_cercando; ?>,";
      } else  {
        tipo = tipo + "";
      }

      if (prezzo_medio > 1000) {
        tipo = tipo + " <?php echo $etichetta_molto_ricco; ?>,";
      } else if (prezzo_medio > 600) {
        tipo = tipo + " <?php echo $etichetta_ricco; ?>,";
      } else if (prezzo_medio > 200) {
        tipo = tipo + " <?php echo $etichetta_benestante; ?>,";
      } else  {
        tipo = tipo + "";
      }

      return tipo;
  }
  function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    // return minutes + "m  " + (seconds < 10 ? '0' : '') + seconds + " s";
    return minutes;
  }
  function secondsToHms(d) {
         d = Number(d);
         var h = Math.floor(d / 3600);
         var m = Math.floor(d % 3600 / 60);
         var s = Math.floor(d % 3600 % 60);

         var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
         var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
         var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
         return hDisplay + mDisplay + sDisplay;
  }
</script>
