<!DOCTYPE html>
<html lang="<?php echo $language; ?>">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="asset/css/main.css" media="all" type="text/css" >
    <script src="asset/js/uiex.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <body>
    <?php include("common/menu.php"); ?>

    <?php if ($appointmentObj[0]["state"]==1) { ?>
        <div class="box_alert">
            <a href="profilo_chenge_appointment.php"><?php echo $etichetta_appuntamento_non_confermato; ?></a>
        </div>
    <?php } ?>
