<div class="contenitore">
  <div id="titolo_last"></div>
  <div><img id="img_last" style="max-width: 50px;max-height: 50px;" src=""></div>
</div>
<script>
  function getLast() {
      var formData = new FormData();
      formData.append("token", "<?php echo $appointmentObj[0]["tokenuiex"]; ?>");
      formData.append("site", "<?php echo $appointmentObj[0]["site"]; ?>");
      var oReq = new XMLHttpRequest();
      oReq.onload = function(e) {
          var arraybuffer = oReq.response;
          var jsonData = JSON.parse(arraybuffer);
          console.log(jsonData);
          document.getElementById('titolo_last').innerHTML = "<a href='" + jsonData[0].Url + "' target='_blank'>" + jsonData[0].Meta.Title + "<img class='contenitore_icone' src='<?php echo $icona_open; ?>'></a>";
          document.getElementById('img_last').src = jsonData[0].Meta.ImgUrl;
      }
      oReq.open("POST", "https://uiextools.salesmeet.it/chronology/last/");
      oReq.send( formData );
  }
  getLast();
</script>
