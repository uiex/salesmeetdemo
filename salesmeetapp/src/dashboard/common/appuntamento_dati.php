<!-- DATI appuntamento  -->
<div class="contenitore">

  <div class="titolo"><?php echo $etichetta_data_selezionata; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["data_selezionata"]; ?> <a href="profilo_chenge_appointment.php"><?php echo $etichetta_elenco_modifica; ?></a></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_freecalendar; ?>:</div>
  <?php
    if ($appointmentObj[0]["freecalendar"]==0) {
        $freecalendar = $etichetta_freecalendar_0;
    } else {
        $freecalendar = $etichetta_freecalendar_1;
    }
  ?>
  <div class="valore"><?php echo $freecalendar; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_note . " " . $etichetta_cliente; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["description"]; ?></div>
  <div class="separatore"></div>
  <!--
  <div class="titolo">Tipologia call:</div>
  <div class="valore">Meet, telefono, Zoom, Salesmeet</div>
  <div class="separatore"></div>
  -->

</div>
