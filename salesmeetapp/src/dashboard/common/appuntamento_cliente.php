<!-- DATI cliente  -->
<div class="contenitore">

  <div class="titolo"><?php echo $etichetta_name . " " . $etichetta_cliente; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["name"]; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_surname . " " . $etichetta_cliente;  ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["surname"]; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_telephone . " " . $etichetta_cliente;  ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["telephone"]; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_email . " " . $etichetta_cliente; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["email"]; ?></div>
  <div class="separatore"></div>

  <!--
  <div class="titolo"><?php echo $etichetta_user; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["user"]; ?></div>
  <div class="separatore"></div>
  -->

  <div class="titolo"><?php echo $etichetta_favourite_language; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["favourite_language"]; ?></div>
  <div class="separatore"></div>
</div>
