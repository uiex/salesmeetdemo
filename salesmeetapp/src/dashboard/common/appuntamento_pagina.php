<!-- DATI pagina  -->
<div class="contenitore">

  <div class="titolo"><?php echo $etichetta_referer; ?>:</div>
  <div class="valore"><a href="<?php echo $appointmentObj[0]["referer"]; ?>" target="-blank"><?php echo $appointmentObj[0]["referer"]; ?><a/></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_title; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["title"]; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_prezzo; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["price"]; ?></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_img; ?>:</div>
  <div class="valore"><img src="<?php echo $appointmentObj[0]["img"]; ?>"></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_language_site; ?>:</div>
  <div class="valore"><?php echo $appointmentObj[0]["language"]; ?></div>
  <div class="separatore"></div>

</div>

<div class="contenitore">

  <div class="titolo"><?php echo $etichetta_numero_prodotti; ?>:</div>
  <div class="valore"><?php echo count($appuntamentiProdottiObj); ?> <a href="appuntamenti_prodotti.php"><img class="contenitore_icone" src="<?php echo $icona_open; ?>"></a></div>
  <div class="separatore"></div>

</div>
