<?php
  $link = $db->getLinkAppointment($id_appointment);
  $linkObj = json_decode($link, true);
?>
<div class="contenitore">

  <?php if ($linkObj[0]["external_expert"]) { ?>

    <div class="titolo"><?php echo $etichetta_apri_appuntamento_default; ?>:</div>
    <div class="valore"><a href="<?php echo $linkObj[0]["external_expert"] ?>" target="_Blank"><img class="contenitore_icone" src="<?php echo $icona_open; ?>"></a></div>
    <div class="separatore"></div>
    <!--
    <div class="titolo"></div>
    <div class="valore"><?php echo $etichetta_or; ?></div>
    <div class="separatore"></div>
    -->
  <?php } ?>

  <!--
  <div class="titolo"><?php echo $etichetta_apri_appuntamento_venditore; ?> Salesmeet:</div>
  <div class="valore"><a href="<?php echo $linkObj[0]["link_salesmeet_expert"] ?>" target="_Blank"><img class="contenitore_icone" src="<?php echo $icona_open; ?>"></a></div>
  <div class="separatore"></div>

  <div class="titolo"><?php echo $etichetta_apri_appuntamento_cliente; ?> Salesmeet:</div>
  <div class="valore"><a href="<?php echo $linkObj[0]["link_salesmeet_customer"] ?>" target="_Blank"><img class="contenitore_icone" src="<?php echo $icona_open; ?>"></a></div>
  <div class="separatore"></div>
-->

</div>
