<?php $nome_pagina = basename($_SERVER["PHP_SELF"]); ?>

<div class="menu" id="menu">
  <div class="logo"><a href="index.php"><img src="asset/img/salesmeet_icona.png"></a></div>
  <div class="menu_mobile_icon">
    <a href="javascript:void(0);" class="icon" onclick="openMenu()">
       <img src="asset/img/menu.png">
    </a>
  </div>
  <div class="menu_mobile_icon" id="menu_breadcrumb">
    <?php if ($nome_pagina=="index.php") { echo $etichetta_menu_home; } ?>
    <?php if ($nome_pagina=="preferiti.php") { echo $etichetta_menu_preferiti; } ?>
    <?php if ($nome_pagina=="consigliati_tag.php") { echo $etichetta_menu_per_tag; } ?>
    <?php if ($nome_pagina=="consigliati_flusso.php") { echo $etichetta_menu_per_flusso; } ?>
    <?php if ($nome_pagina=="page_rank.php") { echo $etichetta_menu_page_rank; } ?>
    <?php if ($nome_pagina=="cronologia.php") { echo $etichetta_menu_cronologia; } ?>
    <?php if ($nome_pagina=="note.php") { echo $etichetta_menu_note; } ?>
    <?php if ($nome_pagina=="profilo.php") { echo $etichetta_menu_profilo; } ?>
  </div>
  <div class="voce_menu <?php if ($nome_pagina=="index.php") {echo "current";}?>"><a class="menu_link" href="index.php"><?php echo $etichetta_menu_home;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="preferiti.php") {echo "current";}?>"><a class="menu_link" href="preferiti.php"><?php echo $etichetta_menu_preferiti;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="consigliati_tag.php") {echo "current";}?>"><a class="menu_link" href="consigliati_tag.php"><?php echo $etichetta_menu_per_tag;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="consigliati_flusso.php") {echo "current";}?>"><a class="menu_link" href="consigliati_flusso.php"><?php echo $etichetta_menu_per_flusso;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="page_rank.php") {echo "current";}?>"><a class="menu_link" href="page_rank.php"><?php echo $etichetta_menu_page_rank;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="cronologia.php") {echo "current";}?>"><a class="menu_link" href="cronologia.php"><?php echo $etichetta_menu_cronologia;?></a></div>
  <div class="voce_menu <?php if ($nome_pagina=="note.php") {echo "current";}?>"><a class="menu_link" href="note.php"><?php echo $etichetta_menu_note;?></a></div>
  <div class="voce_menu_right"><a class="menu_link" href="logout.php"><?php echo $etichetta_menu_logout;?></a></div>
  <div class="voce_menu_right <?php if ($nome_pagina=="profilo.php") {echo "current";}?>"><a class="menu_link" href="profilo.php"><?php echo $etichetta_menu_profilo;?></a></div>
  <div class="voce_menu_right"><a class="menu_link" href="javascript:window.close();">Close</a></div>
  <div style="clear:both;"></div>
</div>

<div class="menu_mobile" id="menu_mobile">
  <div class="voce_menu_mobile <?php if ($nome_pagina=="index.php") {echo "current";}?>"><a href="index.php"><?php echo $etichetta_menu_home;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="preferiti.php") {echo "current";}?>"><a href="preferiti.php"><?php echo $etichetta_menu_preferiti;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="consigliati_tag.php") {echo "current";}?>"><a href="consigliati_tag.php"><?php echo $etichetta_menu_per_tag;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="consigliati_flusso.php") {echo "current";}?>"><a href="consigliati_flusso.php"><?php echo $etichetta_menu_per_flusso;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="page_rank.php") {echo "current";}?>"><a href="page_rank.php"><?php echo $etichetta_menu_page_rank;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="cronologia.php") {echo "current";}?>"><a href="cronologia.php"><?php echo $etichetta_menu_cronologia;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="note.php") {echo "current";}?>"><a href="note.php"><?php echo $etichetta_menu_note;?></a></div>
  <div class="voce_menu_mobile <?php if ($nome_pagina=="profilo.php") {echo "current";}?>"><a href="profilo.php"><?php echo $etichetta_menu_profilo;?></a></div>
  <div class="voce_menu_mobile"><a href="logout.php"><?php echo $etichetta_menu_logout;?></a></div>

</div>

<script>
function openMenu() {
  var x = document.getElementById("menu_mobile");
  console.log(x.className);
  if (x.className === "menu_mobile") {
    $( ".menu_mobile" ).show();
    x.className += " responsive";
  } else {
    $( ".menu_mobile" ).hide();
    x.className = "menu_mobile";
  }
}
</script>
