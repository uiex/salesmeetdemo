<?php

$expertAppointment = $db->getExpertAppointment($expertObj[0]["id_expert"]);
$expertAppointmentObj = json_decode($expert, true);

?>

<link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
<script src="asset/datatables/jquery.dataTables.min.js"></script>

<div class="contenitore">

    <table id="preferiti" style="width:100%">
        <thead>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_data_selezionata;?></th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th><?php echo $etichetta_favourite_language;?></th>
              <th><?php echo $etichetta_note;?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_data_selezionata;?></th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th><?php echo $etichetta_favourite_language;?></th>
              <th><?php echo $etichetta_note;?></th>
            </tr>
        </tfoot>
    </table>

</div>

<script>
  function loadAppointments() {
      $('#preferiti').DataTable( {
          "data" : <?php echo $expertAppointment; ?>,
          "columns": [
              { "data": "id_appointment",
                  "render": function (data, type, row) {
                      return "<a href='index.php?tokennew=" + row.id_token + "'>" + data + "<img class='contenitore_icone' src='<?php echo $icona_open; ?>'></a>";
                  }
              },
              { "data": "data_selezionata"},
              { "data": "title"},
              { "data": "img",
                  "render": function (data, type, row) {
                      if ((data == '')|| (data == null)) {
                          return '<img src="asset/img/salesmeet_icona.png" style="height:25px" />';
                      }  else {
                          return '<img src="' + data + '" style="height:45px" />';
                      }
                  }
              },
              { "data": "favourite_language"},
              { "data": "email",
                  "render": function (data, type, row) {
                          var dati = row.email;
                          if (row.name!="") { dati = dati + " - " + row.name;  }
                          if (row.surname!="") { dati = dati + " - " + row.surname;  }
                          if (row.telephone!="") { dati = dati + " - " + row.telephone;  }
                          return dati;
                  }
              },
          ]
      } );
  }
  loadAppointments();

</script>
