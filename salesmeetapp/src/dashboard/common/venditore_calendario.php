<?php
$expertAppointment = $db->getExpertAppointmentCalendar($expertObj[0]["id_expert"],30);
$expertAppointmentObj = json_decode($expert, true);
?>
<div class="contenitore">

  <div id='calendar'></div>

  <link href='fullcalendar/main.min.css' rel='stylesheet' />
  <script src='fullcalendar/main.min.js'></script>
  <script src='fullcalendar/locales/<?php echo $language; ?>.js'></script>

  <script>
      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          initialView: 'timeGridWeek',
          locale: '<?php echo $language; ?>',
          headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'timeGridWeek,timeGridDay'
          },
          selectable: true,
          dateClick: function(info) {
            inserimentoData(info.dateStr);
          },
          eventClick: function(info) {
            // https://fullcalendar.io/docs/v3/event-object
            // alert('Event: ' + info.event.title);
            // alert('Event: ' + info.event.id);
            <?php if ($flag == 1) { ?>
                window.location.href = "index.php?tokennew=" + info.event.id;
            <?php } ?>
          },
          select: function(info) {
            //alert('selected ' + info.startStr + ' to ' + info.endStr);
          },
          <?php if ($freecalendar=="") { ?>
            <?php echo $expertAppointment; ?>
          <?php } ?>

        });
        calendar.render();
      });
      /*
      businessHours: [ // specify an array instead
        {
          daysOfWeek: [ 1, 2, 3, 4 , 5 ], // Monday, Tuesday, Wednesday, Thursday, Friday
          startTime: '08:00', // 8am
          endTime: '13:00' // 6pm
        },
        {
          daysOfWeek: [ 1, 2, 3, 4 , 5 ],
          startTime: '14:00', // 10am
          endTime: '19:00' // 4pm
        }
      ],
      */
  </script>

</div>
