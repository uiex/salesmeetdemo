<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

<link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
<script src="asset/datatables/jquery.dataTables.min.js"></script>

<div class="contenitore">
  <table id="preferiti" class="display" style="width:100%">
      <thead>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
            <th><?php echo $etichetta_date; ?></th>
          </tr>
      </thead>
      <tfoot>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
            <th><?php echo $etichetta_date; ?></th>
          </tr>
      </tfoot>
  </table>
</div>

<script>
  function getCronologia(token,user,site,url) {
      var table = $('#preferiti').DataTable( {
          "ajax": {
              "url": "https://uiextools.salesmeet.it/chronology/chronology/",
              "type": "POST",
              "dataSrc": "",
              "data" : {
                  "token" : token,
                  "user": user,
                  "site": site
              }
          },
          "columns": [
              { "data": "Meta.ImgUrl",
                  "render": function (data, type, row) {
                      if (data == '') {
                          return '<img src="https://app.salesmeet.it/asset/images/picture.png" style="height:25px" />';
                      }  else {
                          return '<img src="' + data + '" style="height:45px" />';
                      }
                  }
              },
              { "data": "Meta.Title",
                  "render": function (data, type, row) {
                      return createUrl(row.url,data);
                  }
              },
              { "data": "Price.Price"},
              { "data": "Price.Currency"},
              { "data": "Value.Voto",
                  "render": function (data, type, row) {
                      return this.stelle( data );
                  }
              },
              { "data": "Timestamp"},
          ]

      } );
  }
  // getCronologiaTemp("<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>");
  getCronologia("<?php echo $_SESSION["id_token_uiex"]; ?>","","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>");

  function getCronologiaTemp(token,site,urlUiex) {
      var oReq = new XMLHttpRequest();
      oReq.onload = function(e) {
          var arraybuffer = oReq.response;
          var jsonData = JSON.parse(arraybuffer);
          console.log(jsonData);
      }
      oReq.open("POST", "https://uiextools.salesmeet.it/chronology/chronology/");
      var formData = new FormData();
      formData.append("token", token );
      formData.append("user", "" );
      formData.append("site", site);
      formData.append("sito", site);
      formData.append("referer", urlUiex);
      oReq.send( formData );
  }

</script>


<?php include("common/footer.php"); ?>
