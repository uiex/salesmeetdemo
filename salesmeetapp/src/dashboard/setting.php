<?php
error_reporting(E_ERROR);
session_start();

// Class per gestione ed inserimento appuntamento nel DB
include("../db/db.php");
$db = new db();

$id_appointment = "";
$language = "en";

if (isset($_GET['token'])) {
    $id_token = $_GET['token'];
    $_SESSION["id_token"] = $id_token;
}
if (isset($_GET['tokennew'])) {
    $id_token = $_GET['tokennew'];
    $_SESSION["id_token"] = $id_token;
}

if (isset($_POST['email'])) {
    $_SESSION["email"] = $_POST['email'];
}

// dalla video call di salesmeet ...
if (isset($_GET['emailcallsalesmeet'])) {
    $_SESSION["email"] = $_GET['emailcallsalesmeet'];
}
if (isset($_GET['idtokencallsalesmeet'])) {
    $_SESSION["id_token"] = $_GET['idtokencallsalesmeet'];
}

if ($_SESSION["id_token"]!="") {

  // $language = $appointmentObj[0]["favourite_language"];
  if ($language=="") { $language = "en"; }
  include("language/" . $language . ".php");
  
  $appointment = $db->getAppointment($_SESSION["id_token"]);
  $appointmentObj = json_decode($appointment, true);
  if (count($appointmentObj)==0) {

      $id_appointment = "";
      $_SESSION["id_token"] = "";
      $_SESSION["email"] = "";
      echo "Invalid appointment. Error 3001.";
      exit;
  }

  if ($_SESSION["email"]!="") {

      $id_appointment = $appointmentObj[0]["id_appointment"];
      $_SESSION["id_appointment"] = $id_appointment;
      $_SESSION["id_token_uiex"] = $appointmentObj[0]["tokenuiex"];

      $expert = $db->getExpert($id_appointment);
      $expertObj = json_decode($expert, true);

      if ($_SESSION["email"]!=$expertObj[0]["email"])  {

          $id_appointment = "";
          $_SESSION["id_token"] = "";
          $_SESSION["email"] = "";
          echo "Invalid appointment. Error 3003.";
          exit;
      }

      $company = $db->getCompany($appointmentObj[0]["id_company"]);
      $companyObj = json_decode($company, true);

      $appuntamentiCliente = $db->getAppuntamentiCliente($appointmentObj[0]["email"],$appointmentObj[0]["id_company"]);
      $appuntamentiClienteObj = json_decode($appuntamentiCliente, true);

      $appuntamentiProdotti = $db->getAppointmentProdotti($appointmentObj[0]["id_appointment"]);
      $appuntamentiProdottiObj = json_decode($appuntamentiProdotti, true);

  }


} else {

    $_SESSION["id_token"] = "";
    $_SESSION["email"] = "";
    echo "Invalid appointment. Error 3002.";
    exit;

}

?>
