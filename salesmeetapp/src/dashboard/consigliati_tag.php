<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

<link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
<script src="asset/datatables/jquery.dataTables.min.js"></script>

<div class="contenitore">
  <table id="preferiti" class="display" style="width:100%">
      <thead>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
          </tr>
      </thead>
      <tfoot>
          <tr>
            <th><?php echo $etichetta_img;?></th>
            <th><?php echo $etichetta_title;?></th>
            <th><?php echo $etichetta_prezzo; ?></th>
            <th><?php echo $etichetta_currency; ?></th>
            <th><?php echo $etichetta_vote; ?></th>
          </tr>
      </tfoot>
  </table>
</div>

<script>
  function getConsigliatiWords(token,user,site,url,words) {
      var table = $('#preferiti').DataTable( {
          "ajax": {
              "url": "https://uiextools.salesmeet.it/consigliati/pages/words/",
              "type": "POST",
              "dataSrc": "",
              "data" : {
                  "token" : token,
                  "user": "",
                  "site": site,
                  "sito": site,
                  "words" : words,
              }
          },
          "columns": [
              { "data": "Meta.ImgUrl",
                  "render": function (data, type, row) {
                      if (data == '') {
                          return '<img src="https://app.salesmeet.it/dashboard/asset/images/picture.png" style="height:25px" />';
                      }  else {
                          return '<img src="' + data + '" style="height:45px" />';
                      }
                  }
              },
              { "data": "Meta.Title",
                  "render": function (data, type, row) {
                      return createUrl(row.Meta.Url,data);
                  }
              },
              { "data": "Price.Price"},
              { "data": "Price.Currency"},
              { "data": "NFind",
                  "render": function (data, type, row) {
                      return this.dart( data );
                  }
              }
          ]
      } );
  }
  // getConsigliatiWords("consigliati_words","<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>");
  getPageInfo("consigliati_words","<?php echo $_SESSION["id_token_uiex"]; ?>","<?php echo $appointmentObj[0]["site"]; ?>","<?php echo $appointmentObj[0]["referer"]; ?>");
</script>

<?php include("common/footer.php"); ?>
