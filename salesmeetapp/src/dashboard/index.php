<?php

include("setting.php");
if ($id_appointment=="") {  ?>

  <center>
    <div style="background: rgb(0, 149, 221);width: 300px;padding: 30px;margin-top: 30px;">
      <img src="asset/img/salesmeet_icona.png">
      <br><br>
      <form action="index.php" name="" method="post">
        <?php echo $etichetta_email_di_verifica; ?>: <input type="text" id="email" name="email"><br>
        <br><br>
        <input type="submit" value="Login">
      </form>
    </div>
  </center>

<?php

} else {

?>

  <?php include("common/header.php"); ?>

  <div class="box">
      <div class="box_titolo"><img src="asset/img/001-calendar.png"><?php echo $etichetta_dati_appuntamento; ?></div>
      <?php include("common/appuntamento_dati.php"); ?>
      <?php include("common/appuntamento_sito.php"); ?>
      <div class="hr_separatore"></div>
      <div class="box_titolo"><img src="asset/img/007-video-camera.png"><?php echo $etichetta_apri_appuntamento_videocall_salesmeet; ?></div>
      <?php include("common/appuntamento_videocall.php"); ?>
      <?php include("common/appuntamento_resend_email.php"); ?>
      <div class="box_titolo"><img src="asset/img/003-flask.png"><?php echo $etichetta_appuntamento_system; ?></div>
      <?php include("common/appuntamento_struttura.php"); ?>
  </div>
  <div class="box_separatore"></div>
  <div class="box">
      <div class="box_titolo"><img src="asset/img/002-clipboard.png"><?php echo $etichetta_dati_cliente; ?></div>
      <?php include("common/appuntamento_cliente.php"); ?>
      <?php include("common/appuntamento_cliente_media.php"); ?>
      <?php include("common/appuntamento_cliente_system.php"); ?>
      <div class="box_titolo"><img src="asset/img/004-clipboard-1.png"><?php echo $etichetta_cliente_altri_appuntamenti; ?></div>
      <?php include("common/appuntamento_cliente_numero.php"); ?>
      <div class="hr_separatore"></div>
      <div class="box_titolo"><img src="asset/img/006-notes.png"><?php echo $etichetta_ultimo_prodotto; ?></div>
      <?php include("common/appuntamento_ultimo_prodotto.php"); ?>

  </div>
  <div class="box_separatore"></div>
  <div class="box">
      <div class="box_titolo"><img src="asset/img/008-target.png"><?php echo $etichetta_dati_pagina; ?></div>
      <?php include("common/appuntamento_pagina.php"); ?>
  </div>

  <?php include("common/footer.php"); ?>

<?php } ?>
