<?php
include("setting.php");
?>
<?php include("common/header.php"); ?>

  <link rel="stylesheet" href="asset/datatables/jquery.dataTables.min.css">
  <script src="asset/datatables/jquery.dataTables.min.js"></script>

  <div class="contenitore">
    <div class="titolo_pagina"><?php echo $etichetta_elenco_appuntamenti; ?></div>
    <table id="preferiti" style="width:100%">
        <thead>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_data_selezionata;?></th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th><?php echo $etichetta_esperto;?></th>
              <th><?php echo $etichetta_note;?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Id</th>
              <th><?php echo $etichetta_data_selezionata;?></th>
              <th><?php echo $etichetta_title;?></th>
              <th><?php echo $etichetta_img;?></th>
              <th><?php echo $etichetta_esperto;?></th>
              <th><?php echo $etichetta_note;?></th>
            </tr>
        </tfoot>
    </table>
  </div>

  <script>
    function loadAppointments() {
        $('#preferiti').DataTable( {
            "data" : <?php echo $appuntamentiCliente; ?>,
            "columns": [
                { "data": "id_appointment"},
                { "data": "data_selezionata"},
                { "data": "title",
                    "render": function (data, type, row) {
                        var titolo = "";
                        if ((data == '')|| (data == null)) {
                            titolo = row.url;
                        }  else {
                            titolo = data;
                        }
                        return "<a href='" + row.referer + "' target='_blank'>" + titolo + "<img class='contenitore_icone' src='<?php echo $icona_open; ?>'></a>";
                    }
                },
                { "data": "img",
                    "render": function (data, type, row) {
                        if ((data == '')|| (data == null)) {
                            return '<img src="asset/img/salesmeet_icona.png" style="height:25px" />';
                        }  else {
                            return '<img src="' + data + '" style="height:45px" />';
                        }
                    }
                },
                { "data": "nameexpert",
                    "render": function (data, type, row) {
                        return row.nameexpert + " - " + row.surnameexpert + " - " + row.telephoneexpert + " - " + row.emailexpert;
                    }
                },
                { "data": "description"},
            ]
        } );
    }
    loadAppointments();
  </script>

<?php include("common/footer.php"); ?>
