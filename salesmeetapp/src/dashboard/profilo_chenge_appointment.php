<?php include("setting.php");

if (isset($_POST['data_selezionata'])) {

    $data_selezionata_new = $_POST['data_selezionata'];
    if ($data_selezionata_new=="") {
      $db->updateAppointmentDataSelezionataConferma($id_appointment);
    } else {
      $db->updateAppointmentDataSelezionata($data_selezionata_new,$id_appointment);
    }
    $appointment = $db->getAppointment($_SESSION["id_token"]);
    $appointmentObj = json_decode($appointment, true);


    include("../email/sendEmail.php");
    $email = new email();
    $email->sendAppointment($id_appointment);

}


$appointment = $db->getAppointment($_SESSION["id_token"]);
$appointmentObj = json_decode($appointment, true);
// print_r($appointmentObj);
// echo "<hr>";

$link = $db->getLinkAppointment($appointmentObj[0]["id_appointment"]);
$linkObj = json_decode($link, true);
// print_r($linkObj);
// echo "<hr>";
// echo($linkObj[0]["external_customer"]);

include("ics/ics.php");
$properties = array(
    'location' => "online",
    'description' => $appointmentObj[0]["description"],
    'dtstart' => $appointmentObj[0]["data_selezionata"], // $_POST['date_start'],
    'dtend' => $appointmentObj[0]["data_selezionata"] . "+ 30 minutes", // $_POST['date_end'],
    'summary' => "La Centrale de Prevoyance",
    'url' => $linkObj[0]["external_customer"] // $_POST['url']

);
$ics = new ICS($properties);
$ics_file_contents = $ics->to_string();
file_put_contents( "ics/files/" . $id_appointment . ".ics",$ics_file_contents);

?>

<?php include("common/header.php"); ?>

  <div class="box">

      <div class="box_titolo"><img src="asset/img/002-clipboard.png"><?php echo $etichetta_elenco_modifica_data_calendario; ?></div>
      <div class="contenitore">

        <div class="titolo"><?php echo $etichetta_data_selezionata; ?>:</div>
        <div class="valore" id="data_selezionata_new"><?php echo $appointmentObj[0]["data_selezionata"]; ?></div>
        <div class="separatore"></div>

        <script>
            function inserimentoData(data) {
                var d = new Date(data);
                var giorno = d.getDate() + "-" + d.getMonth() + "-" + d.getFullYear();
                var ora = d.getHours() + ":" + d.getMinutes();
                $('#data_selezionata').val(data);
                $('#data_selezionata_new').html(giorno + " " + ora);
            }
        </script>

        <form action="profilo_chenge_appointment.php" method="post">
            <input type="hidden" id="data_selezionata" name="data_selezionata">
            <input type="submit" value="<?php echo $etichetta_elenco_modifica; ?>">
        <form>

      </div>


  </div>
  <div class="box_doppio">
      <div class="box_titolo"><img src="asset/img/006-notes.png"><?php echo $etichetta_elenco_altri_appuntamenti; ?></div>
      <?php $flag = 2; include("common/venditore_calendario.php"); ?>
  </div>

<?php include("common/footer.php"); ?>
