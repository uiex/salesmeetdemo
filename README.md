# SALESMEET


git push https://uiex@bitbucket.org/uiex/salesmeetdemo.git/
Tj7pDkwGJX4JJy3QzPGG


Overview
========

Scadenza dominio 30/07/2021
https://login.ionos.it/

Local domain
===========

Aggiungere all'hosts i seguenti records

52.31.44.218

127.0.0.1 app.salesmeet.it
127.0.0.1 plugin.salesmeet.it
127.0.0.1 booking.salesmeet.it
127.0.0.1 monitor.salesmeet.it
127.0.0.1 profile.salesmeet.it
127.0.0.1 expert.salesmeet.it

127.0.0.1 cobrowsinghub.salesmeet.it
127.0.0.1 uiextools.salesmeet.it
127.0.0.1 uiexfrontend.salesmeet.it
127.0.0.1 phpmyadmin.salesmeet.it

Starting services - Docker
==============================





```
docker-compose up -d
```

Se non è ancora stata creato la networks seguire le indicazioni di docker-compose.

In caso di errore "docker-compose --verbose up"


Avvio siti DEMO

```
docker-compose -f docker-compose-uiex.yml up

o

docker-compose -f docker-compose-salesmeet.yml up
docker-compose -f docker-compose-togetherjs.yml up
docker-compose -f docker-compose-uiex.yml up
```

docker-compose -f docker-compose-uiex-init.yml up

Stoping services
==============================

```
docker-compose stop
```

GIT
==============================


HTTPS locale
==============================

Installare https://certbot.eff.org

Per il mac la cartella di riferimento è: /etc/letsencrypt/archive/.../

Dominio su godaddy.com
facchini.corrado@gmail.com
La solita ...


Creare il certificato

```
sudo certbot certonly --manual -d *.salesmeet.it
https://devcenter.heroku.com/articles/ssl-certificate-self
```

Spostare il certificato nella cartella di traefik/

Verificare che non crei cert4 o cert5 o certN

```
cd  /traefik
sudo cp /etc/letsencrypt/archive/salesmeet.it/cert3.pem cert.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/chain3.pem chain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/fullchain3.pem fullchain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/privkey3.pem privkey.pem


sudo cp /etc/letsencrypt/archive/salesmeet.it-0001/cert.pem cert.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it-0001/chain6.pem chain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it-0001/fullchain6.pem fullchain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it-0001/privkey6.pem privkey.pem


sudo cp /etc/letsencrypt/archive/salesmeet.it/cert2.pem cert1.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/chain2.pem chain1.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/fullchain2.pem fullchain1.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/privkey2.pem privkey1.pem

sudo cp /etc/letsencrypt/archive/salesmeet.it/cert2.pem cert.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/chain2.pem chain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/fullchain2.pem fullchain.pem
sudo cp /etc/letsencrypt/archive/salesmeet.it/privkey2.pem privkey.pem


/etc/letsencrypt/live/salesmeet.it/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/salesmeet.it/privkey.pem

```

==============================
Compression Tools Javascript
==============================

https://javascript-minifier.com/
o
https://closure-compiler.appspot.com/home

==============================
Install linux
==============================

https://cloudaffaire.com/how-to-install-git-in-aws-ec2-instance/
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html
https://acloudxpert.com/how-to-install-docker-compose-on-amazon-linux-ami/


sudo snap install docker          # version 19.03.13, or
sudo apt  install docker-compose  # version 1.25.0-1

==============================
PROBLEMA DA RISOLVERE
==============================

docker ps -a

Entrare in questi docker

    docker exec -ti salesmeetdemo_salesmeet-booking_1 bash
    docker exec -ti salesmeetdemo_salesmeet-app_1 bash
    docker exec -ti salesmeetdemo_salesmeet-expert_1 bash
    docker exec -ti salesmeetdemo_salesmeet-profile_1 bash
    docker exec -ti salesmeetdemo_salesmeet-plugin_1 bash

e lanciare i comandi

    apt-get update
    docker-php-ext-install pdo_mysql
    docker-php-ext-install pdo pdo_mysql

restartare servizio


==============================
docker down
==============================

sudo service docker status
sudo systemctl start docker
