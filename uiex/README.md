# UIEX - user intelligent experience

Overview
========

Local domain
===========

Aggiungere all'hosts i seguenti records

127.0.0.1 www.ecommerce.local www.amado.local www.demohtml.local sottodominio.gestionale.local
127.0.0.1 monitor.uiex.app portainer.uiex.app mongoexpress.uiex.app
127.0.0.1 frontend.uiex.app tools.uiex.app advice.uiex.app

Starting services - Docker
==============================

Per lanciare il servizio

1) traefik - ../uiex/treafik/docker-compose.yml

```
cd traefik
docker-compose up -d
```

Se non è ancora stata creato la networks seguire le indicazioni di docker-compose.

In caso di errore "docker-compose --verbose up"

2) App - ../uiex/docker-compose.yml

```
cd /
docker-compose up -d
```

Avvio siti DEMO

```
docker-compose -f docker-compose-local.yml up
```


In caso di errore "docker-compose --verbose up"


Errori prima installazione - Docker
==============================

1) Al primo avvio di docker non abbiamo golang installato.
Ci sarebbe da alzare docker solo con questa configurazione per uiexappapi.

  uiexappapi:
    image: golang

2) Go non ha le librerie installate

- Se si ha installato GO

go get -v -u github.com/gorilla/mux

- Se NON si ha installato GO

https://github.com/gorilla/mux
https://github.com/avct/uasurfer
https://github.com/robbert229/jwt
https://github.com/go-mgo/mgo/tree/v2
https://github.com/pkg/errors


3) Potrebbe essere il certifiacato scaduto.



Stoping services
==============================

```
docker-compose stop
```


HTTPS locale
==============================

Installare https://certbot.eff.org

Per il mac la cartella di riferimento è: /etc/letsencrypt/archive/.../

Creare il certificato

sudo certbot certonly --manual -d *.uiex.app

add or updarte TXT record "_acme-challenge" in DNS

sudo certbot certonly --manual -d *.uiex.it
# sudo certbot certonly --manual -d uiex.it -d www.uiex.it
# sudo certbot certonly --manual -d uiex.app -d www.uiex.app

Spostare il certiticato nella cartella di traefik/traefik

Verificare che non crei cert4 o cert5 o certN

sudo cp /etc/letsencrypt/archive/uiex.app/cert1.pem cert1.pem
sudo cp /etc/letsencrypt/archive/uiex.app/chain1.pem chain1.pem
sudo cp /etc/letsencrypt/archive/uiex.app/fullchain1.pem fullchain1.pem
sudo cp /etc/letsencrypt/archive/uiex.app/privkey1.pem privkey1.pem

sudo cp /etc/letsencrypt/archive/uiex.app/cert3.pem cert3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/chain3.pem chain3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/fullchain3.pem fullchain3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/privkey3.pem privkey3.pem


sudo cp /etc/letsencrypt/archive/uiex.app/cert4.pem cert3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/chain4.pem chain3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/fullchain4.pem fullchain3.pem
sudo cp /etc/letsencrypt/archive/uiex.app/privkey4.pem privkey3.pem


User
Intelligent
EXperience

GIT
==============================

git clone https://uiex@bitbucket.org/uiex/uiex.git


git add .
git commit -m "docker separato tra standard ed demo ..."
git push


Access service
==============================

* Demo - www.ecommerce.local - Demo e-commerce di base.
* Demo - www.amado.local - Demo e-commerce con: voce menu personalizzata e div di ritorno integrato nel sito.
* Demo - www.demohtml.local - Demo sito aziendale con: popup lead generation al onfocus della pagina, colori e testi personalizzati.

* Demo - sottodominio.gestionale.local - Demo di prova per gestionale.
* Traefik - monitor.uiex.app
* Reader Mongo - mongoexpress.uiex.app
* Container docker - portainer.uiex.app  (http://portainer.uiex.app:9000/#/init/endpoint)



Estensione CHROME
==============================

chrome://extensions/


Idee
==============================

                    Definire una registrazione / TAG per singolo contesto.
                    Ad esempio: sto cercando una tuta da sci. In UIEx definire che tutte le pagine che navigo in quel momento
                    saranno raggruppate per TAG "Tuta Sci".
                    ______________________________
                    Vicino ad ogni singolo link da mettere richiedi informazioni
                    ______________________________
                    Contact Form / Lead:
                    ______________________________
                    Suddivisione per type presi dallo schema.org o capendo logiche della pagina (name, url o parole chiave)
                    ______________________________
                    - Form di richiesta contatti...
                    ______________________________
                    Quando cerco di abbandonare il sito mi esce un form di "Vuoi avere maggiori informazioni sul prodotto più visualizzato? Contattaci..."
                    ______________________________
                    Le pagine di default di HOME / index.html / index.php ... da capire come uscirne per farle vedere sempre prime ...
                    Calcolo del tempo attivo sulla pagina ...
                    Script che monitora l'utilizzo della pagina (movimenti del mouse, hover su oggetti significativi)...
                    ______________________________
                    404 - mi fa uscire un popup o mi rimanda ad un simile / collegato ...
                    Da capire le logiche:
                    - E' la pagina 404?
                    - Che pagine consiglio? Avranno logiche differenti rispetto alla wishlist?
                    ______________________________
                    Tienimi aggioranto ...
                    Potrebbe farlo il software capendo i preferiti...
                    ______________________________
                    Applicato al browser ...
                    Così va su tutti i siti..
                    ______________________________
                    Login ...
                    Per il cross device..
                    ______________________________
                    Esportazione dati verso DMP, acronimo che sta per data management platform.


Competitor
==============================

https://xgen.tech/#/
