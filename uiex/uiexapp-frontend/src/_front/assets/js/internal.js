class UserIntelligentExperienceInternalApp {

    constructor(token,user) {
        this.radice = "vachebelservizio";
        
        this.remoteUrlFront = "https://frontend.uiex.app/";
        this.remoteUrlFrontNotImage = this.remoteUrlFront + "images/picture.png";
        
        this.remoteUrlCall = "https://tools.uiex.app/chronology/";
        this.remoteUrlWhislist = this.remoteUrlCall + "experience/";
        this.remoteUrlConsigliati = "https://advice.uiex.app/consigliati/";
        // this.remoteUrlToolConsigliati= "https://advice." + this.uiexDomain + "/consigliato/";
        this.remoteUrlLast = this.remoteUrlCall + "last/";
        this.remoteUrlChronology = this.remoteUrlCall + "chronology/";
        this.remoteUrlChronologyLast = this.remoteUrlCall + "chronology/last/";
        this.remoteUrlChronologyCELast = this.remoteUrlCall + "customerexperience/";
        this.remoteUrlDelete = this.remoteUrlCall + "delete/";

        this.cookieMilliseconds = this.radice + "milliseconds";
        this.cookieTimeOutside = this.radice + "TimeOutside";
        this.cookieIdBefore = this.radice + "idBefore";
        this.cookieFlagContatoreMovimento = this.radice + "flagContatoreMovimento";
        this.cookieFlagOverOggetti = this.radice + "flagOverOggetti";
        this.cookieScrollY = this.radice + "scrollY";
        this.cookieScrollX = this.radice + "scrollX";
    }

    getCookie(cname) {
        var temp = localStorage.getItem(cname);
        if (temp==null) {
            return "";
        } else {
            return temp;
        }
    }

    callChronology(token,user) {
       var referrer = this.getReferrer();
        // console.log( this.remoteUrlChronology + referrer );    
        $('#chronology').DataTable( {
            "ajax": {
                "url": this.remoteUrlChronology,
                "type": "POST",
                "dataSrc": "",
                "data" : {
                    "token" : token,
                    "user": user,
                    "site": referrer
                }                      
            },
            "columns": [
                { "data": "Meta.ImgUrl",
                    "render": function (data, type, row) {
                        if (data == '') {
                            return '<img src="' + userIntelligentExperienceInternalAppInstance.remoteUrlFrontNotImage + '" style="height:25px" />';
                        }  else {
                            return '<img src="' + data + '" style="height:45px" />';
                        }
                    }
                },    
                { "data": "Meta.Title",
                    "render": function (data, type, row) {
                        var urlTrack = userIntelligentExperienceInternalAppInstance.addTrack( row.Url, 3 );
                        return '<a target="_parent" href="' + urlTrack + '">' + data + '</a>';
                    }
                },    
                { "data": "Price.Price"},
                { "data": "Price.Currency"},
                { "data": "Value.Voto"},   
                { "data": "Tempo.Reale",
                    "render": function (data, type, row) {
                        data = data / 1000;
                        var hrs = ~~(data / 3600);
                        var mins = ~~((data % 3600) / 60);
                        var secs = ~~data % 60;
                        return hrs + ":" + mins + ":" + secs;
                    }
                },
                { "data": "Meta.SchemaType"},
                { "data": "Timestamp",
                    "render": function (data, type, row) {
                        var dataInt = new Date(data);
                        return dataInt.toLocaleString();
                    }
                }
            ]
        } );
    }    


    callMeglioNavigati(token,user) {

        var urlInternal = this.remoteUrlWhislist;
        var idDiv = "whislist"
        var oReq = new XMLHttpRequest();
        var remoteUrlFrontNotImage = this.remoteUrlFrontNotImage;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            //console.log( jsonData ); 
            var element = "";
            for (var i = 0; i < jsonData.length; i++) {
                var urlTrack = userIntelligentExperienceInternalAppInstance.addTrack( jsonData[i]["url"], 2 );
                var remoteUrlFrontNotImageInternal = remoteUrlFrontNotImage;
                if ((jsonData[i]["img"]!=null) && (jsonData[i]["img"]!="")) { 
                    remoteUrlFrontNotImageInternal = jsonData[i]["img"]; 
                }
                var dimensione = 150;
                var title = jsonData[i]["title"];
                if (title.length>dimensione) {
                    title = title.substr(0, 150) + "...";
                }
                element += `<div class="item-box">
                                <div class="contenitore-box">
                                    <div class="img">
                                            <a target="_parent" href="${urlTrack}">
                                            <img style="" src="${remoteUrlFrontNotImageInternal}">
                                            </a>
                                    </div>
                                    <div class="title">
                                            <a target="_parent" href="${urlTrack}">
                                                ${title}
                                                <br>
                                                Voto max: ${jsonData[i]["voto_max"]}
                                                <br>
                                                Voto min: ${jsonData[i]["voto_min"]}
                                                <br>
                                                Numero visualizzazioni: ${jsonData[i]["count"]}
                                            </a>
                                    </div>
                                </div>
                            </div>`; 
            }
            document.getElementById(idDiv).innerHTML = element;
        }
        var referrer = this.getReferrer();
        oReq.open("POST", urlInternal );
        var formData = new FormData();
        formData.append("token", token );
        formData.append("user", user );
        formData.append("site", referrer );        
        oReq.send( formData ); 
    }    


    callConsigliati(token,user) {
        var urlInternal = this.remoteUrlConsigliati
        var idDiv = "consigliati"
        var oReq = new XMLHttpRequest();
        var remoteUrlFrontNotImage = this.remoteUrlFrontNotImage;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            var element = "";
            if (jsonData.length==0) {
                element = "Nessun consigliato.";
            }            
            for (var i = 0; i < jsonData.length; i++) {
                // console.log( jsonData[i] ); 
                var urlTrack = userIntelligentExperienceInternalAppInstance.addTrack( jsonData[i]["Url"], 2 );
                var remoteUrlFrontNotImageInternal = remoteUrlFrontNotImage;
                if ((jsonData[i]["Pagina"][0]["Meta"]["ImgUrl"]!=null) && (jsonData[i]["Pagina"][0]["Meta"]["ImgUrl"]!="")) { 
                    remoteUrlFrontNotImageInternal = jsonData[i]["Pagina"][0]["Meta"]["ImgUrl"]; 
                }
                var dimensione = 150;
                var title = jsonData[i]["Pagina"][0]["Meta"]["Title"];
                if (title.length>dimensione) {
                    title = title.substr(0, 150) + "...";
                }
                element += `<div class="item-box">
                                <div class="contenitore-box">
                                    <div class="img">
                                            <a target="_parent" href="${urlTrack}">
                                            <img style="" src="${remoteUrlFrontNotImageInternal}">
                                            </a>
                                    </div>
                                    <div class="title">
                                            <a target="_parent" href="${urlTrack}">
                                                ${title}
                                                <br>
                                                Count: ${jsonData[i]["Count"]}
                                                <br>
                                                Somma: ${jsonData[i]["Somma"]}
                                                <br>
                                                Nuomer visualizzazioni: ${jsonData[i]["count"]}
                                            </a>
                                    </div>
                                </div>
                            </div>`;
                          
            }
            document.getElementById(idDiv).innerHTML = element;
        }
        var referrer = this.getReferrer();
        oReq.open("POST", urlInternal );
        var formData = new FormData();
        formData.append("token", token );
        formData.append("user", user );
        formData.append("site", referrer );        
        oReq.send( formData ); 
    }    

    async callLast() {
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            var element = "";
            for (var i = 0; i < jsonData.length; i++) {
                document.getElementById("idPagina").innerHTML = jsonData[i]["id"] ;
                document.getElementById("VotoPagina").innerHTML = jsonData[i]["Value"]["Voto"] ;
                document.getElementById("MetaTitle").innerHTML = jsonData[i]["Meta"]["Title"] ;
                document.getElementById("MetaImgUrl").innerHTML = `<img style="max-width:100%;" src="${jsonData[i]["Meta"]["ImgUrl"]}">`;
                document.getElementById("MetaSchemaType").innerHTML = jsonData[i]["Meta"]["SchemaType"] ;
                document.getElementById("MetaDescription").innerHTML = jsonData[i]["Meta"]["Description"] ;
                userIntelligentExperienceInternalAppInstance.callLastCE( jsonData[i]["id"] );

            }
        }
        /*
        var referrer = new URL(document.referrer).hostname;
        var re = new RegExp('/', 'g');
        referrer = referrer.replace(re, "@");
        */
        var referrer = this.getReferrer();
        oReq.open("GET", this.remoteUrlChronologyLast + referrer );
        oReq.send();
    }  

    async callLastCE( id ) {
        // console.log("callLastCE");
        // console.log(id);
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            console.log(jsonData);
            for (var i = 0; i < jsonData.length; i++) {

                var time = "Totale: " + jsonData[i]["Time"]["TotaleMovimento"] + "<br>Reale: " + jsonData[i]["Time"]["Reale"];
                document.getElementById("cookieMilliseconds").innerHTML = time ;
                document.getElementById("cookieTimeOutside").innerHTML = jsonData[i]["Time"]["InOut"] ;

                document.getElementById("cookieFlagContatoreClick").innerHTML =  jsonData[i]["Oggetti"]["Click"]  ;    
                document.getElementById("cookieFlagContatoreMovimento").innerHTML =  jsonData[i]["Oggetti"]["Contatore"]  ;    
                var oggetto = jsonData[i]["Oggetti"]["Elenco"].replace('<', '@');
                document.getElementById("cookieFlagOverOggetti").innerHTML =  oggetto.replace('>', '@');  ;    
                
                document.getElementById("cookieScrollX").innerHTML = jsonData[i]["Scroll"]["X"] ;
                document.getElementById("cookieScrollY").innerHTML = jsonData[i]["Scroll"]["Y"] ;
                
            }
        }
        /*
        var referrer = new URL(document.referrer).hostname;
        var re = new RegExp('/', 'g');
        referrer = referrer.replace(re, "@");
        */
        var referrer = this.getReferrer();
        oReq.open("GET", this.remoteUrlChronologyCELast + id + "/" + referrer );
        oReq.send();
    }  

    async delete(token,user) {
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            $("#context").html('<div class="contenuto">Cancellazione avvenuta correttamente.</div>');
        }        
        var referrer = this.getReferrer();
        oReq.open("POST", this.remoteUrlDelete );
        var formData = new FormData();
        formData.append("token", token );
        formData.append("user", user );
        formData.append("site", referrer );        
        oReq.send( formData );         
    }  

    log(messaggio) {
        console.log(messaggio);
    }  
    getReferrer() {
        var referrer = new URL(document.referrer).hostname;
        var re = new RegExp('/', 'g');
        referrer = referrer.replace(re, "@");
        referrer = this.removeSubdomain(referrer);
        return referrer;
    }  
    removeSubdomain(url) {
            var parts = url.replace(/^(www\.)/,"").split('.');
            //is there a subdomain? 
            while(parts.length > 2){
                //removing it from our array 
                var subdomain = parts.shift();
            }
            //getting the remaining 2 elements
            var domain = parts.join('.');
            return domain.replace(/(^\.*)|(\.*$)/g, "");
    }
    

    // track *******
    addTrack( url, type) {
        if(url.indexOf('?') != -1) {
            url = url + "&uiextrack=" + type;
        } else {
            url = url + "?uiextrack=" + type;
        }
        return url;
    }  
}


let userIntelligentExperienceInternalAppInstance = new UserIntelligentExperienceInternalApp();

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};