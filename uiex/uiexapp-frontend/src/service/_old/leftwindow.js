class UserIntelligentExperienceAppLeftWindow  {

    constructor() {
        let sevolution = new UserIntelligentExperienceApp();
        this.settingContext(sevolution);
        console.log("UserIntelligentExperienceAppLeftWindow");
    }

    callWhislist(idDiv) {
        var oReq = new XMLHttpRequest();
        var radice = this.radice;
        var consigliati_testo = this.consigliati_testo;
        var remoteUrlFrontNotImage = this.remoteUrlFrontNotImage;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            var element = "";
            for (var i = 0; i < jsonData.length; i++) {
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["img"]!=null) ) { 
                    remoteUrlFrontNotImage = jsonData[i]["img"]; 
                }
                element += '<div class="items"><input type="checkbox" name="" value="' + jsonData[i]["title"] + '">' +  
                           '<img style="max-height:45px;max-width: 45px;" src="' + remoteUrlFrontNotImage + '">' + jsonData[i]["title"] + '</a></div>';
            }
            document.getElementById(radice + idDiv).innerHTML = element;
        }
        oReq.open("GET", this.remoteUrlWhislist );
        oReq.send();
    }       

    // LASCIO IL SITO USCENDO DALLA FINESTRA
    createDivLeftWindow() { 

        var eltemp = document.getElementById( this.radice + 'DivLeftWindow');
        if (eltemp == null) { 

            var elemDiv = document.createElement('div');
            elemDiv.setAttribute("id", this.radice + "DivLeftWindow");
            elemDiv.style.cssText = 'position:fixed;top:0px;left:0px;width:100%;height:100%;z-index:99999;' + 
                                    'background: rgba(0, 0, 0, 0.72); padding:10px;';
                                    
            var elemDivClose = document.createElement('div');
            elemDivClose.style.cssText = 'position:relative;top: 20%;right:25%;text-align:right;color:#fff;';
            elemDivClose.setAttribute("id", this.radice + "DivLeftWindowClose");
            elemDivClose.innerHTML = "<a style='color:#fff;' id='DivLeftWindowCloseButton' href='#'>" + this.label_close + "</a>";

            var elemFrame = document.createElement('div');
            elemFrame.innerHTML = this.left_windows_testo + ":<br><br>" + 
                                  "<div id='" + this.radice + "DivLeftWindowWishlist'></div><br>" + 
                                  "<textarea style='width:100%;height: 43px;' placeholder='messaggio'></textarea><br>" + 
                                  "<input style='width:100%;' type='email' value='' placeholder='email'><br><br>" + 
                                  "<input type='button' value='Invia'>";

            elemFrame.style.cssText = 'position:relative;width:50%;left:25%;top:20%;' + 
                                      'background:' + this.left_windows_color + ';padding:10px;' + 
                                      'border: 1px solid ' + this.left_windows_border;

            elemDiv.appendChild(elemDivClose);
            elemDiv.appendChild(elemFrame);
            document.body.appendChild(elemDiv);       

            var el = document.getElementById( this.radice + 'DivLeftWindowClose');
            var radice = this.radice ;
            el.onclick = function (e) {
            var ev = e || window.event;
                    document.getElementById( radice + "DivLeftWindow").remove();
            }        
            this.callWhislist("DivLeftWindowWishlist",);

        }

    }    
    settingContext(sevolution) {
        this.remoteUrlWhislist = sevolution.remoteUrl + "consigliati/limit/";
        // common
        this.label_close = sevolution.label_close;
        // LeftWindowDiv
        this.left_windows_border = sevolution.left_windows_border;
        this.left_windows_color = sevolution.left_windows_color;
        this.left_windows_testo = sevolution.left_windows_testo;

        this.left_windows_border = "#000";
        this.left_windows_color = "#fff;";
        this.left_windows_testo = "Richiedi informazioni per";        

        this.radice = sevolution.radice;
        // Assegno variabili esterne
        this.remoteUrlFrontNotImage = sevolution.remoteUrlFrontNotImage;
    }

}