class UserIntelligentExperienceAppStatistics {

    // https://www.polkandunion.com/blog/anticipatory-design/
    
    constructor(sevolution) {
        this.uiex = sevolution;
        this.init();
    }
    init() {
        this.idDiv = "DivReturnChronology";
        // console.log("UserIntelligentExperienceAppStatistics****");
        // console.log(this.uiex.radice + this.idDiv);
        if (document.getElementById(this.uiex.radice + this.idDiv) ==null) {
            // console.log("null****");
            return "";
        } 
        // console.log("innerHTML****");
        document.getElementById(this.uiex.radice + this.idDiv).innerHTML = `
        <div style="min-width: 240px;" class="numero"><div style="height:40px;font-family:arial;font-size:15px;">${this.uiex.label_statistic}</div>
        <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;font-family:arial;font-size:15px;" id="${this.uiex.radice}StatisticsAll"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontStatisticsGroup}"><br>${this.uiex.label_statistic_all}</div></a>
        <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;padding-left: 30px;font-family:arial;font-size:15px;" id="${this.uiex.radice}StatisticsSingle"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontStatisticsUser}"><br>${this.uiex.label_statistic_single}</div></a>`;
        
        var el = document.getElementById( this.uiex.radice + 'StatisticsAll');
        el.onclick = function (e) {
            // console.log("StatisticsAll****");
            userIntelligentExperienceAppStatistics.all();
        }
        var el = document.getElementById( this.uiex.radice + 'StatisticsSingle');
        el.onclick = function (e) {
            // console.log("StatisticsSingle****");
            userIntelligentExperienceAppStatistics.single();
        }
    }  

    all() {
        var idDiv = this.idDiv;
        if (document.getElementById(this.uiex.radice + idDiv) ==null) {
            return "";
        } 
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var label_statistic = this.uiex.label_statistic + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontStatisticsGroup + '"> ' + this.uiex.label_statistic_all;
        var label_statistic_count = this.uiex.label_statistic_count;
        var label_statistic_last_date = this.uiex.label_statistic_last_date;
        var label_statistic_vote = this.uiex.label_statistic_vote;  
        var label_statistic_medium_vote = this.uiex.label_statistic_medium_vote;  
        var label_statistic_max_vote = this.uiex.label_statistic_max_vote;  
        var label_statistic_min_vote = this.uiex.label_statistic_min_vote;        
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato = this.uiex.nessun_risultato;
        var uiex = this.uiex;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log(jsonData);
            var element =  "";
            for (var i = 0; i < jsonData.length; i++) {
                // Stelle
                var stelleMedia = uiex.stelle( jsonData[i]["Statistic"]["MediaVoto"] / jsonData[i]["Statistic"]["Count"] );
                var stelleMax = uiex.stelle( jsonData[i]["Statistic"]["MaxVoto"] );
                var stelleMin = uiex.stelle( jsonData[i]["Statistic"]["MinVoto"] );
                // immagine presente
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["Meta"]["ImgUrl"]!="")&&(jsonData[i]["Meta"]["ImgUrl"]!=null) ) { 
                    remoteUrlFrontNotImageInterno = jsonData[i]["Meta"]["ImgUrl"]; 
                } 

                var d = new Date(jsonData[i]["Statistic"]["LastVisit"]);
                var lastVisit = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear()  + " - " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

                element += `<div style="min-width:240px;width:100%;" class="numero"><div style="height:38px;font-family:arial;font-size: 16px;">${label_statistic}</div>
                                <div style="float:left;padding-left:3px;padding-right:10px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                <div style="floaf:left;width:100%;">
                                    <div style="color:#000;font-size:13px;padding-left:3px;width:100%;font-family:arial;height:21px;" class="titolo">${jsonData[i]["Meta"]["Title"]}</div>
                                    <div style="color:#000;font-size:13px;padding-left:3px;padding-bottom:5px;width:100%;font-family:arial;height:21px;" class="titolo">
                                        <b>${label_statistic_vote}</b> ${label_statistic_medium_vote}: ${stelleMedia}
                                        - ${label_statistic_max_vote}: ${stelleMax} 
                                        - ${label_statistic_min_vote}: ${stelleMin} 
                                    </div>
                                    <div style="color:#000;font-size:13px;padding-left:3px;padding-bottom:5px;width:100%;font-family:arial;height:21px;" class="titolo">    
                                        <b>${label_statistic_count}</b>: ${jsonData[i]["Statistic"]["Count"]}
                                        &bull; <b>${label_statistic_last_date}</b>: ${lastVisit} 
                                    </div>
                                </div>
                            </div>`;
                            /*
                            <div style="color:#000;font-size:13px;padding-left:3px;width:100%;" class="titolo">${jsonData[i]["Meta"]["Description"]}</div>
                            <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["voto_AI"]}</div>
                            <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["count"]} - ${jsonData[i]["voto_AI"] / jsonData[i]["count"]}</div>
                            */                
            }
            try {
                document.getElementById(radice + idDiv).innerHTML = element;
                
                if (jsonData.length == 0) {
                    document.getElementById(radice + idDiv).innerHTML = nessun_risultato;
                    // elimo banner se non trovo risultati 
                    /*
                        document.getElementById( radice + 'DivContenuto').remove();
                        document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                        alert("");
                    */
                } 
            } catch (e) { }
        }
        oReq.open("GET", this.uiex.remoteUrlToolStatistics);
        oReq.send();

        
    }         

    single() {

        var nessun_risultato = this.uiex.nessun_risultato;
        var idDiv = this.idDiv;
        if (document.getElementById(this.uiex.radice + idDiv) ==null) {
            return "";
        } 
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var label_statistic = this.uiex.label_statistic + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontStatisticsUser + '"> ' + this.uiex.label_statistic_single;
        var label_statistic_count = this.uiex.label_statistic_count;
        // var label_statistic_last_date = this.uiex.label_statistic_last_date;
        var label_statistic_vote = this.uiex.label_statistic_vote;  
        var label_statistic_medium_vote = this.uiex.label_statistic_medium_vote;  
        var label_statistic_max_vote = this.uiex.label_statistic_max_vote;  
        var label_statistic_min_vote = this.uiex.label_statistic_min_vote;        
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var uiex = this.uiex;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log(jsonData);
            var element =  "";
            for (var i = 0; i < jsonData.length; i++) {
                // Stelle
                var stelleMedia = uiex.stelle( jsonData[i]["Votomedio"] / jsonData[i]["Count"] );
                var stelleMax = uiex.stelle( jsonData[i]["Votomax"] );
                var stelleMin = uiex.stelle( jsonData[i]["Votomin"] );
                // immagine presente
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["Imgurl"]!="")&&(jsonData[i]["Imgurl"]!=null) ) { 
                    remoteUrlFrontNotImageInterno = jsonData[i]["Imgurl"]; 
                } 

                element += `<div style="min-width:240px;width:100%;" class="numero"><div style="height:38px;font-family:arial;font-size: 16px;">${label_statistic}</div>
                                <div style="float:left;padding-left:3px;padding-right:10px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                <div style="floaf:left;width:100%;">
                                    <div style="color:#000;font-size:13px;padding-left:3px;width:100%;font-family:arial;height:21px;" class="titolo">${jsonData[i]["Title"]}</div>
                                    <div style="color:#000;font-size:13px;padding-left:3px;padding-bottom:5px;width:100%;font-family:arial;height:21px;" class="titolo">
                                        <b>${label_statistic_vote}</b> ${label_statistic_medium_vote}: ${stelleMedia}
                                        - ${label_statistic_max_vote}: ${stelleMax} 
                                        - ${label_statistic_min_vote}: ${stelleMin} 
                                    </div>
                                    <div style="color:#000;font-size:13px;padding-left:3px;padding-bottom:5px;width:100%;font-family:arial;height:21px;" class="titolo">    
                                        <b>${label_statistic_count}</b>: ${jsonData[i]["Count"]}
                                    </div>
                                </div>
                            </div>`;
                            /*
                            <div style="color:#000;font-size:13px;padding-left:3px;width:100%;" class="titolo">${jsonData[i]["Description"]}</div>
                            <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["voto_AI"]}</div>
                            <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["count"]} - ${jsonData[i]["voto_AI"] / jsonData[i]["count"]}</div>
                            */                
            }
            try {
                document.getElementById(radice + idDiv).innerHTML = element;
                
                if (jsonData.length == 0) {
                    document.getElementById(radice + idDiv).innerHTML = nessun_risultato;
                    // elimo banner se non trovo risultati 
                    /*
                        document.getElementById( radice + 'DivContenuto').remove();
                        document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                        alert("");
                    */
                } 
            } catch (e) { }
        }
        oReq.open("POST", this.uiex.remoteUrlToolStatisticsSingle);
        var formData = new FormData();
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );
    }        

    
}

let userIntelligentExperienceAppStatistics = new UserIntelligentExperienceAppStatistics( userIntelligentExperienceAppInstance );





