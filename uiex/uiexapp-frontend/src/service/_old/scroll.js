class UserIntelligentExperienceAppScroll {

    constructor(sevolution) {
        this.userIntelligentExperienceAppInstance = sevolution;
    }
    get() {
        var scrollX = this.getScrollX();
        if ( scrollX > 0 ) {
            userIntelligentExperienceAppInstance.aScrollX.push(scrollX);
        }
        var scrollY = this.getScrollY();
        if ( scrollY > 0 ) {
            userIntelligentExperienceAppInstance.aScrollY.push(scrollY);
        }
    }
    getScrollX() {
        try { return window.pageXOffset || document.body.scrollLeft || document.html.scrollLeft; } catch(err) { return 0;}
    };
    getScrollY() {
        try { return window.pageYOffset || document.body.scrollTop || document.html.scrollTop; } catch(err) { return 0; }
    }

}
