class UserIntelligentExperienceAppExperience {

    constructor(sevolution) {
        this.uiex = sevolution;
        this.init();
    }

    init() {
        var idDiv = "DivReturnChronology";
        if (document.getElementById(this.uiex.radice + idDiv) ==null) {
            return "";
        } 
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var label_experience = this.uiex.label_experience;
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato = this.uiex.nessun_risultato;
        var uiex = this.uiex;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log(jsonData);
            // console.log(jsonData[jsonData.length - 1]["voto_AI"]);
            var element =  "";
            for (var i = 0; i < jsonData.length; i++) {
                var stelle = uiex.stelle( jsonData[i]["voto_AI"] );
                // Barra
                if (i>0) { label_experience = "&nbsp;"; }
                // immagine presente
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                var flag_img = true;
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["img"]!=null) ) { 
                    remoteUrlFrontNotImageInterno = jsonData[i]["img"]; 
                    flag_img = false;
                } 
                var price = "";
                if ( !uiex.isPriceZero(jsonData[i]["price"]) ){
                    price = "<br><b>" + jsonData[i]["price"] + " " + jsonData[i]["currency"] + "</b>";
                }
                if (!flag_img) {
                    var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["url"], 1);
                    element += `<div style="min-width: 240px;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_experience}</div><a href="${urlTrack}">
                                <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;letter-spacing:0px;line-height:16px;" class="titolo">${stelle}${price}<br>${jsonData[i]["title"]}</div>
                                </a></div>`;
                                /*
                                <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["voto_AI"]}</div>
                                <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;" class="titolo">${jsonData[i]["count"]} - ${jsonData[i]["voto_AI"] / jsonData[i]["count"]}</div>
                                */                
                } 
            }
            try {
                document.getElementById(radice + idDiv).innerHTML = element;
                // elimo banner se non trovo risultati 
                if (jsonData.length == 0) {
                         document.getElementById(radice + idDiv).innerHTML = nessun_risultato;
                        // document.getElementById( radice + 'DivContenuto').remove();
                        // document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                } 
            } catch (e) { }
        }
        oReq.open("POST", this.uiex.remoteUrlToolExperience);
        var formData = new FormData();
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );
    }               
    
}

var userIntelligentExperienceAppExperience = new UserIntelligentExperienceAppExperience( userIntelligentExperienceAppInstance );


