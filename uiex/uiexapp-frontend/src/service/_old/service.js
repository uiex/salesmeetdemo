class UserIntelligentExperienceAppService {

    constructor(radice,button_testo,button_label_color,button_colore,button_colore_border,label_close, remoteUrlFront) {
        this.radice = radice;
        // Bottone
        this.button_testo = button_testo;
        this.button_label_color = button_label_color;
        this.button_colore = button_colore;
        this.button_colore_border = button_colore_border;
        this.label_close = label_close;
        this.remoteUrlFront = remoteUrlFront;
        this.remoteUrlFrontService = remoteUrlFront + "front/index.html?id=";
    }

    // DIV CON TUTTI I CONTENUTI
    createDivService( id, token, user ) { 

        var elemDiv = document.createElement('div');
        elemDiv.setAttribute("id", this.radice + "DivAll");
        elemDiv.style.cssText = 'position:fixed;top:0px;left:0px;width:100%;height:100%;z-index:9999999999 !important;background:#ffffff;';
        
        var elemDivClose = document.createElement('div');
        elemDivClose.style.cssText = 'width:100%;text-align:right;padding-right:5px;';
        elemDivClose.setAttribute("id", this.radice + "divAllClose");
        elemDivClose.innerHTML = `<a style="padding-right: 5px;font-size: 14px;color: #000;font-weight: bold;" id='divAllCloseButton' href='javascript:void(0);'>${this.label_close}</a>`;

        var elemFrame = document.createElement('iframe');
        elemFrame.setAttribute("id", this.radice + "seframeIframe");
        
        elemFrame.setAttribute("src", this.remoteUrlFrontService + id + "&token=" + token + "&user=" + user);
        elemFrame.setAttribute("scrolling", "yes");
        elemFrame.style.cssText = 'width:100%;height:100%;);';

        elemDiv.appendChild(elemDivClose);
        elemDiv.appendChild(elemFrame);
        document.body.appendChild(elemDiv);       

        var el = document.getElementById( this.radice + 'divAllClose');
        var radice = this.radice ;
        el.onclick = function (e) {
          var ev = e || window.event;
          document.getElementById( radice + "DivAll").remove();
        }        
    }    

}