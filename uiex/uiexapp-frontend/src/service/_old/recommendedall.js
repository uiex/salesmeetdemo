class UserIntelligentExperienceAppExperienceAll {

    constructor(sevolution) {
        this.uiex = sevolution;
        this.idDiv = "DivReturnChronology";
        this.to_launch = this.uiex.to_launch;
        this.brand = this.uiex.brand;

        this.brand = this.uiex.brand;
        if (this.brand != "") {

            this.uiex.label_recommended_all = this.brand;
            this.callConsigliatiAddon();

        } else {
                
            if (this.to_launch=="words") {
                this.callConsigliatiPages();
            } else if (this.to_launch=="behavior") {
                this.callConsigliatiNavigation();
            } else if (this.to_launch=="sites") {
                this.callConsigliatiSite();
            } else {
                this.init()
            }

        }
    }

    init() {

        this.idDiv = "DivReturnChronology";
        // console.log("UserIntelligentExperienceAppStatistics****");
        // console.log(this.uiex.radice + this.idDiv);
        if (document.getElementById(this.uiex.radice + this.idDiv) ==null) {
            // console.log("null****");
            return "";
        } 
        if (this.brand != "") {

            this.uiex.label_recommended_all = this.brand;
            userIntelligentExperienceAppExperienceAll.callConsigliatiAddon();

        } else {

            var domainName = "<br>(" + this.uiex.currentDomain.replace("www.", "") + ")";
            // console.log("innerHTML****");
            document.getElementById(this.uiex.radice + this.idDiv).innerHTML = `
            <div style="min-width: 240px;" class="numero"><div style="height:40px;font-family:arial;font-size: 15px;">${this.uiex.label_recommended}</div>
            <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;font-family:arial;font-size: 15px;" id="${this.uiex.radice}recommendedSite"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontRecommendedSite}"><br>${this.uiex.label_recommended_words}${domainName}</div></a>
            <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;padding-left: 30px;font-family:arial;font-size: 15px;" id="${this.uiex.radice}recommendedNavigation"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontRecommendedNavigation}"><br>${this.uiex.label_recommended_navigation}${domainName}</div></a>
            <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;padding-left: 30px;font-family:arial;font-size: 15px;" id="${this.uiex.radice}recommendedAll"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontRecommendedAll}"><br>${this.uiex.label_recommended_all}</div></a>
            <a href="javascript:void(0);"><div style="float:left;text-align:center;color:#000000;padding-left: 30px;font-family:arial;font-size: 15px;" id="${this.uiex.radice}recommendedAddSite"><img style="max-height:21px;display:inline-block;" src="${this.uiex.remoteUrlFrontRecommendedAddSite}"><br>${this.uiex.label_recommended_addsite}</div></a>
            `;    
            var el = document.getElementById( this.uiex.radice + 'recommendedSite');
            el.onclick = function (e) {
                // console.log("StatisticsAll****");
                userIntelligentExperienceAppExperienceAll.callConsigliatiPages();
            }
            var el = document.getElementById( this.uiex.radice + 'recommendedNavigation');
            el.onclick = function (e) {
                // console.log("StatisticsAll****");
                userIntelligentExperienceAppExperienceAll.callConsigliatiNavigation();
            }
            var el = document.getElementById( this.uiex.radice + 'recommendedAll');
            el.onclick = function (e) {
                userIntelligentExperienceAppExperienceAll.callConsigliatiSite();
            }
            var el = document.getElementById( this.uiex.radice + 'recommendedAddSite');
            el.onclick = function (e) {
                userIntelligentExperienceAppExperienceAll.callConsigliatiAddon();
            }
            
        }

    }
    
    callConsigliatiNavigation() {

        var idDiv = "DivReturnChronology";
        if (document.getElementById(this.uiex.radice + idDiv) ==null) {
            return "";
        } 
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato = this.uiex.nessun_risultato;
        var label_recommended = this.uiex.label_recommended_short + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontRecommendedNavigation + '"> ' + this.uiex.label_recommended_navigation;
        var uiex = this.uiex;
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log(jsonData);
            var element = "";

            for (var i = 0; i < jsonData.length; i++) {
                var price = "";
                try {
                    if ( !uiex.isPriceZero(jsonData[i]["Pagina"][0]["Price"]["Price"]) ){
                        price = "<b>" + jsonData[i]["Pagina"][0]["Price"]["Price"] + " " + jsonData[i]["Pagina"][0]["Price"]["Currency"] + "</b><br>";
                    }         
                } catch (e) { }      
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                var flag_img = true;
                if ((jsonData[i]["Imgurl"]!="")&&(jsonData[i]["Imgurl"]!=null) ) { 
                    remoteUrlFrontNotImageInterno = jsonData[i]["Imgurl"]; 
                    flag_img = false;
                } 
                if (!flag_img) {
                    var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 1);
                    element += `<div style="min-width: 240px;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_recommended}</div><a href="${urlTrack}">
                    <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                    <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;letter-spacing:0px;line-height:16px;" class="titolo">${price}${jsonData[i]["Pagina"][0]["Meta"]["Title"]}</div>
                    </a></div>`;
                    label_recommended = "";
                } 
            }
            try { 
                document.getElementById(radice + idDiv).innerHTML = element;                
                if (jsonData.length == 0) {

                    var nessuno = `<div style="min-width: 240px;" class="numero"><div style="height:38px;font-family:arial;font-size:15px;">${label_recommended}</div>`;
                    document.getElementById(radice + idDiv).innerHTML = nessuno + nessun_risultato;

                } 
            } catch (e) { }
        }
        /*
        oReq.open("GET", this.uiex.remoteUrlConsigliati + "consigliati/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.uiex.cookieIdBefore) );
        oReq.send( formData );
        */
        oReq.open("POST", this.uiex.remoteUrlConsigliati + "consigliati/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.uiex.cookieIdBefore) );
        formData.append("titleConsigliato", this.uiex.titleConsigliato);
        formData.append("words", this.uiex.words);
        formData.append("schemaType", this.uiex.type);
        formData.append("ageGender", this.uiex.ageGender);
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );

    }                
    
    callConsigliatiPages() {

        // console.log("____callConsigliatiPages____");
        var idDiv = "DivReturnChronology";
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato = this.uiex.nessun_risultato;
        var label_recommended = this.uiex.label_recommended_short + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontRecommendedSite + '"> ' + this.uiex.label_recommended_words;
        var uiex = this.uiex;
        var dartsImg = this.uiex.remoteUrlFrontRecommendedDarts;  
        oReq.onload = function(e) {          
            // console.log( oReq );  
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log( jsonData );
            var element = "";
            var ifindMax = 0;
            for (var i = 0; i < jsonData.length; i++) {

                if ((jsonData[i]["NFind"]* 1)!=0) {  
                    // console.log("NFind: " + jsonData[i]["NFind"]);
                    ifindMax = jsonData[i]["NFind"] * 1;
                }

                var darts = '<div>';
                for (var ifind = 0; ifind < ifindMax; ifind++) {
                    darts += '<img style="max-width:12px;display:inline-block;" src="' + dartsImg + '">';
                }
                darts += '</div>';    

                var price = "";
                try {
                    if ( !uiex.isPriceZero(jsonData[i]["Price"]["Price"]) ){
                        price = "<b>" + jsonData[i]["Price"]["Price"] + " " + jsonData[i]["Price"]["Currency"] + "</b><br>";
                    }       
                } catch (e) { }          
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["Meta"]["ImgUrl"]!=null) ) { 

                    if (uiex.isValidURL( jsonData[i]["Meta"]["ImgUrl"] )){

                        remoteUrlFrontNotImageInterno = jsonData[i]["Meta"]["ImgUrl"]; 
                    
                        var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 1);
                        element += `<div style="min-width: 240px;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_recommended}</div><a href="${urlTrack}">
                                    <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                    <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;letter-spacing:0px;line-height:16px;" class="titolo">${darts}${price}${jsonData[i]["Meta"]["Title"]}</div>
                                    </a></div>`;

                        label_recommended = " ";

                    }

                }

            }
            try {
                // console.log(idDiv);
                document.getElementById(radice + idDiv).innerHTML = element;                
                if ((jsonData.length == 0) || (element == "")) {

                    var nessuno = `<div style="min-width: 240px;" class="numero"><div style="height:38px;font-family:arial;font-size:15px;">${label_recommended}</div>`;
                    document.getElementById(radice + idDiv).innerHTML = nessuno + nessun_risultato;
                    // elimo banner se non trovo risultati 
                    /*
                        document.getElementById( radice + 'DivContenuto').remove();
                        document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                        alert("");
                    */
                } 
            } catch (e) { }

        }
        oReq.open("POST", this.uiex.remoteUrlConsigliati + "consigliati/pages/words/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.cookieIdBefore) );
        formData.append("titleConsigliato", this.uiex.titleConsigliato);
        formData.append("words", this.uiex.words);
        formData.append("schemaType", this.uiex.type);
        formData.append("ageGender", this.uiex.ageGender);
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );
    }       

    callConsigliatiSite() {

        // console.log("____callConsigliatiSite____");
        var idDiv = "DivReturnChronology";
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato = this.uiex.nessun_risultato;
        var label_recommended = this.uiex.label_recommended_short + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontRecommendedSite + '"> ' + this.uiex.label_recommended_all;
        var uiex = this.uiex;
        var dartsImg = this.uiex.remoteUrlFrontRecommendedDarts;  
        oReq.onload = function(e) {          
            // console.log( oReq );  
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log( jsonData );
            var element = ""; 
            var ifindMax = 0;
            for (var i = 0; i < jsonData.length; i++) {
            
                var darts = '<div>';
                if ((jsonData[i]["NFind"]* 1)!=0) {  
                    ifindMax = jsonData[i]["NFind"] * 1;
                }                

                var price = "";
                try {                    
                    if ( !uiex.isPriceZero(jsonData[i]["Price"]["Price"]) ){
                        price = "<div><b>" + jsonData[i]["Price"]["Price"] + " " + jsonData[i]["Price"]["Currency"] + "</b></div>";
                    }   
                } catch (e) { }     

                for (var ifind = 0; ifind < ifindMax; ifind++) {
                    darts += '<img style="max-width:12px;display:inline-block;" src="' + dartsImg + '">';
                }
                darts += '</div>';                
                
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["Meta"]["ImgUrl"]!=null) ) { 

                    if (uiex.isValidURL( jsonData[i]["Meta"]["ImgUrl"] )){

                    remoteUrlFrontNotImageInterno = jsonData[i]["Meta"]["ImgUrl"]; 
                
                    var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 1);
                    element += `<div style="min-width: 240px;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_recommended}</div><a href="${urlTrack}">
                                <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;letter-spacing:0px;line-height:16px;" class="titolo">${darts}${price}${jsonData[i]["Meta"]["Title"]}</div>
                                </a></div>`;

                                label_recommended = " ";
                    }  
                }             

            }
            try {
                // console.log(idDiv);
                document.getElementById(radice + idDiv).innerHTML = element;                
                if (jsonData.length == 0) {

                    var nessuno = `<div style="min-width: 240px;" class="numero"><div style="height:38px;font-family:arial;font-size:15px;">${label_recommended}</div>`;
                    document.getElementById(radice + idDiv).innerHTML = nessuno + nessun_risultato;
                    // elimo banner se non trovo risultati 
                    /*
                        document.getElementById( radice + 'DivContenuto').remove();
                        document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                        alert("");
                    */
                } 
            } catch (e) { }


        }
        oReq.open("POST", this.uiex.remoteUrlConsigliati + "consigliati/sites/words/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.cookieIdBefore) );
        formData.append("titleConsigliato", this.uiex.titleConsigliato);
        formData.append("words", this.uiex.words);
        formData.append("schemaType", this.uiex.type);
        formData.append("ageGender", this.uiex.ageGender);
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );
    }         


    callConsigliatiAddon () {
        // console.log("____callConsigliatiAddon____");
        var oReq = new XMLHttpRequest();
        var idDiv = "DivReturnChronology";
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var nessun_risultato =  this.uiex.servizio_in_sviluppo;

        var label_recommended_all = this.uiex.label_recommended_all;
        if  (this.brand!="") {
            label_recommended_all = this.brand;
        }
        var label_recommended = this.uiex.label_recommended_short + ' - <img style="max-width:12px;display:inline-block;" src="' + this.uiex.remoteUrlFrontRecommendedSite + '"> ' + label_recommended_all;
        var uiex = this.uiex;
        var dartsImg = this.uiex.remoteUrlFrontRecommendedDarts;  
        oReq.onload = function(e) {          
            // console.log( oReq );  
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log( jsonData );
            var element = ""; 
            var ifindMax = 0;
            for (var i = 0; i < jsonData.length; i++) {
            
                var darts = '<div>';
                if ((jsonData[i]["NFind"]* 1)!=0) {  
                    ifindMax = jsonData[i]["NFind"] * 1;
                }                

                var price = "";
                if ( !uiex.isPriceZero(jsonData[i]["Price"]["Price"]) ){
                    price = "<div><b>" + jsonData[i]["Price"]["Price"] + " " + jsonData[i]["Price"]["Currency"] + "</b></div>";
                }       

                for (var ifind = 0; ifind < ifindMax; ifind++) {
                    darts += '<img style="max-width:12px;display:inline-block;" src="' + dartsImg + '">';
                }
                darts += '</div>';                
                
                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["Meta"]["ImgUrl"]!=null) ) { 

                    if (uiex.isValidURL( jsonData[i]["Meta"]["ImgUrl"] )){

                    remoteUrlFrontNotImageInterno = jsonData[i]["Meta"]["ImgUrl"]; 
                
                    var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 1);
                    element += `<div style="min-width: 240px;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_recommended}</div><a href="${urlTrack}">
                                <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImageInterno}"></div>
                                <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;letter-spacing:0px;line-height:16px;" class="titolo">${darts}${price}${jsonData[i]["Meta"]["Title"]}</div>
                                </a></div>`;

                                label_recommended = " ";
                    }  
                }             

            }
            try {
                document.getElementById(radice + idDiv).innerHTML = element;                
                if (jsonData.length == 0) {

                    var nessuno = `<div style="min-width: 240px;" class="numero"><div style="height:38px;font-family:arial;font-size:15px;">${label_recommended}</div>`;
                    document.getElementById(radice + idDiv).innerHTML = nessuno + nessun_risultato;
                    // elimo banner se non trovo risultati 
                    /*
                        document.getElementById( radice + 'DivContenuto').remove();
                        document.getElementById( radice + 'DivReturnIcone').style.display = "none";
                        alert("");
                    */
                } 
            } catch (e) { }


        }
        // console.log(this.uiex.remoteUrlConsigliati + "consigliati/addon/app/");
        oReq.open("POST", this.uiex.remoteUrlConsigliati + "consigliati/addon/app/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.cookieIdBefore) );
        formData.append("titleConsigliato", this.uiex.titleConsigliato);
        formData.append("words", this.uiex.words);
        formData.append("schemaType", this.uiex.type);
        formData.append("ageGender", this.uiex.ageGender);
        formData.append("token", this.uiex.token );
        formData.append("user", this.uiex.user );
        oReq.send( formData );
    }      


}

let userIntelligentExperienceAppExperienceAll = new UserIntelligentExperienceAppExperienceAll( userIntelligentExperienceAppInstance );





