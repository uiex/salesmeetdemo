class UserIntelligentExperienceAppRecommended {

    constructor(sevolution) {
        this.uiex = sevolution;
        this.callConsigliato();
    }

    callConsigliato() {

        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice; 
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        var id_referrer_costum = this.uiex.id_referrer_costum;
        oReq.onload = function(e) {          
            // console.log( oReq );  
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            // console.log("____callConsigliato____");
            // console.log( jsonData );
            for (var i = 0; i < jsonData.length; i++) {

                var remoteUrlFrontNotImageInterno = remoteUrlFrontNotImage;
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["Pagina"][0]["Meta"]["ImgUrl"]!=null) ) { 
                    remoteUrlFrontNotImageInterno = jsonData[i]["Pagina"][0]["Meta"]["ImgUrl"]; 
                } 
                var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 1);
                if (id_referrer_costum=="") { 

                    var element = `<div style="min-width: 320px;" class=""><a href="${urlTrack}">
                    <div style="float:left;padding-left:3px;" class="img"><img style="max-height:135px;max-width: 135px;" src="${remoteUrlFrontNotImageInterno}"></div>
                    <div style="float:left;">
                        <div style="float:left;color:#000;font-size:15px;width:145px;padding-left:3px;font-family:arial;" class="titolo">${jsonData[i]["Pagina"][0]["Price"]["Price"]} ${jsonData[i]["Pagina"][0]["Price"]["Currency"]}</div>
                        <div style="float:left;color:#000;text-align:right;font-size:15px;width:20px;padding-right:3px;font-weight: bold;" class="titolo"><a href="javascript:document.getElementById('${radice}consigliato').remove();">X</a></div>
                        <div style="float:none;color:#000;font-size:15px;width:165px;padding-left:3px;font-family:arial;" class="titolo">${jsonData[i]["Pagina"][0]["Meta"]["Title"]}</div>
                    </div>
                    </a></div>`;
                    let sp1 = document.createElement("div");
                    sp1.setAttribute("id", radice + "consigliato");
                    sp1.style.cssText = "display:block;z-index:999999;position:fixed;bottom:0px;right:0px;border:1px solid #000;background-color: #fff;";
                    sp1.innerHTML = element;
                    document.body.appendChild(sp1);   

                } else {

                    var element = `<div style="min-width: 320px;" class=""><a href="${urlTrack}">
                    <div style="float:left;padding-left:3px;" class="img"><img style="max-height:135px;max-width: 135px;" src="${remoteUrlFrontNotImageInterno}"></div>
                    <div style="float:left;margin-top:14px;">
                        <div style="float:left;color:#000;font-size:15px;width:165px;padding-left:3px;font-family:arial;" class="titolo">${jsonData[i]["Pagina"][0]["Price"]["Price"]} ${jsonData[i]["Pagina"][0]["Price"]["Currency"]}</div>
                        <div style="float:none;color:#000;font-size:15px;width:165px;padding-left:3px;font-weight: bold;font-family:arial;" class="titolo">${jsonData[i]["Pagina"][0]["Meta"]["Title"]}</div>
                    </div>
                    </a></div>`;
                    let sp1 = document.createElement("div")
                    sp1.setAttribute("id", radice + "consigliato");
                    sp1.style.cssText = "border-right:1px solid #000";
                    sp1.innerHTML = element;
                    let list = document.getElementById( radice + 'DivReturnUrl');
                    let parentDiv = list.parentNode;
                    parentDiv.insertBefore(sp1, list);
                }

            }
        }
        oReq.open("POST", this.uiex.remoteUrlConsigliati + "consigliato/");
        var formData = new FormData();
        formData.append("idBefore", this.uiex.getCookie(this.uiex.cookieIdBefore) );
        oReq.send( formData );
    }       

}
var userIntelligentExperienceAppRecommended = new UserIntelligentExperienceAppRecommended( userIntelligentExperienceAppInstance );


