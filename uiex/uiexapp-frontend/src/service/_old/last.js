class UserIntelligentExperienceAppLast {

    constructor(sevolution) {
        this.uiex = sevolution;
        this.callLast("");
    }
    callLast() {
        var oReq = new XMLHttpRequest();
        var radice = this.uiex.radice;
        var label_last = this.uiex.label_last ;
        var remoteUrlFrontNotImage = this.uiex.remoteUrlFrontNotImage; 
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            for (var i = 0; i < jsonData.length; i++) {
                if ((jsonData[i]["img"]!="")&&(jsonData[i]["Meta"]["ImgUrl"]!=null) ) { 
                    remoteUrlFrontNotImage = jsonData[i]["Meta"]["ImgUrl"]; 
                }
                var urlTrack = userIntelligentExperienceAppInstance.addTrack( jsonData[i]["Url"], 0 );
                var temp = `<div style="min-width: 240px; border-right: 1px solid #eae8e8;" class="numero"><div style="height:27px;font-family:arial;font-size:15px;">${label_last}</div><a href="${urlTrack}">
                           <div style="float:left;padding-left:3px;" class="img"><img style="max-height:75px;max-width: 75px;" src="${remoteUrlFrontNotImage}"></div>
                           <div style="color:#000;font-size:13px;float:left;width:145px;padding-left:3px;font-family:arial;" class="titolo">${jsonData[i]["Meta"]["Title"]}<br></div>
                           </a></div>`;
                document.getElementById( radice + 'DivReturnUrl').innerHTML = temp;
            }
            try {
                // verifico se il dominio è già esistente in uiex
                if (jsonData.length == 0) {
                    // se non esiste non faccio comparire il div di ritorno
                    document.getElementById( radice + 'DivReturn').remove();
                    document.getElementById( radice + 'DivContenuto').remove();
                    userIntelligentExperienceAppInstance.addButtonShow();
                } else {
                    // div di default di UIEX
                    if (userIntelligentExperienceAppInstance.id_referrer_costum=="") {  
                        document.getElementById( radice + 'DivReturn').style.display = "block";
                    }
                }
                // userIntelligentExperienceAppInstance.insert();
            } catch (e) { }
        }
        oReq.open("GET", this.uiex.remoteUrlToolLast );
        oReq.send();
    }        
    
}
var userIntelligentExperienceAppLast = new UserIntelligentExperienceAppLast( userIntelligentExperienceAppInstance );


