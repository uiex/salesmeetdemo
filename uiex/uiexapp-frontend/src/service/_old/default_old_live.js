class UserIntelligentExperienceApp {

    constructor() {
        this.radice = "UserIntelligentExperienceApp";
        this.uiexDomain = "uiex.app";
        this.getBrowserId = this.getBrowserId();
        //** Se vengo chiamato dalla estensione del browser
        this.isExtensionBrowser = "";
        if (document.getElementById('userIntelligentExperienceBrowserExtension') !=null) {
            this.isExtensionBrowser = "Y";
        } 
        //** set name cookie 
        this.cookieStartTime = this.radice + "StartTime";  // Timestamp del caricamento della pagina
        this.cookieMilliseconds = this.radice + "milliseconds";  // Millesecondi in cui l'utente si muove nelle pagina
        this.cookieTimeOutside = this.radice + "TimeOutside";  // Quante volte il cliente esce e rientra nel documento
        this.cookieIdBefore = this.radice + "idBefore";  // Id record
        this.cookieFlagContatoreOggetti = this.radice + "flagContatoreOggetti";  // quanto si è mosso il cliente
        this.cookieFlagOverOggetti = this.radice + "flagOverOggetti"; // oggetti importanti visitati dal cliente con eventuale ritorno sullo stesso.
        this.cookieScrollY = this.radice + "scrollY"; // se il cliente ha scrollato la pagina
        this.cookieScrollX = this.radice + "scrollX"; // se il cliente ha scrollato la pagina in alto e basso
        this.cookieClick = this.radice + "click"; // se il cliente ha scrollato la pagina in alto e basso
        //** URL
        this.currentUrl = window.location.href;
        this.currentDomain = this.getDomainName(window.location.href);
        this.referrer = this.getDomainName(document.referrer);
        this.referrer_url = document.referrer;
        this.hostname = window.location.hostname;
        //** Setting dati CSS ed etichette personalizzate dal cliente / chiamante 
        this.settingContext();
        //** Dati pagina e user
        this.language = this.getLanguage();
        this.title = this.getTitle();
        // this.titleConsigliato = this.getTitleConsigliato();
        this.description = this.getDescription();  
        this.img = this.getImg();     
        this.type = this.getType();
        this.price = this.getPrice();
        this.currency = this.getCurrency();
        this.words = this.getMainWord();
        this.ageGender = this.getAgeGender();
        this.getParametriAmazon();
        this.getParametriAggiuntivi();
        this.getParametroItempropType();
        this.getParametroPrezzo();
        this.user = ""; // this.getBrowserId;
        this.idBefore = this.getCookie(this.cookieIdBefore); 
        //** Setting UIEX parameters Tools
        this.remoteUrlTool = "https://tools." + this.uiexDomain + "/chronology/";
        this.remoteUrlToolLast = this.remoteUrlTool + "last/"; 
        this.remoteUrlToolExperience= this.remoteUrlTool + "experience/limit/"; 
        this.remoteUrlToolRecommended = this.remoteUrlTool + "experience/";
        this.remoteUrlToolStatistics = this.remoteUrlTool + "page/all/";
        this.remoteUrlToolStatisticsSingle = this.remoteUrlTool + "page/single/";
        this.remoteUrlConsigliati = "https://advice." + this.uiexDomain + "/";
        //** Setting UIEX parameters Frontend
        this.remoteUrlFront = "https://frontend." + this.uiexDomain +"/";
        this.remoteUrlFrontService = this.remoteUrlFront + "front/index.html?id=";
        this.remoteUrlFrontNotImage = this.remoteUrlFront + "images/picture.png";
        this.remoteUrlFrontUser = this.remoteUrlFront + "images/user.png";
        this.remoteUrlFrontMenu = this.remoteUrlFront + "images/menu.png";
        this.remoteUrlFrontDelete = this.remoteUrlFront + "images/delete.png";
        this.remoteUrlFrontSetting = this.remoteUrlFront + "images/setting.png";
        this.remoteUrlFrontStar = this.remoteUrlFront + "images/star.png";
        this.remoteUrlFrontStarH = this.remoteUrlFront + "images/starh.png";
        this.remoteUrlFrontExperience = this.remoteUrlFront + "images/heart.png";     
        this.remoteUrlFrontRecommended = this.remoteUrlFront + "images/advice.png";   
        this.remoteUrlFrontRecommendedSite = this.remoteUrlFront + "images/page.png";     
        this.remoteUrlFrontRecommendedAll = this.remoteUrlFront + "images/pages.png";  
        this.remoteUrlFrontRecommendedAddSite = this.remoteUrlFront + "images/addsite.png";  
        this.remoteUrlFrontRecommendedNavigation = this.remoteUrlFront + "images/navigation.png";  
        this.remoteUrlFrontRecommendedDarts = this.remoteUrlFront + "images/darts.png";  
        this.remoteUrlFrontStatistics = this.remoteUrlFront + "images/graph.png";   
        this.remoteUrlFrontStatisticsUser = this.remoteUrlFront + "images/user_statistic.png";   
        this.remoteUrlFrontStatisticsGroup = this.remoteUrlFront + "images/group_statistic.png";   
        // this.remoteUrlFrontSpinner = this.remoteUrlFront + "images/spinner.gif";     
        //** Setting script path
        this.scriptRecommendedOne = "service/recommendedone.js";           
        this.scriptRecommendedAll = "service/recommendedall.js";   
        this.scriptLast = "service/last.js";   
        this.scriptExperience = "service/experience.js";   
        this.scriptStatistics = "service/statistics.js";  
        //** Parametri di navigazione UIEX
        let d = new Date();
        this.startTime = d.getTime(); // Data inizio instanza della pagina  
        this.Tags; // Elementi html da analizzare nella pagina
        this.timeOutside = []; // variabile di uscita e rientro sul domcumento (segno i vari timestamp)
        this.flagOver = false; // semaforo per capire se sono rientrato nella pagina
        this.flagContatoreOggetti = 0; // Conta quante volte il visitatore mouve il mouse.
        this.flagOverOggetti = []; // array degli elementi importanti visualizzati
        this.scroll; // oggetto -> UserIntelligentExperienceAppScroll()
        this.aScrollX = []; // array per anilizzare come si vuove l'utente nella pagina
        this.aScrollY = []; // array per anilizzare come si vuove l'utente nella pagina
        this.aClick = []; // array per anilizzare come si vuove l'utente nella pagina     
    }

    initCommon() {       
        this.getScript("service/service.js", ""); 
        this.getScript("service/scroll.js", "");
        this.getScript("service/leftwindow.js", "");
        this.addButton();
        this.isReferrer();
    }    
    init() {       
        // this.log(this.price.trim());
        if (this.price.trim()!="") {
            this.initCommon();
        } else {
            this.isSitePresent();
        }
    }
    isSitePresent() {
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {         
            var arraybuffer = oReq.response; 
            var jsonData = JSON.parse(arraybuffer);
            if (jsonData.length > 0) {  
                userIntelligentExperienceAppInstance.initCommon();                
                return true;
            } else {
                return false;
            }
        }
        oReq.open("GET", this.remoteUrlToolExperience);
        oReq.send();
    }         
    //** Inserisci dati pagina
    insert() {
        this.chronology( "POST" , this.remoteUrlTool )
    }
    update() {
        this.chronology( "PUT" , this.remoteUrlTool )
    }
    async chronology( method, url ) {
        // this.log("***************************");
        // this.log("chronology log - " + method);
        var oReq = new XMLHttpRequest();
        var methodi = method;
        oReq.onload = function(e) {        
            console.log("*********INSERITO******************"); 
            var arraybuffer = oReq.response; 
            if (methodi=="POST") {
                var jsonData = JSON.parse(arraybuffer);
                userIntelligentExperienceAppInstance.setTags( jsonData["Tags"] );
                userIntelligentExperienceAppInstance.log("Insert log: ");
                userIntelligentExperienceAppInstance.addCookie(userIntelligentExperienceAppInstance.cookieIdBefore, jsonData["id"] );
                userIntelligentExperienceAppInstance.setCookie(userIntelligentExperienceAppInstance.cookieIdBefore, jsonData["id"] );
                // Prodotto raccomandato ...
                userIntelligentExperienceAppInstance.getScript(userIntelligentExperienceAppInstance.scriptRecommendedOne);
                // Crawler
                if (userIntelligentExperienceAppInstance.crawler == "attivo") {
                    userIntelligentExperienceAppInstance.getScript("service/crawler.js"); 
                }               
            }            
        }
        let idBefore = this.idBefore;
        if (methodi!="POST") {
            idBefore = this.getCookie(this.cookieIdBefore);
        }
        /* 
        if (methodi=="POST") {
            this.log("language:" + this.language);
            this.log("title:" + this.title);
            this.log("currency:" + this.currency);
            this.log("price:" + this.price);
            this.log("img:" + this.img);
            this.log("schemaType:" + this.type);
            this.log("description:" + this.description);
            this.log("words:" + this.words);
        } 
        this.log("idBefore: " + this.idBefore );
        this.log("referrer_url: " + this.referrer_url );
        this.log("user: " + this.user );
        this.log("isExtensionBrowser: " + this.isExtensionBrowser );
        this.log("cookieTimeOutside: " + this.getCookie(this.cookieTimeOutside) );
        this.log("cookieMilliseconds: " + this.getCookie(this.cookieMilliseconds) );
        this.log("cookieStartTime: " + this.getCookie(this.cookieStartTime) );
        this.log("cookieFlagContatoreOggetti: " + this.getCookie(this.cookieFlagContatoreOggetti) );
        this.log("cookieFlagOverOggetti: " + this.getCookie(this.cookieFlagOverOggetti) );
        this.log("cookieFlagOverOggetti: " + this.getCookie(this.cookieFlagOverOggetti) );
        this.log("cookieScrollY: " + this.getCookie(this.cookieScrollY) );
        this.log("cookieScrollX: " + this.getCookie(this.cookieScrollX) );
        this.log("cookieClick: " + this.getCookie(this.cookieClick) );
        */
        oReq.open(method , url);
        var formData = new FormData();
        if (methodi=="POST") {
            formData.append("language", this.language);
            formData.append("title", this.title);
            // formData.append("titleConsigliato", this.titleConsigliato);
            formData.append("description", this.description);
            formData.append("schemaType", this.type);
            formData.append("img", this.img);
            formData.append("price", this.price);
            formData.append("currency", this.currency);
            formData.append("words", this.words);
            formData.append("ageGender", this.ageGender);            
        }
        formData.append("user", this.user);
        formData.append("referrerurl ", this.referrer_url );
        formData.append("idBefore", idBefore);
        formData.append("isExtensionBrowser", this.isExtensionBrowser);
        formData.append("tempoInOut", this.getCookie(this.cookieTimeOutside) );
        formData.append("tempoMovimento", this.getCookie(this.cookieMilliseconds)); // 
        formData.append("startTime", this.getCookie(this.cookieStartTime));
        formData.append("contatoreOggetti", this.getCookie(this.cookieFlagContatoreOggetti));
        formData.append("overOggetti", this.getCookie(this.cookieFlagOverOggetti));
        formData.append("scrollY", this.getCookie(this.cookieScrollY));
        formData.append("scrollX", this.getCookie(this.cookieScrollX));
        formData.append("contatoreClick", this.getCookie(this.cookieClick));
        oReq.send( formData );
    }
    //** DIV BOTTONE DI NAVIGAZIONE
    addButton() {
        if (this.id_menu_costum=="") { 
            // apertura del popup è standard cliccando sul bottone UIEX.
            var elemDiv = document.createElement('div');
            elemDiv.setAttribute("id", this.radice + "se");
            elemDiv.style.cssText = 'display:none;position:fixed;bottom:30px;left:0px;width:auto;height:auto;\
                                     padding:3px;z-index:99999;background:' + this.button_colore + ';\
                                     border:1px solid ' + this.button_colore_border + ';border-left:0px solid;';
            elemDiv.innerHTML = "<a style='color:" + this.button_label_color + ";' id='" + this.radice + "seopen' href='javascript:void(0);'>" + this.button_testo + "</a>";
            document.body.appendChild(elemDiv);            
        } else {
            // apertura del popup personalizzato per il sito del cliente.
            // console.log(this.id_menu_costum);
            var el = document.getElementById( this.id_menu_costum );
            el.setAttribute("id", this.radice + "seopen");
        }
    } 
    addButtonShow() {
        // menu custom e dove posizionare il div dei contenuti
        if ( (this.id_menu_costum=="") && (this.id_referrer_costum=="") ) {
            document.getElementById(this.radice + "se").style.display = "block";   
        }
    } 
    //** DIV CON TUTTI I CONTENUTI in popup tutta pagina
    createDivServiceDelete() { 
        this.createDivService( "delete" ); 
    }     
    createDivServiceLogin() { 
        this.createDivService( "login" );
    }     
    createDivServiceSetting() { 
        this.createDivService( "setting" );
    }     
    createDivServiceExperience() { 
        this.createDivService( "experience" );
    }     
    createDivService( id ) { 
        this.update();
        try {
            // service.js
            let userIntelligentExperienceAppServiceInstance = new UserIntelligentExperienceAppService(this.radice, this.button_testo, this.button_label_color, this.button_colore,this.button_colore_border, this.label_close, this.remoteUrlFront); 
            userIntelligentExperienceAppServiceInstance.createDivService( id );
        } catch (e) {}
    }    
    //** RITORNO AL SITO // Chiamato da IsReferer
    createDivReturn() { 
        var elemDiv = document.createElement('div');
        var bordo_close = "border-top: 1px solid #000000;z-index:999999;";
        var bordo_DivContenuto = "border-bottom:1px solid #000;z-index:999999;";
        var bordo_DivReturnIcone = "border-right:1px #000 solid;padding-top:5px;z-index:999999;";
        
        var icone = "";
        var flag_body = false;

        // se il chimante ha un div dedicato sul proprio sito
        if (this.id_referrer_costum!="") {            
            // Azzero elementi per il layout sul sito del cliente. 
            bordo_close = "";
            icone = "display:none;";
            bordo_DivReturnIcone = "border-right:1px #000 solid;padding-top:15px;";
            // Controllo se l'elemento del chimanate è presente. Se no, assegno il body.
            elemDiv = document.getElementById( userIntelligentExperienceAppInstance.id_referrer_costum );
            if (elemDiv==null) { 
                elemDiv = document.body;
                flag_body = true;
                icone = "display:block;";
            } else {
                // elimino bordi per sezione custom
                bordo_DivContenuto = "";
                bordo_DivReturnIcone = "";
            }
        }
        var buttonExperience = `<div style="height:24px;" class="${this.radice}menubutton"><a id='${this.radice}DivMenuExperience' href='javascript:void(0);'><img style='max-width: 20px;' src='${this.remoteUrlFrontExperience}'></a></div>`;
        var buttonRecommended = `<div style="height:24px;" class="${this.radice}menubutton"><a id='${this.radice}DivMenuRecommended' href='javascript:void(0);'><img style='max-width: 20px;' src='${this.remoteUrlFrontRecommended}'></a></div>`;
        var buttonStatistics = `<div style="height:24px;" class="${this.radice}menubutton"><a id='${this.radice}DivMenuStatistics' href='javascript:void(0);'><img style='max-width: 20px;' src='${this.remoteUrlFrontStatistics}'></a></div>`;
        var buttonMenu = `<div style="height:24px;" class="${this.radice}menubutton"><a id='${this.radice}DivMenu' href='javascript:void(0);'><img style='max-width: 20px;' src='${this.remoteUrlFrontMenu}'></a></div>`;

        // console.log(elemDiv)
        // href di chiusura - close
        var elemDivClose = document.createElement('div');
        elemDivClose.style.cssText = 'width:100%;text-align:right;padding-right:20px;background-color:' + this.colore_return_close + ';';
        elemDivClose.setAttribute("id", this.radice + "DivReturnClose");
        elemDivClose.innerHTML = `<a style="" id='DivReturnCloseButton' href='javascript:void(0);'>${this.close_testo}</a>`;
        // Corpo del div
        var elemFrame = document.createElement('div');
        elemFrame.setAttribute("id", this.radice + "DivContenuto");
        elemFrame.innerHTML = `<div id='${this.radice}DivReturnIcone' style='height: 129px;${bordo_DivReturnIcone}float:left;padding-left:10px;padding-right:10px;'>
                                ${buttonExperience}
                                ${buttonRecommended}
                                ${buttonStatistics}
                                ${buttonMenu}
                              </div>
                              <div style='height: 129px;overflow-y:scroll;display:flex;'>
                                <div style='display:flex;float:left;padding-left:10px;' id='${this.radice}DivReturnUrl'></div>
                                <div style='display:flex;float:left;padding-left:10px;width:100%;' id='${this.radice}DivReturnChronology'></div>
                              </div>`;
        elemFrame.style.cssText = `${bordo_DivContenuto}position:relative;width:100%;height:100%;${bordo_close}background-color:${this.colore_return};`;
        // se il chimante ha un div dedicato sul proprio sito
        if (this.id_referrer_costum!="") {   
            elemDivClose.innerHTML = "";
        }

        var stile = 'display:none;position:fixed;bottom:0px;left:0px;width:100%;height:120px;z-index:9999999999;font-size: 13px;'; 
        
        if (flag_body) {
            // inserisco elemento appena dopo il body
            elemFrame.style.cssText = elemFrame.style.cssText + "height: 130px;";
            elemDiv.appendChild(elemDivClose);
            elemDiv.insertBefore(elemFrame, elemDiv.childNodes[0]); 
        } else {
            // inserisco elemento sul l'elemento indicato
            elemDiv.appendChild(elemDivClose);
            elemDiv.appendChild(elemFrame);  
        }
  
        // div standard UIEX
        if (this.id_referrer_costum=="") { 
            elemDiv.setAttribute("id", this.radice + "DivReturn");
            elemDiv.style.cssText = stile;    
            document.body.appendChild(elemDiv);       
        }

        //** azioni assegnate agli elementi in automatico.
        var el = document.getElementById( this.radice + 'DivReturnClose');
        el.onclick = function (e) {
          var ev = e || window.event;
          userIntelligentExperienceAppInstance.addButtonShow();
          userIntelligentExperienceAppInstance.removeDivReturn();
        }       
        // click div isReferrer sui più visitati
        var el = document.getElementById( this.radice + 'DivMenu');
        el.onclick = function (e) {
            var ev = e || window.event;
            userIntelligentExperienceAppInstance.addButtonShow();
            userIntelligentExperienceAppInstance.removeDivReturn();
            userIntelligentExperienceAppInstance.createDivServiceExperience();
        }

        var el = document.getElementById( this.radice + 'DivMenuExperience');
        el.onclick = function (e) {
            userIntelligentExperienceAppInstance.removeDivConsigliato();
            if (typeof userIntelligentExperienceAppExperience != "undefined") {
                userIntelligentExperienceAppExperience.init();
            } else {
                userIntelligentExperienceAppInstance.getScript(userIntelligentExperienceAppInstance.scriptExperience);
            }        
        }          
        var el = document.getElementById( this.radice + 'DivMenuRecommended');
        el.onclick = function (e) {
            userIntelligentExperienceAppInstance.removeDivConsigliato();
            if (typeof userIntelligentExperienceAppExperienceAll != "undefined") {
                userIntelligentExperienceAppExperienceAll.init();
            } else {
                userIntelligentExperienceAppInstance.getScript(userIntelligentExperienceAppInstance.scriptRecommendedAll);
            }              
        }      
        var el = document.getElementById( this.radice + 'DivMenuStatistics');
        el.onclick = function (e) {
            userIntelligentExperienceAppInstance.removeDivConsigliato();
            if (typeof userIntelligentExperienceAppStatistics != "undefined") {
                userIntelligentExperienceAppStatistics.init();
            } else {
                userIntelligentExperienceAppInstance.getScript(userIntelligentExperienceAppInstance.scriptStatistics);
            }              
        }   

        //** chiamo chiamate verso UIEX per riempire gli elementi di dati                   
        // Visualizza l'ultimo visitato solo se ritorno sul sito e non navigo nello stesso.
        if (this.hostname!=this.referrer) {
            // this.callLast();
            this.getScript(this.scriptLast);
        }
        this.firstScriptToLaunch();
    }        

    firstScriptToLaunch() { 
        if (this.to_launch=="experience") {
            console.log("Script lanciato experience");
            this.getScript(this.scriptExperience);
        } else if (this.to_launch=="words") {
            console.log("Script lanciato words");
            this.getScript(this.scriptRecommendedAll);
        } else if (this.to_launch=="behavior") {
            console.log("Script lanciato behavior");
            this.getScript(this.scriptRecommendedAll);
        } else if (this.to_launch=="sites") {
            console.log("Script lanciato sites");
            this.getScript(this.scriptRecommendedAll);            
        } else {
            console.log("Script lanciato default experience");
            this.getScript(this.scriptExperience);
        } 
    }

    // Se arrivo da un altro dominio faccio uscire il pop-up in basso ...
    isReferrer() { 
        // verifico che non siano un sito UIEX
        if (this.isSiteNotIndex(this.hostname)) {  
            return;
        }
        // ITER NORMALE 
        var referrer = this.referrer;
        let params = new URL(this.currentUrl).searchParams;
        if (params.get('uiextrack')!=null) {
            // se è presente "uiextrack" non apre il div di ritorno...
            referrer = this.hostname;
        }
        // se il chimante ha un div dedicato sul proprio sito fa vedere sempre il div ...
        if (this.id_referrer_costum!="") {  
            referrer = "id_referrer_costum";
        }
        
        if (this.hostname!=referrer) {
            userIntelligentExperienceAppInstance.insert();
            this.createDivReturn();
        } else {
            this.addButtonShow();
            this.insert();
        }
    }  
    // COMMON function
    stelle( value ) {
        var stelle = "";
        var voto_AItemp = parseInt( value ).toString().length / 2; 
        for (var i = 0; i < Math.floor(voto_AItemp); i++) {
            stelle += `<img style="display: inline-block;" src="${this.remoteUrlFrontStar}">`;
        } 
        if (voto_AItemp.toString().indexOf(".") != -1) {
            stelle += `<img style="display: inline-block;" src="${this.remoteUrlFrontStarH}">`;
        }
        return stelle;
    }     
    log(messaggio) {
        console.log(messaggio);
    }     
    isSiteNotIndex(url) {
        // TODO: da capire come utilizzare / traccare questa informzione.
        if (url.indexOf(this.uiexDomain)>0) {
            return true;            
        } else {
            // filtro per non apparire in siti da non indicizzare
            var res = url.split(".");
            if (res.length>2) {
                url = res[1] + "." + res[2];
            }
            if (this.sites_not_index.includes(url)) {
                return true;
            } 
        }
        return false;
    }
    // nome assoluto del dominio.
    getDomainName(url) {
        if (url=="") { return ""; }
        var domain =(new URL(url)).hostname;
        return domain;
    }
    // carico dinamicamente gli script esternamente 
    getScript(url, flag) {
        var imported = document.createElement('script');
        imported.src = this.remoteUrlFront + url;
        imported.rel = 'import';
        /*
        imported.onload = function(e) { console.log("carica"); };
        imported.onerror = function(e) { 'Error loading import: ' + e.target.href};  
        */      
        document.head.appendChild(imported);
    }
    // https://andywalpole.me/blog/140739/using-javascript-create-guid-from-users-browser-information
    getBrowserId() {
        return window.navigator.userAgent.replace(/\D+/g, '');
    }   
    isPriceZero(price) {
        // this.log("******isPriceZero***********");
        // this.log(price);
        if (price==undefined) { return false; }
        price = price.replace(".", '');
        price = price.replace(",", '');
        // this.log(price);
        if ( (price * 1) > 0 ) {
            return false;
        }
        return true;
    }       
    //** META - prende meta e parametri dalla pagina con gli standard OG e schema.org
    getLanguage() {
        var temp = "";
        try {
            temp = document.getElementsByTagName("META")["language"].content;
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        try {
            temp = this.checkParametro( document.getElementsByTagName('html')[0].getAttribute('lang') );
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        if ( (temp==undefined) || (temp=="") ) {
            return this.getParametroCommon( ["lang"] , ["lang"], ["lang"], ["lang"] , [], false);
        } 
        return temp;
    }    
    getTitle() {
        var temp = this.checkParametro( document.title );
        if ( (temp==undefined) || (temp=="") ) {
            return this.getParametroCommon( ["title"] , ["title"], ["name"], ["title"] , [], false);
        } 
        return temp;
    }
    getDescription() {
        try {
            var temp = document.getElementsByTagName("META")["description"].content;
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        return this.getParametroCommon( ["description"] , ["description"], ["description"], ["description"] , [], false);
    }
    getImg() {
        var url = this.getParametroCommon( ["image"] , ["image"], ["image","image.thumbnail","image[0]"], ["image"] , [], true );
        return this.getValidUrl(url);
    }
    getType() {
        return this.getParametroCommon( ["type"] , ["type"], ["@type","type"], ["type","['product']['ecomm_pagetype']"] , [], false);       
    }
    getPrice() {
        return this.getParametroCommon( ["price:amount","product:price:amount","price"] , ["price","lowPrice"], ["offers.price","offers[0].price"], ["price","['product']['ecomm_totalvalue']"] , ["ecomm_totalvalue"], false); 
    }
    getCurrency() {
        return this.getParametroCommon( ["price:currency","product:price:currency"] , ["priceCurrency"], ["offers.priceCurrency","offers[0].priceCurrency"], ["currency_code"] , [], false );
    }
    getParametroCommon( og , itemprop, ldjson, googleTag, googleDRP, isFile ) {
        try {
            for (var i = 0; i < googleTag.length; i++) {     
                var temp = this.checkParametro( this.getParametroGTManager( googleTag[i] , isFile)  );        
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < itemprop.length; i++) { 
                var temp = this.checkParametro( this.getParametroItemprop( itemprop[i] , isFile) );        
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < ldjson.length; i++) { 
                var temp = this.checkParametro( this.getParametroLDjson( ldjson[i] , isFile) );        
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < og.length; i++) { 
                var temp = this.checkParametro( this.getParametroOg( og[i] , isFile)  );        
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < og.length; i++) { 
                var temp = this.checkParametro( this.getParametroTwitter( og[i] , isFile)  );        
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < googleDRP.length; i++) { 
                var temp = this.checkParametro( this.getParametroDynamicRemarketingParameters( googleDRP[i] , isFile)  );        
                if (temp!="") { return temp; }
            }
        } catch (e) { } 
        return "";
    }
    checkParametro(temp) {
        try {
            temp = temp + "";
            if (temp.trim()!="") { 
                return temp; 
            }
        } catch (e) { } 
        return "";  
    }
    // itemProp="price" - og Open Graph - facebook
    getParametroOg( dato, isFile) {
        try {
            var temp = document.querySelectorAll('meta[property="og:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="og:' + dato + '"]')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[name="og:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="product:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="' + dato + '"]')[0].getAttribute("value") ;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }   
    // og - twitter
    getParametroTwitter( dato, isFile) {
        try {
            var temp = document.querySelectorAll('meta[name="twitter:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[name="twitter:' + dato + '"]')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }   
    // schema.org tag
    getParametroItemprop( dato , isFile ) {
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].textContent;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }   
    // google tag manager (GTM)
    getParametroGTManager( dato, isFile ) {
        try {
            for (var i = 0; i < dataLayer.length; i++) {
                try {
                    var ret = dataLayer[i][dato];
                    if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
                } catch (e) { }    
                try {
                    // array di array ...
                    var ret = eval("dataLayer[" + i + "]" + dato) + "";
                    if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
                } catch (e) {  }   
            }            
        } catch (e) {}
        return "";
    }   
    // google tag manager (Dynamic Remarketing Parameters)
    getParametroDynamicRemarketingParameters( dato, isFile ) {
        try {
            var ret = eval("google_tag_params." + dato);
            if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
        } catch (e) { }                
        return "";
    }   
    // schema.org JSON
    getParametroLDjson( dato, isFile ) {
        try {
            if (document.querySelectorAll('script[type="application/ld+json"]')!=null) {
                var ldJson = document.querySelectorAll('script[type="application/ld+json"]');            
                for (var i = 0; i < ldJson.length; i++) {
                    var jsonld = JSON.parse(ldJson[i].innerText);
                    try {
                        var ret = eval("jsonld." + dato);
                        if (ret!=undefined) { if (ret.isArray) { 
                            if (this.doesFileExist(ret[0], isFile)) { return ret[0];  } 
                        } else { 
                            if (this.doesFileExist(ret, isFile)) { return ret;  } } 
                        }
                    } catch (e) {}                
                    try {
                        var ret = eval( "jsonld[0]." + dato);
                        if (ret!=undefined) {  if (ret.isArray) { if (this.doesFileExist(ret[0], isFile)) { return ret[0];  } } else { if (this.doesFileExist(ret, isFile)) { return ret;  } } }
                    } catch (e) { }   
                    // es. @type
                    try {
                        var ret = eval( "jsonld['" + dato + "']" );
                        if (ret!=undefined) { if (ret.isArray) { 
                            if (this.doesFileExist(ret[0], isFile)) { return ret[0];  } 
                        } else { 
                            if (this.doesFileExist(ret, isFile)) { return ret;  } } 
                        }
                    } catch (e) { }   
                }
            }
        } catch (e) { }   
        return "";
    }    
    getParametriAggiuntivi() {
        this.getParametriAggiuntiviCommon(  ["price","amount","prezzo","features__price"], [] );
        this.getParametriAggiuntiviCommon(  [], ["imagewrapper","image"] );
    }    
    getParametriAmazon() {
        this.getParametriAggiuntiviCommon(  ["priceblock_ourprice","priceblock_dealprice","a-color-price"], [] );
        this.getParametriAggiuntiviCommon(  [], ["landingImage","imgBlkFront"] );
        this.type = "product"; 
    }    
    getParametriAggiuntiviCommon( tagsPrezzo, tagsImg ) {
        if (this.price=="")  {
            for (var i = 0; i < tagsPrezzo.length; i++) { 
                try {
                    var temp = document.getElementById(tagsPrezzo[i]).innerHTML;
                    if ( (typeof temp === 'string') && (temp!="") ) { this.price = temp;  return "";  }  
                } catch (e) {  
                    try {
                        var temp = document.getElementsByClassName(tagsPrezzo[i])[0].innerHTML;
                        if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return ""; }  
                    } catch (e) {   
                        try {
                            var temp = document.getElementsByClassName(tagsPrezzo[i]).innerHTML;
                            if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return "";  }  
                        } catch (e) {       
                            try {
                                var temp = document.querySelectorAll(tagsPrezzo[i])[0].content;
                                if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return "";  }  
                            } catch (e) {                      
                            }                                             
                        }                                                
                    }    
                }
            }
        }
        if (this.img=="") {    
            for (var i = 0; i < tagsImg.length; i++) { 
                try {
                    var temp = document.getElementById(tagsImg[i]).src;
                    if ((temp!=undefined) && (temp!="")) { this.img = temp; return ""; }
                } catch (e) { 
                    try {
                        var temp = document.getElementById(tagsImg[i]).dataset.oldHires;
                        if ((temp!=undefined) && (temp!="")) { this.img = temp; return ""; }
                    } catch (e) {     
                        try {
                            var temp = document.getElementsByClassName(tagsImg[i])[0].src;
                            if ( (typeof temp === 'string') && (temp!="") ) {  this.img = temp;  return "";  }  
                        } catch (e) {     
                        }                               
                    }    
                }
            }
        }        
    }    
    // schema.org tag - itemtype="http://schema.org/Product">
    getParametroItempropType(index = 0) {
        if (this.type!="" ) { return "";}  
        try {            
            var temp = document.querySelectorAll('[itemtype]')[index].getAttribute("itemtype");
            if ((temp!=undefined) && (temp!="")) { 
                if(temp.indexOf('schema.org') == -1) {
                    // se non è uno schema.org
                    this.getParametroItempropType(index +1);  
                } else {
                    this.type = temp;  
                }
            }
        } catch (e) { }
    }   
    // prezzo da altri elementi ... TODO: da spostare in GOLANG
    getParametroPrezzo() {
        if (this.price!="" ) { return "";}  
        try {            
            // Verifico che nel titolo ci sia il prezzo ...
            /* TODO: da perfezionare se:
             valuta differente
             valuta attaccata al valore
             altre casistiche ...
            */
            // Titolo di default
            var res = this.title.split(" ");
            var parola_before = "";
            for (var i = 0; i < res.length; i++) { 
                if ((res[i]=="€") || (res[i]=="EUR") ) {
                    var temp = parseFloat(parola_before); 
                    if (!isNaN(temp)) { this.price = temp; return "";}
                    temp = parseFloat(res[i+1]); 
                    if (!isNaN(temp)) { this.price = temp;  return "";}
                }
                parola_before = res[i];
            }         
            // se non trovo passo ad altro ...
            var title = this.getParametroCommon( ["title"] , ["title"], ["name"], ["title"] , [], false);
            var res = title.split(" ");
            var parola_before = "";
            for (var i = 0; i < res.length; i++) { 
                if ((res[i]=="€") || (res[i]=="EUR") ) {
                    var temp = parseFloat(parola_before); 
                    if (!isNaN(temp)) { this.price = temp; return "";}
                    temp = parseFloat(res[i+1]); 
                    if (!isNaN(temp)) { this.price = temp;  return "";}
                }
                parola_before = res[i];
            }               

        } catch (e) { console.log(e); }
    }   
    // verifica sei il link / file esiste in remoto.
    doesFileExist(urlToFile, isFile) {
        if (typeof urlToFile === 'string') { 
            if (isFile==true) {
                var xhr = new XMLHttpRequest();
                xhr.open('HEAD', urlToFile);
                xhr.send();
                if (xhr.status == "404") { return false; } else { return true; }    
            } else {
                return true;
            }
        } else  {
            if (typeof urlToFile === 'number') { 
                return true; 
            }
            throw "object not string";
        }
    }    
    getValidUrl(url) {
        if (url==""){ return ""; }
        try {
          new URL(url);
          return url;
        } catch (_) {
            if (url.substring(0,2)=="//") {
                return "http:" + url;
            }
            var host = window.location.href;
            return host.replace(document.URL.match(/[^\/]+$/) , "") + "/" + url;
        }
    }

    /*** ****/
    removeDivReturn() {
        try {
            document.getElementById( this.radice + "DivReturn").remove();
        } catch (e) {
        }
    }      
    removeDivConsigliato() {
        try {
            document.getElementById( this.radice + "consigliato").remove();
        } catch (e) {
        }
    }          
    // memorizzazione variabili ...
    addCookie(cname, cvalue) {
        localStorage.setItem(cname, cvalue);
    }
    setCookie(name,value) {
        // console.log("setCookie: " + name);
        var expires = "";
        var date = new Date();
        date.setTime(date.getTime() + (30*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    getCookie(cname) {
        var temp = localStorage.getItem(cname);
        if (temp==null) {
            return "";
        } else {
            return temp;
        }
    }
    addAllCookie() {
        // Calcolo tempo pagina 
        var dEnd = new Date();
        //  console.log(dEnd.getTime() + " - " + this.startTime);
        var difference_ms = dEnd.getTime() - this.startTime;
        // var milliseconds = difference_ms/1000;
        // Creo strutttura dei cookie 
        // da valutqre gli elementi troppo lugnhi ...
        // this.addCookie( this.cookieMilliseconds ,milliseconds);
        this.addCookie( this.cookieStartTime ,this.startTime);
        this.addCookie( this.cookieMilliseconds ,difference_ms);
        this.addCookie( this.cookieTimeOutside , this.timeOutside.toString());
        this.addCookie( this.cookieFlagContatoreOggetti , this.flagContatoreOggetti );
        this.addCookie( this.cookieFlagOverOggetti , this.flagOverOggetti.toString());
        this.addCookie( this.cookieScrollY , this.aScrollY.toString());
        this.addCookie( this.cookieScrollX , this.aScrollX.toString());
        this.addCookie( this.cookieClick , this.aClick.toString());
    }
    //** Valori di default e Personalizzazione del sistema
    settingContext() {
        // servizi
        this.exit_popup = "n"; // Apre popup lead generation al onfocus della pagina.
        this.id_menu_costum = ""; // Id del menu dove applicare il click dell'apertura del pop-up. Non visualizza il bottone standard.
        this.id_referrer_costum = ""; // id del div di return if
        this.to_launch = "experience"; // primo script da lanciare per visualizzare i dati
        // Testi
        // common
        this.label_close = "Close";
        // Bottone
        this.button_testo = "chronology";
        this.button_label_color = "#fff";
        this.button_colore = "#000";
        // ReturnDiv
        this.button_colore_border = "#000";
        this.colore_return_close = "rgba(255, 255, 255, 0.66)";
        this.colore_return = "#fff";
        // Etichette 
        this.label_last = "L\'ultimo tuo visitato";
        this.label_experience = "I tuoi preferiti";
        this.label_recommended = "Consigliati per te";
        this.label_recommended_short = "Consigliati";
        this.label_recommended_words = "Per parole chiave";
        this.label_recommended_all = "Altri siti comunity";
        this.label_recommended_addsite = "Altri siti sponsorizzati";
        this.label_recommended_navigation = "Per comportamento";
        this.label_statistic = "Statistiche pagina";
        this.label_statistic_all = "Tutti i visitatori";
        this.label_statistic_single = "Le mie";
        this.label_statistic_count = "Visualizzazioni";
        this.label_statistic_last_date = "Ultima visita";
        this.label_statistic_vote = "Voto";
        this.label_statistic_medium_vote = "medio";
        this.label_statistic_max_vote = "max";
        this.label_statistic_min_vote = "min";
        this.nessun_risultato = "Nessuno risultato trovato";
        // LeftWindowDiv
        this.left_windows_border = "#000";
        this.left_windows_color = "#fff;";
        this.left_windows_testo = "Richiedi informazioni per";
        // No site
        this.sites_not_index = ["google.com","bind.com","facebook.com","whatsapp.com","telegram.com","twitter.com","mongodb.com","uiex.it"]; 
        // sesso
        this.sesso_page = ["uomo","donna","bambino","bambina","ragazza","ragazzo"];        
        // crawler
        this.crawler = ""; // "attivo";    
        // extension browser per brand
        this.brand = ""; // se valorizzata prende                   
        // Assegno variabili esterne
        if (window.uiex_parameters!=null) {
            for (let [key, value] of Object.entries(window.uiex_parameters)) {
                eval( "this." + key + " = '" + value + "';" );
            }    
        }
    }  
    // setto tag di analisi pagina passati dalle API in chiamata.
    setTags(Tags) {
        this.Tags = Tags;
    }       
    // track ******* è utilizzato il servizio uiex
    addTrack( url, type ) {
        try {
            if(url.indexOf('?') != -1) {
                url = url + "&uiextrack=" + type;
            } else {
                url = url + "?uiextrack=" + type;
            }
        } catch (e) {
        }
        return url;
    }  
    // auth *******
    /*
    callAuth() {
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            // console.log(oReq);
            var jsonData = JSON.parse(arraybuffer);
            for (var i = 0; i < jsonData.length; i++) {
                // console.log(jsonData[i]["Id"]);
            }
        }
        var formData = new FormData();
        formData.append("user", "facchini.corrado@gmail.com");
        formData.append("pass", "prova");
        oReq.open("POST", this.remoteUrlTool + "auth/new" );
        oReq.send(formData);
    }  
    isAuth() {
        var oReq = new XMLHttpRequest();
        oReq.onload = function(e) {            
            var arraybuffer = oReq.response; 
            // console.log(oReq);
            var jsonData = JSON.parse(arraybuffer);
            for (var i = 0; i < jsonData.length; i++) {
                // console.log(jsonData[i]["Id"]);
            }
        }
        oReq.open("GET", this.remoteUrlTool + "auth/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6IiJ9.eyJleHAiOjE1NDQ0NzczNDQsImlhdCI6MTU0NDQ3NzI4NCwicGFzcyI6InByb3ZhIiwidXNlciI6ImZhY2NoaW5pLmNvcnJhZG9AZ21haWwuY29tIn0.R4siWB_XVcRV67svfFjCuv1uPId6zTD_3v9moQTjmcY"  );
        oReq.send();
    }  
    */      

    /* Raccoglie le parole più importanti della pagina senza caratteri speciali e senza dominio della pagina */
    getMainWord() {
        var res = [];
        var resTitle = this.title.toLowerCase().split(" ");
        var resDescription = this.description.split(" ");     
        // Titolo della pagina
        for (var i = 0; i < resTitle.length; i++) {
            if (resTitle[i].length > 2) {
                if (!this.isSpecialCharacter(resTitle[i])) {
                    if ( !this.isDomain(resTitle[i].toLowerCase()) ) {
                        var number = this.getNumberWord(resTitle[i].toLowerCase());
                        if (this.getByTagNameWord( "h1", resTitle[i].toLowerCase() ) ) {
                            number = number * 1000;
                        }
                        res[resTitle[i].toLowerCase()] = number;    
                    }    
                }
            }
        }
        // descrizione della pagina
        for (var i = 0; i < resDescription.length; i++) {
            if (!this.isSpecialCharacter(resDescription[i])) {
                if ( !this.isDomain(resDescription[i].toLowerCase()) ) {
                    if (resTitle.indexOf(resDescription[i].toLowerCase()) > - 1 ) {
                        res[resDescription[i].toLowerCase()] = res[resDescription[i].toLowerCase()] * 100;
                    } else {
                        if (resDescription[i].length > 2) {
                            var number = this.getNumberWord(resDescription[i].toLowerCase());
                            res[resDescription[i].toLowerCase()] = number;  
                        }
                    }
                }
            }
        }
        // ordina e prendi i risulati con il valore maggioe
        var sortable = [];
        for (var ordinati in res) {
            if (Number.isInteger(res[ordinati])) {
                sortable.push([ordinati, res[ordinati]]);
            }
        }
        sortable.sort(function(a, b) {
            return b[1] - a[1];
        });
        console.log(sortable.slice(0, 10));
        return sortable.slice(0, 10);;
    }           
    isDomain(word) {        
        var cur = this.currentDomain.toLowerCase();
        if (cur.indexOf(word) !== -1) {
            return true;
        }
        return false;
    }     
    isSpecialCharacter(word) {
        try {
            // var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
            var format = /[!@#$%^&*()_+ \[\]{};':"\\|,.<>\/?]+/; // tolto - per T-shirt
            if(format.test(word)){ return true; } 
        } catch (e) {       
        }
        try {
            return word.match(/^[^a-zA-Z0-9]+$/) ? true : false;
        } catch (e) {
        }
        return false;             
    }     
    getAgeGender() {
        var numberMax = 0;
        var value = "";
        for (var i = 0; i < this.sesso_page.length; i++) {
            var number = 0
            if ( this.title.toLowerCase().indexOf(this.sesso_page[i].toLowerCase() ) > -1 ) {
                number = 1000;
            }            
            number += this.getNumberWord(this.sesso_page[i]);
            number += this.getWordByLink(this.sesso_page[i]);
            // this.getByTagNameWord( "h1", this.sesso_page[i] ) ;
            // console.log(this.sesso_page[i] + " - " + number);
            if (number > numberMax) {
                value = this.sesso_page[i];
                numberMax = number;
            }            
        }
        if (numberMax==0) { value = ""; }
        console.log(value);
        return value;
    }        
    // conta il numero delle volte che la parola si ripete nella pagina
    getNumberWord(word) {
        var number = 0;
        try {
            var queue = [document.body];
            var curr;
            while (curr = queue.pop()) {
                if (!curr.textContent.toLowerCase().match(word.toLowerCase())) continue;
                for (var i = 0; i < curr.childNodes.length; ++i) {
                    switch (curr.childNodes[i].nodeType) {
                        case Node.TEXT_NODE : // 3
                            if (curr.childNodes[i].textContent.toLowerCase().match(word.toLowerCase() )) {
                                number++;
                            }
                            break;                                             
                        case Node.ELEMENT_NODE : // 1
                            queue.push(curr.childNodes[i]);
                            break;
                    }
                }
            }       
        } catch (e) {
        }
        return number;
   }      
   // Conta se la parola è contenuta nei tag principali della pagina. Es H1.
   getByTagNameWord( tagName, word ) {
        // console.log( tagName + "-" + word + "####################");    
        try {
            var h1s = document.getElementsByTagName( tagName );
            for (var i = 0; i < h1s.length; i++) {
                // console.log( h1s[i] );    
                if ( h1s[i].toLowerCase().indexOf(word.toLowerCase() ) > -1 ) {
                    // console.log( h1s[i] +  " -- " + word + " - found!");   
                    return true;
                }
            }
        } catch (e) {
            return false;             
        }
        return false; 
    }       
    getWordByLink( word ) {
        var number = 0;
        try {
            var array = [];
            var links = document.getElementsByTagName("a");
            for(var i=0; i<links.length; i++) {
                if ( links[i].href.toLowerCase().indexOf(word.toLowerCase() ) > -1 ) {
                    number++;
                }
            }
        } catch (e) {          
        }
        return number; 
    }    
    isValidURL( temp) {
        var res = temp.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        return (res !== null)
    };

    // Ritorna il titolo della pagina normalizzato, senza caratteri speciali e senza dominio
    /*
    getTitleConsigliato() {
        var sentence = this.title;
        var newSentence = "";
        var words = sentence.toLowerCase().split(' ');
        var max = 5;
        for (var i = 0; i < max; i++) {
        // for (var i = 0; i < words.length; i++) {
            if (!this.isSpecialCharacter(words[i])) {
                if ( !this.isDomain(words[i]) ) {
                    if (newSentence.indexOf(words[i]) !== -1) {
                        max++;
                    } else {
                        newSentence += words[i] + " ";
                    }             
                }    
            }            
        }
        return newSentence;
    }       
    */
    /*
    getByTagName( tagName ) {
        console.log( tagName + "@@@@@@@@@@@@@@@@@@");    
        var number = 0;
        try {
            var h1s = document.getElementsByTagName( tagName );
            for (var i = 0; i < h1s.length; i++) {
                    console.log(  h1s[i] );
            }
        } catch (e) {
        }
        // console.log("@@@@@@@@@@@@@@@@@@");    
        return number; 
    }       
    */   

}

//*** Init *********************************************************/

// Instanzio la classe
let userIntelligentExperienceAppInstance = new UserIntelligentExperienceApp(); 

window.onload = function() {
    userIntelligentExperienceAppInstance.init();
    // userIntelligentExperienceAppInstance.callAuth();
    // userIntelligentExperienceAppInstance.isAuth();
    var el = document.getElementById( userIntelligentExperienceAppInstance.radice + "seopen" );

    el.onclick = function (e) {
      var ev = e || window.event;
      userIntelligentExperienceAppInstance.addAllCookie(); 
      userIntelligentExperienceAppInstance.createDivServiceExperience();      
    }
}
addEvent(window,"load",function(e) {

    addEvent(document, "mouseout", function(e) {
        e = e ? e : window.event;
        var from = e.relatedTarget || e.toElement;
        if (!from || from.nodeName == "HTML") {
            // console.log("out----");
            //*** Tempo In e out ******************************/
            userIntelligentExperienceAppInstance.timeOutside.push( "o-" + new Date().getTime() );
            userIntelligentExperienceAppInstance.flagOver = true;
            userIntelligentExperienceAppInstance.addAllCookie(); 
            userIntelligentExperienceAppInstance.update();
            //*** PopUp lead generation ******************************/
            if (userIntelligentExperienceAppInstance.exit_popup == "y") {
                let userIntelligentExperienceAppInstanceLeft = new UserIntelligentExperienceAppLeftWindow();
                userIntelligentExperienceAppInstanceLeft.createDivLeftWindow();                 
            }
        }
    });
    //*** Scrolling PAGE ******************************/  
    addEvent(document, "scroll", function(e) {        
        try {
            if (userIntelligentExperienceAppInstance.scroll==null) { userIntelligentExperienceAppInstance.scroll = new UserIntelligentExperienceAppScroll( userIntelligentExperienceAppInstance ); } 
            userIntelligentExperienceAppInstance.scroll.get();
        } catch (e) {
            console.log(e);
        }
        userIntelligentExperienceAppInstance.addAllCookie();    
    });

    //*** Click ******************************/  
    addEvent(document, "click", function(e) {    
        e = e ? e : window.event;
        var from = e.relatedTarget || e.toElement;
        try {
            userIntelligentExperienceAppInstance.aClick.push( e.clientX ); 
        } catch (e) { }
        try {
            userIntelligentExperienceAppInstance.aClick.push( e.clientY );            
        } catch (e) {  }
        userIntelligentExperienceAppInstance.addAllCookie();    
    });    
    //*** Srrolling PAGE ******************************/  
    /*
    addEvent(document, "mousedown", function(e) {
        userIntelligentExperienceAppInstance.addAllCookie();    
    });    
    */
    addEvent(document, "mouseover", function(e) {
        e = e ? e : window.event;
        var from = e.relatedTarget || e.toElement;
        //*** Tempo In e out ******************************/        
        if (userIntelligentExperienceAppInstance.flagOver) {
            // console.log("in----");
            // riprendo il focus della finestra se non è presente ... ad esempio ho aperto un altro browser o app in parallelo sulla stessa schermata.
            if (!document.hasFocus()) {
                try {
                    var open = window.open("","");
                    open.close();        
                } catch (e) {  }
            } 
            userIntelligentExperienceAppInstance.flagOver = false;
            userIntelligentExperienceAppInstance.timeOutside.push( "i-" + new Date().getTime() );
            userIntelligentExperienceAppInstance.update();
        }            

        //*** Contatore di movimento del mouse - Count quanto si passa da un elemento all'altro. ******************************/
        userIntelligentExperienceAppInstance.flagContatoreOggetti++;
        //*** Analisi elementi trovati ******************************/
        if (userIntelligentExperienceAppInstance.Tags!=null){
            for (var i = 0; i < userIntelligentExperienceAppInstance.Tags.length; i++){
                // look for the entry with a matching `code` value
                if (userIntelligentExperienceAppInstance.Tags[i].TagName == e.toElement.nodeName.toUpperCase()){     
                    userIntelligentExperienceAppInstance.flagOverOggetti.push( e.toElement.nodeName );
                    userIntelligentExperienceAppInstance.flagOverOggetti.push( eval( "e.toElement." + userIntelligentExperienceAppInstance.Tags[i].Attributi) );
                    try {
                        userIntelligentExperienceAppInstance.flagOverOggetti.push( event.clientX ); // Get the horizontal coordinate
                    } catch (e) {  }
                    try {
                        userIntelligentExperienceAppInstance.flagOverOggetti.push( event.clientY );
                    } catch (e) {  }
                }
            }
        }
        userIntelligentExperienceAppInstance.addAllCookie();
    });
});

//*** INTERCETTO USCITA DALLA FINESTRA ******************************/
function addEvent(obj, evt, fn) {

    if (obj.addEventListener) {
        obj.addEventListener(evt, fn, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}
//*** Lascio la pagina *************************************************/
window.addEventListener('beforeunload', function (e) {    
    userIntelligentExperienceAppInstance.addAllCookie();    
    // Cancel the event as stated by the standard.
    e.preventDefault();
    // Chrome requires returnValue to be set.
    // e.returnValue = '';
});