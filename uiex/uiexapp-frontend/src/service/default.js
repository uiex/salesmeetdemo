class UserIntelligentExperienceApp {

    constructor() {
        this.radice = "UserIntelligentExperienceApp";
        this.uiexDomain = "salesmeet.it"; // uiextools.salesmeet.it
        this.idUser = this.getIdUser();
        this.token = this.getToken();
        //** Se vengo chiamato dalla estensione del browser
        this.isExtensionBrowser = "";
        if (document.getElementById('userIntelligentExperienceBrowserExtension') !=null) {
            this.isExtensionBrowser = "Y";
        }
        //** set name cookie
        this.cookieStartTime = this.radice + "StartTime";  // Timestamp del caricamento della pagina
        this.cookieMilliseconds = this.radice + "milliseconds";  // Millesecondi in cui l'utente si muove nelle pagina
        this.cookieTimeOutside = this.radice + "TimeOutside";  // Quante volte il cliente esce e rientra nel documento
        this.cookieIdBefore = this.radice + "idBefore";  // Id record
        this.cookieFlagContatoreOggetti = this.radice + "flagContatoreOggetti";  // quanto si è mosso il cliente
        this.cookieFlagOverOggetti = this.radice + "flagOverOggetti"; // oggetti importanti visitati dal cliente con eventuale ritorno sullo stesso.
        this.cookieScrollY = this.radice + "scrollY"; // se il cliente ha scrollato la pagina
        this.cookieScrollX = this.radice + "scrollX"; // se il cliente ha scrollato la pagina in alto e basso
        this.cookieClick = this.radice + "click"; // se il cliente ha scrollato la pagina in alto e basso
        this.cookieWords = this.radice + "words"; //
        //** URL
        this.currentUrl = window.location.href;
        this.currentDomain = this.getDomainName(window.location.href);

        this.referrer = this.getDomainName(document.referrer);
        this.referrer_url = document.referrer;
        this.hostname = window.location.hostname;
        //** Setting dati CSS ed etichette personalizzate dal cliente / chiamante
        this.settingContext();
        //** Dati pagina e user
        this.language = this.getLanguage();
        this.title = this.getTitle();
        // this.titleConsigliato = this.getTitleConsigliato();
        this.description = this.getDescription();
        this.img = this.getImg();
        this.type = this.getType();
        this.price = this.getPrice();
        this.currency = this.getCurrency();
        this.words = this.getMainWord();
        this.ageGender = this.getAgeGender();
        this.getParametriAmazon();
        this.getParametriAggiuntivi();
        this.getParametroItempropType();
        this.getParametroPrezzo();
        this.user = this.idUser;
        this.idBefore = this.getCookie(this.cookieIdBefore);
        //** Setting UIEX parameters Tools
        this.remoteUrlTool = "https://uiextools." + this.uiexDomain + "/chronology/";
        //** Parametri di navigazione UIEX
        let d = new Date();
        this.startTime = d.getTime(); // Data inizio instanza della pagina
        this.Tags; // Elementi html da analizzare nella pagina
        this.timeOutside = []; // variabile di uscita e rientro sul domcumento (segno i vari timestamp)
        this.flagOver = false; // semaforo per capire se sono rientrato nella pagina
        this.flagContatoreOggetti = 0; // Conta quante volte il visitatore mouve il mouse.
        this.flagOverOggetti = []; // array degli elementi importanti visualizzati
        this.scroll; // oggetto -> UserIntelligentExperienceAppScroll()
        this.aScrollX = []; // array per anilizzare come si vuove l'utente nella pagina
        this.aScrollY = []; // array per anilizzare come si vuove l'utente nella pagina
        this.aClick = []; // array per anilizzare come si vuove l'utente nella pagina
    }
    init() {
        this.isReferrer();
    }
    //** Inserisci dati pagina
    insert() {
        this.chronology( "POST" , this.remoteUrlTool )
    }
    update() {
        this.chronology( "PUT" , this.remoteUrlTool )
    }
    async chronology( method, url ) {
        //this.log("***************************");
        //this.log("chronology log - " + method);
        var oReq = new XMLHttpRequest();
        var methodi = method;
        oReq.onload = function(e) {
            var arraybuffer = oReq.response;
            if (methodi=="POST") {
                var jsonData = JSON.parse(arraybuffer);
                userIntelligentExperienceAppInstance.log(jsonData);
                userIntelligentExperienceAppInstance.setTags( jsonData["Tags"] );
                userIntelligentExperienceAppInstance.log("Insert log");
                userIntelligentExperienceAppInstance.addCookie(userIntelligentExperienceAppInstance.cookieIdBefore, jsonData["id"] );
                userIntelligentExperienceAppInstance.setCookie(userIntelligentExperienceAppInstance.cookieIdBefore, jsonData["id"] );
            }
        }
        let idBefore = this.idBefore;
        if (methodi!="POST") {
            idBefore = this.getCookie(this.cookieIdBefore);
        }
        /*
        this.log("url:" + url);
        if (methodi=="POST") {
            this.log("inserttttttttttttt.....");
            this.log("language:" + this.language);
            this.log("title:" + this.title);
            this.log("currency:" + this.currency);
            this.log("price:" + this.price);
            this.log("img:" + this.img);
            this.log("schemaType:" + this.type);
            this.log("description:" + this.description);
            this.log("words:" + this.words);
        }
        this.log("token:" + this.token);
        this.log("idBefore: " + this.idBefore );
        this.log("referrer_url: " + this.referrer_url );
        this.log("currenturl: " + encodeURI(this.currentUrl) );
        this.log("user: " + this.user );
        this.log("isExtensionBrowser: " + this.isExtensionBrowser );
        this.log("cookieTimeOutside: " + this.getCookie(this.cookieTimeOutside) );
        this.log("cookieMilliseconds: " + this.getCookie(this.cookieMilliseconds) );
        this.log("cookieStartTime: " + this.getCookie(this.cookieStartTime) );
        this.log("cookieFlagContatoreOggetti: " + this.getCookie(this.cookieFlagContatoreOggetti) );
        this.log("cookieFlagOverOggetti: " + this.getCookie(this.cookieFlagOverOggetti) );
        this.log("cookieFlagOverOggetti: " + this.getCookie(this.cookieFlagOverOggetti) );
        this.log("cookieScrollY: " + this.getCookie(this.cookieScrollY) );
        this.log("cookieScrollX: " + this.getCookie(this.cookieScrollX) );
        this.log("cookieClick: " + this.getCookie(this.cookieClick) );
        */
        oReq.open(method , url);
        var formData = new FormData();
        if (methodi=="POST") {
            formData.append("language", this.language);
            formData.append("title", this.title);
            // formData.append("titleConsigliato", this.titleConsigliato);
            formData.append("description", this.description);
            formData.append("schemaType", this.type);
            formData.append("img", this.img);
            formData.append("price", this.price);
            formData.append("currency", this.currency);
            formData.append("words", this.words);
            formData.append("ageGender", this.ageGender);
        }
        formData.append("currenturl", this.currentUrl );
        formData.append("user", this.user);
        formData.append("token", this.token);
        formData.append("referrerurl", this.referrer_url );
        formData.append("idBefore", idBefore);
        formData.append("isExtensionBrowser", this.isExtensionBrowser);
        formData.append("tempoInOut", this.getCookie(this.cookieTimeOutside) );
        formData.append("tempoMovimento", this.getCookie(this.cookieMilliseconds)); //
        formData.append("startTime", this.getCookie(this.cookieStartTime));
        formData.append("contatoreOggetti", this.getCookie(this.cookieFlagContatoreOggetti));
        formData.append("overOggetti", this.getCookie(this.cookieFlagOverOggetti));
        formData.append("scrollY", this.getCookie(this.cookieScrollY));
        formData.append("scrollX", this.getCookie(this.cookieScrollX));
        formData.append("contatoreClick", this.getCookie(this.cookieClick));
        oReq.send( formData );
    }
    // Se arrivo da un altro dominio faccio uscire il pop-up in basso ...
    isReferrer() {
        // verifico che non siano un sito UIEX
        if (this.isSiteNotIndex(this.hostname)) {
            return;
        }
        // ITER NORMALE
        var referrer = this.referrer;
        let params = new URL(this.currentUrl).searchParams;
        if (params.get('uiextrack')!=null) {
            // se è presente "uiextrack" non apre il div di ritorno...
            referrer = this.hostname;
        }
        // se il chimante ha un div dedicato sul proprio sito fa vedere sempre il div ...
        if (this.id_referrer_costum!="") {
            referrer = "id_referrer_costum";
        }
        if (this.hostname!=referrer) {
            userIntelligentExperienceAppInstance.insert();
            // this.createDivReturn();
        } else {
            // this.addButtonShow();
            this.insert();
        }
    }
    log(messaggio) {
        console.log(messaggio);
    }
    isSiteNotIndex(url) {
        // TODO: da capire come utilizzare / traccare questa informzione.
        if (url.indexOf(this.uiexDomain)>0) {
            return true;
        } else {
            // filtro per non apparire in siti da non indicizzare
            var res = url.split(".");
            if (res.length>2) {
                url = res[1] + "." + res[2];
            }
            if (this.sites_not_index.includes(url)) {
                return true;
            }
        }
        return false;
    }
    // nome assoluto del dominio.
    getDomainName(url) {
        if (url=="") { return ""; }
        var domain =(new URL(url)).hostname;
        return domain;
    }
    //** META - prende meta e parametri dalla pagina con gli standard OG e schema.org
    getLanguage() {
        var temp = "";
        try {
            temp = document.getElementsByTagName("META")["language"].content;
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        try {
            temp = this.checkParametro( document.getElementsByTagName('html')[0].getAttribute('lang') );
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        if ( (temp==undefined) || (temp=="") ) {
            return this.getParametroCommon( ["lang"] , ["lang"], ["lang"], ["lang"] , [], false);
        }
        return temp;
    }
    getTitle() {
        var temp = this.checkParametro( document.title );
        if ( (temp==undefined) || (temp=="") ) {
            return this.getParametroCommon( ["title"] , ["title"], ["name"], ["title"] , [], false);
        }
        return temp;
    }
    getDescription() {
        try {
            var temp = document.getElementsByTagName("META")["description"].content;
            if ((temp!=undefined) && (temp!="")) { return temp;  }
        } catch (e) { }
        return this.getParametroCommon( ["description"] , ["description"], ["description"], ["description"] , [], false);
    }
    getImg() {
        var url = this.getParametroCommon( ["image"] , ["image"], ["image","image.thumbnail","image[0]"], ["image"] , [], true );
        return this.getValidUrl(url);
    }
    getType() {
        return this.getParametroCommon( ["type"] , ["type"], ["@type","type"], ["type","['product']['ecomm_pagetype']"] , [], false);
    }
    getPrice() {
        return this.getParametroCommon( ["price:amount","product:price:amount","price"] , ["price","lowPrice"], ["offers.price","offers[0].price"], ["price","['product']['ecomm_totalvalue']"] , ["ecomm_totalvalue"], false);
    }
    getCurrency() {
        return this.getParametroCommon( ["price:currency","product:price:currency"] , ["priceCurrency"], ["offers.priceCurrency","offers[0].priceCurrency"], ["currency_code"] , [], false );
    }
    getParametroCommon( og , itemprop, ldjson, googleTag, googleDRP, isFile ) {
        try {
            for (var i = 0; i < googleTag.length; i++) {
                var temp = this.checkParametro( this.getParametroGTManager( googleTag[i] , isFile)  );
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < itemprop.length; i++) {
                var temp = this.checkParametro( this.getParametroItemprop( itemprop[i] , isFile) );
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < ldjson.length; i++) {
                var temp = this.checkParametro( this.getParametroLDjson( ldjson[i] , isFile) );
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < og.length; i++) {
                var temp = this.checkParametro( this.getParametroOg( og[i] , isFile)  );
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < og.length; i++) {
                var temp = this.checkParametro( this.getParametroTwitter( og[i] , isFile)  );
                if (temp!="") { return temp; }
            }
            for (var i = 0; i < googleDRP.length; i++) {
                var temp = this.checkParametro( this.getParametroDynamicRemarketingParameters( googleDRP[i] , isFile)  );
                if (temp!="") { return temp; }
            }
        } catch (e) { }
        return "";
    }
    checkParametro(temp) {
        try {
            temp = temp + "";
            if (temp.trim()!="") {
                return temp;
            }
        } catch (e) { }
        return "";
    }
    // itemProp="price" - og Open Graph - facebook
    getParametroOg( dato, isFile) {
        try {
            var temp = document.querySelectorAll('meta[property="og:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="og:' + dato + '"]')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[name="og:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="product:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[property="' + dato + '"]')[0].getAttribute("value") ;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }
    // og - twitter
    getParametroTwitter( dato, isFile) {
        try {
            var temp = document.querySelectorAll('meta[name="twitter:' + dato + '"]')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('meta[name="twitter:' + dato + '"]')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }
    // schema.org tag
    getParametroItemprop( dato , isFile ) {
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].textContent;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].src;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        try {
            var temp = document.querySelectorAll('[itemprop=' + dato + ']')[0].content;
            if ((temp!=undefined) && (temp!="")) { if (this.doesFileExist(temp, isFile)) { return temp;  } }
        } catch (e) { }
        return "";
    }
    // google tag manager (GTM)
    getParametroGTManager( dato, isFile ) {
        try {
            for (var i = 0; i < dataLayer.length; i++) {
                try {
                    var ret = dataLayer[i][dato];
                    if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
                } catch (e) { }
                try {
                    // array di array ...
                    var ret = eval("dataLayer[" + i + "]" + dato) + "";
                    if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
                } catch (e) {  }
            }
        } catch (e) {}
        return "";
    }
    // google tag manager (Dynamic Remarketing Parameters)
    getParametroDynamicRemarketingParameters( dato, isFile ) {
        try {
            var ret = eval("google_tag_params." + dato);
            if (ret!=undefined) { if (this.doesFileExist(ret, isFile)) { return ret;  } }
        } catch (e) { }
        return "";
    }
    // schema.org JSON
    getParametroLDjson( dato, isFile ) {
        try {
            if (document.querySelectorAll('script[type="application/ld+json"]')!=null) {
                var ldJson = document.querySelectorAll('script[type="application/ld+json"]');
                for (var i = 0; i < ldJson.length; i++) {
                    var jsonld = JSON.parse(ldJson[i].innerText);
                    try {
                        var ret = eval("jsonld." + dato);
                        if (ret!=undefined) { if (ret.isArray) {
                            if (this.doesFileExist(ret[0], isFile)) { return ret[0];  }
                        } else {
                            if (this.doesFileExist(ret, isFile)) { return ret;  } }
                        }
                    } catch (e) {}
                    try {
                        var ret = eval( "jsonld[0]." + dato);
                        if (ret!=undefined) {  if (ret.isArray) { if (this.doesFileExist(ret[0], isFile)) { return ret[0];  } } else { if (this.doesFileExist(ret, isFile)) { return ret;  } } }
                    } catch (e) { }
                    // es. @type
                    try {
                        var ret = eval( "jsonld['" + dato + "']" );
                        if (ret!=undefined) { if (ret.isArray) {
                            if (this.doesFileExist(ret[0], isFile)) { return ret[0];  }
                        } else {
                            if (this.doesFileExist(ret, isFile)) { return ret;  } }
                        }
                    } catch (e) { }
                }
            }
        } catch (e) { }
        return "";
    }
    getParametriAggiuntivi() {
        this.getParametriAggiuntiviCommon(  ["price","amount","prezzo","features__price"], [] );
        this.getParametriAggiuntiviCommon(  [], ["imagewrapper","image"] );
    }
    getParametriAmazon() {
        this.getParametriAggiuntiviCommon(  ["priceblock_ourprice","priceblock_dealprice","a-color-price"], [] );
        this.getParametriAggiuntiviCommon(  [], ["landingImage","imgBlkFront"] );
        this.type = "product";
    }
    getParametriAggiuntiviCommon( tagsPrezzo, tagsImg ) {
        if (this.price=="")  {
            for (var i = 0; i < tagsPrezzo.length; i++) {
                try {
                    var temp = document.getElementById(tagsPrezzo[i]).innerHTML;
                    if ( (typeof temp === 'string') && (temp!="") ) { this.price = temp;  return "";  }
                } catch (e) {
                    try {
                        var temp = document.getElementsByClassName(tagsPrezzo[i])[0].innerHTML;
                        if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return ""; }
                    } catch (e) {
                        try {
                            var temp = document.getElementsByClassName(tagsPrezzo[i]).innerHTML;
                            if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return "";  }
                        } catch (e) {
                            try {
                                var temp = document.querySelectorAll(tagsPrezzo[i])[0].content;
                                if ( (typeof temp === 'string') && (temp!="") ) {  this.price = temp;  return "";  }
                            } catch (e) {
                            }
                        }
                    }
                }
            }
        }
        if (this.img=="") {
            for (var i = 0; i < tagsImg.length; i++) {
                try {
                    var temp = document.getElementById(tagsImg[i]).src;
                    if ((temp!=undefined) && (temp!="")) { this.img = temp; return ""; }
                } catch (e) {
                    try {
                        var temp = document.getElementById(tagsImg[i]).dataset.oldHires;
                        if ((temp!=undefined) && (temp!="")) { this.img = temp; return ""; }
                    } catch (e) {
                        try {
                            var temp = document.getElementsByClassName(tagsImg[i])[0].src;
                            if ( (typeof temp === 'string') && (temp!="") ) {  this.img = temp;  return "";  }
                        } catch (e) {
                        }
                    }
                }
            }
        }
    }
    // schema.org tag - itemtype="http://schema.org/Product">
    getParametroItempropType(index = 0) {
        if (this.type!="" ) { return "";}
        try {
            var temp = document.querySelectorAll('[itemtype]')[index].getAttribute("itemtype");
            if ((temp!=undefined) && (temp!="")) {
                if(temp.indexOf('schema.org') == -1) {
                    // se non è uno schema.org
                    this.getParametroItempropType(index +1);
                } else {
                    this.type = temp;
                }
            }
        } catch (e) { }
    }
    // prezzo da altri elementi ... TODO: da spostare in GOLANG
    getParametroPrezzo() {
        if (this.price!="" ) { return "";}
        try {
            // Verifico che nel titolo ci sia il prezzo ...
            /* TODO: da perfezionare se:
                - valuta differente
                - valuta attaccata al valore
                - altre casistiche ...
            */
            // Titolo di default
            var res = this.title.split(" ");
            var parola_before = "";
            for (var i = 0; i < res.length; i++) {
                if ((res[i]=="€") || (res[i]=="EUR") ) {
                    var temp = parseFloat(parola_before);
                    if (!isNaN(temp)) { this.price = temp; return "";}
                    temp = parseFloat(res[i+1]);
                    if (!isNaN(temp)) { this.price = temp;  return "";}
                }
                parola_before = res[i];
            }
            // se non trovo passo ad altro ...
            var title = this.getParametroCommon( ["title"] , ["title"], ["name"], ["title"] , [], false);
            var res = title.split(" ");
            var parola_before = "";
            for (var i = 0; i < res.length; i++) {
                if ((res[i]=="€") || (res[i]=="EUR") ) {
                    var temp = parseFloat(parola_before);
                    if (!isNaN(temp)) { this.price = temp; return "";}
                    temp = parseFloat(res[i+1]);
                    if (!isNaN(temp)) { this.price = temp;  return "";}
                }
                parola_before = res[i];
            }

        } catch (e) { console.log(e); }
    }
    // verifica sei il link / file esiste in remoto.
    doesFileExist(urlToFile, isFile) {
        if (typeof urlToFile === 'string') {
            if (isFile==true) {
                try {
                    var xhr = new XMLHttpRequest();
                    xhr.open('HEAD', urlToFile);
                    xhr.send();
                    if (xhr.status == "404") { return false; } else { return true; }
                } catch (e) { return false; }
            } else {
                return true;
            }
        } else  {
            if (typeof urlToFile === 'number') {
                return true;
            }
            throw "object not string";
        }
    }
    getValidUrl(url) {
        if (url==""){ return ""; }
        try {
          new URL(url);
          return url;
        } catch (_) {
            if (url.substring(0,2)=="//") {
                return "http:" + url;
            }
            var host = window.location.href;
            return host.replace(document.URL.match(/[^\/]+$/) , "") + "/" + url;
        }
    }
    // memorizzazione variabili ...
    addCookie(cname, cvalue) {
        localStorage.setItem(cname, cvalue);
    }
    setCookie(name,value) {
        // console.log("setCookie: " + name);
        var expires = "";
        var date = new Date();
        date.setTime(date.getTime() + (30*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    getCookie(cname) {
        var temp = localStorage.getItem(cname);
        if (temp==null) {
            return "";
        } else {
            return temp;
        }
    }
    addAllCookie() {
        // Calcolo tempo pagina
        var dEnd = new Date();
        //  console.log(dEnd.getTime() + " - " + this.startTime);
        var difference_ms = dEnd.getTime() - this.startTime;
        // var milliseconds = difference_ms/1000;
        // Creo strutttura dei cookie
        // da valutqre gli elementi troppo lugnhi ...
        // this.addCookie( this.cookieMilliseconds ,milliseconds);
        this.addCookie( this.cookieStartTime ,this.startTime);
        this.addCookie( this.cookieMilliseconds ,difference_ms);
        this.addCookie( this.cookieTimeOutside , this.timeOutside.toString());
        this.addCookie( this.cookieFlagContatoreOggetti , this.flagContatoreOggetti );
        this.addCookie( this.cookieFlagOverOggetti , this.flagOverOggetti.toString());
        this.addCookie( this.cookieScrollY , this.aScrollY.toString());
        this.addCookie( this.cookieScrollX , this.aScrollX.toString());
        this.addCookie( this.cookieClick , this.aClick.toString());
        this.addCookie( this.cookieWords , this.words.toString());
    }
    //** Valori di default e Personalizzazione del sistema
    settingContext() {
        // servizi
        this.exit_popup = "n"; // Apre popup lead generation al onfocus della pagina.
        this.id_referrer_costum = ""; // id del div di return if
        // No site
        this.sites_not_index = ["google.com","bind.com","facebook.com","whatsapp.com","telegram.com","twitter.com","mongodb.com","uiex.it"];
        // sesso
        this.sesso_page = ["uomo","donna","bambino","bambina","ragazza","ragazzo"];
        // crawler
        this.crawler = ""; // "attivo";
        // extension browser per brand
        this.brand = ""; // se valorizzata prende
        // Assegno variabili esterne
        if (window.uiex_parameters!=null) {
            for (let [key, value] of Object.entries(window.uiex_parameters)) {
                eval( "this." + key + " = '" + value + "';" );
            }
        }
    }
    // setto tag di analisi pagina passati dalle API in chiamata.
    setTags(Tags) {
        this.Tags = Tags;
    }
    /* Raccoglie le parole più importanti della pagina senza caratteri speciali e senza dominio della pagina */
    getMainWord() {
        var res = [];
        var resTitle = this.title.toLowerCase().split(" ");
        // console.log("title:" + resTitle);
        var resDescription = this.description.split(" ");
        // console.log("description:" + resDescription);
        // Titolo della pagina
        for (var i = 0; i < resTitle.length; i++) {
            if (resTitle[i].length > 2) {
                if (!this.isSpecialCharacter(resTitle[i])) {
                    if ( !this.isDomain(resTitle[i].toLowerCase()) ) {
                        var number = this.getNumberWord(resTitle[i].toLowerCase());
                        if (this.getByTagNameWord( "h1", resTitle[i].toLowerCase() ) ) {
                            number = number * 1000;
                        }
                        res[resTitle[i].toLowerCase()] = number;
                    }
                }
            }
        }
        // descrizione della pagina
        for (var i = 0; i < resDescription.length; i++) {
            if (!this.isSpecialCharacter(resDescription[i])) {
                if ( !this.isDomain(resDescription[i].toLowerCase()) ) {
                    if (resTitle.indexOf(resDescription[i].toLowerCase()) > - 1 ) {
                        res[resDescription[i].toLowerCase()] = res[resDescription[i].toLowerCase()] * 100;
                    } else {
                        if (resDescription[i].length > 2) {
                            var number = this.getNumberWord(resDescription[i].toLowerCase());
                            res[resDescription[i].toLowerCase()] = number;
                        }
                    }
                }
            }
        }
        // ordina e prendi i risulati con il valore maggioe
        var sortable = [];
        for (var ordinati in res) {
            if (Number.isInteger(res[ordinati])) {
                sortable.push([ordinati, res[ordinati]]);
            }
        }
        sortable.sort(function(a, b) {
            return b[1] - a[1];
        });
        // console.log(sortable.slice(0, 10));
        return sortable.slice(0, 10);;
    }
    isDomain(word) {
        var cur = this.currentDomain.toLowerCase();
        if (cur.indexOf(word) !== -1) {
            return true;
        }
        return false;
    }
    isSpecialCharacter(word) {
        try {
            // var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
            var format = /[!@#$%^&*()_+ \[\]{};':"\\|,.<>\/?]+/; // tolto - per T-shirt
            if(format.test(word)){ return true; }
        } catch (e) {
        }
        try {
            return word.match(/^[^a-zA-Z0-9]+$/) ? true : false;
        } catch (e) {
        }
        return false;
    }
    getAgeGender() {
        var numberMax = 0;
        var value = "";
        for (var i = 0; i < this.sesso_page.length; i++) {
            var number = 0
            if ( this.title.toLowerCase().indexOf(this.sesso_page[i].toLowerCase() ) > -1 ) {
                number = 1000;
            }
            number += this.getNumberWord(this.sesso_page[i]);
            number += this.getWordByLink(this.sesso_page[i]);
            // this.getByTagNameWord( "h1", this.sesso_page[i] ) ;
            // console.log(this.sesso_page[i] + " - " + number);
            if (number > numberMax) {
                value = this.sesso_page[i];
                numberMax = number;
            }
        }
        if (numberMax==0) { value = ""; }
        // console.log(value);
        return value;
    }
    // conta il numero delle volte che la parola si ripete nella pagina
    getNumberWord(word) {
        var number = 0;
        try {
            var queue = [document.body];
            var curr;
            while (curr = queue.pop()) {
                if (!curr.textContent.toLowerCase().match(word.toLowerCase())) continue;
                for (var i = 0; i < curr.childNodes.length; ++i) {
                    switch (curr.childNodes[i].nodeType) {
                        case Node.TEXT_NODE : // 3
                            if (curr.childNodes[i].textContent.toLowerCase().match(word.toLowerCase() )) {
                                number++;
                            }
                            break;
                        case Node.ELEMENT_NODE : // 1
                            queue.push(curr.childNodes[i]);
                            break;
                    }
                }
            }
        } catch (e) {
        }
        return number;
   }
   // Conta se la parola è contenuta nei tag principali della pagina. Es H1.
   getByTagNameWord( tagName, word ) {
        // console.log( tagName + "-" + word + "####################");
        try {
            var h1s = document.getElementsByTagName( tagName );
            for (var i = 0; i < h1s.length; i++) {
                // console.log( h1s[i] );
                if ( h1s[i].toLowerCase().indexOf(word.toLowerCase() ) > -1 ) {
                    // console.log( h1s[i] +  " -- " + word + " - found!");
                    return true;
                }
            }
        } catch (e) {
            return false;
        }
        return false;
    }
    getWordByLink( word ) {
        var number = 0;
        try {
            var array = [];
            var links = document.getElementsByTagName("a");
            for(var i=0; i<links.length; i++) {
                if ( links[i].href.toLowerCase().indexOf(word.toLowerCase() ) > -1 ) {
                    number++;
                }
            }
        } catch (e) {
        }
        return number;
    }
    isValidURL( temp) {
        var res = temp.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
        return (res !== null)
    };
    // https://andywalpole.me/blog/140739/using-javascript-create-guid-from-users-browser-information
    getBrowserId() {
        return window.navigator.userAgent.replace(/\D+/g, '');
    }
    generate_token(length){
        //edit the token allowed characters
        var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
        var b = [];
        for (var i=0; i<length; i++) {
            var j = (Math.random() * (a.length-1)).toFixed(0);
            b[i] = a[j];
        }
        return b.join("");
    }
    getToken() {
        var token = this.getCookie( this.radice + "token" );
        if (token=="")  {
            token = this.generate_token(32) + "-" + this.getBrowserId();
            this.addCookie( this.radice + "token", token);
        }
        return token;
    }
    getIdUser() {
        return "";
    }


    getScroll() {
        var scrollX = this.getScrollX();
        if ( scrollX > 0 ) {
            userIntelligentExperienceAppInstance.aScrollX.push(scrollX);
        }
        var scrollY = this.getScrollY();
        if ( scrollY > 0 ) {
            userIntelligentExperienceAppInstance.aScrollY.push(scrollY);
        }
    }
    getScrollX() {
        try { return window.pageXOffset || document.body.scrollLeft || document.html.scrollLeft; } catch(err) { return 0;}
    };
    getScrollY() {
        try { return window.pageYOffset || document.body.scrollTop || document.html.scrollTop; } catch(err) { return 0; }
    }

    getTagNameHtml(e) {
      var temp = "";
      try {
          temp = e.toElement.nodeName.toUpperCase();
      } catch (e) {  }
      //console.log(temp);
      if (temp === "") {
        try {
            temp = e.srcElement.nodeName.toUpperCase();
            //console.log(temp);
        } catch (e) {  }
      }
      return temp;
    }

}

// Instanzio la classe
let userIntelligentExperienceAppInstance = new UserIntelligentExperienceApp();
userIntelligentExperienceAppInstance.init();

function addEventUserIntelligentExperience(obj, evt, fn) {
    if (obj.addEventListener) {
        obj.addEventListener(evt, fn, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}



addEventUserIntelligentExperience(document, "mouseout", function(e) {
    e = e ? e : window.event;
    var from = e.relatedTarget || e.toElement;
    if (!from || from.nodeName == "HTML") {
        // console.log("out----");
        //*** Tempo In e out ******************************/
        userIntelligentExperienceAppInstance.timeOutside.push( "o-" + new Date().getTime() );
        userIntelligentExperienceAppInstance.flagOver = true;
        userIntelligentExperienceAppInstance.addAllCookie();
        userIntelligentExperienceAppInstance.update();
        //*** PopUp lead generation ******************************/
        if (userIntelligentExperienceAppInstance.exit_popup == "y") {
            let userIntelligentExperienceAppInstanceLeft = new UserIntelligentExperienceAppLeftWindow();
            userIntelligentExperienceAppInstanceLeft.createDivLeftWindow();
        }
    }
});
//*** Scrolling PAGE ******************************/
addEventUserIntelligentExperience(document, "scroll", function(e) {
    try {
        userIntelligentExperienceAppInstance.getScroll();
    } catch (e) {
        console.log(e);
    }
    userIntelligentExperienceAppInstance.addAllCookie();
});

//*** Click ******************************/
addEventUserIntelligentExperience(document, "click", function(e) {
    e = e ? e : window.event;
    var from = e.relatedTarget || e.toElement;
    try {
        userIntelligentExperienceAppInstance.aClick.push( e.clientX );
    } catch (e) { }
    try {
        userIntelligentExperienceAppInstance.aClick.push( e.clientY );
    } catch (e) {  }
    userIntelligentExperienceAppInstance.addAllCookie();
});
//*** Srrolling PAGE ******************************/
/*
addEventUserIntelligentExperience(document, "mousedown", function(e) {
    userIntelligentExperienceAppInstance.addAllCookie();
});
*/
addEventUserIntelligentExperience(document, "mouseover", function(e) {
    e = e ? e : window.event;
    var from = e.relatedTarget || e.toElement;
    //*** Tempo In e out ******************************/
    if (userIntelligentExperienceAppInstance.flagOver) {
        // console.log("in----");
        // riprendo il focus della finestra se non è presente ... ad esempio ho aperto un altro browser o app in parallelo sulla stessa schermata.

        // TODO: da capire se lasciare o togliere ...
        /*
        if (!document.hasFocus()) {
            try {
                var open = window.open("","");
                open.close();
            } catch (e) {  }
        }
        */
        userIntelligentExperienceAppInstance.flagOver = false;
        userIntelligentExperienceAppInstance.timeOutside.push( "i-" + new Date().getTime() );
        userIntelligentExperienceAppInstance.update();
    }

    //*** Contatore di movimento del mouse - Count quanto si passa da un elemento all'altro. ******************************/
    userIntelligentExperienceAppInstance.flagContatoreOggetti++;
    //*** Analisi elementi trovati ******************************/
    if (userIntelligentExperienceAppInstance.Tags!=null){
        for (var i = 0; i < userIntelligentExperienceAppInstance.Tags.length; i++){
            // look for the entry with a matching `code` value
            if (userIntelligentExperienceAppInstance.Tags[i].TagName == userIntelligentExperienceAppInstance.getTagNameHtml(e)){
                userIntelligentExperienceAppInstance.flagOverOggetti.push( userIntelligentExperienceAppInstance.getTagNameHtml(e) );
                try {
                  // ******** DA CONTROLLARE per Mozilla
                    userIntelligentExperienceAppInstance.flagOverOggetti.push( eval( "e.toElement." + userIntelligentExperienceAppInstance.Tags[i].Attributi) );
                } catch (e) {  }
                try {
                    userIntelligentExperienceAppInstance.flagOverOggetti.push( event.clientX ); // Get the horizontal coordinate
                } catch (e) {  }
                try {
                    userIntelligentExperienceAppInstance.flagOverOggetti.push( event.clientY );
                } catch (e) {  }
            }
        }
    }
    userIntelligentExperienceAppInstance.addAllCookie();
});


//*** Lascio la pagina *************************************************/
window.addEventListener('beforeunload', function (e) {
    userIntelligentExperienceAppInstance.addAllCookie();
    // Cancel the event as stated by the standard.


    // ******** DA CONTROLLARE per Mozilla
    // e.preventDefault();

    // Chrome requires returnValue to be set.
    // e.returnValue = '';
});
