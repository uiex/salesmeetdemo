package main

// GOPATH="/Users/corrado/go"
// export GOPATH=/Users/corrado/git/uiex/uiexapp-api
// export GOPATH=/var/www/uiex/uiexapp-api

// export GOROOT=/var/www/uiex/uiexapp-api/
// export PATH=$PATH:$GOROOT/bin
// export GOPATH=/var/www/uiex/uiexapp-api/src
// export PATH=$PATH:/var/www/uiex/uiexapp-api/src
// export GOPATH=/var/www/uiex/uiexapp-api/src

import (
	"log"
	"net/http"
	"os"
	"uiexapp/common"

	"github.com/gorilla/mux"
)

func pageRoot(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("uiextools.salesmeet.it\n"))
}
func pageRootChronology(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Chronology\n"))
}

func getPort() string {
	var PORT string
	if PORT = os.Getenv("PORT"); PORT == "" {
		PORT = "8000"
	}
	return PORT
}

func main() {

	// common.SendEmail()

	router := mux.NewRouter()
	// Routes consist of a path and a handler function.
	router.HandleFunc("/", pageRoot)

	// DB - Chronology
	common.StartUp()
	// common.CreateDefault()
	router.HandleFunc("/chronology", pageRootChronology).Methods("GET")           // root
	router.HandleFunc("/chronology/", common.InsertChronology).Methods("POST")    // Tools // Chronology.go
	router.HandleFunc("/chronology/", common.UpdateChronology).Methods("PUT")     // Tools // Chronology.go
	router.HandleFunc("/chronology/", common.UpdateChronology).Methods("OPTIONS") // Tools // Chronology.go
	router.HandleFunc("/chronology/", common.UpdateChronology).Methods("GET")     // Tools // Chronology.go

	// router.HandleFunc("/chronology/delete/", common.DeleteChronology).Methods("POST")    // Tools // Delete.go

	// router.HandleFunc("/chronology/experience/{sito}", common.GetExperience).Methods("POST")      // Front interno // experience.go
	router.HandleFunc("/chronology/experience/", common.GetExperience).Methods("POST")      // Front interno // experience.go
	router.HandleFunc("/chronology/experience/limit/", common.GetExperienceLimit).Methods("POST") // Front interno // experience.go

	// router.HandleFunc("/chronology/last/{sito}", common.GetLast).Methods("GET") // Front interno // last.go
	router.HandleFunc("/chronology/last/", common.GetLast).Methods("POST")       // Tools // last.go

	router.HandleFunc("/chronology/chronology/", common.GetChronology).Methods("POST")          // Front interno // Chronology.go
	router.HandleFunc("/chronology/chronology/limit/", common.GetChronologyLimit).Methods("GET")     // Tools // Chronology.go
	router.HandleFunc("/chronology/chronology/last/{sito}", common.GetChronologyLast).Methods("GET") // Front interno // Chronology.go

	router.HandleFunc("/chronology/customerexperience/{id}/{sito}", common.GetCustomerExperienceById).Methods("GET") // Front interno
	
	// router.HandleFunc("/chronology/page/all/", common.GetPageNotUrl).Methods("GET") // pages.single
	router.HandleFunc("/chronology/page/all/", common.GetPageNotUrl).Methods("POST") // pages.single
	router.HandleFunc("/chronology/page/single/", common.GetChronologyByUrl).Methods("POST")  // Chronology.go
	router.HandleFunc("/chronology/pages/", common.GetAllPages).Methods("GET")  // pages.go

	/*
	router.HandleFunc("/chronology/consigliati/", common.GetConsigliati).Methods("GET")        // Front interno
	router.HandleFunc("/chronology/consigliati/{sito}", common.GetConsigliati).Methods("GET")  // Front interno
	router.HandleFunc("/chronology/consigliato/", common.GetConsigliato).Methods("POST")       // Front interno
	router.HandleFunc("/chronology/consigliato/{sito}", common.GetConsigliato).Methods("POST") // Front interno
	*/
	router.HandleFunc("/consigliati/", common.GetConsigliati).Methods("GET")        // Front interno
	router.HandleFunc("/consigliati/", common.GetConsigliati).Methods("POST")  // Front interno
	router.HandleFunc("/consigliato/", common.GetConsigliato).Methods("POST")       // Front interno
	router.HandleFunc("/consigliato/{sito}", common.GetConsigliato).Methods("POST") // Front interno


	// router.HandleFunc("/consigliati/pages/words/", common.GetExperience).Methods("POST")      // Front interno // experience.go
	router.HandleFunc("/consigliati/pages/words/", common.GetPagesConsigliateWords).Methods("POST") // Front interno
	router.HandleFunc("/consigliati/pages/title/", common.GetPagesConsigliateTitle).Methods("POST") // Front interno
	router.HandleFunc("/consigliati/sites/words/", common.GetConsiglatiWords).Methods("POST") // Front interno


	router.HandleFunc("/useranalytics/", common.GetUserInfoCommon).Methods("POST")        // Front interno

	// addon
	// non usata ancora
	router.HandleFunc("/consigliati/addon/app/", common.GetAddonApp).Methods("POST")


	// prova per auth auth
	/*
	router.HandleFunc("/chronology/auth/new", common.NewUser).Methods("POST")           // Front interno
	router.HandleFunc("/chronology/auth/login", common.Login).Methods("POST")           // Front interno
	router.HandleFunc("/chronology/auth/{token}", common.IsAutenticaton).Methods("GET") // Front interno
	*/

	// estensione Chrome
	router.HandleFunc("/extensionBrowser/", common.GetExtensionBrowser).Methods("GET")

	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(":"+getPort(), router))



}
