package common

import (
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

type (
	siteParameters struct {
		site              string
		referer           string
		ip                string
		language          string
		useragent         string
		useragentcompleto string // user agent completo per capire browser ed altro così da fasare l'analisi per anche tipologia cliente
		device            string // tipo device (smartphone, desktop ...)
		uiextrack         string // se è un link trackatto
		user         	  string
		token         	  string
	}
)

func getSiteParameters(r *http.Request) siteParameters {

	/*

		fmt.Println("___________getSiteParameters__________________________________________________________")
		fmt.Println(r.Header.Get("X-Forwarded-Proto"))
		fmt.Println(r.Header.Get("Accept-Language"))
		fmt.Println(r)
		fmt.Println(r.Header)
		fmt.Println(r.Method, r.URL, r.Proto)
		fmt.Println("_____________________________________________________________________")
		fmt.Println("Referrer : ", r.Referer())
		fmt.Println("_____________________________________________________________________")
		//Iterate over all header fields

		// for k, v := range r.Header {
		for k := range r.Header {
			// fmt.Println(w, "Header field %q, Value %q\n", k, v)
			fmt.Println("Header field %q, Value = ", r.Header[k], k)
		}
		fmt.Println("Referer = ", r.Header["Referer"])
		fmt.Println("Origin = ", r.Header["Origin"])

		fmt.Println("Host = ", r.Host)
		fmt.Println("URL.Path = ", r.URL.Path)
	*/
	// protocollo := r.Header.Get("X-Forwarded-Proto")

	origin := r.Header.Get("Referer")
	if origin == "" {
		origin = r.Header.Get("Origin")
	}
	u, err := url.Parse(origin)
	if err != nil {
		log.Fatal(err)
	}
	parts := strings.Split(u.Hostname(), ".")
	sito := parts[len(parts)-2] + "." + parts[len(parts)-1]
	lingua := r.Header.Get("Accept-Language")
	referer := r.Header.Get("Referer")
	realIp := r.Header.Get("X-Real-Ip")
	device := ""

	reg, err := regexp.Compile("[^0-9]+")
	// reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		log.Fatal(err)
	}

	agent := r.Header.Get("User-Agent")
	agentId := reg.ReplaceAllString(agent, "")

	user := getUser(r)
	token := getToken(r,agentId)

	// pulisco il track dell'applicazione ...
	referer, uiextrack := isTrackUrl(referer, origin)

	return siteParameters{site: sito, referer: referer,
		ip: realIp, language: lingua,
		useragent: agentId, useragentcompleto: agent,
		uiextrack: uiextrack, device: device, user: user, token: token}

}

// Inserisci elementi
func setAccessControl(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS")
	/*
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	*/
}
