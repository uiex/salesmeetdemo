// Autenticazione
package common

//  https://jwt.io/
// mgo "gopkg.in/mgo.v2"

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	"github.com/robbert229/jwt"
)

/*
func Login(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)

	fmt.Println("_______login______________________________________________________________")
	email := getEmail(r)
	fmt.Println(email)

	session.SetMode(mgo.Monotonic, true)

	c := session.DB(db_utente).C(collection_utente)
	err := c.Insert(&Utente{user: user, pass: ""})
	if err != nil {
		fmt.Println("Errore:", err)
	}

	auth := Auth{Id: "belllllll"}
	json.NewEncoder(w).Encode(&auth)

}
*/

/*
func NewUser(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)

	// fmt.Println("_______login______________________________________________________________")

	user := getUser(r)

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	// Creo id così da poterlo portare fuori
	c := session.DB(db_utente).C(collection_utente)
	err := c.Insert(&Utente{user: user, pass: ""})
	if err != nil {
		fmt.Println("Errore:", err)
	}

	auth := Auth{Id: "belllllll"}
	json.NewEncoder(w).Encode(&auth)

}
*/

func Login2(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)

	// fmt.Println("_______login______________________________________________________________")

	user := getUser(r)
	secret := "ThisIsMySuperSecret"
	algorithm := jwt.HmacSha256(secret)

	claims := jwt.NewClaim()
	claims.Set("user", user)
	// claims.Set("pass", pass)
	// claims.SetTime("exp", time.Now().Add(time.Minute))

	token, err := algorithm.Encode(claims)
	if err != nil {
		panic(err)
	}

	// fmt.Println("_______Token______________________________________________________________")
	// fmt.Println("Token: ", token)

	auth := []Auth{}
	auth = append(auth, Auth{Id: token})
	// fmt.Println("auth: ", auth)

	json.NewEncoder(w).Encode(auth)
	// fmt.Println("_____________________________________________________________________")

}

func IsAutenticaton(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)

	// fmt.Println("_______Autenticazione______________________________________________________________")

	secret := "ThisIsMySuperSecret"
	algorithm := jwt.HmacSha256(secret)

	params := mux.Vars(r)
	token := params["token"]

	// fmt.Println("_______Token______________________________________________________________")
	// fmt.Printf("Token: ", token)

	loadedClaims, err := algorithm.Decode(token)
	if err != nil {
		panic(err)
	}

	resultUser, err := loadedClaims.Get("user")
	if err != nil {
		panic(err)
	}
	resultUserString, ok := resultUser.(string)
	if !ok {
		panic(err)
	}

	resultPass, err := loadedClaims.Get("pass")
	if err != nil {
		panic(err)
	}
	resultPassString, ok := resultPass.(string)
	if !ok {
		panic(err)
	}

	fmt.Println("user:" + resultUserString)
	fmt.Println("pass:" + resultPassString)

	/*
		if strings.Compare(roleString, "Admin") == 0 {
			//user is an admin
			fmt.Println("User is an admin")
		}
	*/

	// fmt.Println("_____________________________________________________________________")

	auth := Auth{Id: "belllllll"}
	json.NewEncoder(w).Encode(&auth)

}


type (
	Auth struct {
		Id string
	}
)

type (
	Utente struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Email 		string
		Chiave      string
		Attivo      int
		Timestamp   time.Time
	}
)
