// Gestione connection al DB
// Creazione naming Collection e document
package common

import (
	"encoding/json"
	"fmt"
	"strings"
	"log"
	"net/http"
	"os"
	"time"
	"crypto/tls"
	"net"

	"github.com/gorilla/mux"
	mgo "gopkg.in/mgo.v2"
)

// "time"
// "crypto/tls"
// "net"
//


// chronology
// Tabelle contenenti i log del singolo sito. Tutti i dati di navigazione dell'utente sono in "customer_experience"
var collection_chronology = "chronology"
var db_chronology = "chronology"

// customer_experience
// Tabelle contenenti i log di navigazione e utilizzo dell'utente del singolo sito e pagina.
// Con questi dati vengono assegnati i valori di Navigazione e di utilkizzo della pagina.
// I dati sono comuni sia per i DB "chronology" e "chrome"
var collection_customer_experience = "customer_experience"
var db_customer_experience = "customer_experience"

// last
// Ultima pagina visitata dall'utente. Suddiviso per sito.
// Creata un DB singolo per velocizzarne la lettura per singola funzione.
// Vengono registrate le ultime 3 pagine visitate.
var collection_last = "last"
var db_last = "last"

// pagine
// Elenco delle pagine visitata suddivise per sito.
// Creata un DB singolo per velocizzarne la lettura per singola funzione.
var collection_pages = "pages"
var db_pages = "pages"

// track di utilizzo uiex
// Tabella di memorizzazione dei link utilizzati dal visitatore, usando la struttura ed i link forniti da uiex.
// es: https://www.sito.it/pagina.html?uiextrack=0
var collection_track = "track"
var db_track = "track"

// track di navigazione utente
// Per evitare una join su più record su "chronology"
// Creata per avere una struttura di join tra link precente e quello attuale.
// Creata per avere una select non complessa ma una view come vuole mongo.
// La stessa è in stratta relazione con "last"
var collection_track_chronology = "trackchronology"
var db_track_chronology = "trackchronology"

// chrome
// Tabelle contenenti i log di navigazione del visitatore se ha installato l'extensione di CHROME.
// A differenza delle altre strutture di DB questa ha tutti i dati nello stesso DB / Collection / Document
// Tutti i dati di navigazione dell'utente sono in "customer_experience"
var collection_chrome = "chrome_log"
var db_chrome = "chrome"

// user
var collection_utente = "utenti"
var db_utente = "utenti"

// consigliati
var collection_consigliati = "consigliati"
var db_consigliati = "consigliati"

// consigliati
var collection_consigliati_words = "consigliatiwords"
var db_consigliati_words= "consigliatiwords"

// addon
var collection_addon = "addonlist"
var db_addon = "addonlist"

var collection_addon_user = "addonsuer"
var collection_addon_app = "addonapp"

// https://docs.mongodb.com/manual/tutorial/query-documents/
// https://docs.mongodb.com/manual/core/aggregation-pipeline/
// https://gist.github.com/laeshiny/89c5ec444cfa275f456f340aadddf102

// AppConfig holds the configuration values from config.json file
var AppConfig configuration

// Initialize AppConfig
func initConfig() {
	file, err := os.Open("config.json")
	defer file.Close()
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
	decoder := json.NewDecoder(file)
	AppConfig = configuration{}
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatalf("[logAppConfig]: %s\n", err)
	}
}

// Session holds the mongodb session for database access
var session *mgo.Session


func getSessionDB() {
	if session == nil {
		session = createDbSession()
	}
	session.SetMode(mgo.Monotonic, true)
}

// Create database session
func createDbSession() (*mgo.Session) {

	var err error

	setdb := os.Getenv("SETDB")

	if setdb == "local" {

		session, err = mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    []string{AppConfig.MongoDBHost},
			Username: AppConfig.DBUser,
			Password: AppConfig.DBPwd,
			Timeout:  60 * time.Second,
		})

		if err != nil {
			fmt.Println("DB LOCAL ----- Errore:", err)
		} else {
			fmt.Println("DB LOCAL ----- API UIEX - STATUS: ATTIVE ...", "")
		}

		return session

	} else {

		/*
		dialInfo := &mgo.DialInfo{
			Addrs: []string{"salesmeet-shard-00-00.tyind.mongodb.net:27017",
							"salesmeet-shard-00-01.tyind.mongodb.net:27017",
							"salesmeet-shard-00-02.tyind.mongodb.net:27017"},
			Database: "",
			*/
		dialInfo := &mgo.DialInfo{
			Addrs: []string{"cluster0-shard-00-00.llwtf.mongodb.net:27017",
							"cluster0-shard-00-01.llwtf.mongodb.net:27017",
							"cluster0-shard-00-02.llwtf.mongodb.net:27017"},
			Database: "",
			Username: "salesmeetuser",
			Password: "123#4567Asz",
			Timeout:  60 * time.Second,
		}

		tlsConfig := &tls.Config{}
		dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
			return conn, err
		}

		sessionproduzione, err := mgo.DialWithInfo(dialInfo)
		fmt.Println("sessionproduzione:", sessionproduzione)

		if err != nil {
			fmt.Println("DB PRODUZIONE ----- Errore:", err)
		} else {
			fmt.Println("DB PRODUZIONE ----- API UIEX - STATUS: ATTIVE ...", "")
		}

		return sessionproduzione

	}


}

func GetSiteCollection(w http.ResponseWriter, r *http.Request) string {
	params := mux.Vars(r)
	sito := params["sito"]
	if sito != "" {
		return setDB(sito)
	}
	sito = r.PostFormValue("sito")
	if sito != "" {
		return setDB(sito)
	}
	siteParameters := getSiteParameters(r)
	return setDB(siteParameters.site)
}

func setDB(db string) string {
	db = strings.Replace(db, "www.", "", -1)
	fmt.Println("db:", db)
	split := strings.Split(db, ".")
	if len(split) > 2 {
		db = split[1] + "." + split[2]
	}
	db = strings.Replace(db, ".", "_", -1)
	return db
}
