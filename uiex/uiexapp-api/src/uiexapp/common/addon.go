// Elenco delle pagine.
package common


import (
	"fmt"
	"net/http"
	"time"
	"encoding/json"
	"strings"

	"gopkg.in/mgo.v2/bson"
)


// **** Istruzioni setting Mongo ****
// Creare DB da interfaccia mongoDB o Robot 3D
// Creare Collection da interfaccia mongoDB o Robot 3D
// Insert document da interfaccia mongoDB o Robot 3D
	// { site : "farfetch_com", attivo : 0, nome: "Farfetch", logo: "" } 
	// { site : "zalando_it", attivo : 0, nome: "Zalando", logo: "" } 
	// { site : "chiaraferragnicollection_com", attivo : 0, nome: "Chiara Ferragni", logo: "" } 

func InsertAddonList(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	getSessionDB()

	c := session.DB(db_addon).C(collection_addon)

	siteParameters := getSiteParameters(r)
	site := setDB(siteParameters.site)
	addon := []AddonList{} 
	query := bson.M{"Site": site}
	err := c.Find(query).All(&addon)
	if err == nil {
		fmt.Printf("non trovato find fail %v\n", err)
	}

	if len(addon) == 0 {
		// fmt.Println("Insert new InsertAddon **************** ")
		errInsert := c.Insert(&AddonList{Site: site, Attivo: 0, Timestamp: time.Now()})
		if errInsert != nil {
			fmt.Println("Errore:", errInsert)
		}
	} 

}

// Recupero tutta la lista degli addon ...
func GetAddonList(w http.ResponseWriter, r *http.Request)  {

	// fmt.Println("___________GetAddonList__________________________________________________________")

	setAccessControl(w)
	getSessionDB()

	c := session.DB(db_addon).C(collection_addon)

	// fmt.Println(db_addon)
	// fmt.Println(collection_addon)


	result := []AddonList{}
	query := bson.M{"attivo": 0}
	err := c.Find(query).All(&result)
	if err != nil {
		fmt.Println("Errore:", err)
	}

	// fmt.Println(result)
	// fmt.Println("___________GetAddonList FINE__________________________________________________________")
	json.NewEncoder(w).Encode(&result)

}


//*****************************************

func InsertAddonUser(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	getSessionDB()

	c := session.DB(db_addon).C(collection_addon)

	siteParameters := getSiteParameters(r)
	site := setDB(siteParameters.site)
	addon := []AddonUser{} 
	query := bson.M{"Site": site}
	err := c.Find(query).All(&addon)
	if err == nil {
		fmt.Printf("non trovato find fail %v\n", err)
	}

	if len(addon) == 0 {

		ip := siteParameters.ip
		useragent := siteParameters.useragent
		user := getUser(r)
		// fmt.Println("Insert new InsertAddon **************** ")
		errInsert := c.Insert(&AddonUser{UserAgent: useragent, Ip: ip, User: user, Site: site, Timestamp: time.Now()})
		if errInsert != nil {
			fmt.Println("Errore:", errInsert)
		}

	} 

}


// **** Istruzioni setting Mongo ****
// Creare DB da interfaccia mongoDB o Robot 3D
// Creare Collection da interfaccia mongoDB o Robot 3D
// Insert document da interfaccia mongoDB o Robot 3D
	// { Site : "farfetch_com", Attivo : 0, Nome: "Farfetch", Logo: "" } 
	// { Site : "zalando_it", Attivo : 0, Nome: "Zalando", Logo: "" } 
	// { Site : "chiaraferragnicollection_com", Attivo : 0, Nome: "Chiara Ferragni", Logo: "" } 

func InsertAddonApp(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	getSessionDB()

	c := session.DB(db_addon).C(collection_addon)

	siteParameters := getSiteParameters(r)
	site := setDB(siteParameters.site)
	addon := []AddonUser{} 
	query := bson.M{"Site": site}
	err := c.Find(query).All(&addon)
	if err == nil {
		fmt.Printf("non trovato find fail %v\n", err)
	}

	if len(addon) == 0 {

		ip := siteParameters.ip
		useragent := siteParameters.useragent
		user := getUser(r)
		// fmt.Println("Insert new InsertAddon **************** ")
		errInsert := c.Insert(&AddonUser{UserAgent: useragent, Ip: ip, User: user, Site: site, Timestamp: time.Now()})
		if errInsert != nil {
			fmt.Println("Errore:", errInsert)
		}

	} 

}


// Recupero tutta la lista degli addon ...
func GetAddonApp(w http.ResponseWriter, r *http.Request)  {

	// fmt.Println("___________GetAddonApp__________________________________________________________")

	getSessionDB()

	c := session.DB(db_addon).C(collection_addon)

	// fmt.Println(db_addon)
	// fmt.Println(collection_addon)


	result := []AddonList{}
	query := bson.M{"attivo": 0}
	err := c.Find(query).All(&result)
	if err != nil {
		fmt.Println("Errore:", err)
	}

	// fmt.Println(result[0].Site)
	// fmt.Println("___________GetAddonApp FINE__________________________________________________________")
	// json.NewEncoder(w).Encode(  GetAddonAppList(w, r, 10, "tods_com")  )
	// GetAddonAppList(w, r, 10, "zalando_it")
	GetAddonAppList(w, r, 10, result[0].Site)

	// fmt.Println("___________GetAddonApp FINE 2__________________________________________________________")

}

func GetAddonAppList(w http.ResponseWriter, r *http.Request, limit int, collection_pages string) { 

	setAccessControl(w)

	// fmt.Println("___________GetAddonAppList__________________________________________________________")

	c := session.DB(db_pages).C(collection_pages)

	Nfind := 4
	words := getWords(r)
	s := strings.Split(words, ",")
	s = cleanArray(s)

	ageGender := ""
	// fmt.Println("getTipo(r):", getTipo(r))
	// fmt.Println("getAgeGender(r):", getAgeGender(r))
	if getTipo(r) == "product" {
		ageGender = getAgeGender(r)
	}
	resultTot := []WordsAll{}
	codeProducts := make([]bson.ObjectId, 11) // [10]bson.ObjectId{}
	idCodeProducts := 0
	// count := 0

	siteParameters := getSiteParameters(r)
	url := siteParameters.referer


	/* DA riprisitnare
	result4 := []WordsAll{}
	orQuery := GetQueryWords(s,url,"words.words","url",4,"",codeProducts,0,ageGender,0)
	fmt.Println("orQuery:", orQuery)
	err := c.Find(   bson.M{"$and":orQuery}   ).Limit(limit).Sort("-timestamp").All(&result4);	
	if err != nil {
		fmt.Println("Errore:", err)
	}
	fmt.Println("result:", len(result4))
	resultTot = append(resultTot, result4...)
	*/
	
	result3 := []WordsAll{}
	//if len(result4) < 11 {		
	orQuery, _ := GetQueryWords(s,url,"words.words","url",3,"",codeProducts,0,ageGender,0)
	// fmt.Println("orQuery:", orQuery)
	err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
	if err != nil {
		fmt.Println("Errore:", err)
	} else {
		Nfind = len(resultTot)	
		resultTot = append(resultTot, result3...)
		if len(result3) > 0 {
			resultTot[Nfind].NFind = 3
			// codici prodotti già esistenti
			for i := 0; i < len(result3); i++ {
				codeProducts[idCodeProducts] = result3[i].Id
				idCodeProducts = idCodeProducts + 1
			}
		}	
	}
	//} 

	result3 = []WordsAll{}
	if len(resultTot) < 10 {	
		orQuery, _ := GetQueryWords(s,url,"words.words","url",3,"",codeProducts,0,ageGender,1)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)	
			resultTot = append(resultTot, result3...)
			if len(result3) > 0 {
				resultTot[Nfind].NFind = 3
				// codici prodotti già esistenti
				for i := 0; i < len(result3); i++ {
					codeProducts[idCodeProducts] = result3[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}	
		}
	} 

	result2 := []WordsAll{}
	if len(resultTot) < 10 {		
		orQuery, _  = GetQueryWords(s,url,"words.words","url",2,"",codeProducts,0,ageGender,0)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)			
			resultTot = append(resultTot, result2...)  	
			if len(result2) > 0 {
				resultTot[Nfind].NFind = 2
				// codici prodotti già esistenti
				for i := 0; i < len(result2); i++ {
					// codeProducts[idCodeProducts] = result2[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}	
		}   
	}

	result2 = []WordsAll{}
	if len(resultTot) < 10 {		
		orQuery, _  = GetQueryWords(s,url,"words.words","url",2,"",codeProducts,0,ageGender,1)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)			
			resultTot = append(resultTot, result2...)  	
			if len(result2) > 0 {
				resultTot[Nfind].NFind = 2
				// codici prodotti già esistenti
				for i := 0; i < len(result2); i++ {
					// codeProducts[idCodeProducts] = result2[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}	
		}   
	}
	
	/*
	result1 := []WordsAll{}
	if len(resultTot) <= 10 {		
		orQuery, _  = GetQueryWords(s,url,"words.words","url",1,"",codeProducts,0,ageGender,0)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result1);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)
			resultTot = append(resultTot, result1...)
			if len(result1) > 0 {
				resultTot[Nfind].NFind = 1
			}	 
		}  
	}
	*/

	json.NewEncoder(w).Encode(&resultTot)

}