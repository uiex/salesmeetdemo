package common

import (
	"strings"
)

// Inserisci elementi
func gatTags(tipo string, price string) []Tag {

	// TODO: Da abbinare al DB ... ?
	theTag := []Tag{}

	tipo = castType(tipo, price)
	if tipo == "product" {

		theTag = append(theTag, Tag{"A", "href"})
		theTag = append(theTag, Tag{"H1", "innerHTML"})
		theTag = append(theTag, Tag{"H2", "innerHTML"})
		theTag = append(theTag, Tag{"IMG", "src"})
		theTag = append(theTag, Tag{"BUTTON", "innerHTML"})

	} else if tipo == "website" {

		theTag = append(theTag, Tag{"H1", "innerHTML"})
		theTag = append(theTag, Tag{"P", "innerHTML"})

	} else if tipo == "book" {

		theTag = append(theTag, Tag{"H1", "innerHTML"})
		theTag = append(theTag, Tag{"P", "innerHTML"})

	} else if tipo == "organization" {

		theTag = append(theTag, Tag{"H1", "innerHTML"})
		theTag = append(theTag, Tag{"P", "innerHTML"})

	} else if tipo == "article" {

		theTag = append(theTag, Tag{"H1", "innerHTML"})
		theTag = append(theTag, Tag{"P", "innerHTML"})

	} else {

		theTag = append(theTag, Tag{"A", "href"})
		theTag = append(theTag, Tag{"H1", "innerHTML"})

	}

	return theTag

}

// TODO: da mettere nel DB?
func castType(tipo string, price string) string {

	tipo = strings.TrimSpace(tipo)
	tipo = strings.ToLower(tipo)
	if tipo == "" {
		if price != "" {
			return "product"
		}
		return ""
	}
	switch tipo {
	case "http://schema.org/organization":
		return "organization"
	case "organization":
		return "organization"

	case "website":
		return "website"

	case "product.item":
		return "product"
	case "ebay-objects:item":
		return "product"
	case "product.group":
		return "product"
	case "og:product":
		return "product"
	case "zalando_sharing:product":
		return "product"
	case "ebay-objects:ecommerce":
		return "product"
	case "spartoo_com:article":
		return "product"
	case "product":
		return "product"
	case "http://schema.org/product":
		return "product"
	case "https://schema.org/offer":
		return "product"

	case "book":
		return "book"

	case "newsarticle":
		return "article"
	case "article":
		return "article"
	case "http://schema.org/qapage":
		return "article"
	case "http://data-vocabulary.org/breadcrumb":
		return "article"
	case "http://schema.org/breadcrumbList":
		return "article"
	case "http://schema.org/searchresultspage":
		return "article"

	case "undefined":
		return "website"
	}

	return ""
}
