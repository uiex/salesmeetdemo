// Link consigliato in base alla navigazione deglinaltri clienti.
package common

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
)

func checkConsigliato(w http.ResponseWriter, r *http.Request, i bson.ObjectId) {

	checkConsigliatoCommon(w, r, i)

}

func checkConsigliatoCommon(w http.ResponseWriter, r *http.Request, i bson.ObjectId) {

	// fmt.Println("___________checkConsigliatoCommon__________________________________________________________")

	// TODO:
	// - da capire se utilizzare la navigazione dello stesso utente
	// - da aggiungere la logica della esperienza utente
	// - per e-commerce da capire se è un prodotto con prezzo  ... se no meno importante.

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	// prendo ultimo URL inserito
	result := GetLastTrack(w, r)
	urlBeforeUno := ""
	urlBeforeDue := ""
	urlBeforeTre := ""
	if len(result) > 0 {
		urlBeforeUno = result[0].Url
		urlBeforeDue = result[0].UrlBeforeUno
		urlBeforeTre = result[0].UrlBeforeDue
	}

	collection_track_chronology := GetSiteCollection(w, r)
	c := session.DB(db_track_chronology).C(collection_track_chronology)

	consigliatiTC := []ConsigliatiTrackChronology{}
	query := bson.M{"urlbeforeuno": urlBeforeUno, "urlbeforedue": urlBeforeDue, "urlbeforetre": urlBeforeTre}
	punteggio := 0
	if urlBeforeTre != "" {
		query = bson.M{"urlbeforeuno": urlBeforeUno, "urlbeforedue": urlBeforeDue, "urlbeforetre": urlBeforeTre}
		punteggio = 3
	} else if urlBeforeDue != "" {
		query = bson.M{"urlbeforeuno": urlBeforeUno, "urlbeforedue": urlBeforeDue}
		punteggio = 2
	} else {
		/* TEST */
		query = bson.M{"urlbeforeuno": urlBeforeUno}
		punteggio = 1
	}

	// Query attiva solo se abbiamo due link di navigazione del visitatore.
	// La stessa logica la ritroviamo in InsertTrackChronology()
	// group := bson.M{"urlnow": "$urlnow", "count": bson.M{"$sum": 1}}
	group := bson.M{"_id": "$idchronology", "urlnow": bson.M{"$max": "$urlnow"}, "count": bson.M{"$sum": 1}, "IdChronology": bson.M{"$last": "$idchronology"}}
	project := bson.M{"urlnow": "$urlnow", "count": "$count", "idchronology": "$idchronology"}

	/*
		db.zalando_it.aggregate( [
		                     { $group: { _id: "$idchronology", count: { $sum: 1 }, urlnow: { $max: "$urlnow" }, idchronology: { $max: "$idchronology" } } }
		]);
				group := bson.M{"_id": "$url", "count": bson.M{"$sum": 1}, "title": bson.M{"$max": "$meta.title"}, "img": bson.M{"$max": "$meta.imgurl"}, "voto": bson.M{"$sum": "$value.voto"}, "voto_max": bson.M{"$max": "$value.voto"}, "voto_min": bson.M{"$min": "$value.voto"}}
				project := bson.M{"_id": 0, "url": "$_id", "voto_AI": bson.M{"$divide": []interface{}{"$voto", bson.M{"$multiply": []interface{}{"$count", count_AI}}}}, "title": "$title", "img": "$img", "count": "$count", "count_AI": bson.M{"$multiply": []interface{}{"$count", count_AI}}, "voto_max": "$voto_max", "voto_min": "$voto_min", "voto_totale": "$voto"}
				pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}, {"$limit": limit}}
			pipe := c.Pipe(pipeline)
			result := []bson.M{}
			err := pipe.All(&result)
	*/

	// fmt.Println("__punteggio__", punteggio)

	if punteggio > 0 {

		// fmt.Println("query", query)

		// pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}, {"$limit": limit}}
		pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}}
		// result := []bson.M{}
		pipe := c.Pipe(pipeline)
		err := pipe.All(&consigliatiTC)
		// err := c.Find(query).Limit(limit).Sort("-value.voto").All(&trackChronology)
		if err != nil {
			fmt.Printf("non trovato find fail %v\n", err)
		}

		// fmt.Println("___________Consigliati_________________________________________________________")
		// fmt.Println( len(consigliatiTC) )

		if len(consigliatiTC) > 0 {

			for _, element := range consigliatiTC {
				// index is the index where we are
				// element is the element from someSlice for where we are
				// DAti della pagina trovarta
				// pages.go
				pages := GetPage(w, r, element.UrlNow)

				// fmt.Println("___________Consigliati______Pagine____________________________________________________")
				// fmt.Println(pages)

				// if insertNew == true {
				// fmt.Println("___________Consigliati______insertNew____________________________________________________")
				InsertConsiglati(r, element, pages, punteggio, i)
				// }

			}

		}

	}

	// fmt.Println("___________Consigliati______FINE____________________________________________________")

	// TODO: devo ritornare quelli della tabella di riferimento ...
	// json.NewEncoder(w).Encode(&consigliatiTC)

}

// Inserisci elementi
func InsertConsiglati(r *http.Request, element ConsigliatiTrackChronology, pages []Pages, punteggio int, i bson.ObjectId) {

	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	ip := siteParameters.ip
	useragent := siteParameters.useragent
	token := siteParameters.token
	collection_consigliati = setDB(siteParameters.site)
	user := getUser(r)

	c := session.DB(db_consigliati).C(collection_consigliati)

	/*
	fmt.Println("_____________________________________________________________________")
	fmt.Println("trackChronology : ", element.UrlNow)
	fmt.Println("trackChronology : ", element.IdChronology)
	fmt.Println("trackChronology : ", element.Count)
	fmt.Println("trackChronology : ", element.Value)
	fmt.Println("pages : ", pages[0].Meta.Title)
	fmt.Println("punteggio : ", punteggio)
	*/

	err := c.Insert(&Consigliati{Id: i, Url: element.UrlNow, Punteggio: punteggio, Count: element.Count, Value: element.Value, Pagina: pages, Ip: ip, Token:token, User: user, UserAgent: useragent, Timestamp: time.Now()})
	if err != nil {
		fmt.Println("Errore:", err)
	}

}

func GetConsigliato(w http.ResponseWriter, r *http.Request) {
	GetConsigliatoCommon(w, r, 1)
}

func GetConsigliatoCommon(w http.ResponseWriter, r *http.Request, limit int) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	idBefore := r.PostFormValue("idBefore")
	// idBefore := ReadCookieServer(w, r, "idBefore")
	result := []Consigliati{}

	if idBefore != "" {

		siteParameters := getSiteParameters(r)
		// ip := siteParameters.ip
		// useragent := siteParameters.useragent
		token := siteParameters.token
		collection_consigliati = setDB(siteParameters.site)
		c := session.DB(db_consigliati).C(collection_consigliati)

		recordId := castToBsonID(idBefore)

		// query := bson.M{"_id": recordId, "ip": ip, "useragent": useragent}
		query := bson.M{"_id": recordId, "token": token}

		err := c.Find(query).Limit(limit).Sort("-timestamp").All(&result)
		if err != nil {
			fmt.Println("Errore:", err)
		}
	}

	json.NewEncoder(w).Encode(&result)

}

func GetConsigliati(w http.ResponseWriter, r *http.Request) {

	GetConsigliatiCommon(w, r, 10)

}

func GetConsigliatiCommon(w http.ResponseWriter, r *http.Request, limit int) { /* []Consigliati */

	fmt.Println("___________GetConsigliatiCommon__________________________________________________________")
	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	result := []ConsigliatiAll{}

	siteParameters := getSiteParameters(r)
	// ip := siteParameters.ip
	// useragent := siteParameters.useragent
	token := siteParameters.token
	sito := getSite(r)
	if sito != "" {
		collection_consigliati = setDB(sito)
	} else {
		collection_consigliati = GetSiteCollection(w, r)
	}

	// collection_consigliati = GetSiteCollection(w, r)
	c := session.DB(db_consigliati).C(collection_consigliati)

	fmt.Println("___db_consigliati__ ", db_consigliati)
	fmt.Println("___collection_consigliati__ ", collection_consigliati)

	group := bson.M{"_id": "$url", "url": bson.M{"$max": "$url"}, "pagina": bson.M{"$max": "$pagina"}, "imgurl": bson.M{"$max": "$pagina.meta.imgurl"}, "count": bson.M{"$sum": 1}, "somma": bson.M{"$sum": "$punteggio"}}
	project := bson.M{"Id": "$url", "url": "$url", "pagina": "$pagina", "imgurl": "$imgurl", "count": "$count", "somma": "$somma"}
	// query := bson.M{"ip": ip, "useragent": useragent}
	query := bson.M{"token": token}


	/*
				db.zalando_it.aggregate( [
					{
		                            $group: {
		                                _id: "$url", count: { $sum: 1 }, urlnow: { $max: "$url" }, somma: { $sum: "$punteggio" }
		                            , pagina: { $min: "$pagina.meta.imgurl" }
		                            , pagina2: { $min: "$pagina" }
		                            }
		                        }
				]);
				group := bson.M{"_id": "$url", "count": bson.M{"$sum": 1}, "title": bson.M{"$max": "$meta.title"}, "img": bson.M{"$max": "$meta.imgurl"}, "voto": bson.M{"$sum": "$value.voto"}, "voto_max": bson.M{"$max": "$value.voto"}, "voto_min": bson.M{"$min": "$value.voto"}}
						project := bson.M{"_id": 0, "url": "$_id", "voto_AI": bson.M{"$divide": []interface{}{"$voto", bson.M{"$multiply": []interface{}{"$count", count_AI}}}}, "title": "$title", "img": "$img", "count": "$count", "count_AI": bson.M{"$multiply": []interface{}{"$count", count_AI}}, "voto_max": "$voto_max", "voto_min": "$voto_min", "voto_totale": "$voto"}
						pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}, {"$limit": limit}}
					pipe := c.Pipe(pipeline)
					result := []bson.M{}
					err := pipe.All(&result)
	*/

	// pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}}
	pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"somma": -1}}}
	pipe := c.Pipe(pipeline)
	err := pipe.All(&result)
	if err != nil {
		fmt.Println("Errore:", err)
	}
	// fmt.Println(&result)
	// fmt.Println("___________GetConsigliatiCommon______FINE____________________________________________________")

	json.NewEncoder(w).Encode(&result)

}
