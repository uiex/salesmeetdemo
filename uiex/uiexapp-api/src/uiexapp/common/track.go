// Tiene traccia dei click efettuati sui link proposti da UIEX
// Viene richiamanto se nell'url abbiamo la presenza della variabile "uiextrack"
package common

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var var_uiextrack = "uiextrack"

func getNameVArTrack() string {
	return var_uiextrack
}

// Inserisci elementi
func InsertTrack(w http.ResponseWriter, r *http.Request, sParameters siteParameters, idBefore string, IdNow string) {

	if sParameters.uiextrack != "" {

		setAccessControl(w)
		getSessionDB() // session.SetMode(mgo.Monotonic, true)

		siteParameters := getSiteParameters(r)
		ip := siteParameters.ip
		referer := siteParameters.referer
		useragent := siteParameters.useragent
		token := siteParameters.token 
		uiextrack := siteParameters.uiextrack
		site := setDB(siteParameters.site)
		user := getUser(r) 
		c := session.DB(db_track).C(collection_track)
		err := c.Insert(&Track{IdBefore: idBefore, IdNow: IdNow, Site:site, Tipo: uiextrack, Url: referer, Ip: ip, User: user, UserAgent: useragent, Token: token, Timestamp: time.Now()})
		if err != nil {
			fmt.Println("Errore:", err)
		}

	}

}

//
func isTrackUrl(referer string, origin string) (string, string) {

	// pulisco il track dell'applicazione ...
	uiextrack := ""
	i := strings.Index(referer, var_uiextrack+"=")
	if i > -1 {
		v, err := url.Parse(referer)
		if err != nil {
			// panic(err)
		}
		q := v.Query()
		uiextrack = q.Get(var_uiextrack)
	}
	referer = strings.Replace(origin, "&"+var_uiextrack+"="+uiextrack, "", -1)
	referer = strings.Replace(origin, "?"+var_uiextrack+"="+uiextrack, "", -1)

	return referer, uiextrack

}