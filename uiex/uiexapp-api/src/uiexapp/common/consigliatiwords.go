// Link consigliato in base alla navigazione deglinaltri clienti.
package common

import (
	"fmt"
	"net/http"
	"time"
	"encoding/json"
	"strings"

	"gopkg.in/mgo.v2/bson" 
)

// Inserisci elementi 
// func InsertConsiglatiWords(i bson.ObjectId, titleConsigliato string, ex_words string, collection string, theMetatag Metatag, thePrice Prezzo, theUrl string) {
func InsertConsiglatiWords(i bson.ObjectId, ex_words string, collection string, theMetatag Metatag, thePrice Prezzo, theUrl string, ageGender string) {

	getSessionDB() // session.SetMode(mgo.Monotonic, true)
	c := session.DB(db_consigliati_words).C(collection_consigliati_words)
	// err := c.Insert(&WordsAll{Id: i, Title: titleConsigliato, Words: ex_words, CollectionPages: collection, Meta :theMetatag , Price :thePrice, Url: theUrl, Timestamp: time.Now()})
	err := c.Insert(&WordsAll{Id: i, Words: ex_words, CollectionPages: collection, Meta :theMetatag , Price :thePrice, Url: theUrl, Timestamp: time.Now(), AgeGender: ageGender})
	if err != nil {
		fmt.Println("Errore:", err)
	}

}

func GetConsiglatiWords(w http.ResponseWriter, r *http.Request) {
	GetConsiglatiWordsCommon(w, r, 10)
}
 

func GetConsiglatiWordsCommon(w http.ResponseWriter, r *http.Request, limit int) { 

	// fmt.Println("___________GetConsiglatiWordsCommon__________________________________________________________")
	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)
	c := session.DB(db_consigliati_words).C(collection_consigliati_words)

	collectionPages := GetSiteCollection(w, r)
	
	Nfind := 4
	words := getWords(r)	
	s := strings.Split(words, ",")
	s = cleanArray(s)

	ageGender := ""
	if getTipo(r) == "product" {
		ageGender = getAgeGender(r)
	}
	resultTot := []WordsAll{}
	codeProducts := make([]bson.ObjectId, 11) // [10]bson.ObjectId{}
	idCodeProducts := 0
	nProducts := 0
	count := 0

	/* DA riprisitnare
	result4 := []WordsAll{}
	orQuery := GetQueryWords(s,"","words","",4,collectionPages,codeProducts,0,ageGender,0)
	fmt.Println("orQuery:", orQuery)
	err := c.Find(   bson.M{"$and":orQuery}   ).Limit(limit).Sort("-timestamp").All(&result4);
	
	if err != nil {
		fmt.Println("Errore:", err)
	}
	fmt.Println("result:", len(result4))
	resultTot = append(resultTot, result4...)
	*/
	
	result3 := []WordsAll{}
	//if len(result4) < 11 {		
	orQuery, count  := GetQueryWords(s,"","words","",3,collectionPages,codeProducts,0,ageGender,0)
	// fmt.Println("orQuery:", orQuery)
	err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
	if err != nil {
		fmt.Println("Errore:", err)
	} 
	Nfind = len(resultTot)	
	resultTot = append(resultTot, result3...)
	nProducts = len(result3)
	if nProducts > 0 {
		resultTot[Nfind].NFind = 3
		// codici prodotti già esistenti
		for i := 0; i < nProducts; i++ {
			codeProducts[idCodeProducts] = result3[i].Id
			idCodeProducts = idCodeProducts + 1
		}
	}

	result3 = []WordsAll{}
	if len(resultTot) < 10 {	
		orQuery, _  := GetQueryWords(s,"","words","",3,collectionPages,codeProducts,0,ageGender,1)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
		if err != nil {
			fmt.Println("Errore:", err)
		} 
		Nfind = len(resultTot)	
		resultTot = append(resultTot, result3...)
		nProducts = len(result3)
		if nProducts > 0 {
			resultTot[Nfind].NFind = 3
			// codici prodotti già esistenti
			for i := 0; i < nProducts; i++ {
				codeProducts[idCodeProducts] = result3[i].Id
				idCodeProducts = idCodeProducts + 1
			}
		}
	}
	
	result2 := []WordsAll{}
	if len(resultTot) < 10 {		
		orQuery, count  = GetQueryWords(s,"","words","",2,collectionPages,codeProducts,0,ageGender,0)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		}
		Nfind = len(resultTot)			
		resultTot = append(resultTot, result2...)  	
		nProducts = len(result2)
		if nProducts > 0 {
			resultTot[Nfind].NFind = 2
			// codici prodotti già esistenti
			for i := 0; i < nProducts; i++ {
				codeProducts[idCodeProducts] = result2[i].Id
				idCodeProducts = idCodeProducts + 1
			}
		}	
	}

	// parto dal secondo valore spostato di 1
	result2 = []WordsAll{}
	if len(resultTot) < 10 {		
		orQuery, count  = GetQueryWords(s,"","words","",2,collectionPages,codeProducts,0,ageGender,1)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		} 
		Nfind = len(resultTot)			
		resultTot = append(resultTot, result2...)  	
		nProducts = len(result2)
		if nProducts > 0 {
			resultTot[Nfind].NFind = 2
			// codici prodotti già esistenti
			for i := 0; i < nProducts; i++ {
				codeProducts[idCodeProducts] = result2[i].Id
				idCodeProducts = idCodeProducts + 1
			}
		}	
	}	

	/*
	result1 := []WordsAll{}
	if len(resultTot) <= 10 {		
		orQuery, count  = GetQueryWords(s,"","words","",1,collectionPages,codeProducts,0,ageGender,0)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result1);
		if err != nil {
			fmt.Println("Errore:", err)
		} 
		if len(result1) == 0 {	
			orQuery, count  = GetQueryWords(s,"","words","",0,collectionPages,codeProducts,count + 2,ageGender,0)
			fmt.Println("orQuery:", orQuery)
			err2 := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result1);	
			if err2 != nil {
				fmt.Println("Errore:", err)
			} 
		} 
		fmt.Println("count:", count)
		Nfind = len(resultTot)
		resultTot = append(resultTot, result1...)
		if len(result1) > 0 {
			resultTot[Nfind].NFind = 1
		}	

	}
	*/
	fmt.Println("count:", count)
	json.NewEncoder(w).Encode(&resultTot)
	
}

