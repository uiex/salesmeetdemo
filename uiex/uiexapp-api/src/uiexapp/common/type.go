package common

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Gestione configurazione DB
type (
	configuration struct {
		Server, MongoDBHost, DBUser, DBPwd, Database string
	}
)

type (
	ChronologyInsert struct {
		Id   bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Tags []Tag
	}
)

type (
	ChronologyShort struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Url       string
		Timestamp time.Time
		Meta      Metatag
		Value     Valutazione
		Price     Prezzo
	}
)

type (
	Chronology struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Url       string
		Ip        string
		Language  string
		UserAgent string
		Token     string
		User      string
		Timestamp time.Time
		Meta      Metatag
		Price     Prezzo
		Value     Valutazione
		Tempo     TempoChronology
	}
)

type (
	ChronologyStatistics struct {
		Id     string
		Url    string
		Imgurl string
		Count  int
		Votomedio  int
		Votomax  int
		Votomin  int
		Title string
		Description string
	}
)

type (
	CustomerExperience struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Timestamp time.Time
		UserAgent string // completo così da avere cosa usa il cliente da analizzare
		Token     string
		Type      string // tipo pagina (prodotto, news ...)
		Device    string // tipo device (smartphone, desktop ...)
		Time      Tempo
		Value     Valutazione
		Scroll    Scroll
		Oggetti   Oggetti
	}
)

type (
	Last struct {
		Url          string
		Ip           string
		UserAgent    string
		Token     string
		User         string
		Site         string 
		IdChronology bson.ObjectId `json:"id" bson:"_id,omitempty"` // Chronology._Id
		Timestamp    time.Time
		Meta         Metatag
		Value        Valutazione
		UrlBeforeUno string // mi serve per utilizzarlo in TrackChronology
		UrlBeforeDue string // mi serve per utilizzarlo in TrackChronology
	}
)

type (
	Track struct {
		IdBefore  string // id pagina precedente
		IdNow     string // id pagina corrente
		Tipo      string // tipologia di chiamante interno (es. 0 last, 1 consigliati ...)
		Url       string
		UserAgent string
		Token	  string
		User      string
		Ip        string
		Site        string
		Timestamp time.Time
	}
)

type (
	TrackChronology struct {
		UrlNow       string
		UrlBeforeUno string
		UrlBeforeDue string
		UrlBeforeTre string
		Value        Valutazione
		IdChronology bson.ObjectId
		UserAgent    string
		Token     string
		User         string
		Ip           string
		Timestamp    time.Time
	}
)

type (
	ConsigliatiTrackChronology struct {
		IdChronology bson.ObjectId
		UrlNow       string
		Count        int
		Value        Valutazione
		Meta         Metatag
	}
)

type (
	Consigliati struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Url       string
		UserAgent string
		Token     string
		Ip        string
		User      string
		Timestamp time.Time
		Count     int
		Value     Valutazione
		Punteggio int
		Pagina    []Pages
	}
)

type (
	ConsigliatiAll struct {
		Id     string
		Url    string
		Imgurl []string
		Pagina []Pages
		Count  int
		Somma  int
	}
)

type (
	LastId struct {
		Id           bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Url          string
		UserAgent    string
		Token     string
		Ip           string
		User         string
		Timestamp    time.Time
		Meta         Metatag
		UrlBeforeUno string // mi serve per utilizzarlo in TrackChronology
		UrlBeforeDue string // mi serve per utilizzarlo in TrackChronology
	}
)

type (
	Pages struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Url       string
		Language  string
		Timestamp time.Time
		Price     Prezzo
		Meta      Metatag
		Statistic Statistics
		Words Words
		AgeGender string
	}
)
type (
	PagesUrl struct {
		Url       string
	}
)

type (
	AddonList struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Site         string
		Nome         string
		Logo         string
		Attivo       int
		Timestamp    time.Time
	}
)

type (
	AddonUser struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		UserAgent    string
		Token     string
		Ip           string
		User         string
		Site         string
		Attivo       int
		Timestamp    time.Time
	}
)

type (
	AddonApp struct {
		Id        bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		Site         string
		Attivo       int
		Timestamp    time.Time
	}
)

/****** dataTables *****/
type (
	DataTables struct {
		data string
	}
)

/****** sotto documenti *****/

type (
	Metatag struct {
		Title       string
		Description string
		SchemaType  string // tipo pagina (prodotto, news ...)
		ImgUrl      string
		Device      string // tipo device (smartphone, desktop ...)
	}
)

type (
	Words struct {
		Title       string
		Words 		string
	}
) 

type (
	WordsAll struct {
		Id          bson.ObjectId `json:"id"        bson:"_id,omitempty"`
		// Title       string
		Words 		string
		CollectionPages 		string
		Price     Prezzo
		Meta      Metatag
		Url       string
		AgeGender string
		NFind 		int // usato per passare il numero di parole trovate
		Timestamp    time.Time
	}
)

type (
	Prezzo struct {
		Price    string
		Currency string
	}
)

type (
	Tempo struct {
		Reale           string // Millesecondi reali in cui l'utente è stato dentro nella pagina
		TotaleMovimento string // Millesecondi in cui l'utente si muove nella pagina
		StartPage       string // Millesecondi in cui l'utente è entrato nella pagina
		InOut           string // Quante volte il cliente esce e rientra nel documento
		NumInOut        int    // numero di uscita e ritorna nella pagina
	}
)

type (
	TempoChronology struct {
		Reale           string // Millesecondi reali in cui l'utente è stato dentro nella pagina
		TotaleMovimento string // Millesecondi in cui l'utente sta nella pagina
		StartPage       string // Millesecondi in cui l'utente è entrato nella pagina
	}
)

type (
	Valutazione struct {
		Voto       int
		Operazione int
	}
)

type (
	Scroll struct {
		X         string // se il cliente ha scrollato la pagina in alto e basso
		Y         string // se il cliente ha scrollato la pagina sx a dx
		NumScroll int    // numero di movimenti nellapagina
	}
)

type (
	Oggetti struct {
		Elenco    string // elenco degli oggetti sulla quale è passato il mouse solo quelli importanti
		Contatore string // numero degli oggetti sulla quale è passato il mouse
		Click     string // coordinate del click
		NumClick  int	 // numero dei click effetuati
		NumElenco int 	 // Numero elementi importanti - vedi valore Elenco
	}
)

type (
	Tag struct {
		TagName   string // A o H1 o H2 ...
		Attributi string // href, innerHtml, value ...
	}
)

type (
	Statistics struct {
		Count   int
		MaxVoto int
		MinVoto int
		MediaVoto int
		Visit time.Time // visita attuale
		LastVisit time.Time // penultima visita
	}
)


