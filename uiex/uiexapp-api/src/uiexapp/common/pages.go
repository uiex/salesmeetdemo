// Elenco delle pagine.
package common

import (
	"fmt"
	"net/http"
	"time"
	"encoding/json"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

// Inseriso la pagina nell'elengo delle pagine per il sito.
// Ogni pagina risulterà unica per URL. Verranno aggiornati solo i dati.
func InsertPage(w http.ResponseWriter, r *http.Request, voto int) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// url := siteParameters.referer
	url := getCurrentUrl(r)
	collection_pages = setDB(siteParameters.site)
	c := session.DB(db_pages).C(collection_pages)

	// fmt.Println(url)

	// TODO - Query sotto da perfezionare e fare una unica operazione
	pages := []Pages{}
	query := bson.M{"url": url}
	err := c.Find(query).All(&pages)

	if err == nil {
		fmt.Printf("non trovato find fail %v\n", err)
	}

	languagePage := getLanguagePage(r)
	title := getTitle(r)
	description := getDescription(r)
	img := getImg(r)
	tipo := getTipo(r)
	// titleConsigliato := getTitleConsigliato(r)
	words := getWords(r)
	ageGender := getAgeGender(r)


	theMetatag := Metatag{}
	theMetatag.Title = title
	theMetatag.Description = description
	theMetatag.ImgUrl = img
	theMetatag.SchemaType = tipo
	thePrice := Prezzo{}
	thePrice.Price = getPrice(r)
	thePrice.Currency = getCurrency(r)
	theWords := Words{}
	// theWords.Title = titleConsigliato
	theWords.Words = words

	if len(pages) == 0 {

		// fmt.Println("Insert new Page **************** ")
		i := bson.NewObjectId()
		theStatistics := Statistics{}
		theStatistics.Count = 1
		theStatistics.MaxVoto = 0
		theStatistics.MinVoto = 0
		theStatistics.MediaVoto = 0
		theStatistics.Visit = time.Now()
		theStatistics.LastVisit = time.Now()
		errInsert := c.Insert(&Pages{Id: i,Url: url, Language: languagePage, Meta: theMetatag, Price: thePrice, Timestamp: time.Now(), Statistic: theStatistics, Words: theWords, AgeGender: ageGender})
		if errInsert != nil {
			fmt.Println("Errore:", errInsert)
		}

		// InsertConsiglatiWords(i, titleConsigliato, words, collection_pages, theMetatag, thePrice, url)
		InsertConsiglatiWords(i, words, collection_pages, theMetatag, thePrice, url, ageGender)


	} else {

		// fmt.Println("Update POST new Page **************** ")

		theStatistics := Statistics{}
		theStatistics.Count = pages[0].Statistic.Count + 1
		/*
		theStatistics.MinVoto = pages[0].Statistic.MinVoto
		theStatistics.MaxVoto = pages[0].Statistic.MaxVoto
		theStatistics.MediaVoto = pages[0].Statistic.MediaVoto
		*/
		theStatistics.Visit = pages[0].Statistic.Visit
		theStatistics.LastVisit = pages[0].Statistic.LastVisit

		if (pages[0].Statistic.MinVoto < voto ) {
			if (pages[0].Statistic.MinVoto == 0 ) {
				theStatistics.MinVoto = voto
			} else {
				theStatistics.MinVoto = pages[0].Statistic.MinVoto
			}
		} else {
			theStatistics.MinVoto = voto
		}
		if (pages[0].Statistic.MaxVoto > voto ) {
			theStatistics.MaxVoto = pages[0].Statistic.MaxVoto
		} else {
			theStatistics.MaxVoto = voto
		}
		// fmt.Println(pages[0].Statistic.MediaVoto)
		theStatistics.MediaVoto = pages[0].Statistic.MediaVoto + voto

		// fmt.Println(pages[0].Statistic.MediaVoto)
		// fmt.Println(theStatistics.MediaVoto)
		// fmt.Println(theStatistics.MaxVoto)
		// fmt.Println(theStatistics.MinVoto)

		errUpdate := c.Update(bson.M{"_id": pages[0].Id}, bson.M{"$set": bson.M{"price": thePrice, "language": languagePage, "meta": theMetatag, "statistic": theStatistics, "words": theWords,  "timestamp": time.Now()}})
		if errUpdate != nil {
			fmt.Printf("update fail %v\n", errUpdate)
		}

	}

}

func GetPageNotUrl(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	// siteParameters := getSiteParameters(r)
	// getReferer("referer")
	// pages := GetPage(w, r, siteParameters.referer)
	pages := GetPage(w, r, getReferer(r))
	json.NewEncoder(w).Encode(pages)
}

func GetPage(w http.ResponseWriter, r *http.Request, url string) []Pages {

	collection_pages := GetSiteCollection(w, r)
	c := session.DB(db_pages).C(collection_pages)
	/*
	fmt.Println("GetPage ************************************************************************************************ ")
	// fmt.Println("db_chronology **************** ")
	fmt.Println(db_pages)
	fmt.Println(collection_pages)
	fmt.Println(url)
	*/

	// siteParameters := getSiteParameters(r)
	// url := siteParameters.referer
	result := []Pages{}
	query := bson.M{"url": url}
	err := c.Find(query).All(&result)

	if err != nil {
		fmt.Println("Errore:", err)
	}

	return result

}


func GetAllPages(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	collection_pages := GetSiteCollection(w, r)
	/*
	fmt.Println("GetAllPages ************************************************************************************************ ")
	// fmt.Println("db_chronology **************** ")
	fmt.Println(db_pages)
	fmt.Println(collection_pages)
	*/

	c := session.DB(db_pages).C(collection_pages)
	result := []PagesUrl{}
	query := bson.M{"url" : bson.M{ "$ne" : "a" }}
	err := c.Find(query).All(&result)
	if err != nil {
		fmt.Println("Errore:", err)
	}
	json.NewEncoder(w).Encode(&result)

}


func GetPagesConsigliateWords(w http.ResponseWriter, r *http.Request) {

	GetPagesConsigliateWordsCommon(w, r, 10)
}

func GetPagesConsigliateWordsCommon(w http.ResponseWriter, r *http.Request, limit int) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	collection_pages := GetSiteCollection(w, r)
	c := session.DB(db_pages).C(collection_pages)

	/*
  fmt.Println("GetPagesConsigliateWordsCommon **************** ")
	fmt.Println(collection_pages)
	fmt.Println(db_pages)
	*/

	Nfind := 4
	words := getWords(r)
	fmt.Println("words **************** ")
	fmt.Println(words)
	s := strings.Split(words, ",")
	s = cleanArray(s)

	ageGender := ""
	// fmt.Println("getTipo(r):", getTipo(r))
	// fmt.Println("getAgeGender(r):", getAgeGender(r))
	if getTipo(r) == "product" {
		ageGender = getAgeGender(r)
	}
	resultTot := []WordsAll{}
	codeProducts := make([]bson.ObjectId, 11) // [10]bson.ObjectId{}
	idCodeProducts := 0
	// count := 0

	// siteParameters := getSiteParameters(r)
	// url := siteParameters.referer
	url := getSite(r)


	/* DA riprisitnare
	result4 := []WordsAll{}
	orQuery := GetQueryWords(s,url,"words.words","url",4,"",codeProducts,0,ageGender,0)
	fmt.Println("orQuery:", orQuery)
	err := c.Find(   bson.M{"$and":orQuery}   ).Limit(limit).Sort("-timestamp").All(&result4);
	if err != nil {
		fmt.Println("Errore:", err)
	}
	fmt.Println("result:", len(result4))
	resultTot = append(resultTot, result4...)
	*/

	result3 := []WordsAll{}
	//if len(result4) < 11 {
	orQuery, _ := GetQueryWords(s,url,"words.words","url",3,"",codeProducts,0,ageGender,0)
	// fmt.Println("orQuery:", orQuery)
	err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
	if err != nil {
		fmt.Println("Errore:", err)
	} else {
		Nfind = len(resultTot)
		resultTot = append(resultTot, result3...)
		if len(result3) > 0 {
			resultTot[Nfind].NFind = 3
			// codici prodotti già esistenti
			for i := 0; i < len(result3); i++ {
				codeProducts[idCodeProducts] = result3[i].Id
				idCodeProducts = idCodeProducts + 1
			}
		}
	}
	//}

	result3 = []WordsAll{}
	if len(resultTot) < 10 {
		orQuery, _ := GetQueryWords(s,url,"words.words","url",3,"",codeProducts,0,ageGender,1)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result3);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)
			resultTot = append(resultTot, result3...)
			if len(result3) > 0 {
				resultTot[Nfind].NFind = 3
				// codici prodotti già esistenti
				for i := 0; i < len(result3); i++ {
					codeProducts[idCodeProducts] = result3[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}
		}
	}

	result2 := []WordsAll{}
	if len(resultTot) < 10 {
		orQuery, _  = GetQueryWords(s,url,"words.words","url",2,"",codeProducts,0,ageGender,0)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)
			resultTot = append(resultTot, result2...)
			if len(result2) > 0 {
				resultTot[Nfind].NFind = 2
				// codici prodotti già esistenti
				for i := 0; i < len(result2); i++ {
					// codeProducts[idCodeProducts] = result2[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}
		}
	}

	result2 = []WordsAll{}
	if len(resultTot) < 10 {
		orQuery, _  = GetQueryWords(s,url,"words.words","url",2,"",codeProducts,0,ageGender,1)
		// fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result2);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)
			resultTot = append(resultTot, result2...)
			if len(result2) > 0 {
				resultTot[Nfind].NFind = 2
				// codici prodotti già esistenti
				for i := 0; i < len(result2); i++ {
					// codeProducts[idCodeProducts] = result2[i].Id
					idCodeProducts = idCodeProducts + 1
				}
			}
		}
	}

	/*
	result1 := []WordsAll{}
	if len(resultTot) <= 10 {
		orQuery, _  = GetQueryWords(s,url,"words.words","url",1,"",codeProducts,0,ageGender,0)
		fmt.Println("orQuery:", orQuery)
		err := c.Find(bson.M{"$and":orQuery}).Limit(limit - idCodeProducts).Sort("-timestamp").All(&result1);
		if err != nil {
			fmt.Println("Errore:", err)
		} else {
			Nfind = len(resultTot)
			resultTot = append(resultTot, result1...)
			if len(result1) > 0 {
				resultTot[Nfind].NFind = 1
			}
		}
	}
	*/

	json.NewEncoder(w).Encode(&resultTot)

}



// NON UTILIZZATI LATO FRONT-END - DA CANCELLARE?
func GetPagesConsigliateTitle(w http.ResponseWriter, r *http.Request) {
	GetPagesConsigliateTitleCommon(w, r, 10)
}

// NON UTILIZZATI LATO FRONT-END - DA CANCELLARE?
func GetPagesConsigliateTitleCommon(w http.ResponseWriter, r *http.Request, limit int) {

	// fmt.Println("___________GetPagesConsigliateTitleCommon__________________________________________________________")
	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	collection_pages := GetSiteCollection(w, r)
	c := session.DB(db_pages).C(collection_pages)

	siteParameters := getSiteParameters(r)
	url := siteParameters.referer
	titleConsigliato := getTitleConsigliato(r)
	query := bson.M{"words.title": titleConsigliato, "url" : bson.M{ "$ne" : url } }
	result := []Pages{}
	err := c.Find(query).All(&result)

	if err != nil {
		fmt.Println("Errore:", err)
	}
	// fmt.Println(&result)
	json.NewEncoder(w).Encode(&result)

}
