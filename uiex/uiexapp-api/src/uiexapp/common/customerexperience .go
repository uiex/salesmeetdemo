// Gestione dell'esperienza utente sulla pagina.
package common

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

// Inserisci elementi
func InsertCustomerExperience(w http.ResponseWriter, r *http.Request, idBefore bson.ObjectId) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)
	siteParameters := getSiteParameters(r)
	useragentcompleto := siteParameters.useragentcompleto

	collection_customer_experience = GetSiteCollection(w, r)
	c := session.DB(db_customer_experience).C(collection_customer_experience)
	errInsert := c.Insert(&CustomerExperience{Id: idBefore, UserAgent: useragentcompleto})
	if errInsert != nil {
		fmt.Println("Errore:", errInsert)
	}

}

// Update elementi
func UpdateCustomerExperience(w http.ResponseWriter, r *http.Request, idBefore string) int {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)
	voto := 0
	if idBefore != "" {

		tempoInOut := r.PostFormValue("tempoInOut")         // Quante volte il cliente esce e rientra nel documento
		tempoMovimento := r.PostFormValue("tempoMovimento") // Millesecondi in cui l'utente si muove nelle pagina
		startTime := r.PostFormValue("startTime")           // Timestamp del caricamento della pagina

		theTempo := Tempo{}
		theTempo.InOut = tempoInOut               // Quante volte il cliente esce e rientra nel documento
		theTempo.TotaleMovimento = tempoMovimento // Millesecondi in cui l'utente si muove nelle pagina
		theTempo.StartPage = startTime

		scrollX := r.PostFormValue("scrollX")                   // se il cliente ha scrollato la pagina
		scrollY := r.PostFormValue("scrollY")                   // se il cliente ha scrollato la pagina in alto e basso
		overOggetti := r.PostFormValue("overOggetti")           // oggetti visitati dal cliente con eventuale ritorno sullo stesso.
		contatoreOggetti := r.PostFormValue("contatoreOggetti") // quanto si è mosso il cliente
		clickOggetti := r.PostFormValue("contatoreClick")       // quanto si è mosso il cliente

		oggetti := Oggetti{}
		oggetti.Contatore = contatoreOggetti // Over quanti oggetti importatni
		oggetti.Elenco = overOggetti         // Elenco oggetti
		oggetti.Click = clickOggetti         // Click sulla pagina

		scroll := Scroll{}
		scroll.X = scrollX // Quante volte il cliente esce e rientra nel documento
		scroll.Y = scrollY // Millesecondi in cui l'utente sta nella pagina

		siteParameters := getSiteParameters(r)
		device := siteParameters.useragentcompleto
		tipo := getTipo(r)

		collection_customer_experience = GetSiteCollection(w, r)

		recordId := castToBsonID(idBefore)
		c := session.DB(db_customer_experience).C(collection_customer_experience)
		err := c.UpdateId(recordId, bson.M{"$set": bson.M{"device": device, "type": tipo, "time": theTempo, "oggetti": oggetti, "scroll": scroll}})
		if err != nil {
			fmt.Printf("find fail %v\n", err)
		}

		// CalcolateExperience.go
		// calcolo l'esperienza utente
		voto = CalcolateExperience(w, r, theTempo, oggetti, scroll, device, tipo, recordId)
	}
	return voto

}

// Call to main.go
func GetCustomerExperienceById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	GetCustomerExperienceCommon(w, r, 1, id)
}

/*
func GetCustomerExperience(w http.ResponseWriter, r *http.Request) {
	GetCustomerExperienceCommon(w, r, 1000, "")
}
*/
func GetCustomerExperienceCommon(w http.ResponseWriter, r *http.Request, limit int, id string) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	collection_customer_experience = GetSiteCollection(w, r)
	// fmt.Println(collection_customer_experience)

	c := session.DB(db_customer_experience).C(collection_customer_experience)

	result := []CustomerExperience{}
	query := bson.M{}
	if id != "" {
		// fmt.Println(id)
		recordId := castToBsonID(id)
		query = bson.M{"_id": recordId}
	}
	// err := c.Find(query).Limit(limit).Sort("-timestamp").All(&result) // Sort(-) = desc
	err := c.Find(query).All(&result)
	if err != nil {
		fmt.Println("Errore:", err)
	}
	json.NewEncoder(w).Encode(&result)

}
