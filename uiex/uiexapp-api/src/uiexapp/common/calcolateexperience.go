// Gestione dell'esperienza utente sulla pagina.
package common

// "fmt"
// "github.com/avct/uasurfer"
import (
	"net/http"
	"strconv"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

// Chiamato da CustomerExperience.go
// TODO: DA portare fuori ...  Mettere in un thread
// Sin potrebbero prenbdere i dati direttamente dal DB tramite bson.ObjectId
func CalcolateExperience(w http.ResponseWriter, r *http.Request, theTempo Tempo, oggetti Oggetti, scroll Scroll, device string, tipo string, id bson.ObjectId) int  {

	// Algoritmo di calcolo
	moltiplicatoreInOut := 1 // 10
	moltiplicatoreScroll := 1 // 50
	moltiplicatoreOggetti := 1 // 30
	moltiplicatoreClick := 1 // 80
	// moltiplicatoreNumElenco := 20
	divisoreTempoReale := 100

	divisore := 10

	// TODO: da perfezionare anche il device ...
	// questo potrebbe cambiare il valore dei moltiplicatori.
	// - Probabilmente su mobile si scrolla di più.
	getDevice(device)

	// Funziona
	theTempo = CalcolateExperienceTempo(theTempo)

	theTempoChronology := TempoChronology{}
	theTempoChronology.Reale = theTempo.Reale                     // Quante volte il cliente esce e rientra nel documento
	theTempoChronology.TotaleMovimento = theTempo.TotaleMovimento // Millesecondi in cui l'utente sta nella pagina
	theTempoChronology.StartPage = theTempo.StartPage

	// calcolo il tempo Totale in cul è stato all'interno della pagina...
	tempoReale, _ := strconv.Atoi(theTempoChronology.Reale)
	votoTempo := (tempoReale / divisoreTempoReale) + (theTempo.NumInOut * moltiplicatoreInOut)

	// calocolo il tempo di scrolling della pagina
	scroll = CalcolateExperienceScroll(scroll)
	votoScroll := scroll.NumScroll * moltiplicatoreScroll

	// oggetti = Oggetti{}
	oggetti = CalcolateExperienceOggetti(oggetti)
	oggetti = CalcolateClick(oggetti)

	// Quanto si è mosso il cliente? Sinonimo di interesse.
	contatore, _ := strconv.Atoi(oggetti.Contatore) // quanto si è mosso il cliente ...
	votoOggetti := contatore * moltiplicatoreOggetti

	// Quanti elementi importanti è passato sopra il visitatore
	// votoNumElenco := oggetti.NumElenco * moltiplicatoreNumElenco

	// Quanti click ha fatto il visitatore
	votoClick := oggetti.NumClick * moltiplicatoreClick
	if (votoTempo==0) { votoTempo = 1 }
	if (votoOggetti==0) { votoOggetti = 1 }
	if (votoScroll==0) { votoScroll = 1 }
	if (votoClick==0) { votoClick = 1 }

	// Tempo + Quanto scrollo + Che oggetti vedo e quanti click faccio.
	voto := (votoTempo * votoOggetti * votoScroll * votoClick) / divisore
	// voto := ( votoTempo + votoScroll + votoOggetti + votoClick ) / divisore
	operazioni := theTempo.NumInOut + scroll.NumScroll + oggetti.NumElenco + contatore + oggetti.NumClick

	//  chronology.go
	UpdateChronologyCalcolate(w, r, id, voto, operazioni, theTempoChronology)
	// last.go
	UpdateLastCalcolate(w, r, id, voto, operazioni)
	// pages.go
	// UpdatePage(w, r, voto)

	if IsExtensionBrowser(r) != "" {
		// ExtensionBrowser.go
		// UpdateExtensionBrowserCalcolate(w, r, id, strconv.Itoa(voto), strconv.Itoa(operazioni), theTempoChronology)
		UpdateExtensionBrowserCalcolate(w, r, id, voto, operazioni, theTempoChronology)
	}

	return voto

}

func getDevice(device string) {
	// https://github.com/avct/uasurfer
	// ua := uasurfer.Parse(device)
	/*
		fmt.Println("___________Device")
		fmt.Println("___________Device")
		fmt.Println("___________Device")
		fmt.Println("___________Device")
		// browserName, browserVersion, platform, osName, osVersion, deviceType,
		// https://godoc.org/github.com/avct/uasurfer#ParseUserAgent
	*/
	// fmt.Println(ua)
	/*
		fmt.Println(ua.Browser)
		fmt.Println(ua.DeviceType)
	*/
}

func CalcolateExperienceOggetti(oggetti Oggetti) Oggetti {
	// ad oggi non calcolo il valore degli elemnti trovati ma solo il numero.
	nElenco := strings.Split(oggetti.Elenco, ",")
	// divido per quattro perchè i parametri sono tag, valore, X e Y per singolo elemento
	oggetti.NumElenco = len(nElenco) / 4
	return oggetti
}

func CalcolateClick(oggetti Oggetti) Oggetti {
	nClick := strings.Split(oggetti.Click, ",")
	// divido per due perchè i parametri sono X e Y per singolo click
	oggetti.NumClick = len(nClick)	/ 2
	return oggetti
}

func CalcolateExperienceScroll(scroll Scroll) Scroll {

	scrollX := strings.Split(scroll.X, ",")
	scroll.NumScroll = len(scrollX)
	scrollY := strings.Split(scroll.Y, ",")
	scroll.NumScroll = scroll.NumScroll + len(scrollY)
	return scroll

}

// TODO: sembra vada bene ... Fare check per migliorarlo ...
func CalcolateExperienceTempo(theTempo Tempo) Tempo {

	// Algoritmo di calcolo
	//** fmt.Println("___________CalcolateExperienceTempo__________________________________________________________")

	inOut := strings.Split(theTempo.InOut, ",")
	theTempo.NumInOut = len(inOut)
	// for index, element := range inOut {
	tempoMovimento, _ := strconv.Atoi(theTempo.TotaleMovimento)
	startTime, _ := strconv.Atoi(theTempo.StartPage)
	tempoTransitorioOut := 0
	tempoTransitorioIn := 0
	for _, element := range inOut {
		//** fmt.Println("-------------------------" + element)
		if strings.Index(element, "o-") == 0 {

			temp := strings.Replace(element, "o-", "", -1)
			tempoTransitorioOut, _ = strconv.Atoi(temp)

		} else {

			temp := strings.Replace(element, "i-", "", -1)
			//** fmt.Println("-i")
			//** fmt.Println(temp)
			tempoTransitorioIn, _ = strconv.Atoi(temp)
			if tempoTransitorioOut > 0 {
				// tempoTransitorio = (tempoTransitorioIn - tempoTransitorioOut)
				//** temp2 := (tempoTransitorioIn - tempoTransitorioOut)
				//** fmt.Println("--- OUT CON IN --- ")
				//** fmt.Println(temp2)
				tempoMovimento = tempoMovimento - (tempoTransitorioIn - tempoTransitorioOut)
			} else {
				//** fmt.Println("--- IN ")
				if !strings.Contains(theTempo.InOut, "o-") {
					// temp2 := (tempoTransitorioIn - startTime)
					//** fmt.Println("--- IN 2 --- ")
					//** fmt.Println(tempoMovimento)
					//** fmt.Println(tempoTransitorioIn)
					//** fmt.Println(startTime)
					if tempoTransitorioIn == 0 {
						tempoMovimento = 0
					} else {
						tempoMovimento = tempoMovimento - (tempoTransitorioIn - startTime)
					}
				} else {
					//** temp2 := (tempoTransitorioIn - startTime)
					//** fmt.Println("--- IN 3 --- ")
					//** fmt.Println(temp2)
					tempoMovimento = tempoMovimento - (tempoTransitorioIn - startTime)
				}
			}
			tempoTransitorioOut = 0
			tempoTransitorioIn = 0
		}
		// index is the index where we are
		// element is the element from someSlice for where we are
	}

	if tempoTransitorioOut > 0 {
		//** fmt.Println("--- SOLO OUT --- ")
		//** fmt.Println(strings.Contains(theTempo.InOut, "i-"))
		if strings.Contains(theTempo.InOut, "i-") == false {
			//** fmt.Println("--- SOLO OUT dentro inizio")
			tempoMovimento = tempoTransitorioOut - startTime
			//** fmt.Println(tempoMovimento)
			//** fmt.Println("--- SOLO OUT dentro fine")
		}
	}

	theTempo.Reale = strconv.Itoa(tempoMovimento)

	//** fmt.Println("___________CalcolateExperienceTempo____out______________________________________________________")
	return theTempo

}
