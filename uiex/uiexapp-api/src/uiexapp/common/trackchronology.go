// Gestione esperienza navigazione utente.
package common

import (
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Inserisci elementi
func InsertTrackChronology(w http.ResponseWriter, r *http.Request, id_link bson.ObjectId) {

	//  fmt.Println("___________InsertTrackChronology__________________________________________________________")

	// prendo ultimo URL inserito
	result := GetLastTrack(w, r)
	urlBeforeUno := ""
	urlBeforeDue := ""
	urlBeforeTre := ""
	value := Valutazione{}
	idChronology := id_link
	if len(result) > 0 {
		urlBeforeUno = result[0].Url
		urlBeforeDue = result[0].UrlBeforeUno
		urlBeforeTre = result[0].UrlBeforeDue

		// cecco che non si abbia fatto F5 / ricaricato la pagina
		if urlBeforeDue == urlBeforeTre {
			urlBeforeDue = ""
		}
		if urlBeforeUno == urlBeforeDue {
			urlBeforeDue = ""
		}

		value.Voto = result[0].Value.Voto
		value.Operazione = result[0].Value.Operazione
		// idChronology = result[0].IdChronology

	}

	// inserisco solo se ho una cronologia che abbia un senso di analisi...
	// TODO: da decidere se fermarsi al 2 o al 3
	if urlBeforeDue != "" {

		// fmt.Println("___________InsertTrackChronology_________urlBeforeDue_________________________________________________")
		// fmt.Println("link: ", id_link)
		// fmt.Println("idChronology: ", idChronology)
		// fmt.Println("urlBeforeUno: ", urlBeforeUno)

		// parmetri da inserire
		siteParameters := getSiteParameters(r)
		user := getUser(r)
		ip := siteParameters.ip
		link := siteParameters.referer
		useragent := siteParameters.useragent
		token := siteParameters.token
		collection_track = setDB(siteParameters.site)

		c := session.DB(db_track_chronology).C(collection_track)
		err := c.Insert(&TrackChronology{UrlNow: link, UrlBeforeUno: urlBeforeUno, UrlBeforeDue: urlBeforeDue, UrlBeforeTre: urlBeforeTre, Value: value, Ip: ip, IdChronology: idChronology, User: user, UserAgent: useragent, Token: token, Timestamp: time.Now()})
		if err != nil { 
			fmt.Println("InsertTrackChronology Errore:", err)
		}

	}

	// fmt.Println("___________InsertTrackChronology____Fine______________________________________________________")

}
