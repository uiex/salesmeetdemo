// Elementi consigliati e ordinati in base alla valutazione della costumer experience e del numero di volte visualizzato.
package common

// "github.com/gorilla/mux"
import (
	"encoding/json"
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

// MAIN -
func GetExperienceLimit(w http.ResponseWriter, r *http.Request) {
	GetExperienceCommon(w, r, 50)
}
// MAIN -
func GetExperience(w http.ResponseWriter, r *http.Request) {
	GetExperienceCommon(w, r, 1000)
}

func GetExperienceCommon(w http.ResponseWriter, r *http.Request, limit int) {

	fmt.Printf("GetExperienceCommon ****************************")
	fmt.Printf("GetExperienceCommon ****************************")
	fmt.Printf("GetExperienceCommon ****************************")
	fmt.Printf("GetExperienceCommon ****************************")

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	// params := mux.Vars(r)
	// sito := params["sito"]
	sito := getSite(r)
	siteParameters := getSiteParameters(r)
	if sito != "" {
		collection_chronology = setDB(sito)
	} else {
		collection_chronology = setDB(siteParameters.site)
	}

	fmt.Printf(db_chronology)
	fmt.Printf(collection_chronology)


	// ip := siteParameters.ip
	// useragent := siteParameters.useragent
	token := siteParameters.token

	c := session.DB(db_chronology).C(collection_chronology)

	count_AI := 1 // 0.6

	// Where
	// query := bson.M{"ip": ip, "useragent": useragent, "meta.imgurl" : bson.M{ "$ne" : "" } , "value.voto" : bson.M{ "$gt" : 0 } }
	// query := bson.M{"token": token, "meta.imgurl" : bson.M{ "$ne" : "" } , "value.voto" : bson.M{ "$gt" : 0 } }
	query := bson.M{"token": token, "meta.title" : bson.M{ "$ne" : "La Centrale de Prevoyance" }, "meta.imgurl" : bson.M{ "$ne" : "" } , "value.voto" : bson.M{ "$gt" : 0 } }
	// query := bson.M{"token": token }
	// Valori da visualizzare - Raggruppati
	group := bson.M{"_id": "$url", "count": bson.M{"$sum": 1}, "title": bson.M{"$max": "$meta.title"}, "price": bson.M{"$max": "$price.price"}, "currency": bson.M{"$max": "$price.currency"}, "img": bson.M{"$max": "$meta.imgurl"}, "voto": bson.M{"$sum": "$value.voto"}, "voto_max": bson.M{"$max": "$value.voto"}, "voto_min": bson.M{"$min": "$value.voto"}, "operazioni": bson.M{ "$max": "$value.operazione" }  }
	//
	project := bson.M{"_id": 0, "url": "$_id", "voto_AI": bson.M{"$divide": []interface{}{"$voto", bson.M{"$multiply": []interface{}{"$count", count_AI}}}}, "title": "$title", "price": "$price", "currency": "$currency", "img": "$img", "count": "$count", "count_AI": bson.M{"$multiply": []interface{}{"$count", count_AI}}, "voto_max": "$voto_max", "voto_min": "$voto_min", "voto_totale": "$voto", "operazioni" : "$operazioni" }

	// query costruita con sort e limit
	pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"voto_AI": -1}}, {"$limit": limit}}

	/*
		db.amazon_it.aggregate( [
			{ $match: { "ip": "172.19.0.1", "meta.imgurl" : { $ne : "" } , "value.voto" : { $gt : 0 } } },
			{ $group: { _id: "$url",
						count: { $sum: 1 },
						voto: { $sum: "$value.voto" },
						voto_max: { $max: "$value.voto" },
						voto_min: { $min: "$value.voto" },
						img: { $max: "$meta.imgurl" },
						title: { $max: "$meta.title" },
						"operazioni" : "$operazioni",
						"voto_utente" : bson.M{"$divide":[]interface{}{"voto", "$count"}
					}
			},
			{"$project":{
				"_id":0,
				"url":"$_id",
				"img": 1,
				"title": 1,
				"count" : "$count",
				"count_AI": {$multiply:["$count",0.6]},
				"voto_max" : "$voto_max",
				"voto_min" : "$voto_min",
				"voto_totale" : "$voto",
				"voto_AI": {$divide:["$voto",{$multiply:["$count",0.6]} ]},
			}},
			{ $sort: { "voto_AI": -1, "value.voto": -1, count: -1 } }
		]);

	*/

	pipe := c.Pipe(pipeline)
	result := []bson.M{}
	err := pipe.All(&result)

	if err != nil {
		fmt.Println("Errore:", err)
	}
	json.NewEncoder(w).Encode(&result)

}
