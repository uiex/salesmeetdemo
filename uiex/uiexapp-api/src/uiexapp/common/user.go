// Prende le informazioni per l'utente
package common

import (
	"encoding/json"
	"fmt"
	"net/http"
	"gopkg.in/mgo.v2/bson"
)


func GetUserInfo(w http.ResponseWriter, r *http.Request) {
	GetUserInfoCommon(w, r)
}

type (
	UserInfoCommon struct {
		Id        string
		Count     int
		Price     int
		Voto 			int
		Votopagina 	 int
		Tempo 	 int
		Operazioni 	 int
		Pricepagina 	 int
    Pricemax     string
    Pricemin     string
	}
)

func GetUserInfoCommon(w http.ResponseWriter, r *http.Request) {

	fmt.Println("GetUserInfoCommon_________________")
  fmt.Println("GetUserInfoCommon_________________")
  fmt.Println("GetUserInfoCommon_________________")
  fmt.Println("GetUserInfoCommon_________________")

	setAccessControl(w)
	getSessionDB()

	siteParameters := getSiteParameters(r)
	token := siteParameters.token
  fmt.Println(token)

	sito := getSite(r)
	if sito != "" {
		collection_chronology = setDB(sito)
	} else {
		collection_chronology = GetSiteCollection(w, r)
	}

	fmt.Println(db_chronology)
	fmt.Println(collection_chronology)

	c := session.DB(db_chronology).C(collection_chronology)

	result := []UserInfoCommon{}

  query := bson.M{"token": token}
  // group := bson.M{"_id": "$Token", "price": bson.M{"$sum": "$price.price"} , "count": bson.M{"$sum": 1}, "pricemin": bson.M{"$min": "$price.price"}, "pricemax": bson.M{"$max": "$price.price"}}
	group := bson.M{"_id": "$Token", "tempo": bson.M{"$sum": bson.M{"$convert": bson.M{"input": "$tempo.totalemovimento", "to": "double","onError": 0,"onNull": 0} } }, "operazioni": bson.M{"$sum": bson.M{"$convert": bson.M{"input": "$value.operazione", "to": "double","onError": 0,"onNull": 0} } },"votopagina": bson.M{"$last": "$value.voto"}, "pricepagina": bson.M{"$last": "$price.price"}, "price": bson.M{"$sum": bson.M{"$convert": bson.M{"input": "$price.price", "to": "double","onError": 0,"onNull": 0} } } , "pricemin": bson.M{"$min": "$price.price"}, "pricemax": bson.M{"$max": "$price.price"},"count": bson.M{"$sum": 1} ,"voto": bson.M{"$sum": bson.M{"$convert": bson.M{"input": "$value.voto", "to": "double","onError": 0,"onNull": 0} } } }
	project := bson.M{"Id": "$token", "count": "$count", "operazioni": "$operazioni", "tempo": "$tempo", "votopagina": "$votopagina", "pricepagina": "$pricepagina", "price": "$price", "prices": "$price", "pricemin": "$pricemin", "pricemax": "$pricemax", "voto" : "$voto"}
  pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}}
	pipe := c.Pipe(pipeline)
	err := pipe.All(&result)
  fmt.Println(&result)
  fmt.Println(result)
	if err != nil {
		fmt.Println("Errore:", err)
	}
	json.NewEncoder(w).Encode(&result)

  	/*
  		db.zalando_it.aggregate( [
  		                     { $group: { _id: "$idchronology", count: { $sum: 1 }, urlnow: { $max: "$urlnow" }, idchronology: { $max: "$idchronology" } } }
  		]);
  				group := bson.M{"_id": "$url", "count": bson.M{"$sum": 1}, "title": bson.M{"$max": "$meta.title"}, "img": bson.M{"$max": "$meta.imgurl"}, "voto": bson.M{"$sum": "$value.voto"}, "voto_max": bson.M{"$max": "$value.voto"}, "voto_min": bson.M{"$min": "$value.voto"}}
  				project := bson.M{"_id": 0, "url": "$_id", "voto_AI": bson.M{"$divide": []interface{}{"$voto", bson.M{"$multiply": []interface{}{"$count", count_AI}}}}, "title": "$title", "img": "$img", "count": "$count", "count_AI": bson.M{"$multiply": []interface{}{"$count", count_AI}}, "voto_max": "$voto_max", "voto_min": "$voto_min", "voto_totale": "$voto"}
  				pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project}, {"$sort": bson.M{"count": -1}}, {"$limit": limit}}
  			pipe := c.Pipe(pipeline)
  			result := []bson.M{}
  			err := pipe.All(&result)
  	*/


}
