// Cancellazione soft della cronologia dell'utente.
package common

import (
	"fmt"
	"net/http"
	// "context" // manage multiple requests

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)


// MAIN - Aggiorna l'esperienza utente ...
func DeleteChronology(w http.ResponseWriter, r *http.Request) {
	DeleteCronologia(w, r)
	// DeleteLast(w, r)
	DeleteConsigliati(w, r)
}


func DeleteCronologia(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, false)
	session.SetSafe( &mgo.Safe{WMode: "majority"} )

	siteParameters := getSiteParameters(r)
	// ip := siteParameters.ip 
	// useragent := siteParameters.useragent
	token := siteParameters.token
	sito := getSite(r)
	if sito != "" {
		collection_chronology = setDB(sito)
	} else {
		collection_chronology = GetSiteCollection(w, r)
	}
	
	c := session.DB(db_chronology).C(collection_chronology)

	// where := bson.M{"ip": ip, "useragent": useragent}
	// setnew := bson.M{"useragent": useragent + "_old"}
	where := bson.M{"token": token}
	// setnew := bson.M{"token": token + "_old"}	

	result := []Chronology{}
	err := c.Find(where).All(&result) 
	if err != nil {
		// log.Fatal(err)
		fmt.Println("Errore:", err)
	}

	if len(result) > 0 {
		for _, element := range result {
			// err := c.UpdateId(element.Id, setnew)
			err := c.Update(bson.M{"_id": element.Id}, bson.M{"$set": bson.M{"token": token + "_old"}})
			if err != nil {
				fmt.Printf("find fail %v\n", err)
			}

		}
	}	

	/*
	info, errUpdate := c.UpdateAll( where, setnew )
	// info, errUpdate := c.UpdateMany( context.Background(), where, setnew )
	if errUpdate != nil {
		fmt.Printf("update fail %v\n", errUpdate)
	}
	fmt.Println("info: ", info)
	fmt.Println("err: ", errUpdate)
	fmt.Println("___________DeleteChronology_______________")
	*/
	 
}


// TODO
func DeleteLast(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, false)

	siteParameters := getSiteParameters(r)
	//ip := siteParameters.ip 
	// useragent := siteParameters.useragent
	token := siteParameters.token
	sito := getSite(r)
	if sito != "" {
		collection_last = setDB(sito)
	} else {
		collection_last = GetSiteCollection(w, r)
	}

	c := session.DB(db_last).C(collection_last)

	// where := bson.M{"ip": ip, "useragent": useragent}
	// setnew := bson.M{"useragent": useragent + "_old"}
	where := bson.M{"token": token}
	setnew := bson.M{"token": token + "_old"}	
	
	err := c.Update(where, setnew)
	if err != nil {
		fmt.Printf("find fail %v\n", err)
	}
}


func DeleteConsigliati(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	session.SetMode(mgo.Monotonic, false)
	session.SetSafe( &mgo.Safe{WMode: "majority"} )

	siteParameters := getSiteParameters(r)
	//ip := siteParameters.ip 
	// useragent := siteParameters.useragent
	token := siteParameters.token
	sito := getSite(r)
	if sito != "" {
		collection_consigliati = setDB(sito)
	} else {
		collection_consigliati = GetSiteCollection(w, r)
	}
	c := session.DB(db_consigliati).C(collection_consigliati)

	// where := bson.M{"ip": ip, "useragent": useragent}
	// setnew := bson.M{"useragent": useragent + "_old"}
	where := bson.M{"token": token}
	// setnew := bson.M{"token": token + "_old"}	
	
	result := []Consigliati{}
	err := c.Find(where).All(&result) 
	if err != nil {
		// log.Fatal(err)
		fmt.Println("Errore:", err)
	}

	if len(result) > 0 {
		for _, element := range result {
			err := c.Update(bson.M{"_id": element.Id}, bson.M{"$set": bson.M{"token": token + "_old"}})
			// err := c.UpdateId(element.Id, setnew)
			if err != nil {
				fmt.Printf("find fail %v\n", err)
			}
		}
	}	

	/*
	info, errUpdate := c.UpdateAll( where, setnew )
	// info, errUpdate := c.UpdateMany( context.Background(), where, setnew )
	if errUpdate != nil {
		fmt.Printf("update fail %v\n", errUpdate)
	}
	fmt.Println("info: ", info)
	fmt.Println("err: ", errUpdate)
	fmt.Println("___________DeleteChronology_______________")
	*/
	
}

 