// Gestione inserimento e update della visualizzazione della pagina.
package common

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"


	"gopkg.in/mgo.v2/bson"
)

// mgo "gopkg.in/mgo.v2"

// MAIN - Inserisci elementi
func InsertChronology(w http.ResponseWriter, r *http.Request) {

	// fmt.Println("InsertChronology*******************")

	// Aggiorno l'esperienza utente sulla pagina.
	// fmt.Println("UpdateChronologyCommon*******************")
	voto := UpdateChronologyCommon(w, r)

	// fmt.Println("voto:", voto)


	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// referer := siteParameters.referer
	currentUrl := getCurrentUrl(r)
	ip := siteParameters.ip
	useragent := siteParameters.useragent
	token := siteParameters.token
	collection_chronology = GetSiteCollection(w, r)

	//title := getTitle(r)
	//img := getImg(r)
	languagePage := getLanguagePage(r)
	tipo := getTipo(r)
	user := getUser(r)

	theMetatag := Metatag{}
	theMetatag.Title = getTitle(r)
	theMetatag.Description = getDescription(r)
	theMetatag.ImgUrl = getImg(r)
	theMetatag.SchemaType = tipo

	thePrice := Prezzo{}
	thePrice.Price = getPrice(r)
	thePrice.Currency = getCurrency(r)

	theTempoChronology := TempoChronology{}
	theTempoChronology.Reale = ""
	theTempoChronology.TotaleMovimento = ""
	theTempoChronology.StartPage = ""


	// Creo id così da poterlo portare fuori
	i := bson.NewObjectId()
	c := session.DB(db_chronology).C(collection_chronology)
	err := c.Insert(&Chronology{Id: i, Url: currentUrl, Ip: ip, User: user, Language: languagePage, UserAgent: useragent, Token: token, Timestamp: time.Now(), Price: thePrice, Meta: theMetatag, Tempo: theTempoChronology})
	if err != nil {
		fmt.Println("Errore:", err)
	}

	// INSERISCO il track del cliente così da avere un parametro in più per le logiche ...
	InsertTrackChronology(w, r, i)

	// Feature: Insert(&Chronology , InsertCustomerExperience e InsertExtensionBrowser hanno lo stesso ID
	// così la procedura che darà il voto alla pagina potrà aggiornare il valore del voto in maniera facile sut tutti i sistemi...

	// Inserisco ultimo elemento così da riproporlo velocemente nell'history...
	InsertLast(w, r, i)
	// Creo document per la gestione dell'esperienza utente sulla pagina.
	InsertCustomerExperience(w, r, i)

	// Tracco se utilizzo UIEX
	idBefore := r.PostFormValue("idBefore")
	// Tengo traccia se il link passa attraverso UIEX
	InsertTrack(w, r, siteParameters, bson.ObjectId(i).Hex(), idBefore)

	// SE è un'estensione installata sul browser inserisco cronologia
	InsertExtensionBrowser(w, r, i)

	// Ritorno ID cosi da poter modifica i parametri in successione. Tempo attivo, cosa ha fatto ...
	cio := &ChronologyInsert{Id: i, Tags: gatTags(theMetatag.SchemaType, thePrice.Price)}

	/*
		cwwww, errwww := r.Cookie("UserIntelligentExperienceAppStartTime")
		if errwww != nil {
			fmt.Println("Errore:", errwww)
		}
		sessionToken := cwwww.Value
		fmt.Println("sessionToken:", sessionToken)
	*/

	// Inseriso la pagina nell'elengo delle pagine per il sito.
	// Ogni pagina risulterà unica per URL. Verranno aggiornati solo i dati.
	InsertPage(w, r, voto)

	// Verifico se esiste un consigliato.
	// WriteCookieServer(w, r, "idBefore", idBefore)
	checkConsigliato(w, r, i)

	// fmt.Println("FINE ... InsertChronology*******************")


	json.NewEncoder(w).Encode(cio)

}

func UpdateChronologyCommon(w http.ResponseWriter, r *http.Request) int {

	getSessionDB() // session.SetMode(mgo.Monotonic, true)
	idBefore := r.PostFormValue("idBefore")
	voto := 0
	if idBefore != "" {
		// aggiorna campi esperienza utente
		// CustomerExperience.go
		voto = UpdateCustomerExperience(w, r, idBefore)
	}
	return voto

}

func UpdateChronology(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	UpdateChronologyCommon(w, r)

}

// https://docs.mongodb.com/manual/tutorial/query-documents/
// https://docs.mongodb.com/manual/core/aggregation-pipeline/
// https://gist.github.com/laeshiny/89c5ec444cfa275f456f340aadddf102

// MAIN -
func GetChronologyLast(w http.ResponseWriter, r *http.Request) {
	GetChronologyCommon(w, r, 1)
}
// MAIN -
func GetChronologyLimit(w http.ResponseWriter, r *http.Request) {
	GetChronologyCommon(w, r, 3)
}
// MAIN -
func GetChronology(w http.ResponseWriter, r *http.Request) {
	GetChronologyCommon(w, r, 1000)
}

func GetChronologyCommon(w http.ResponseWriter, r *http.Request, limit int) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// ip := siteParameters.ip
	// useragent := siteParameters.useragent
	token := siteParameters.token

	sito := getSite(r)
	if sito != "" {
		collection_chronology = setDB(sito)
	} else {
		collection_chronology = GetSiteCollection(w, r)
	}


	fmt.Println("GetChronologyCommon_________________")
	fmt.Println("GetChronologyCommon_________________")
	fmt.Println(db_chronology)
	fmt.Println(collection_chronology)

	c := session.DB(db_chronology).C(collection_chronology)

	//	if limit == 1 {

	fmt.Println("___________GetChronologyCommon_______________")
	fmt.Println(sito)
	fmt.Println(token)
	fmt.Println(collection_chronology)

	result := []Chronology{}
	// err := c.Find(bson.M{"name": name}).One(&result)
	// err := c.Find(bson.M{"name": name}).Sort("-timestamp").All(&result)
	// query := bson.M{"ip": ip, "useragent": useragent}
	query := bson.M{"token": token}
	err := c.Find(query).Limit(limit).Sort("-timestamp").All(&result) // Sort(-) = desc
	if err != nil {
		// log.Fatal(err)
		fmt.Println("Errore:", err)
	}

	json.NewEncoder(w).Encode(&result)
	/*
		} else {

			result := []Chronology{}
			// err := c.Find(bson.M{"name": name}).One(&result)
			// err := c.Find(bson.M{"name": name}).Sort("-timestamp").All(&result)
			query := bson.M{"ip": ip, "useragent": useragent}
			err := c.Find(query).Limit(limit).Sort("-timestamp").All(&result) // Sort(-) = desc
			if err != nil {
				// log.Fatal(err)
				fmt.Println("Errore:", err)
			}
			json.NewEncoder(w).Encode(&result)
		}
	*/
}

// aggiorna il record in base al calcolo stimanto per la pagina ...
// richiamata dopo il calcolo della esperienza utente
func UpdateChronologyCalcolate(w http.ResponseWriter, r *http.Request, id bson.ObjectId, voto int, operazioni int, theTempo TempoChronology) {

	// fmt.Println("___________UpdateChronologyCalcolate_______________")
	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	collection_chronology = GetSiteCollection(w, r)

	value := Valutazione{}
	value.Voto = voto
	value.Operazione = operazioni

	c := session.DB(db_chronology).C(collection_chronology)
	errUpdate := c.UpdateId(id, bson.M{"$set": bson.M{"value": value, "tempo": theTempo}})
	// da eliminare - errUpdate := c.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"value": value, "tempo": theTempo}})
	if errUpdate != nil {
		fmt.Printf("update UpdateChronology fail %v\n", errUpdate)
	}

}

func GetChronologyByUrl(w http.ResponseWriter, r *http.Request) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// ip := siteParameters.ip
	// useragent := siteParameters.useragent
	token := siteParameters.token
	pages := getReferer(r)
	// pages := siteParameters.referer

	collection_chronology = GetSiteCollection(w,r)

	/*
	fmt.Println("GetChronologyByUrl_________________")
	fmt.Println("GetChronologyByUrl_________________")
	fmt.Println(db_chronology)
	fmt.Println("collection_chronology:" + collection_chronology)
	fmt.Println(db_chronology)
	fmt.Println("pages:" + pages)
	fmt.Println("token:" + token)
	*/

	/*
	sito := getSite(r)
	if sito != "" {
		collection_chronology = setDB(sito)
	} else {
		collection_chronology = GetSiteCollection(w, r)
	}
	fmt.Println(collection_chronology)
	*/

	c := session.DB(db_chronology).C(collection_chronology)

	// result := []Chronology{}
	result := []ChronologyStatistics{}
	group := bson.M{"_id": "$url", "url": bson.M{"$max": "$url"}, "imgurl": bson.M{"$max": "$meta.imgurl"}, "count": bson.M{"$sum": 1}, "votomedio": bson.M{"$sum": "$value.voto"}, "votomin": bson.M{"$min": "$value.voto"}, "votomax": bson.M{"$max": "$value.voto"}, "title": bson.M{"$max": "$meta.title"}, "description": bson.M{"$max": "$meta.description"}}
	project := bson.M{"Id": "$url", "url": "$url", "imgurl": "$imgurl", "count": "$count", "votomedio": "$votomedio", "votomax": "$votomax", "votomin": "$votomin", "title": "$title", "description": "$description"}
	// query := bson.M{"ip": ip, "useragent": useragent, "url": pages}
	query := bson.M{"token": token, "url": pages}

	pipeline := []bson.M{{"$match": query}, {"$group": group}, {"$project": project} }
	pipe := c.Pipe(pipeline)
	err := pipe.All(&result)

	// fmt.Println(&result)

	if err != nil {
		// log.Fatal(err)
		fmt.Println("Errore:", err)
	}

	// fmt.Println("GetChronologyByUrl ************************************************************************************************ ")
	json.NewEncoder(w).Encode(&result)

}
