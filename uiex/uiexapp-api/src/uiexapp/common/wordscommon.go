// Link consigliato in base alla navigazione degli altri clienti.
package common

import (
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

// "fmt"

func GetQueryWords( s []string, url string, campowords string, campourl string, numero int, collectionPages string, codeProducts []bson.ObjectId, single int, ageGender string, numeroPartenza int  ) ([]bson.M, int) {

	orQuery := []bson.M{}
	i := 0
	word := ""

	if numeroPartenza > 0 {
		i = numeroPartenza
	}

	if numero > 0 {
		word, i = getWord(s,i)
		aQuery := bson.M{campowords: bson.M{ "$regex" : word + ",", "$options": "si" }}
		orQuery = append(orQuery, aQuery)
	}
	if numero > 1 {
		word, i = getWord(s,i)
		bQuery := bson.M{campowords: bson.M{ "$regex" : word + ",", "$options": "si" }}
		orQuery = append(orQuery, bQuery)
	}
	if numero > 2 {
		word, i = getWord(s,i)
		cQuery := bson.M{campowords: bson.M{ "$regex" : word + ",", "$options": "si" }}
		orQuery = append(orQuery, cQuery)
	}
	if numero > 3 {
		word, i = getWord(s,i)
		dQuery := bson.M{campowords: bson.M{ "$regex" : word + ",", "$options": "si" }}
		orQuery = append(orQuery, dQuery)
	}
	// singolo valore prescelto
	if single > 0 {
		word, i = getWord(s,single)
		dQuery := bson.M{campowords: bson.M{ "$regex" : word + ",", "$options": "si" }}
		orQuery = append(orQuery, dQuery)
	}

	if url != "" {
		eQuery := bson.M{campourl : bson.M{ "$ne" : url }}
		orQuery = append(orQuery, eQuery)
	}
	if collectionPages != "" {
		colQuery := bson.M{"collectionpages" : bson.M{ "$ne" : collectionPages }}
		orQuery = append(orQuery, colQuery)
	}
	if ageGender != "" {
		ageGenderQuery := bson.M{"agegender" : ageGender }
		orQuery = append(orQuery, ageGenderQuery)
	}

	// codici prodotti già esistenti ...
	for i := 0; i < len(codeProducts); i++ {
		if codeProducts[i] != "" {
			colQuery := bson.M{"_id" : bson.M{ "$ne" : codeProducts[i] }}
			orQuery = append(orQuery, colQuery)
		}
	}
	return orQuery, i
}

func getWord( word []string, i int ) (string, int) { 
	if len(word[i]) < 3 {
		i = i + 1
	}
	return word[i], i + 1
}

func containsInArray(arr []string, str string) bool {
	for _, a := range arr {
	   if a == str {
		  return true
	   }
	}
	return false
 }

 func isNumeric(s string) bool {
    _, err := strconv.ParseFloat(s, 64)
    return err == nil
}

 func cleanArray(arr []string) []string {

	var new []string
	for _, a := range arr {
		articoli := []string{"con", "gli", "uno", "una","the"}
		gender := []string{"uomo","donna","bambino","bambina","ragazza","ragazzo"}
		all := append(articoli, gender...)
		if !containsInArray( all ,a ) {
			if !isNumeric( a ) {
				new = append(new, a)
			}
		}
	}
	return new
 }
