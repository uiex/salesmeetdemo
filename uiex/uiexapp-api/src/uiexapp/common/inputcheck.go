// Gestione del check sui dati in  input
package common

// "fmt"

import (
	"net/http"
	"strconv"
	"strings"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func getLanguagePage(r *http.Request) string {
	return r.PostFormValue("language")
}
func getEmail(r *http.Request) string {
	return r.PostFormValue("email")
}
func getUser(r *http.Request) string {
	return r.PostFormValue("user")
}
func getCurrentUrl(r *http.Request) string {
	currentUrl := r.PostFormValue("currenturl")
	if currentUrl == "" {
		params := mux.Vars(r)
		currentUrl = params["currenturl"]
	}
	return currentUrl
}
func getSite(r *http.Request) string {
	return strings.Trim(r.PostFormValue("site"), " ")
}
func getReferer(r *http.Request) string {
	return strings.Trim(r.PostFormValue("referer"), " ")
}
func getToken(r *http.Request, agentId string) string {
	token := r.PostFormValue("token")
	if token == ""  {
		// fmt.Println("**** Token non presente")
		return strings.Trim(agentId, " ")
	}
	return strings.Trim(token, " ")
	/*
	if strings.Contains(token, agentId) == false {
		fmt.Println("**** Agente non presente nel token")
		return agentId
	} else {
		fmt.Println("**** Agente presente nel token")
		return token
	}
	*/
}
func getTitle(r *http.Request) string {
	return r.PostFormValue("title")
}
func getImg(r *http.Request) string {
	return r.PostFormValue("img")
}
func getTipo(r *http.Request) string {
	return castType( r.PostFormValue("schemaType") , "")
}
func getDescription(r *http.Request) string {
	return r.PostFormValue("description")
}
func getTitleConsigliato(r *http.Request) string {
	return r.PostFormValue("titleConsigliato")
}
func getWords(r *http.Request) string {
	return r.PostFormValue("words")
}
func getAgeGender(r *http.Request) string {
	return r.PostFormValue("ageGender")
}

/*
func getReferrerUlr(r *http.Request) string {
	return r.PostFormValue("referrer")
}
*/

func castToBsonID(id string) bson.ObjectId {
	idObject := bson.ObjectIdHex(id)
	return idObject
}

func castBsonIdToString(id string) bson.ObjectId {
	idObject := bson.ObjectIdHex(id)
	return idObject
}
func getPrice(r *http.Request) string {
	price := r.PostFormValue("price")
	// fmt.Println(price)
	price = checkPrice(price)
	// fmt.Println(price)
	// return price
	return price
}
func getCurrency(r *http.Request) string {
	currency := r.PostFormValue("currency")
	currency = strings.TrimSpace(currency)
	// fmt.Println("currency")
	// fmt.Println(currency)
	price := r.PostFormValue("price")
	// fmt.Println(price)
	if currency == "" {
		currency = getCurrencyByPrice(price)
	}
	return currency

}

// Check valori
func checkPrice(price string) string {
	price = strings.TrimSpace(price)
	if price != "" {
		// verifico se ho un calore del tipo 19,00 EUR
		price_p := strings.Split(price, " ")
		for _, element := range price_p {
			_, err := strconv.ParseFloat(castPrice(element), 64)
			if err == nil {
				return element
			}
		}
		// verifico se ho un calore del tipo 19EUR ... Senza spazi
		price_p_nospace := strings.Split(price, "")
		price_total := ""
		for _, element_nospace := range price_p_nospace {
			if element_nospace != "" {
				if element_nospace == "," || element_nospace == "." {
					if price_total != "" {
						price_total = price_total + "."
					}
				} else {
					_, err := strconv.ParseFloat(castPrice(element_nospace), 64)
					if err == nil {
						price_total = price_total + element_nospace
					} else {
						if price_total != "" {
							return price_total
						}
					}
				}
			}
		}
		if price_total != "" {
			return price_total
		}
	}
	return price
}

func castPrice(price string) string {
	price = strings.Replace(price, ".", "", -1)  // elimino punto
	price = strings.Replace(price, ",", ".", -1) // sostituisco la virgola con il punto
	return price
}

// Check valori
func getCurrencyByPrice(price string) string {
	price = strings.TrimSpace(price)
	if price != "" {
		// elimino virgole dei decimali
		price = castPrice(price)
		price = strings.Replace(price, ",", ".", -1)
		// ciclo per singolo carattere della stringa
		price_p_nospace := strings.Split(price, "")
		currency_total := ""
		flag := false
		for _, element_nospace := range price_p_nospace {

			element_nospace = strings.TrimSpace(element_nospace)
			if element_nospace != "" {
				// verifico se è un numerico
				_, err := strconv.ParseFloat(castPrice(element_nospace), 64)
				if err == nil {
				} else {
					// non è numerico
					if currency_total != "" || flag == false {
						// ricreo stringa
						currency_total = currency_total + element_nospace
						// fmt.Println(currency_total)
					}
					flag = true
				}

			} else {

				// sono al termine della parola - EUR 19,90
				if currency_total != "" {
					return currency_total
				}
			}
		}
	}
	return ""
}
