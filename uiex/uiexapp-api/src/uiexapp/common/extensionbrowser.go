package common

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Inserisci elementi
func InsertExtensionBrowser(w http.ResponseWriter, r *http.Request, i bson.ObjectId) {
	// SE è un'estensione installata sul browser inserisco cronologia
	if IsExtensionBrowser(r) != "" {
		insertExtensionBrowserInternal(w, r, i)
	}
}

// Inserisci elementi
func insertExtensionBrowserInternal(w http.ResponseWriter, r *http.Request, i bson.ObjectId) {

	// fmt.Println("___________Insert ExtensionBrowser__________________________________________________________")
	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// site := siteParameters.site
	referer := siteParameters.referer
	ip := siteParameters.ip
	useragent := siteParameters.useragent
	token := siteParameters.token
	collection_chronology = setDB(siteParameters.site)

	title := getTitle(r)
	user := getUser(r)
	img := getImg(r)
	tipo := getTipo(r)

	theMetatag := Metatag{}
	theMetatag.Title = title
	theMetatag.ImgUrl = img
	theMetatag.SchemaType = tipo

	thePrice := Prezzo{}
	thePrice.Price = getPrice(r)
	thePrice.Currency = getCurrency(r)

	theTempoChronology := TempoChronology{}
	theTempoChronology.Reale = ""
	theTempoChronology.TotaleMovimento = ""
	theTempoChronology.StartPage = ""

	// Creo id così da poterlo portare fuori
	c := session.DB(db_chrome).C(collection_chrome)
	err := c.Insert(&Chronology{Id: i, Url: referer, Ip: ip, User: user, UserAgent: useragent, Token:token, Timestamp: time.Now(), Meta: theMetatag, Price: thePrice, Tempo: theTempoChronology})
	if err != nil {
		fmt.Println("Errore:", err)
	}

}

// aggiorna il record in base al calcolo stimanto per la pagina ...
func UpdateExtensionBrowserCalcolate(w http.ResponseWriter, r *http.Request, id bson.ObjectId, voto int, operazioni int, theTempo TempoChronology) {

	// fmt.Println("___________UpdateExtensionBrowserCalcolate_______________")

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true) 

	siteParameters := getSiteParameters(r)
	collection_chronology = setDB(siteParameters.site)

	value := Valutazione{}
	value.Voto = voto
	value.Operazione = operazioni

	c := session.DB(db_chrome).C(collection_chrome)
	errUpdate := c.UpdateId(id, bson.M{"$set": bson.M{"value": value, "tempo": theTempo}})
	// da eliminare - errUpdate := c.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"value": value, "tempo": theTempo}})
	if errUpdate != nil {
		fmt.Printf("update UpdateChronology fail %v\n", errUpdate)
	}

}

func GetExtensionBrowser(w http.ResponseWriter, r *http.Request) {
	GetExtensionBrowserCommon(w, r, 300)
}

func GetExtensionBrowserCommon(w http.ResponseWriter, r *http.Request, limit int) {

	setAccessControl(w)  
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// useragent := siteParameters.useragent
	// ip := siteParameters.ip
	token := siteParameters.token
	c := session.DB(db_chrome).C(collection_chrome)

	result := []Chronology{}
	// err := c.Find(bson.M{"name": name}).One(&result)
	// err := c.Find(bson.M{"name": name}).Sort("-timestamp").All(&result)
	// query := bson.M{"useragent": useragent, "ip": ip}
	query := bson.M{"token": token}

	err := c.Find(query).Limit(limit).Sort("-timestamp").All(&result) // Sort(-) = desc
	if err != nil {
		fmt.Println("Errore:", err)
	}

	json.NewEncoder(w).Encode(&result)

}

// Verifico se devo attivare la funzione ...
func IsExtensionBrowser(r *http.Request) string {
	return r.PostFormValue("isExtensionBrowser")
}
