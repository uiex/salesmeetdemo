// Ultimo link visitato così da riproporlo velocemente come elemento singolo...
package common

import (
	"encoding/json"
	"fmt"
	"net/http"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Inserisci elementi
func InsertLast(w http.ResponseWriter, r *http.Request, idChronology bson.ObjectId) {

	setAccessControl(w)
	// Optional. Switch the session to a monotonic behavior.
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	siteParameters := getSiteParameters(r)
	// site := siteParameters.site
	referer := siteParameters.referer
	ip := siteParameters.ip
	useragent := siteParameters.useragent
	token := siteParameters.token
	// collection_last = setDB(siteParameters.site)
	site := GetSiteCollection(w, r)

	c := session.DB(db_last).C(collection_last)

	// TODO - Query sotto da perfezionare e fare una unica operazione
	last := []LastId{}
	// query := bson.M{"ip": ip, "useragent": useragent, "site": site}
	query := bson.M{"token": token, "site": site}
	err := c.Find(query).All(&last)

	if err == nil {
		fmt.Printf("non trovato find fail %v\n", err)
	}

	title := getTitle(r)
	user := getUser(r)
	img := getImg(r)
	tipo := getTipo(r)
	theMetatag := Metatag{}
	theMetatag.Title = title
	theMetatag.ImgUrl = img
	theMetatag.SchemaType = tipo
	value := Valutazione{}
	value.Voto = 0
	value.Operazione = 0


	if len(last) == 0 {

		errInsert := c.Insert(&Last{Url: referer, Ip: ip, User: user, UserAgent: useragent, Token: token, Site: site, Meta: theMetatag, IdChronology: idChronology, Value: value})
		if errInsert != nil {
			fmt.Println("Errore:", errInsert)
		}

	} else {
		errUpdate := c.Update(bson.M{"_id": last[0].Id}, bson.M{"$set": bson.M{"url": referer, "urlbeforeuno": last[0].Url, "urlbeforedue": last[0].UrlBeforeUno, "site": site, "meta": theMetatag, "idchronology": idChronology, "value": value}})
		if errUpdate != nil {
			fmt.Printf("update fail %v\n", errUpdate)
		}
	}

}

// aggiorna il record in base al calcolo stimanto per la pagina ...
func UpdateLastCalcolate(w http.ResponseWriter, r *http.Request, id bson.ObjectId, voto int, operazioni int) {

	setAccessControl(w)
	getSessionDB() // session.SetMode(mgo.Monotonic, true)

	// siteParameters := getSiteParameters(r)
	// collection_last = setDB(siteParameters.site)

	value := Valutazione{}
	value.Voto = voto
	value.Operazione = operazioni

	c := session.DB(db_last).C(collection_last)

	// errUpdate := c.UpdateId(idchronology, bson.M{"$set": bson.M{"value": value}})
	errUpdate := c.Update(bson.M{"idchronology": id}, bson.M{"$set": bson.M{"value": value}})

	if errUpdate != nil {
		fmt.Printf("update UpdateLastCalcolate fail %v\n", errUpdate)
	}

}

func GetLast(w http.ResponseWriter, r *http.Request) {
	setAccessControl(w)
	session.SetMode(mgo.Monotonic, true)
	result := GetLastCommon(w, r)
	json.NewEncoder(w).Encode(result)
}

func GetLastTrack(w http.ResponseWriter, r *http.Request) []Last {
	result := GetLastCommon(w, r)
	return result
}

func GetLastCommon(w http.ResponseWriter, r *http.Request) []Last {

	// fmt.Println("GetLastCommon_________________")
	// fmt.Println("GetLastCommon_________________")
	// collection_last := GetSiteCollection(w, r)

	site := getSite(r)
	if site != "" {
		site = setDB(site)
	} else {
		site = GetSiteCollection(w, r)
	}

	c := session.DB(db_last).C(collection_last)
	siteParameters := getSiteParameters(r)
	// ip := siteParameters.ip
	// useragent := siteParameters.useragent
	token := siteParameters.token

	// fmt.Println("db_last: " + db_last)
	// fmt.Println("collection_last: " + collection_last)
	// fmt.Println("site: " + site)
	// fmt.Println("token: " + token)


	result := []Last{}
	// query := bson.M{"ip": ip, "useragent": useragent}
	// query := bson.M{"ip": ip, "useragent": useragent, "site": site}
	query := bson.M{"token": token, "site": site}
	err := c.Find(query).All(&result)

	if err != nil {
		fmt.Println("Errore:", err)
	}

	return result

}
