# SALESMEET - Setting per azienda


================================================================================================
Creazione azienda di base
================================================================================================

Censire una azienda nella tabella "company_registry"

- `site`
Sito con www. o us.

- `time_slot_appointment`

E' lo slot per il singolo appuntamento.
Es se è settato 30 sara possibile richiedere un appuntamento dalle 12:00 alle 12:30

- `in_store_enable`

Decide la tipologia ti appuntamento abilitato per l'azienda.
0 All
1 online
2 in store
3 to home
e poi abbiamo le combinazioni 12 23 123 13  


================================================================================================
Azienda - Censire il cliente
================================================================================================

Censire i parametri dell'azienda nella tabella "company_action_cta"

Un record per l'azienda deve essere sempre valorizzato e attivo.


  `all_site` int(11) DEFAULT '0', /* 0 = nessuna pagina - 1 = tutte le pagine */
  `url_page` text DEFAULT NULL,  /* se valorizzata l'url della pagina la regola vale solo per quella */
  `CSS_TAG` varchar(256) DEFAULT '',
  `CSS` text DEFAULT NULL,  /* js alternativo al booking, lanciato se le clausole non permetto di aprire il booking */


================================================================================================
Azienda - Preselezione cliente - Apertura "book an appointment" o un sistema del sito alternativo
================================================================================================

Censire i parametri dell'azienda nella tabella "company_action"

- `action_js`

js alternativo al booking, lanciato se le clausole sotto non permetto di aprire il booking

Es.  $crisp.push([\'do\', \'chat:open\']);  Apre una chatbot

Es window.location.href = "https://www.sito.com/contatti"  Rimanda il click ad una pagina con un form


- `pre_visibility_button`

0 rende visibile il tasto "consulta un esperto" lato plugin sulla pagina
1 il tasto "consulta un esperto" appare solo se almenu una della clausole sotto è valida


- `minimum_price`

prezzo medio minimo per la visualizzazione sulla somma di tutte le pagine visitate

- `minimum_number_of_pages`

numero minino di pagine visualizzate

-  `minimum_vote_site`

voto AI minimo sulla somma di tutte le pagine visitate

- `minimum_vote_page`

voto AI minimo per la singola pagina

- `minimum_price_page`

prezzo minimo per la singola pagina

- `action_type`

0 = esterno, 1 = chatbot, 2 = booking, 3 instant messenger

- `action_order`

Ordine di importanza. Il numero più alto è il più importante




================================================================================================
Azienda - Setting lingua du default
================================================================================================

Censire i parametri dell'azienda nella tabella "company_language"

Se non è presente il record con l'id_company il sistema abilita tutte le lingue.


- `language_list`

Parametrizzazione da inserire nel record =  it,en o it,en,fr (i parametri dovranno essere separati da virgola)


================================================================================================
Azienda o esperto - Setting orario
================================================================================================

Censire i parametri dell'azienda nella tabella "company_business_hours"

Se per i parametri `id_expert` e `id_company` abbiamo n record valorizzati a 0 e per l'azienda
non abbiamo record, il sistema prenderà quelli a 0 (di default)

L'orario potrà essere gestito sia globalmente per azienda o per singolo esperto.

- `days_of_week`

Parametrizzazione da inserire nel record =  1, 2, 3, 4 , 5, 6, 7

che corrispondono a Monday, Tuesday, Wednesday, Thursday, Friday ...

Ad esempio per inserire i giorni da lunedì a venerdì bisognerà inserire 1, 2, 3, 4 , 5


- `start_time`

ora di inizio giornata nel seguente formato 08:00

- `end_time`

ora di fine giornata nel seguente formato 19:00

Per avere più orari nella giornata inserire più record come da esempio:

insert into company_business_hours (id_expert,id_company,days_of_week,start_time,end_time) values (1,1,"1,2,3,4,5","08:00","13:00");
insert into company_business_hours (id_expert,id_company,days_of_week,start_time,end_time) values (1,1,"1,2,3,4,5","14:00","19:00");


================================================================================================
Personalizzazione layout
================================================================================================

Censire i parametri del layout nella tabella "company_css"

Se non sono presenti record legati all'azienda non applicherà nessun CSS.

Esempio inserimento CSS

insert into company_css (id_company,code) values (2,"#div-left-body { background: #03A9F4; color: #fff;}");

- `company_name_visible`

se 0 fa vedere il nome se 1 non fa vedere il nome ... (Es. se nel logo c'è il nome allora conviene non metterlo ...)


================================================================================================
Personalizzazione flusso di accettazione automatica appuntamento
================================================================================================

Censire i parametri nella tabella "appointments_email_setting"

Se per i parametri `id_expert` e `id_company` abbiamo n record valorizzati a 0 e per l'azienda
non abbiamo record, il sistema prenderà quelli a 0 (di default)

Il setting potrà essere gestito sia globalmente per azienda o per singolo esperto.

I campi da parametrizzare sono

`action_customer_booking`  

Se il valore è a 0 il sistema prevede la conferma da parte dell'esperto
Se il valore è a 1 appuntamento è già confermato in automatico

`id_expert`

`id_company`



================================================================================================
Personalizzazione testo delle email
================================================================================================

Censire i parametri nella tabella "appointments_email_template"

Se per i parametri `id_expert` e `id_company` abbiamo n record valorizzati a 0 e per l'azienda
non abbiamo record, il sistema prenderà quelli a 0 (di default)

I campi da parametrizzare sono

- `description`

Inserire il test che si vuole nella lingua desiderata.

Si ha la possibilità di avere dei valori sostituibili dinamicamente come ad esempio {customer_name}.
Vedere nel codice quali sono.

- `language`

en o it o ...

- `type`

booking_expert
booking_customer
booking_customer_pre_confirmation
booking_expert_pre_confirmation

- `subject`

Inserire il test che si vuole nella lingua desiderata.

Si ha la possibilità di avere dei valori sostituibili dinamicamente come ad esempio {customer_name}.
Vedere nel codice quali sono.


================================================================================================
Inserimento esperto per azienda
================================================================================================

Censire i parametri nella tabella "experts"

I parametri necessari sono:


- `img`

Immagine / Foto dell'esperto.

- `name`
- `surname`
- `telephone`

Questo parametro non verrà visualizzato

- `email`

Questo parametro non verrà visualizzato
Tutte le comunicazioni, la richiesta di appuntamento e altro verranno inviate a questa email

- `id_company`



================================================================================================
Inserimento dati dell'esperto per la machine learning e le automatizzazioni di sistema
================================================================================================

Censire i parametri nella tabella "experts_details"

I parametri necessari sono:


- `specialization`

Descrizione delle specializzazioni dell'esperto.
Verrà visualizzato al cliente in fase di booking.


- `features`

Descrizione delle caratteristiche dell'esperto
Verrà visualizzato al cliente in fase di booking.

- `keyword_work`

Parametri utilizzati dalla machine learning per abbinare esperto all'appuntamento
Non verrà visualizzato al cliente in fase di booking.

Es: scarpe,uomo,sneakers,premiata

I parametri dovranno essere separati da virgola

- `keywords_life`

Parametri utilizzati dalla machine learning per abbinare esperto all'appuntamento.
Non verrà visualizzato al cliente in fase di booking.

Es: scarpe,uomo,sneakers,premiata

I parametri dovranno essere separati da virgola

- `id_language`

1 = italiano
2 = inglese

- `id_expert`
- `id_company`
- `active`



================================================================================================
Inserimento parametri per gestire la creazione di una video call esterna da salesmeet. Es Zoom o Meet ...
================================================================================================

Censire i parametri nella tabella "experts_videocall"

Il setting potrà essere gestito sia per azienda o per singolo esperto.

I parametri necessari sono:

- `id_expert`

Se messo a zero prenderà il riferimento dell'azienda.

- `id_company`

- `type_video_call`

Inserire i parametri  = Meet o Zoom o ...

- `user_video_call`
- `pass_video_call`

Inserire qui i parametri ....

- `jwt_video_call`

Se si avrà la possibilità utilizzare questo parametro (Es Zoom usa questo).




================================================================================================
Inserimento le location attive per singola azienda
================================================================================================

Censire i parametri nella tabella "location"

I parametri necessari sono:

- `type`

 1 = ufficio
 2 = store
 3 = a casa
 10 = all

 - `country`

 Nome della nazione

 - `timezone`

Fuso orario così da calcolare e gestire la fase di orario di presa appuntamento

- `region`
- `province`
- `city`
- `email`
- `street`
- `id_company`

- `sequence`

corrisponde all'ordine di visualizzazione della location.
