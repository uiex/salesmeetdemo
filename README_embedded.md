# SALESMEET - Setting per azienda


================================================================================================
Come inserire SalesMeet sul sito e-commerce
================================================================================================

Esempio di base:

```

<script>
var salesmeet_parameters={"button_automatic":"0","button_text":"bella li"}
</script>
<script type='text/javascript' src='https://plugin.salesmeet.it/salesmeet.js'></script>

```

Dove alla variabile "salesmeet_parameters" vengono assegnate tutti valori per la personalizzazione grafica e label degli automatismi di SalesMeet.


Nel caso il cliente voglia personalizzare la sua CTA qui di seguito un esempio:

```
<a href="javascript:SalesMeetAppInstance.startBooking();">Demo del Servizio</a>
```

Dove viene chiamata la funzione JS di riferimento.
