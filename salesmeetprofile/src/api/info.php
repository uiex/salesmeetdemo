<?
header("Access-Control-Allow-Origin: *");
$site = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$url = $_SERVER['HTTP_REFERER'];

if ($url=="") {
    echo "Non autorizzato - 1";
    exit;
}

$referer = $_POST['referer'];
// $infouser = $_POST['infouser'];
$count = $_POST['count'];
$price = $_POST['price'];
// $pricemax = $_POST['pricemax'];
// $pricemin = $_POST['pricemin'];
$voto = $_POST['voto'];
$votopagina = $_POST['votopagina'];
$prezzopagina = intval($_POST['prezzopagina']);

/*
echo "referer: " . $referer . "\n";
echo "count: " . $count . "\n";
echo "price: " . $price . "\n";
// echo "pricemax: " . $pricemax . "\n";
// echo "pricemin: " . $pricemin . "\n";
echo "voto: " . $voto . "\n";
echo "votopagina: " . $votopagina . "\n";
echo "prezzopagina: " . $prezzopagina . "\n";
*/

if ( ($referer=="") || ($count=="") || ($price=="") || ($voto=="") || ($votopagina=="") || ($prezzopagina=="") ) {
    echo "Non autorizzato - 2";
    exit;
}

$price_medio_sito = $price / $count;
$voto_medio_sito = $voto / $count;

include("db.php");
$db = new db();
$companyInfo = $db->getCompanyInfoByReferer($referer);
if (count($companyInfo)>0) {

  $response = "";
  foreach ($companyInfo as &$value) {
       if ($value["action_type"]==2) {
          $response =  "booking_url_dedicato";
          break;
       } else {
          $response =  $value["action_js"];
          break;
       }
       if ($response=="") {
          $response =  "booking_default";
       }
  }
  echo $response;

} else {

    $companyInfo = $db->getCompanyInfo($site);

    /*
    echo $infouser . " - infouser\n";
    echo $count . " - count\n";
    echo $voto . " - voto\n";
    echo $votopagina . " - votopagina\n";
    echo $price . " - price\n";
    echo $prezzopagina . " - prezzopagina\n";
    echo $voto_medio_sito . " - voto_medio_sito - voto / count \n";
    echo $price_medio_sito . " - price_medio_sito - price / count\n";
    */

    $response = "";
    foreach ($companyInfo as &$value) {

        /*
        echo "\n______________________________\n";
        echo $value["action_type"] . " - action_type\n";
        echo $value["minimum_price_page"] . " - minimum_price_page\n";
        echo $value["minimum_price"] . " - minimum_price\n";
        echo $value["minimum_vote_site"] . " - minimum_vote_site\n";
        echo $value["minimum_vote_page"] . " - minimum_vote_page\n";
        echo $value["minimum_number_of_pages"] . " - minimum_number_of_pages\n";
        */

        // prezzo per singola pagina
        if ( ( $prezzopagina >= $value["minimum_price_page"] ) && ($value["minimum_price_page"] != 0 ) ) {
             if ($value["action_type"]==2) {
                $response =  "booking_prezzo_pagina_" . $prezzopagina . "_" . $value["minimum_price_page"];
             } elseif ($value["action_type"]==3) {
                $response =  "instantmessaging_prezzo_pagina_" . $prezzopagina . "_" . $value["minimum_price_page"];
             } else {
                $response =  $value["action_js"];
             }
             break;
        // prezzo medio dei prodotti visitato sul sito
        } elseif ( ( $price_medio_sito >= $value["minimum_price"] ) && ($value["minimum_price"] != 0 ) )  {
             if ($value["action_type"]==2) {
                $response =  "booking_prezzo_minimo_" . $price_medio_sito . "_" . $value["minimum_price"] ;
             } elseif ($value["action_type"]==3) {
                 $response =  "instantmessaging_prezzo_minimo_" . $price_medio_sito . "_" . $value["minimum_price"];
             } else {
                $response =  $value["action_js"];
             }
             break;
        // voto totale interesse di navigazione sul sito ...
        } elseif ( ( $voto_medio_sito >= $value["minimum_vote_site"] ) && ($value["minimum_vote_site"] != 0 ) ) {
             if ($value["action_type"]==2) {
               $response =  "booking_voto_medio_" . $voto_medio_sito . "_" . $value["minimum_vote_site"] ;
             } elseif ($value["action_type"]==3) {
                $response =  "instantmessaging_voto_medio_" . $voto_medio_sito . "_" . $value["minimum_vote_site"];
             } else {
               $response =  $value["action_js"];
             }
             break;
       // voto di interesse della pagina
       } elseif ( ( $votopagina >= $value["minimum_vote_page"] ) && ($value["minimum_vote_page"] != 0 ) ) {
            if ($value["action_type"]==2) {
               $response =  "booking_vote_page_" . $votopagina . "_" . $value["minimum_vote_page"] ;
            } elseif ($value["action_type"]==3) {
               $response =  "instantmessaging_vote_page_" . $votopagina . "_" . $value["minimum_vote_page"];
            } else {
               $response =  $value["action_js"];
            }
            break;
        // numero di pagine navigato
        } elseif ( ( $count >= $value["minimum_number_of_pages"] ) && ($value["minimum_number_of_pages"] != 0 ) ) {
             if ($value["action_type"]==2) {
                $response =  "booking_number_of_pages_" . $count . "_" . $value["minimum_number_of_pages"] ;
             } elseif ($value["action_type"]==3) {
                $response =  "instantmessaging_number_of_pages_" . $count . "_" . $value["minimum_number_of_pages"];
             } else {
                $response =  $value["action_js"];
             }
             break;
        }
        if ($response=="") {
            $response =  "booking_default";
        }

        /*
        echo $response . " - response\n";
        echo "\n______________________________\n";
        */

    }
    echo $response;

}

?>
