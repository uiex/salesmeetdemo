<?
header("Access-Control-Allow-Origin: *");
$site = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$token = $_POST['token'];

function curl($url, $post_param = null) {
      $ch = curl_init();
      $fh = fopen('curl.txt', 'a+');
      curl_setopt($ch, CURLOPT_STDERR, $fh);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array
      (
        'Host'             => 'salesmeet.it',
        'User-Agent'       => 'Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
        'Accept'           => 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language'  => 'en-us,en;q=0.5',
        'Accept-Encoding'  => 'gzip, deflate',
        'Accept-Charset'   => 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
        'Keep-Alive'       => '115',
        'Connection'       => 'keep-alive',
        'X-Requested-With' => 'XMLHttpRequest',
      ));

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
      curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
      echo $post_param;
      curl_setopt($ch, CURLOPT_URL, $url.'?'.$post_param);
      $html = curl_exec($ch);
      if(curl_errno($ch)){
          echo "Request Error:" . curl_error($ch);
      }
      curl_close($ch);
      fclose($fh);
      return $html;
}

function get_dataa($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_REFERER, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
    $str = curl_exec($curl);
    if(curl_errno($curl)){
        $str = 'Request Error:' . curl_error($curl);
    }
    curl_close($curl);
    echo $str;
}

// curl("https://uiextools.salesmeet.it/useranalytics/","token=wewewewewewewe" . $token);
// get_dataa("https://uiextools.salesmeet.it/useranalytics/");
// exit;

$infouser = $_POST['infouser'];
$count = $_POST['count'];
$price = $_POST['price'];
$pricemax = $_POST['pricemax'];
$pricemin = $_POST['pricemin'];
$count = $_POST['count'];
$voto = $_POST['voto'];
$votopagina = $_POST['votopagina'];
$prezzopagina = intval($_POST['prezzopagina']);

$price_medio_sito = $price / $count;
$voto_medio_sito = $voto / $count;

include("db.php");
$db = new db();
$companyInfo = $db->getCompanyInfo($site);



// Algoritmo per l'apertura del booking o di un'altro sistema del sito al click della CTA di salesmeet ...
$response = "";
$stato = "booking";
if ( ( $prezzopagina > $companyInfo[0]["minimum_price_page"] ) && ( $companyInfo[0]["minimum_price_page"] > 0 ) ) {

   $response =  "booking_prezzo_pagina_" . $prezzopagina . "_" . $companyInfo[0]["minimum_price_page"];

} elseif (( $price_medio_sito > $companyInfo[0]["minimum_price"] )  && ( $companyInfo[0]["minimum_price"] >= 0 ) )  {

    $response =  "booking_prezzo_minimo_" . $price_medio_sito . "_" . $companyInfo[0]["minimum_price"] ;

} elseif ( ( $voto_medio_sito > $companyInfo[0]["minimum_vote_site"] ) && ( $companyInfo[0]["minimum_vote_site"] >= 0 ) )  {

    $response =  "booking_voto_medio_" . $voto_medio_sito . "_" . $companyInfo[0]["minimum_vote_site"] ;

} else {

    // apertura di un sistema proprietario del sito (chatbot, form contatti o altro ...)
    $response =  $companyInfo[0]["action_js"];
    $stato = "";

}


// visualizzazione dinamica della CTA start booking ...
$pre_visibility_button = $companyInfo[0]["pre_visibility_button"];
$step = $_POST['step'];
if ($step=="addcta"){
    if ( ($pre_visibility_button==1) && ($stato=="booking") ){
        echo "addcta_1";
    } elseif ($pre_visibility_button==0) {
        echo "addcta_2";
    } else {
        echo "_3";
    }
} else {
    // start booking o altro sistema del sito ...
    echo $response;
}

?>
